<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
	// return what you want
	die("done.");
});

Route::group([ "middleware" => "dashboard"], function(){
	Route::get("/dashboard", "admin\DashboardController@index")->name("dashboard");
	Route::post("/dashboard/commonajax", "admin\CommonController@index")->name("commonajax");

	Route::get("/dashboard/skills", "admin\SkillController@index")->name("skills");
	Route::get("/dashboard/add_skill", "admin\SkillController@add_skill")->name("add-skill");
	Route::get("/dashboard/skill_data", "admin\SkillController@skillData")->name("skilldata");
	Route::get("/dashboard/edit_skill/{id}", "admin\SkillController@edit")->name("editskill");
	Route::post("/dashboard/update-skill-status","admin\SkillController@update_status");
	Route::post("/dashboard/save_skill", "admin\SkillController@save")->name("saveskill");
	Route::post("/dashboard/delete_skill", "admin\SkillController@delete")->name("deleteskill");
	Route::get("/dashboard/payments", "admin\PaymentController@index")->name("payments");
	Route::get("/dashboard/blog", "admin\BlogController@index")->name("admin-blogs");	
	Route::get("/dashboard/add-blog", "admin\BlogController@add_blog")->name("add-blog");	
	Route::get("/dashboard/edit_blog/{id}", "admin\BlogController@edit")->name("editblog");	
	Route::post("/dashboard/update-blog-status","admin\BlogController@update_status");	
	Route::post("/dashboard/save_blog", "admin\BlogController@save")->name("saveblog");	
    Route::post("/dashboard/delete_blog", "admin\BlogController@delete")->name("deleteblog");	


	Route::get("/dashboard/category", "admin\CategoryController@index")->name("category");
	Route::get("/dashboard/add-category/{id}", "admin\CategoryController@add_category")->name("add-category");
	Route::get("/dashboard/edit_category/{id}", "admin\CategoryController@add_category")->name("editcategory");

	Route::get('/dashboard/add-category/{id}/{step}', 'admin\CategoryController@add_question');
	Route::post('/dashboard/save_question', 'admin\CategoryController@save_question')->name('savequestion');

	Route::get("/dashboard/category_data", "admin\CategoryController@categoryData")->name("categorydata");
	Route::post("/dashboard/update-category-status","admin\CategoryController@update_status");
	Route::post("/dashboard/save_category", "admin\CategoryController@save")->name("savecategory");
	// Route::get("/dashboard/edit_category/{id}", "admin\CategoryController@edit")->name("editcategory");
	Route::post("/dashboard/delete_category", "admin\CategoryController@delete")->name("deletecategory");
               /*Admin Fresslancer section Start*/  
	Route::post("/dashboard/update-user-status","admin\UserController@update_status");
	Route::post("/dashboard/releasedFund","admin\CommonController@releasedFund");
	Route::post("/dashboard/approve-user-status","admin\UserController@approve_status");
	Route::get("/dashboard/approvel-user", "admin\UserController@approvel_user")->name("approvel-user");
	Route::get("/dashboard/user_profile/{id}", "admin\UserController@user_profile")->name("userprofile");
	Route::post("/dashboard/delete_user/{id}", "admin\UserController@delete_user")->name("deleteuser");
	Route::post("/dashboard/delete_approveduser/{id}", "admin\UserController@delete_approveduser")->name("deleteapproveduser");
  Route::get('/dashboard/trashed-user', 'admin\UserController@trashed_user')->name('trashed-user');
  Route::get('/dashboard/trashed-approveduser', 'admin\UserController@trashed_approveduser')->name('trashed-approveduser');
	Route::put('/dashboard/restore-user/{id}', 'admin\UserController@restore_user')->name('restore-user');
	Route::put('/dashboard/restore-approveduser/{id}', 'admin\UserController@restore_approved_user')->name('restore-approveduser');
Route::get("/dashboard/approved-user", "admin\UserController@approved_user")->name("approved-user");
Route::post("/dashboard/unapprove-user-status","admin\UserController@unapprove_status");
                   /*Admin Fresslancer section End*/  
                      /*Admin Client section Start*/  

	Route::get("/dashboard/approvel-client", "admin\ClientController@approvel_client")->name("approvel-client");
Route::get("/dashboard/approved-client", "admin\ClientController@approved_client")->name("approved-client");

Route::get('/dashboard/trashed-client', 'admin\ClientController@trashed_client')->name('trashed-client');
  Route::get('/dashboard/trashed-approvedclient', 'admin\ClientController@trashed_approvedclient')->name('trashed-approvedclient');
  Route::post("/dashboard/update-client-status","admin\ClientController@update_status");
	Route::post("/dashboard/approve-client-status","admin\ClientController@approve_status");
	Route::get("/dashboard/client_profile/{id}", "admin\ClientController@client_profile")->name("clientprofile");
	Route::post("/dashboard/delete_client/{id}", "admin\ClientController@delete_client")->name("deleteclient");
	Route::post("/dashboard/delete_approvedclient/{id}", "admin\ClientController@delete_approvedclient")->name("deleteapprovedclient");
	Route::put('/dashboard/restore-client/{id}', 'admin\ClientController@restore_client')->name('restore-client');
	Route::put('/dashboard/restore-approvedclient/{id}', 'admin\ClientController@restore_approved_client')->name('restore-approvedclient');
Route::post("/dashboard/unapprove-client-status","admin\ClientController@unapprove_status");
	
	   /*Admin Client section End*/  


Route::get("/dashboard/contract-list", "admin\ContractController@index")->name("listcontract");

  Route::get("/dashboard/contractDetails/{id}", "admin\ContractController@contractDetails")->name("contract-details");

        Route::get("/dashboard/query-list", "admin\QueryController@index")->name("listquery");
       Route::get("/dashboard/query_data", "admin\QueryController@queryData")->name("querydata");
	
	Route::get("/dashboard/view_query/{id}", "admin\QueryController@view_query")->name("viewquery");

 Route::get("/dashboard/settings", "admin\SettingController@index")->name("settings");	
	Route::get("/dashboard/add-payment-info", "admin\SettingController@add_payment")->name("add-payment-info");	
	Route::get("/dashboard/edit-payment-info/{id}", "admin\SettingController@edit")->name("edit-payment-info");	
		
	Route::post("/dashboard/save-payment-info", "admin\SettingController@save")->name("save-payment-info");	
    Route::post("/dashboard/delete-payment-info", "admin\SettingController@delete")->name("delete-payment-info");	


	Route::get("/dashboard/countries", "admin\CountryController@index")->name("countries");
	Route::get("/dashboard/countriesListing", "admin\CountryController@countriesListing")->name("countriesListing");
	Route::post("/dashboard/saveCountry", "admin\CountryController@save")->name("saveCountry");
	Route::post("/dashboard/deletename", "admin\CountryController@delete")->name("deletename");
	Route::get("/dashboard/countriesDropdown", "admin\CountryController@countriesDropdown")->name("countriesDropdown");

	Route::get("/dashboard/state", "admin\StateController@index")->name("state");
	Route::get("/dashboard/stateListing", "admin\StateController@stateListing")->name("stateListing");
	Route::post("/dashboard/saveState", "admin\StateController@save")->name("saveState");
	Route::post("/dashboard/deleteState", "admin\StateController@delete")->name("deleteState");
	Route::get("/dashboard/stateDropdown", "admin\StateController@stateDropdown")->name("stateDropdown");

	Route::get("/dashboard/cities", "admin\CityController@index")->name("cities");
	Route::get("/dashboard/cityListing", "admin\CityController@cityListing")->name("cityListing");
	Route::post("/dashboard/saveCity", "admin\CityController@save")->name("saveCity");
	Route::post("/dashboard/deleteCity", "admin\CityController@delete")->name("deleteCity");
	Route::get("/dashboard/cityDropdown", "admin\CityController@cityDropdown")->name("cityDropdown");

	Route::get("/dashboard/job_list", "admin\JobController@index")->name("listjob");

	Route::get("/dashboard/job_data", "admin\JobController@jobData")->name("jobdata");
	Route::get("/dashboard/view_job/{id}", "admin\JobController@view_job")->name("viewjob");
	Route::get("/dashboard/job_req_data/{id}", "admin\JobController@job_req_data")->name("jobreqdata");
	
});

// Route::get('/dashboard/updateUrl', 'admin\UpdateController@ajaxRequest');
Route::get('/dashboard/updateurl', 'admin\UpdateController@ajaxRequestPost')->name("activedeactive2");
Route::post('/dashboard/updateurl', 'admin\UpdateController@ajaxRequestPost')->name("activedeactive");

Route::get("/login", "AuthController@login")->name("login");
Route::post("/check-login", "AuthController@checkLogin")->name("checklogin");
Route::get("/forgot-password", "AuthController@forgotPassword")->name("forgotpassword");
Route::get("/logout", "AuthController@logout")->name("logout");


//Route::get('/', "AuthController@login")->name("home");
Route::get('/',"HomeController@index")->name("home");
Route::get('/logoutfront',"HomeController@logout")->name("logoutfront");


/*priya*/
Route::get('/hire-to-talent','HomeController@hiretotalent')->name("hire-to-talent");
Route::get('common/categary-search', 'CommonController@categary_search')->name("categary-search");



Route::get('/search-result/{catId}','HomeController@searchResult')->name("search-result");
Route::get('/question-filter/{catId}/{step}','HomeController@questionFilter')->name("question-filter");
Route::post('/setfilter', 'HomeController@setfilter')->name("setfilter");


Route::post('common/search_gaurd', 'CommonController@search_gaurd');
//Route::get('/user-dashboard','HomeController@dashboard')->name("user-dashboard");
Route::get('/set-new-password/{token}','HomeController@forgotpassword')->name("set-new-password");
Route::get('/verify-email/{token}','HomeController@verifyEmail')->name("verify-email");
Route::get('/about-us',"HomeController@about_us")->name("about-us");

Route::get('/blog',"HomeController@blog")->name("blog");
Route::get('/blog/{id}',"HomeController@blog_details")->name("blog-details");
Route::get('/why',"HomeController@why")->name("why");
Route::get('/howitworks',"HomeController@howitworks")->name("howitworks");
Route::get('/enterprise',"HomeController@enterprise")->name("enterprise");
Route::match(['get', 'post'], 'contact-us',"HomeController@contact_us")->name("contact-us");
Route::get('/faq',"HomeController@faq")->name("faq");
Route::get('/security-service',"HomeController@security_service")->name("security-service");

/*Route::get('/front', function () {
    return view('welcome');
});*/
//Route::resource("/common","CommonController");
Route::get('/payment', 'PayPalController@payment')->name('payment');
Route::get('/cancel', 'PayPalController@cancel')->name('payment.cancel');
Route::get('payment/success', 'PayPalController@success')->name('payment.success');

Route::post('/common',"CommonController@index")->name("common");
Route::match(['get', 'post'], 'ajax-image-upload', 'UserController@ajaxImage');
Route::match(['get', 'post'], 'ajax-client-image-upload', 'client\DashboardController@ajaxImage');
Route::match(['get', 'post'], 'ajax-user-image-upload', 'UserController@ajaxImage');

Route::group([ "middleware" => "loginuser"], function(){
	Route::get("/freelancer-detail/{id}", "HomeController@freelancerDetail")->name("freelancer-detail");
	Route::get('generate-pdf/{contractId}','HomeController@generatePDF')->name("generate-pdf");
});
Route::group([ "middleware" => "userdashboard"], function(){
	Route::get('/user-dashboard','UserController@index')->name("user-dashboard");
	Route::get('/user-dashboard/contract-list','ContractController@contract_list')->name("user-contract-list");
	Route::get('/user-dashboard/contract-detail/{contractId}', 'ContractController@contract_detail')->name('user-contract-detail');
	Route::get('/user-dashboard/profile','UserController@profile')->name("user-profile");
	Route::get('/user-dashboard/chat/{chatId}','UserController@chat')->name("user-chat");
    Route::match(['get', 'post'], '/user-dashboard/change-password', 'UserController@change_password')->name("user-change-password");
	Route::get('/user-dashboard/edit-profile','UserController@edit_profile')->name("user-edit-profile");
	Route::post("/user-dashboard/save-user-data", "UserController@save_user_data")->name("saveuserdata");
	Route::get('/user-dashboard/user-task','UserController@user_task')->name("user-task");
	Route::get('/user-dashboard/job-request','UserController@request_listing')->name("job-request");
	Route::get('/user-dashboard/user-task-history','UserController@history')->name("user-task-history");
	Route::get('/user-dashboard/search-job','UserController@searchJob')->name("search-job");
	Route::get("/user-dashboard/payment-request", "ContractController@payment_request")->name("user-payment-request");
	
	Route::post("/user/delete-portfolio","UserController@delete_portfolio");
	Route::post("user/delete-experience","UserController@delete_experience");
	Route::post("user/delete-project","UserController@delete_project");
	Route::post("user/delete-video","UserController@delete_video");
	Route::post("user/delete-project","UserController@delete_project");
	Route::post("user/delete-education","UserController@delete_education");
	Route::get('/user-dashboard/job-detail/{id}', 'UserController@open_job_detail')->name('open-job-detail');
    Route::post('common/search_job', 'CommonController@search_job');
});

Route::group([ "middleware" => "clientdashboard"], function(){
Route::get("/hire-freelancer/{reqId}", "HomeController@hireFreelancer")->name("hire-freelancer");
Route::get("/payment/{contractId}", "client\ContractController@payment")->name("payment");
Route::post("/client/save-contract", "client\ContractController@save_contract")->name("savecontract");
Route::post("/client/save-payment", "client\ContractController@save_payment")->name("savepayment");
Route::get("/client/chat/{chatId}", "client\ChatController@chat")->name("client-chat");
Route::get("/client/payment-request", "client\ContractController@payment_request")->name("client-payment-request");

Route::get('/client','client\DashboardController@index')->name("client-dashboard");
Route::post("/client/delete-portfolio","client\DashboardController@delete_portfolio");
Route::post("/client/delete-experience","client\DashboardController@delete_experience");
Route::post("/client/delete-project","client\DashboardController@delete_project");
Route::post("/client/delete-video","client\DashboardController@delete_video");
Route::post("/client/delete-education","client\DashboardController@delete_education");
Route::get('/client/profile','client\DashboardController@profile')->name("client-profile");
Route::match(['get', 'post'], '/client/change-password', 'client\DashboardController@change_password')->name("client-change-password");
Route::get('/client/edit-profile','client\DashboardController@edit_profile')->name("client-edit-profile");
Route::post("/client/save-client-data", "client\DashboardController@save_user_data")->name("saveclientdata");
Route::get('/client/client-task','client\DashboardController@user_task')->name("client-task");
Route::post('/client/payment', 'client\PayPalController@payment')->name('payment');
Route::get('/client/cancel', 'client\PayPalController@cancel')->name('payment.cancel');
Route::get('/client/payment/success', 'client\PayPalController@success')->name('payment.success');
Route::get('/client/contract-list', 'client\ContractController@contract_list')->name('client-contract-list');
Route::get('/client/contract-detail/{id}', 'client\ContractController@contract_detail')->name('client-contract-detail');


});

// Check Subscriber Email
Route::post('/check-subscriber-email','NewsletterController@checkSubscriber');

// Add Subscriber Email
Route::post('/add-subscriber-email','NewsletterController@addSubscriber');
Route::get('/dashboard/newsletter-subscribers','NewsletterController@viewNewsletterSubscribers')->name("newsletter-subscribers");	


	// Update Newsletter Status
	Route::post("/dashboard/update-newsletter-status","NewsletterController@update_status");
	// Delete Newsletter Email
	Route::post("/dashboard/delete_newsletter", "NewsletterController@delete")->name("deletenewsletter");

	// Export Newsletter Emails
	Route::get('/dashboard/export-newsletter-emails','NewsletterController@exportNewsletterEmails');