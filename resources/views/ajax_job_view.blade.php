<div class="total_search">
	<?php if (isset($_POST) && !empty($_POST)) { ?>
		<div class="total_filter">
			<ul>
				<?php
				if (isset($_POST['category']) && !empty($_POST['category'])) { 
					foreach($_POST['category'] as $category){
						$category = user_category_data($category);
						echo "<li>".$category->category_name."</li>";
					}
				}
				?>
				<?php
				if( isset($_POST['price']) && !empty($_POST['price'])){
					foreach($_POST['price'] as $price){
						echo "<li>".$price."</li>";
					}
				}
				?>															
			</ul>
			
			<a href="{{route('search-job')}}"><i class="fas fa-times-circle"></i> Clear all Filters</a>
		</div>
	<?php } ?>								
</div>
							
<?php
if(!empty($jobs) && count($jobs)>0) {
	
?>

@foreach($jobs as $job)
	<div class="freelancer_list">

		<div class="profile">
			<div class="name_loct">
				<h4>
				@if(!empty($job->category_name)) 
					{{ $job->category_name }}
				@endif
				</h4>
				<!-- Location -->
				<div class="location">
					<div class="locktion2">
						<h3>
							<a class="checkuserLogin" href="{{asset('/user-dashboard/job-detail/'.$job->id)}}"> 
							{{ucfirst ($job->title)}}
							</a>

							@if ($job->is_user_verified == '1')
								<span class="verified"><i class="fas fa-check"></i> Verified</span>
							@endif 
						</h3>
					</div>

					<div class="loc-box">
						<h4>
							<span><img src="{!! asset('assets/frontend/img/loct.png') !!}" alt="Location"> Location</span>
							<b>{{ $job->country }}</b>
						</h4>
						<h4>
							<span><img src="{!! asset('assets/frontend/img/rate_img.png') !!}" alt="Rate"> Rate</span>
							<b>{{Config::get('app.site_currency_symbol')}} {{ $job->price}}</b>
						</h4>
						<h4>
							<span> Job Period</span>
							<b>{{ date('d  M Y', strtotime($job->job_start_on)).' - '. date('d  M Y', strtotime($job->job_end_on))}}</b>
						</h4>
					</div>
				</div>
			</div>
			<div class="star_rate">
			<button type="button" class="btn btn-warning" onclick="makeanoffer('<?php echo  $job->id; ?>','<?php echo session::get('role'); ?>',event)">Make an offer</button>
			</div>
		</div>
		
		<div class="details">
		<?php
		   $stringCut = substr($job->description, 0, 500);
            $endPoint = strrpos($stringCut, ' ');

           //if the string doesn't contain any space then it will cut without word basis.
            $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
	       $string .= '...';
		?>
		<p>{{$string}}</p>
		</div>
	</div>
@endforeach

<div class="paginating">
	<ul class="pagination searchpagination">
		{{ $jobs->links() }}	
	</ul>
</div>

<?php } else { echo '<div class="freelancer_list">No record found</div>'; } ?>

							
							<style>
.location {
    display: flex;
    text-align: center;
    font-family: 'Roboto', sans-serif;
    width: 90%;
    justify-content: space-between;
    margin: 0 auto;
    flex-direction: column;
}
.loc-box {
    display: flex;
}
.profile .name_loct h4 {
    font-size: 16px;
    color: #7a7a7a;
    margin-right: 23px;
}
</style>