@extends('frontend.layouts.layout')
@section('title', 'Justzoom')
@section('body')


<div class="profile-listing-page">
	<div class="container">
		<div class="inner-profile-listing">
			<div class="row">
				<div class="col-md-6" align="center">
					@if(session("msg"))
							<div class="alert-dismiss">
								<div class="alert alert-danger alert-dismissible fade show">
									<span>{{session("msg")}}</span><button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span class="fa fa-times"></span> </button><br/>
								</div>
							</div>
				@endif
				</div>
				<div class="col-md-12">
					<div class="iconic-heading">
						<a href="#" class="back-btn"><img src="img/left-arrow.png" alt=""></a>
						<div class="heading">
							
							<h2>
								<?php echo (isset($categoryDeatil) && !empty($categoryDeatil)) ? ucfirst($categoryDeatil->category_name): ''; ?>
								<span><img src="img/heading-icon.png" alt=""></span>
							</h2>

						</div>
						
							
						</div>
					
				</div>

				<div class="profile-listing-section" style="width:100%">
				@include('ajax_freelancer_view')
				</div>
			</div>
		</div>
	</div>
</div>

@include("frontend.job_request_poup")
@endsection



