@extends('frontend.layouts.layout')







@section('title', 'Justzoom')



@section('body')



<div class="banner">



        <div class="container-fluid">



            <div class="row">



                <div class="col-md-6">



                    <h2>Hire the top of freelance <br>talent</h2>



                    <p>Justzoom expertly connects professionals and agencies to businesses seeking specialized talent.</p>



                    <div class="hear-button">



                        <a href="{{ route('hire-to-talent') }}"><button type="button" class="btn btn-secondary green-bg">Hire top Talent</button></a>



                        <a href="#"><button type="button" class="btn btn-secondary white-border">Watch Video</button></a>



                    </div>



                </div>



                <div class="col-md-6">



                    <div class="banner-lapto">



                        <img id="headerImg" src="assets/frontend/img/Motion1.gif">



                    </div>



                </div>



            </div>



        </div>



    </div>



    <div class="logo-list phone">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
					<marquee>
                    <ul>

                        <li><h2>TRUSTED BY LEADING <br>BRANDS AND STARTUPS</h2></li>
                        <li><img src="assets/frontend/img/plastic1.png"></li>
                        <li><img src="assets/frontend/img/plastic2.png"></li>
                        <li><img src="assets/frontend/img/plastic3.png"></li>
                        <li><img src="assets/frontend/img/plastic4.png"></li>
                        <li><img src="assets/frontend/img/plastic5.png"></li>
                        <li><img src="assets/frontend/img/plastic6.png"></li>
                    </ul>
					</marquee>
                </div>
            </div>
        </div>
    </div>
	<div class="logo-list destop">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <ul>

                        <li><h2>TRUSTED BY LEADING <br>BRANDS AND STARTUPS</h2></li>
                        <li><img src="assets/frontend/img/plastic1.png"></li>
                        <li><img src="assets/frontend/img/plastic2.png"></li>
                        <li><img src="assets/frontend/img/plastic3.png"></li>
                        <li><img src="assets/frontend/img/plastic4.png"></li>
                        <li><img src="assets/frontend/img/plastic5.png"></li>
                        <li><img src="assets/frontend/img/plastic6.png"></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>



    



    <div class="we-talent">



        <div class="container">



            <div class="row">



                <div class="col-md-6">



                    <h3>We connect the world's top talent with the world's top organizations.</h3>



                </div>



                <div class="col-md-6">



                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla aliquam eros et imperdiet luctus. In ullamcorper lorem vel ante interdum, ac blandit dolor volutpat. Phasellus vehicula nunc est, ac egestas diam semper sed. In mollis malesuada venenatis. </p>



                    <a href="{{route('about-us')}}">About</a>



                </div>



            </div>



        </div>



    </div>


    <div class="client-gallrey">
		<div class="popular-course">
        @foreach($users as $value)
        <?php 
        if(!empty($value->image)){
            $img = $value->image;
        }else{
            $img = 'no-img.png';
        }
        ?>
        

			<div class="slide-box">
				<div class="slider-bbbbb"><img src="{{asset('/uploads/user_images/'.$img)}}"></div>
				<div class="slide-tex">
					<h2>{{ $value->first_name.' '.$value->last_name }}</h2>
					<p><span><img src="assets/frontend/img/pen.png"></span>@if(!empty($value->category))
                        {{$value->category->category_name}}
                        @else
                        N/A
                    @endif</p>
				</div>
			</div>
        @endforeach
			
		</div>
		<div class="range-slider">
			
			<button type="button" class="btn btn-secondary diceover-btt">Decover More</button>
		</div>
	</div>
	</div>

    <div class="connect-demand demand-man2">

<div class="container">

    <div class="row">

        <div class="col-md-6" style="background-color: #f9f9f9;padding: 100px 100px;">

            <h2>Connect with in-demand professionals</h2>

            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla aliquam eros et imperdiet luctus.</p>

            <ul class="demant-list">

                <li><span><i class="fas fa-check"></i></span><h3>Complex projects</h3></li>

                <li><span><i class="fas fa-check"></i></span><h3>Long-term contracts</h3></li>

                <li><span><i class="fas fa-check"></i></span><h3>Short-term jobs</h3></li>

            </ul>

            <button type="button" class="btn btn-secondary Get-Started red">Get Started</button>

        </div>

        <div class="col-md-6" style="background-color: #003043;padding: 100px 100px;">

            <img src="assets/frontend/img/motion2.gif">

        </div>

    </div>

    <div class="row two">

        <div class="col-md-6" style="background-color: #f9f9f9;padding: 100px 100px;">

            <h2>Hiring Made Easy</h2>

            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla aliquam eros et imperdiet luctus.</p>

            <button type="button" class="btn btn-secondary Get-Started green">Get Started</button>

        </div>

        <div class="col-md-6" style="background-color: #003043;padding: 100px 100px;">

            <div class="curcle-banner">

            <img src="assets/frontend/img/motion3.gif">

            </div>

        </div>

    </div>

    <div class="row three">

        <div class="col-md-6" style="background-color: #f9f9f9;padding: 100px 100px;">

            <h2>Hire professional for any <br>industry</h2>

            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla aliquam eros et imperdiet luctus.</p>

            <ul class="demant-list">

                <li><span><i class="fas fa-check"></i></span><h3>Complex projects</h3></li>

                <li><span><i class="fas fa-check"></i></span><h3>Long-term contracts</h3></li>

                <li><span><i class="fas fa-check"></i></span><h3>Short-term jobs</h3></li>

            </ul>

            <button type="button" class="btn btn-secondary Get-Started yello">Get Started</button>

        </div>

        <div class="col-md-6" style="background-color: #003043;padding: 100px 100px;">

            <div class="curcle-img">

                <img src="assets/frontend/img/motion4.gif">

                

            </div>

        </div>

    </div>

</div>

</div>



    <div class="connect-demand">



        <div class="container">



            <div class="row comend-row">



				<div class="col-md-6" style="padding: 0 !important;background-color: #f9f9f9;">



					<div class="left-row">



						<div class="" style="background-color: #f9f9f9;padding: 0px 100px;">



							<div id="part1">



							<h2>Connect with in-demand professionals</h2>



							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla aliquam eros et imperdiet luctus.</p>



							<ul class="demant-list">



								<li><span><i class="fas fa-check"></i></span><h3>Complex projects</h3></li>



								<li><span><i class="fas fa-check"></i></span><h3>Long-term contracts</h3></li>



								<li><span><i class="fas fa-check"></i></span><h3>Short-term jobs</h3></li>



							</ul>



							<button type="button" class="btn btn-secondary Get-Started red">Get Started</button>



							</div>



							<div id="part2">



							<h2>Hiring Made Easy</h2>



							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla aliquam eros et imperdiet luctus.</p>



							<button type="button" class="btn btn-secondary Get-Started green">Get Started</button>



							 </div>



							 <div id="part3">



							<h2>Hire professional for any <br>industry</h2>



							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla aliquam eros et imperdiet luctus.</p>



							<ul class="demant-list">



								<li><span><i class="fas fa-check"></i></span><h3>Complex projects</h3></li>



								<li><span><i class="fas fa-check"></i></span><h3>Long-term contracts</h3></li>



								<li><span><i class="fas fa-check"></i></span><h3>Short-term jobs</h3></li>



							</ul>



							<button type="button" class="btn btn-secondary Get-Started yello">Get Started</button>



							</div>



						</div>



					</div>



				</div>	



				<div class="col-md-6" style="padding: 0 !important;background-color: #003043;">



				<div class="right-row">



					 <div class="" style="background-color: #003043;">



						<div class="curcle-img">                     



						<img id="pic1" src="assets/frontend/img/motion2.gif" >



						<img  id="pic2" src="assets/frontend/img/motion3.gif" style="display:none;">



						<img id="pic3" src="assets/frontend/img/motion4.gif" style="display:none;">



						</div>



					</div>



				</div>



				</div>



            </div>



        </div>



    </div>



    <div class="hire-project">



        <div class="container">



            <div class="row">



                <div class="col-md-6">



                    <div class="hare-left">



                        <h2>Hire the Top of <br>Project Managers</h2>



                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla aliquam eros et imperdiet luctus. In ullamcorper lorem vel ante interdum, ac blandit dolor volutpat. Phasellus vehicula nunc est, ac egestas diam semper sed.</p>



                        <button type="button" class="btn btn-secondary green-bttt ">Hire Top Project Manager</button>



                        <h4>No Risk Trial Pay Only if Satisfied</h4>



                    </div>



                </div>



                <div class="col-md-6">



                    <div class="crwford-bg">



                        <ul>



                            <li>



                                <img src="assets/frontend/img/croud1.png">



                                <div class="crwford-box">



                                    <h2>Carole Crawford, CFA</h2>



                                    <h3><span><img src="assets/frontend/img/pen.png"></span>Interim CFO</h3>



                                    <h5>Previously at</h5>



                                    <h4>Morgan Stanley</h4>



                                </div>



                            </li>



                            <li>



                                <img src="assets/frontend/img/croud2.png">



                                <div class="crwford-box">



                                    <h2>Carole Crawford, CFA</h2>



                                    <h3><span><img src="assets/frontend/img/pen.png"></span>Interim CFO</h3>



                                    <h5>Previously at</h5>



                                    <h4>Morgan Stanley</h4>



                                </div>



                            </li>



                            <li>



                                <img src="assets/frontend/img/croud3.png">



                                <div class="crwford-box">



                                    <h2>Carole Crawford, CFA</h2>



                                    <h3><span><img src="assets/frontend/img/pen.png"></span>Interim CFO</h3>



                                    <h5>Previously at</h5>



                                    <h4>Morgan Stanley</h4>



                                </div>



                            </li>



                        </ul>



                    </div>



                </div>



            </div>



        </div>



    </div>



    <div class="demand-talent">



        <div class="container-fluid">



            <div class="row">



                <div class="col-md-12">



                    <h2>In-demand talent on demand.<br>Justzoom is how.</h2>



                    <p>Justzoom expertly connects professionals and agencies to businesses seeking <br>specialized talent.</p>



                </div>



            </div>



        </div>



        <div class="clinet-say">



            <ul>



                <li>



                    <h3>CLIENT AND AGENCIES</h3>



                    <h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h4>



                    <a href="#">Find out more<span><i class="fas fa-chevron-right"></i></span></a>



                </li>



                <li>



                    <h3>FREELANCER</h3>



                    <h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h4>



                    <a href="#">Find out more<span><i class="fas fa-chevron-right"></i></span></a>



                </li>



            </ul>



        </div>



    </div>



    <div class="testmonial-bg">



        <div class="container-fluid">



            <div class="row">



                <div class="col-md-12">



                    <div class="testmonial-title">



                        <h2>Hear From Our Clients</h2>



                        <ul>



                            <li><h3>Our customers say </h3></li>



                            <li><h4>Excellent    <img src="assets/frontend/img/star.png"></h4></li>



                            <li><h4>4.7 out of 5 based on 1,123 reviews<img src="assets/frontend/img/trustpilot.png"></h4></li>



                        </ul>



                    </div>



                </div>



                <div class="col-md-12">



                    <div class="testmonial">



                        <div class="test-box">



                            <span><i class="fas fa-quote-left"></i></span>



                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In molestie neque et tortor pretium, a facilisis justo tempus. Maecenas facilisis maximus vehicula. Proin sem lacus, eleifend sed orci at, fermentum aliquet dolor. Duis ac erat ac augue ullamcorper aliquet nec quis quam. Donec interdum iaculis erat sit amet commodo. Donec eu porttitor magna, id aliquet ligula. </p>



                            <div class="mc-nallay">



                                <img src="assets/frontend/img/aval.png">



                                <div class="testmonial-text">



                                    <h3>Ryan Walker, Director of Product</h3>



                                    <h4>Rand McNally</h4>



                                </div>



                            </div>



                        </div>



                        <div class="test-box">



                            <span><i class="fas fa-quote-left"></i></span>



                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In molestie neque et tortor pretium, a facilisis justo tempus. Maecenas facilisis maximus vehicula. Proin sem lacus, eleifend sed orci at, fermentum aliquet dolor. Duis ac erat ac augue ullamcorper aliquet nec quis quam. Donec interdum iaculis erat sit amet commodo. Donec eu porttitor magna, id aliquet ligula. </p>



                            <div class="mc-nallay">



                                <img src="assets/frontend/img/aval.png">



                                <div class="testmonial-text">



                                    <h3>Ryan Walker, Director of Product</h3>



                                    <h4>Rand McNally</h4>



                                </div>



                            </div>



                        </div>



                        <div class="test-box">



                            <span><i class="fas fa-quote-left"></i></span>



                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In molestie neque et tortor pretium, a facilisis justo tempus. Maecenas facilisis maximus vehicula. Proin sem lacus, eleifend sed orci at, fermentum aliquet dolor. Duis ac erat ac augue ullamcorper aliquet nec quis quam. Donec interdum iaculis erat sit amet commodo. Donec eu porttitor magna, id aliquet ligula. </p>



                            <div class="mc-nallay">



                                <img src="assets/frontend/img/aval.png">



                                <div class="testmonial-text">



                                    <h3>Ryan Walker, Director of Product</h3>



                                    <h4>Rand McNally</h4>



                                </div>



                            </div>



                        </div>



                        <div class="test-box">



                            <span><i class="fas fa-quote-left"></i></span>



                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In molestie neque et tortor pretium, a facilisis justo tempus. Maecenas facilisis maximus vehicula. Proin sem lacus, eleifend sed orci at, fermentum aliquet dolor. Duis ac erat ac augue ullamcorper aliquet nec quis quam. Donec interdum iaculis erat sit amet commodo. Donec eu porttitor magna, id aliquet ligula. </p>



                            <div class="mc-nallay">



                                <img src="assets/frontend/img/aval.png">



                                <div class="testmonial-text">



                                    <h3>Ryan Walker, Director of Product</h3>



                                    <h4>Rand McNally</h4>



                                </div>



                            </div>



                        </div>



                        <div class="test-box">



                            <span><i class="fas fa-quote-left"></i></span>



                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In molestie neque et tortor pretium, a facilisis justo tempus. Maecenas facilisis maximus vehicula. Proin sem lacus, eleifend sed orci at, fermentum aliquet dolor. Duis ac erat ac augue ullamcorper aliquet nec quis quam. Donec interdum iaculis erat sit amet commodo. Donec eu porttitor magna, id aliquet ligula. </p>



                            <div class="mc-nallay">



                                <img src="assets/frontend/img/aval.png">



                                <div class="testmonial-text">



                                    <h3>Ryan Walker, Director of Product</h3>



                                    <h4>Rand McNally</h4>



                                </div>



                            </div>



                        </div>



                        <div class="test-box">



                            <span><i class="fas fa-quote-left"></i></span>



                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In molestie neque et tortor pretium, a facilisis justo tempus. Maecenas facilisis maximus vehicula. Proin sem lacus, eleifend sed orci at, fermentum aliquet dolor. Duis ac erat ac augue ullamcorper aliquet nec quis quam. Donec interdum iaculis erat sit amet commodo. Donec eu porttitor magna, id aliquet ligula. </p>



                            <div class="mc-nallay">



                                <img src="assets/frontend/img/aval.png">



                                <div class="testmonial-text">



                                    <h3>Ryan Walker, Director of Product</h3>



                                    <h4>Rand McNally</h4>



                                </div>



                            </div>



                        </div>



                    </div>



                </div>



            </div>



        </div>



    </div>



   @include("frontend.layouts.common_register_user")



    <div class="Justzoom-Developers">



        <div class="container">



            <div class="row">



                <div class="col-md-12">



                    <h2 class="just-title">Justzoom Developers</h2>



                </div>



                <div class="col-md-3">



                    <ul>



                        @foreach($category_list as $category)



                        <li><h3>{{$category}}</h3></li>



                        @endforeach



                    </ul>



                </div>



                



                



                



            </div>



        </div>


</div>
    



    <style>



	div#part1 {



    margin-bottom: 0px;



}







div#part2 {



    margin-bottom: 0px;



}



    



.fixed {



    position:fixed;



    



    



}



    </style>



<script> 



        window.onscroll = function () {



            var fixmeTop = $('.comend-row').offset().top;

            var hireProjectHeight = $('.hire-project').offset().top;

            screenHeight = $(window).height();

            stopFixedRightSection = hireProjectHeight - screenHeight

            var currentScroll = $(window).scrollTop();

            if (currentScroll >fixmeTop && currentScroll <stopFixedRightSection) {  



               // alert("ddd");         // apply position: fixed if you



                $('.right-row').css({                      // scroll to that element or below it



                    position: 'fixed',



                    top: 0,



					



                   



                });



            } else {                                   // apply position: static



                $('.right-row').css({                      // if you scroll above it



                    position: 'absolute',
					top: '58%',


                }); 



            }



            scrollFunction(); 



        }; 



  



        function scrollFunction() { 



            hireProjectHeight = $('.hire-project').offset().top;

            screenHeight = $(window).height();

            halfHeight  = screenHeight/2;

            var part1 = $('#part1').offset().top;



            var part2 = $('#part2').offset().top;



            var part3 = $('#part2').offset().top;

        var xx = $('.crwford-bg').offset().top;



           

            if (document.documentElement.scrollTop > 170){



                //alert("dd");



                document.getElementById("headerImg").style.display = "none";



            }else{



                document.getElementById("headerImg").style.display = "block";







            }



            if(document.documentElement.scrollTop >= ($('.comend-row').offset().top - halfHeight) && 

            document.documentElement.scrollTop <= (part2 - halfHeight)){

                console.log(document.documentElement.scrollTop+" - pic1");



                document.getElementById("pic1").style.display = "block";



                document.getElementById("pic2").style.display = "none";



                document.getElementById("pic3").style.display = "none";







            }



            else if (document.documentElement.scrollTop > (part2 - halfHeight) && 

            document.documentElement.scrollTop <= part3) 



            { 

                console.log(document.documentElement.scrollTop+" - pic2");



            



            document.getElementById("pic1").style.display = "none";



            document.getElementById("pic2").style.display = "block";



            document.getElementById("pic3").style.display = "none";



            



            }else if(document.documentElement.scrollTop > (part3 - halfHeight) &&  document.documentElement.scrollTop <= xx){



                document.getElementById("pic1").style.display = "none";



            document.getElementById("pic2").style.display = "none";



            document.getElementById("pic3").style.display = "block";

                console.log(document.documentElement.scrollTop+" - pic3");







            }



            







        }   
    
        const slider = $(".popular-course");
slider
  .slick({
    dots: true,
	slidesToShow: 7,
	responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
  });








    </script> 



     



      



@endsection