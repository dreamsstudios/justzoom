@extends('frontend.layouts.layout')

@section('title', 'Justzoom')
@section('body')

    <div class="blog-banner">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <img src="{{asset('/uploads/blogs/'.$blog->image)}}" class="rounded-circle">
                    <h2>{{$blog->title}}</h2>
                    <h3>{{$blog->content}}</h3>
                    <h4>{{ Carbon\Carbon::parse($blog->created_at)->format('d M Y') }}</h4>
                </div>
            </div>
        </div>
    </div>
    <div class="blog-content">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p>{!!$blog->description!!}
                    </p>
                    <a href="{{ route('blog') }}"><button type="button" class="btn btn-secondary back-button">Back to Articales</button></a>
                    
                   
                </div>
            </div>
        </div>
    </div>
    
     @include("frontend.layouts.common_register_user")
    <div class="Justzoom-Developers">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="just-title">Justzoom Developers</h2>
                </div>
               <div class="col-md-3">
                    <ul>
                        @foreach($category_list as $category)
                        <li><h3>{{$category}}</h3></li>
                        @endforeach
                    </ul>
                </div>
               
            </div>
        </div>
    </div>

@endsection
