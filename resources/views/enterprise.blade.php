@extends('frontend.layouts.layout')



@section('title', 'Justzoom')

@section('body')

<div class="page-wraper">

   

        <div class="business-banner">

            <div class="container-fluid">

                <div class="row">

                    <div class="col-md-7">

                        <h2>Enterprise Solutions Customized <br>for Your Business</h2>

                        <p>Justzoom Enterprise offers everything you need to work efficiently and affordably online with Freelancers.</p>

                    </div>

                </div>

            </div>

        </div>

    </div>

    </div>

    <div class="custom-bulit">

        <div class="container-fluid">

            <div class="row">

                <div class="col-md-6">

                    <h2>Custom Built for You</h2>

                    <p>We offer tailor-made solutions that accommodate the needs of your business<br> and team to optimize your freelancing spend.</p>

                </div>

                <div class="col-md-6">

                    <div class="custom-img">

                        <img src="assets/frontend/img/custom-img.png">

                    </div>

                </div>

            </div>

        </div>

    </div>  

    <div class="high-perfomance">

        <div class="container-fluid">

            <div class="row">

                <div class="col-md-12">

                    <div class="title">

                        <h2>High-Performance Teams, On Your Terms</h2>

                        <p>Work with hand-selected talent, customized to fit your needs at scale.</p>

                    </div>

                </div>

            </div>

            <div class="row involis-project">

                <div class="col-md-4">

                    <div class="teams-list">

                        <span><img src="assets/frontend/img/Clients1.png"></span>

                        <h3>Individuals</h3>

                        <p>Fill a missing skill set or role on your team.</p>

                    </div>

                </div>

                <div class="col-md-4">

                    <div class="teams-list">

                        <span><img src="assets/frontend/img/Clients2.png"></span>

                        <h3>Teams</h3>

                        <p>Assemble a full team, ready to follow your lead.</p>

                    </div>

                </div>

                <div class="col-md-4">

                    <div class="teams-list">

                        <span><img src="assets/frontend/img/Clients4.png"></span>

                        <h3>Projects</h3>

                        <p>Leverage a fully-managed team, operating using agile best practices.</p>

                    </div>

                </div>

                <div class="col-md-12">

                    <div class="sign-button">

                        <p>If you're interested in joining our team, email us at:</p>

                       
                         <a href="#sign-up"> <button type="button" class="btn btn-secondary sign-btt page-scroll">Sign Up</button></a>

                    </div>

                </div>

            </div>

        </div>

    </div>

    <div class="Benefits">

        <div class="container-fluid">

            <div class="row">

                <div class="col-md-6">

                    <h2>Compliance & Security Benefits</h2>

                    <p>We offer compliance services and custom contracts to<br> help safeguard your company.</p>

                    <ul>

                        <li><span><i class="fas fa-check"></i></span><h3>Worker Classification & Compliance Services</h3></li>

                        <li><span><i class="fas fa-check"></i></span><h3>Advanced Account Monitoring</h3></li>

                        <li><span><i class="fas fa-check"></i></span><h3>Custom Contracts</h3></li>

                    </ul>

                </div>

                <div class="col-md-6">

                    <div class="lock-img">

                        <img src="assets/frontend/gif/motion5.gif">

                    </div>

                </div>

            </div>

            <div class="row benifit-2">

                <div class="col-md-6">

                    <div class="lock-img">

                        <img src="assets/frontend/gif/motion6.gif">

                    </div>

                </div>

                <div class="col-md-6">

                    <h2>Industry Lowest Fees</h2>

                    <p>We charge the lowest fees in the industry and ensure unmatched value <br>for your spend.</p>

                    <ul>

                        <li><span><i class="fas fa-check"></i></span><h3>Lowest Fees</h3></li>

                        <li><span><i class="fas fa-check"></i></span><h3>Tailor-Made Fee Structure</h3></li>

                        <li><span><i class="fas fa-check"></i></span><h3>No Enterprise Fee</h3></li>

                    </ul>

                </div>

            </div>

        </div>

    </div>

    <div class="find-expert">

        <div class="container">

            <div class="row">

                <div class="col-md-12">

                    <h2>Find an expert for anything</h2>

                    <p>Work with curated freelance talent from all over the world. Manage the entire project with Justzoom.<br>Pay securely with confidence.</p>

                     <a href="#sign-up"> <button type="button" class="btn btn-link page-scroll">Join for Free</button></a>


                    <h4>No upfront payments, no hidden fees.</h4>

                </div>

            </div>

        </div>

    </div>

    <div class="testmonial-bg">

        <div class="container">

            <div class="row">

                <div class="col-md-12">

                    <div class="testmonial-title">

                        <h2>Hear From Our Clients</h2>

                        <ul>

                            <li><h3>Our customers say </h3></li>

                            <li><h4>Excellent    <img src="assets/frontend/img/star.png"></h4></li>

                            <li><h4>4.7 out of 5 based on 1,123 reviews<img src="assets/frontend/img/trustpilot.png"></h4></li>

                        </ul>

                    </div>

                </div>

                <div class="col-md-12">

                    <div class="testmonial">

                        <div class="test-box">

                            <span><i class="fas fa-quote-left"></i></span>

                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In molestie neque et tortor pretium, a facilisis justo tempus. Maecenas facilisis maximus vehicula. Proin sem lacus, eleifend sed orci at, fermentum aliquet dolor. Duis ac erat ac augue ullamcorper aliquet nec quis quam. Donec interdum iaculis erat sit amet commodo. Donec eu porttitor magna, id aliquet ligula. </p>

                            <div class="mc-nallay">

                                <img src="assets/frontend/img/aval.png">

                                <div class="testmonial-text">

                                    <h3>Ryan Walker, Director of Product</h3>

                                    <h4>Rand McNally</h4>

                                </div>

                            </div>

                        </div>

                        <div class="test-box">

                            <span><i class="fas fa-quote-left"></i></span>

                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In molestie neque et tortor pretium, a facilisis justo tempus. Maecenas facilisis maximus vehicula. Proin sem lacus, eleifend sed orci at, fermentum aliquet dolor. Duis ac erat ac augue ullamcorper aliquet nec quis quam. Donec interdum iaculis erat sit amet commodo. Donec eu porttitor magna, id aliquet ligula. </p>

                            <div class="mc-nallay">

                                <img src="assets/frontend/img/aval.png">

                                <div class="testmonial-text">

                                    <h3>Ryan Walker, Director of Product</h3>

                                    <h4>Rand McNally</h4>

                                </div>

                            </div>

                        </div>

                        <div class="test-box">

                            <span><i class="fas fa-quote-left"></i></span>

                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In molestie neque et tortor pretium, a facilisis justo tempus. Maecenas facilisis maximus vehicula. Proin sem lacus, eleifend sed orci at, fermentum aliquet dolor. Duis ac erat ac augue ullamcorper aliquet nec quis quam. Donec interdum iaculis erat sit amet commodo. Donec eu porttitor magna, id aliquet ligula. </p>

                            <div class="mc-nallay">

                                <img src="assets/frontend/img/aval.png">

                                <div class="testmonial-text">

                                    <h3>Ryan Walker, Director of Product</h3>

                                    <h4>Rand McNally</h4>

                                </div>

                            </div>

                        </div>

                        <div class="test-box">

                            <span><i class="fas fa-quote-left"></i></span>

                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In molestie neque et tortor pretium, a facilisis justo tempus. Maecenas facilisis maximus vehicula. Proin sem lacus, eleifend sed orci at, fermentum aliquet dolor. Duis ac erat ac augue ullamcorper aliquet nec quis quam. Donec interdum iaculis erat sit amet commodo. Donec eu porttitor magna, id aliquet ligula. </p>

                            <div class="mc-nallay">

                                <img src="assets/frontend/img/aval.png">

                                <div class="testmonial-text">

                                    <h3>Ryan Walker, Director of Product</h3>

                                    <h4>Rand McNally</h4>

                                </div>

                            </div>

                        </div>

                        <div class="test-box">

                            <span><i class="fas fa-quote-left"></i></span>

                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In molestie neque et tortor pretium, a facilisis justo tempus. Maecenas facilisis maximus vehicula. Proin sem lacus, eleifend sed orci at, fermentum aliquet dolor. Duis ac erat ac augue ullamcorper aliquet nec quis quam. Donec interdum iaculis erat sit amet commodo. Donec eu porttitor magna, id aliquet ligula. </p>

                            <div class="mc-nallay">

                                <img src="assets/frontend/img/aval.png">

                                <div class="testmonial-text">

                                    <h3>Ryan Walker, Director of Product</h3>

                                    <h4>Rand McNally</h4>

                                </div>

                            </div>

                        </div>

                        <div class="test-box">

                            <span><i class="fas fa-quote-left"></i></span>

                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In molestie neque et tortor pretium, a facilisis justo tempus. Maecenas facilisis maximus vehicula. Proin sem lacus, eleifend sed orci at, fermentum aliquet dolor. Duis ac erat ac augue ullamcorper aliquet nec quis quam. Donec interdum iaculis erat sit amet commodo. Donec eu porttitor magna, id aliquet ligula. </p>

                            <div class="mc-nallay">

                                <img src="assets/frontend/img/aval.png">

                                <div class="testmonial-text">

                                    <h3>Ryan Walker, Director of Product</h3>

                                    <h4>Rand McNally</h4>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

     @include("frontend.layouts.common_register_user")

    <div class="Justzoom-Developers">

        <div class="container">

            <div class="row">

                <div class="col-md-12">

                    <h2 class="just-title">Justzoom Developers</h2>

                </div>

                

                 <div class="col-md-3">

                    <ul>

                        @foreach($category_list as $category)

                        <li><h3>{{$category}}</h3></li>

                        @endforeach

                    </ul>

                </div>

              

                

               

            </div>

        </div>

    </div>



@endsection

