<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
 <title>Contract Detail</title>
 <style>

body {
    padding: 0;
    margin: 0;
	    background-color: #fafafa;
}
.contact-view-page {
    background-color: #fafafa;
    padding: 50px;
}

 </style>
</head>
<body>
	<div class="contact-view-page">
		<table class="table" style="padding: 0;
    margin: 0;">
  <tbody>
    <tr>
      <td><h2 style="font-size: 34px;
    font-weight: 400;
    color: #142e3f;
    padding-right: 20px;
    font-family: avenir-roman;">Contract info</h2></td>
	</tr>
	 <tr>
      <td ><h3 style="color: #142e3f;
    font-size: 16px;
    font-weight: 500;
    padding-right: 40px;
    font-family: 'Roboto', sans-serif;">Contract ID: #{{$contract_detail->contractId}}</h3></td>
	</tr>
	 <tr>
      <td ><h3 style="color: #a9a9a9;
    font-size: 14px;
    font-weight: 400;
    font-family: 'Roboto', sans-serif;
    margin-bottom: 20px;">Freelnacer Name: 
    {{ucfirst($contract_detail->first_name .' '.$contract_detail->last_name)}}</h3></td>
	</tr>
    <?php
										    if($contract_detail->priceType == 'hourly'){
												$price = $contract_detail->freelancerPrice/($contract_detail->duration*24);
											}else{
												$price = $contract_detail->freelancerPrice;
											}
									    ?>
	<tr>	
      <td><h4 style="color: #000000;
    font-size: 16px;
    font-weight: 500;
    font-family: 'Roboto', sans-serif;
    margin: 0;">£{{$price}}</h4></td>
      <td ><h4 style="color: #000000;
    font-size: 16px;
    font-weight: 500;
    font-family: 'Roboto', sans-serif;
    margin: 0;">{{$contract_detail->priceType}} Rate</h4></td>
    </tr>
	<tr>	
      <td ><h4 style="color: #000000;
    font-size: 16px;
    font-weight: 500;
    font-family: 'Roboto', sans-serif;margin-bottom: 0px;
   ">{{$contract_detail->duration}}</h4></td>
      <td><h4  style="color: #000000;
    font-size: 16px;
    font-weight: 500;
    font-family: 'Roboto', sans-serif;margin-bottom: 0px;
    ">Hire Duration</h4></td>
    </tr>
	<tr>	
      <td ><h4 style="color: #000000;
    font-size: 16px;
    font-weight: 500;
    font-family: 'Roboto', sans-serif;margin-bottom: 0px;
   ">£{{$contract_detail->freelancerPrice}}</h4></td>
      <td><h4  style="color: #000000;
    font-size: 16px;
    font-weight: 500;
    font-family: 'Roboto', sans-serif;margin-bottom: 0px;
    ">In Escrow</h4></td>
    </tr>
    <?php
										    if($contract_detail->IsContractComplete == 1){
										    
											$closeContract = "Done";
											}else{
                        $closeContract = "In progress";
											}
									    ?>	
	<tr>
 
      <td ><h4 style="color: #000000;
    font-size: 16px;
    font-weight: 500;
    font-family: 'Roboto', sans-serif;margin-bottom: 0px;
   ">{{$closeContract}}</h4></td>
      <td><h4  style="color: #000000;
    font-size: 16px;
    font-weight: 500;
    font-family: 'Roboto', sans-serif;margin-bottom: 0px;
    ">Work Status</h4></td>
    </tr>
    
    
	 <tr>
      <td ><h3 style="color: #142e3f;
    font-size: 16px;
    font-weight: 500;
    padding-right: 40px;
    font-family: 'Roboto', sans-serif;">What the Project About</h3></td>
	</tr>
    <tr>
      <td ><h3 style="color: #606060;
    font-family: avenir-roman;
    font-size: 16px;
    font-weight: 400;
    line-height: 30px;
    margin-bottom: 0px;">{{$contract_detail->aboutProject}}</h3></td>
	</tr>
	<tr>
      <td ><h3 style="color: #142e3f;
    font-size: 16px;
    font-weight: 500;
    padding-right: 40px;
    font-family: 'Roboto', sans-serif;">What do you need to get done</h3></td>
	</tr>
	<tr>
      <td ><h3 style="color: #606060;
    font-family: avenir-roman;
    font-size: 16px;
    font-weight: 400;
    line-height: 30px;
    margin-bottom: 0px;">{{$contract_detail->requirement}}</h3></td>
	</tr>
	<tr>
      <td ><h3 style="color: #142e3f;
    font-size: 16px;
    font-weight: 500;
    padding-right: 40px;
    font-family: 'Roboto', sans-serif;margin-bottom: 0px;">Project Discription</h3></td>
	</tr>
	<tr>
      <td ><p style="color: #606060;
    font-family: avenir-roman;
    font-size: 16px;
    font-weight: 400;
    line-height: 30px;
    margin-bottom: 0px;margin-bottom: 0px;">{{$contract_detail->description}}</p></td>
	</tr>
	<tr>
      <td ><p style="color: #a9a9a9;
    font-size: 14px;
    font-weight: 400;
    font-family: 'Roboto', sans-serif;
    margin-bottom: 20px;">Contact Detail</p></td>
	</tr>
	<tr>	
      <td><h4 style="color: #000000;
    font-size: 16px;
    font-weight: 500;
    font-family: 'Roboto', sans-serif;
    margin: 0;">Email</h4></td>
      <td ><h4 style="color: #000000;
    font-size: 16px;
    font-weight: 500;
    font-family: 'Roboto', sans-serif;
    margin: 0;">{{$contract_detail->email}}</h4></td>
    </tr>
	<tr>
    <td><h4 style="color: #000000;
    font-size: 16px;
    font-weight: 500;
    font-family: 'Roboto', sans-serif;
    margin: 0;">Phone</h4></td>	
     <td ><h4 style="color: #000000;
    font-size: 16px;
    font-weight: 500;
    font-family: 'Roboto', sans-serif;
    margin: 0;">{{$contract_detail->phone}}</h4></td>
      
    </tr>
  </tbody>
</table>
	</div>
</body>
</body>
</html>
