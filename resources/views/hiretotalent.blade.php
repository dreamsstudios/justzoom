@extends('frontend.layouts.layout')



@section('title', 'Justzoom')

@section('body')



<div class="hiretoptelent-page">

		<div class="container">

			<div class="hiretoptelent-bg">

				<div class="row">

					<div class="col-md-12">

						<div class="you-interest">

							<h2>Thank you for your interest in Justzoom.</h2>

							<h3>Which type freelancer you find?</h3>

							<div class="surch-form">

							  <input type="text" name="cate-search" id="cate-search" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm" placeholder="Web Design">

							  <input type="hidden" name="cate-id" id="cate-id" class="form-control">
							  <input type="hidden" name="question" id="question" class="form-control">								

                                <span><i class="fas fa-search"></i></span>

							</div>



                            <div id="category_list" style="width: 61%; margin: 0 auto;"></div> 



                            <?php 

                            if(isset($category_list) && !empty($category_list)) {

                                $i=0;

                            ?>

								<h4>Popular:

									@foreach($category_list as $k=>$V)

										{{ ($i=='0') ? "" : "," }}

										{{ ucfirst($V) }}

										@php($i++)

									@endforeach 

								</h4>

                            <?php

                            }

                            ?>

                            

							<button type="button" class="btn btn-primary join-btt" onclick="gotoSearch('')">GET STARTED</button>

						</div>

					</div>

				</div>

				<div class="row">

					<div class="col-md-12">

						<h2 class="just-title">Justzoom Developers</h2>

					</div>

					<div class="col-md-3">

						<ul class="Finance-listing">

							 @foreach($category_list as $category)
                        <li><h3>{{$category}}</h3></li>
                        @endforeach

						</ul>

					</div>

					
					

					

				</div>

			</div>

		</div>

	</div>

    <script>

   function gotoSearch(catId){


		  var catId =  $("#cate-id").val() || catId;
		 
		  var step = "step-1";
        var question  = $("#question").val();
		if(question>0){
			var url  = "question-filter/"+catId+"/step-1";

		}else{
			var url = '{{route("search-result", ":catId") }}';
			url = url.replace(':catId', catId);
			 
		}
       
       
		
		   // url = url.replace(':step', step);	
       

	   	if (catId != "") {

			window.open(url);

		}       

    }

    </script>

   

@endsection



