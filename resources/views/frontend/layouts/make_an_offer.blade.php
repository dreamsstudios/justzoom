<!-- Make an offer -->
<div class="modal fade" id="sendofffer" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    <div class="alert alert-success" style="display:none"></div>
    <div class="alert alert-danger" style="display:none"></div>
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Make an offer</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form action="#" id="offersubmit" class="needs-validation" onsubmit="sendoffer(this,event)" novalidate>
				@csrf
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">offer Price({{Config::get('app.site_currency_symbol')}}):</label>
            <input  type="hidden" value="" name="offer_job_id" id="offer_job_id"/>
            <input  type="hidden" value="add_offer" name="action"/>
            <input  type="hidden" value="" name="offer_by" id="offer_by"/>
            <input  type="hidden" value="" name="user_id" id="user_id"/>
            <input class="form-control" type="number" value="" max="999" name="offer_price" id="offer_price"/>
          </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Send offer</button>
      </div>
      </form>
    </div>
   
  </div>
</div>