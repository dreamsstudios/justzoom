<div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <img src="assets/frontend/img/footer-logo.png">
                    <h4>Hire the top of freelance talent</h4>
                    <a class="now-btt" href="#" data-toggle="modal" data-target="#exampleModal2">Subscribe Now</a>
                    <ul class="social-link">
                        <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                        <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <h2>Most In-Demand Talent</h2>
                    <ul class="footer-link">
                        <li><a href="#">iOS Developers</a></li>
                        <li><a href="#">Front-End Developers</a></li>
                        <li><a href="#">UX Designers</a></li>
                        <li><a href="#">UI Designers</a></li>
                        <li><a href="#">Financial Modeling Consultants</a></li>
                        <li><a href="#">Interim CFOs</a></li>
                        <li><a href="#">Digital Project Managers</a></li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <h2>About</h2>
                    <ul class="footer-link">
                        <li><a href="#">Clients</a></li>
                        <li><a href="#">Freelance Developers</a></li>
                        <li><a href="#">Freelance Designers</a></li>
                        <li><a href="#">Freelance Finance Experts</a></li>
                        <li><a href="#">Freelance Project Managers</a></li>
                        <li><a href="#">Freelance Product Managers</a></li>
                        <li><a href="#">Specialized Services</a></li>
                        <li><a href="#">About Us</a></li>
                    </ul>
                </div>
                <div class="col-md-2">
                    <h2>Contact</h2>
                    <ul class="footer-link">
                        <li><a href="#">Contact Us</a></li>
                        <li><a href="#">Press Center</a></li>
                        <li><a href="#">Careers</a></li>
                        <li><a href="#">FAQ</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="copy-right">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <p>Copyright © 2020 Justzoom. All Right Reserved.</p>
                    </div>
                    <div class="col-md-6">
                        <ul>
                            <li><a href="#">Terms & Conditions   |  </a></li>
                            <li><a href="#">Privacy Policy</a></li> 
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="login-modal">
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="login-form">
                  <h2>Login</h2>
                    <h3>Welcome back!</h3>
                  <div class="alert alert-danger print-error-msg" style="display:none">
                <ul></ul>
            </div>
            <form action="#" class="needs-validation loginauth" onsubmit="logincheck(this,event)" novalidate>
                @csrf
                <div class="form-group msg">
                </div>
                    
                       <div class="form-group">
                    <label for="uname">Email:</label>
                    <input type="text" class="form-control" id="uname" placeholder="Enter email" name="uname" required>
                    <div class="valid-feedback">Valid.</div>
                    <div class="invalid-feedback">Please fill out this field.</div>
                </div>
                      <div class="form-group">
                    <label for="pwd">Password:</label>
                    <input type="password" class="form-control" id="pass" placeholder="Enter password" name="pswd" required>
                    <div class="valid-feedback">Valid.</div>
                    <div class="invalid-feedback">Please fill out this field.</div>
                </div>
                     
                       <div class="form-forget">
                    <a href="javascript:" data-toggle="modal" data-target="#Forgot_pass" data-dismiss="modal">Forgot Password</a>
                </div>
                      <div class="login-form-button">
                         <input type="hidden" name="action" value="login_Form">
                <button type="button" class="btn btn-primary mu-send-btn validate-form registerform btn-regis" value="Login">Login</button>
                       
                        <h4>Don’t have a account? <a class="jumper" href=".abhi" data-dismiss = "modal" data-target=".abhi">Register here</a></h4>
                      </div>
                    </form>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
    
    <div class="login-modal">
        <div class="modal fade" id="Forgot_pass" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="login_form">
                    <h2>Forgot Password</h2>
                   <form method="post" action="#" class="needs-validation mu-contact-form" onsubmit="forgotcheck(this,event)">
                      @csrf
                      <div class="form-group msg">
                      </div> 
                      <div class="form-group">
                        <label for="exampleInputEmail1">Email</label>
                       <input type="text" class="form-control" id="forgotemail" placeholder="Enter email address" name="email" required>
    <div class="invalid-feedback">Please fill out this field.</div>
                      </div>
                      <input type="hidden" name="action" value="forgot_Form">
                     
                       
                      <div class="login-form-button">
                        <button type="button" class="btn btn-primary btn-regis mu-send-btn validate-forgot-password" value="Submit" >Submit</button>
                        <h4><a href="#" data-toggle="modal" data-target="#exampleModal" data-dismiss="modal">Back to login</a></h4>
                      </div>
                    </form>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
    <div class="login-modal">
        <div class="modal fade" id="register" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="login-form">
                    <h2>Create account</h2>
                   @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
            </div>
            <br>
            @endif
            <div class="alert alert-danger print-error-msg" style="display:none">
                <ul></ul>
            </div>
            <form action="#" id="registrationform" class="needs-validation" onsubmit="registration(this,event)" novalidate>
              @csrf
              <div class="form-group login-checkbox">
                    <div class="form-check-inline">
                        <label class="form-check-label">
                        <input type="radio" class="form-check-input userRoleUser" name="role" value="user" checked>Freelancer
                        </label>
                    </div>
                    <div class="form-check-inline">
                        <label class="form-check-label">
                        <input type="radio" class="form-check-input userRoleClient" name="role" value="client">Client
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="uname">First Name:</label>
                    <input type="text" class="form-control" id="fname"  name="fname" required>
                    <span class="text-danger">{{ $errors->first('fname') }}</span>
                  
                    <div class="invalid-feedback">Please fill out this field.</div>
                </div>
                <div class="form-group">
                    <label for="uname">Last Name:</label>
                    <input type="text" class="form-control" id="lname"  name="lname" required>
                    <span class="text-danger">{{ $errors->first('lname') }}</span>
                  
                    <div class="invalid-feedback">Please fill out this field.</div>
                </div>
                <div class="form-group">
                    <label for="uname">Email:</label>
                    <input type="email" class="form-control" id="email"  name="email" required>
                    <span class="text-danger">{{ $errors->first('email') }}</span>
                    
                    <div class="invalid-feedback">Please fill out this field.</div>
                </div>
                <div class="form-group">
                    <label for="pwd">Password:</label>
                    <input type="password" class="form-control" id="password"  name="pswd" required>
                    <span class="text-danger">{{ $errors->first('pswd') }}</span>
                  
                    <div class="invalid-feedback">Please fill out this field.</div>
                </div>
                <div class="form-group">
                    <label for="pwd">Confirm Password:</label>
                    <input type="password" class="form-control" id="confirmPassword" name="cpswd" required>
                    <span class="text-danger">{{ $errors->first('cpswd') }}</span>
                  
                    <div class="invalid-feedback">Please fill out this field.</div>
                </div>
                      <div class="login-checkbox">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="remember" required id="agree">
                            <label class="custom-control-label" for="agree">By signing up, you agree to our terms of service and privacy policy.
                         <div class="invalid-feedback">Check this checkbox to continue.</div>

                            </label>
                        </div>
                      </div>    
                       <input type="hidden" name="action" value="registration_Form">  
                      <div class="login-form-button">
                         <button type="button" class="btn btn-primary mu-send-btn validate-password registerform btn-regis" value="Register">Register</button>

                <div class="form-group msg">
                </div>
                        <h4>If you have account? <a href="#"  data-toggle="modal" data-target="#exampleModal" data-dismiss="modal">Login Now</a></h4>
                      </div>
                    </form>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
    <div class="freelance-modal">
        <div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel3" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="star-living">
                    <h2>Start living your work dream</h2>
                    <p>What do you want to do? (you can edit this later)</p>
                    <div class="freelance-button">
                        <a class="want-green" href="#">I want to <br>Hire a Freelancer</a>
                        <a class="want-blue" href="#">I want to <br>Work as a freelancer</a>
                    </div>
                    <div class="creative-account">
                        <button type="button" class="btn btn-primary create-blue">Create Account</button>
                        <button type="button" class="btn btn-warning create-orange">Sign Up with Email</button>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
</div>

    <script type="text/javascript" src= "{!! asset('assets/frontend/js/popper.min.js') !!}"></script>
    <script type="text/javascript" src= "{!! asset('assets/frontend/js/bootstrap.min.js') !!}"></script>
    <!--<script type="text/javascript" src= "{!! asset('assets/frontend/js/custom.js') !!}"></script>-->

<script type="text/javascript">
      $(document).ready(function() {
  $(".jumper").on("click", function( e ){

    e.preventDefault();

    $("body, html").animate({ 
      scrollTop: $($(this).attr('href') ).offset().top 
    }, 600);

  });
});


            $(document).ready(function () {
             
                $('#cate-search').on('keyup',function() {
                    var query = $(this).val();
                    alert("gggg");
                    $.ajax({
                       
                        url:"{{ route('categary-search') }}",
                  
                        type:"GET",
                       
                        data:{'category':query},
                       
                        success:function (data) {
                          
                            $('#category_list').html(data);
                        }
                    })
                    // end of ajax call
                });

                
                $(document).on('click', 'li', function(){
                  
                    var value = $(this).text();
                    var catId = $(this).attr('data-cat-id');
                    $('#cate-id').val(catId);
                    $('#cate-search').val(value);
                    $('#category_list').html("");
                });
            });
       
    </script>