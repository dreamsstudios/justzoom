<!DOCTYPE html>
<html lang="en">
<head>
    <title>@yield('title')</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
   
    
    <link rel="icon" href="{!! asset('assets/frontend/img/Favicon.png') !!}" type="img/png" sizes="16x16">
   <link rel="stylesheet" href="{!! asset('assets/frontend/css/bootstrap.min.css') !!}">
  
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <link rel="stylesheet" href="{!! asset('assets/frontend/css/style.css') !!}">
    <link rel="stylesheet" href="{!! asset('assets/frontend/css/responsive.css') !!}">
    <link rel="stylesheet" href="{!! asset('assets/frontend/css/slick.css') !!}">
    <link rel="stylesheet" href="{!! asset('assets/frontend/css/slick-theme.css') !!}">
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
      
  <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/additional-methods.min.js"></script>-->
   <script src="https://code.jquery.com/jquery-1.11.0.min.js"></script> 
{{-- <script type="text/javascript" src= "{!! asset('assets/js/jquery.min.js') !!}"></script> --}}
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
<script type="text/javascript" src= "{!! asset('assets/frontend/js/front.js') !!}"></script>
  <script type="text/javascript">
        var BASEURL = "{{route('home')}}";
        var COMMONURL = BASEURL+'/common';
    </script>


</head>
<body>
    <div class="page-wraper">
        
        @include("frontend.layouts.header_layout")

         @yield('body')

         @include("frontend.layouts.footer_layout")
 
</html>  
      