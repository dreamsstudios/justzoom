
 @if(Request::segment(1)=='why')
 <div class="header why-banner">
    <div class="unveiled-navigation">
      <nav class="navbar navbar-expand-lg navbar-light bg-light nav-transparent">
        <a class="navbar-brand" href="{{ route('home') }}"><img class="logo-nav" src="{!! asset('assets/frontend/img/footer-logo.png') !!}"> <img class="logo-white" src="{!! asset('assets/frontend/img/logo.png') !!}"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
        </button>
       @if (empty(Session::get('roleId')))
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
       

      <ul class="navbar-nav mr-auto">
        <li class="nav-item <?php echo (isset($page_title) && $page_title=='why')?'active':''?>">
        <a class="nav-link" href="{{ route('why') }}">Why  <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item <?php echo (isset($page_title) && $page_title=='howitworks')?'active':''?>">
        <a class="nav-link" href="{{ route('howitworks') }}">How It Works</a>
          </li>
        <li class="nav-item <?php echo (isset($page_title) && $page_title=='enterprise')?'active':''?>">
        <a class="nav-link" href="{{ route('enterprise') }}">Enterprise </a>
        </li>
        <li class="nav-item <?php echo (isset($page_title) && $page_title=='blog')?'active':''?>">
        <a class="nav-link" href="{{ route('blog') }}">Blog </a>
        </li>
        
        <li class="nav-item <?php echo (isset($page_title) && $page_title=='about')?'active':''?>">
          <a class="nav-link" href="{{ route('about-us') }}">About Us</a>
        </li>
      </ul>
      <ul class="navbar-nav ml-auto">
        <li class="nav-item">
        <a class="nav-link" href="#" data-toggle="modal" data-target="#exampleModal3">Apply as a Freelancer </a>
        </li>
        
        <li class=" nav-item "><a class="nav-link" href="javascript:void(0)" data-toggle="modal" data-target="#exampleModal">Log in</a></li>
        <li class="nav-item">
        <a class="nav-link talant-btt" href="{{ route('hire-to-talent') }}">Hire Top Talent</a>
        </li>
      </ul>
    </div>
      @else
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        
      </ul>
      <ul class="navbar-nav ml-auto right-nav">
       
        <li>
        <div class="dropdown nav">
            <?php
                      $loginuser = loginuserDetail(Session::get('roleId'),Session::get('role'));
                      if(isset($loginuser->image) && !empty($loginuser->image))
                        {
                          $loginuserimg = $loginuser->image;
                      }else{
                          $loginuserimg = 'no-img.png';

                      }
                      ?>
          <button class="btn  dropdown-toggle bttt" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <a class="nav-link" href="#"><img style="width: 5%; height: auto;"  class="img-responsive" src="{{asset('/uploads/user_images/'.$loginuserimg)}}" alt="">@if(session::get('first_name'))  {{ Session::get('first_name') }} @endif</a>
          </button>
          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
          <ul class="nav-dropdown">
            <li>
              <img src="{!! asset('assets/frontend/img/drop1.png') !!}">
              <a href="{{(Session::get('role') == 'client')?route('client-dashboard'):route('user-dashboard')}}">Dashboard</a>
            </li>
            <li>
              <img src="{!! asset('assets/frontend/img/drop2.png') !!}">
              <a href="{{(Session::get('role') == 'client')?route('client-profile'):route('user-profile')}}">Profile</a>
            </li>
            <li>
              <img src="{!! asset('assets/frontend/img/drop3.png') !!}">
              <a href="{{(Session::get('role') == 'client')?route('client-edit-profile'):route('user-edit-profile')}}">Edit Profile</a>
            </li>
             <li>
              <img src="{!! asset('assets/frontend/img/drop3.png') !!}">
              <a href="{{(Session::get('role') == 'client')?route('client-change-password'):route('user-change-password')}}">Change Password</a>
            </li>
           
            <li>
              <img src="{!! asset('assets/frontend/img/drop4.png') !!}">
              <a href="{{(Session::get('role') == 'client')?route('client-payment-request'):route('user-payment-request')}}">Payment Request</a>
            </li>
            <li>
              <img src="{!! asset('assets/frontend/img/drop5.png') !!}">
              <a href="{{route('user-contract-list')}}">Contracts</a>
            </li>
           <!--  <li>
              <img src="{!! asset('assets/frontend/img/drop6.png') !!}">
              <a href="#">Support</a>
            </li> -->
            <li>
              <img src="{!! asset('assets/frontend/img/drop7.png') !!}">
              <a href="{{route('logoutfront')}}">Sign Out</a>
            </li>
          </ul>
          </div>
        </div>
        </li>
      </ul>
      </div>
       @endif
      </nav>
    </div>  
   
  
   @elseif(Request::segment(1)=='enterprise')
   <div class="header Enterprise-banner">
    <div class="unveiled-navigation">
      <nav class="navbar navbar-expand-lg navbar-light bg-light nav-transparent">
        <a class="navbar-brand" href="{{ route('home') }}"><img class="logo-nav" src="{!! asset('assets/frontend/img/footer-logo.png') !!}"> <img class="logo-white" src="{!! asset('assets/frontend/img/logo.png') !!}"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
        </button>

         @if (empty(Session::get('roleId')))
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
       

      <ul class="navbar-nav mr-auto">
        <li class="nav-item <?php echo (isset($page_title) && $page_title=='why')?'active':''?>">
        <a class="nav-link" href="{{ route('why') }}">Why  <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item <?php echo (isset($page_title) && $page_title=='howitworks')?'active':''?>">
        <a class="nav-link" href="{{ route('howitworks') }}">How It Works</a>
          </li>
        <li class="nav-item <?php echo (isset($page_title) && $page_title=='enterprise')?'active':''?>">
        <a class="nav-link" href="{{ route('enterprise') }}">Enterprise </a>
        </li>
        <li class="nav-item <?php echo (isset($page_title) && $page_title=='blog')?'active':''?>">
        <a class="nav-link" href="{{ route('blog') }}">Blog </a>
        </li>
        
        <li class="nav-item <?php echo (isset($page_title) && $page_title=='about')?'active':''?>">
          <a class="nav-link" href="{{ route('about-us') }}">About Us</a>
        </li>
      </ul>
      <ul class="navbar-nav ml-auto">
        <li class="nav-item">
        <a class="nav-link" href="#" data-toggle="modal" data-target="#exampleModal3">Apply as a Freelancer </a>
        </li>
        
        <li class=" nav-item "><a class="nav-link" href="javascript:void(0)" data-toggle="modal" data-target="#exampleModal">Log in</a></li>
        <li class="nav-item">
        <a class="nav-link talant-btt" href="{{ route('hire-to-talent') }}">Hire Top Talent</a>
        </li>
      </ul>
    </div>
      @else
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
     
      <ul class="navbar-nav ml-auto right-nav">
        
        
        <li>
        <div class="dropdown nav">
            <?php
                      $loginuser = loginuserDetail(Session::get('roleId'),Session::get('role'));
                      if(isset($loginuser->image) && !empty($loginuser->image))
                        {
                          $loginuserimg = $loginuser->image;
                      }else{
                          $loginuserimg = 'no-img.png';

                      }
                      ?>
          <button class="btn  dropdown-toggle bttt" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <a class="nav-link" href="#"><img style="width: 5%; height: auto;"  class="img-responsive" src="{{asset('/uploads/user_images/'.$loginuserimg)}}" alt="">@if(session::get('first_name'))  {{ Session::get('first_name') }} @endif</a>
          </button>
          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
          <ul class="nav-dropdown">
            <li>
              <img src="{!! asset('assets/frontend/img/drop1.png') !!}">
              <a href="{{(Session::get('role') == 'client')?route('client-dashboard'):route('user-dashboard')}}">Dashboard</a>
            </li>
            <li>
              <img src="{!! asset('assets/frontend/img/drop2.png') !!}">
              <a href="{{(Session::get('role') == 'client')?route('client-profile'):route('user-profile')}}">Profile</a>
            </li>
            <li>
              <img src="{!! asset('assets/frontend/img/drop3.png') !!}">
              <a href="{{(Session::get('role') == 'client')?route('client-edit-profile'):route('user-edit-profile')}}">Edit Profile</a>
            </li>
             <li>
              <img src="{!! asset('assets/frontend/img/drop3.png') !!}">
              <a href="{{(Session::get('role') == 'client')?route('client-change-password'):route('user-change-password')}}">Change Password</a>
            </li>
           
            <li>
              <img src="{!! asset('assets/frontend/img/drop4.png') !!}">
              <a href="{{(Session::get('role') == 'client')?route('client-payment-request'):route('user-payment-request')}}">Payment Request</a>
            </li>
             <li>
              <img src="{!! asset('assets/frontend/img/drop5.png') !!}">
              <a href="{{route('user-contract-list')}}">Contracts</a>
            </li>
            <!-- <li>
              <img src="{!! asset('assets/frontend/img/drop6.png') !!}">
              <a href="#">Support</a>
            </li> -->
            <li>
              <img src="{!! asset('assets/frontend/img/drop7.png') !!}">
              <a href="{{route('logoutfront')}}">Sign Out</a>
            </li>
          </ul>
          </div>
        </div>
        </li>
      </ul>
      </div>
      @endif
      </nav>
    </div>
 @elseif(Request::segment(1)=='about-us')

 <div class="header about-banner">
    <div class="unveiled-navigation">
      <nav class="navbar navbar-expand-lg navbar-light bg-light nav-transparent">
        <a class="navbar-brand" href="{{ route('home') }}"><img class="logo-nav" src=" {!! asset('assets/frontend/img/footer-logo.png') !!}"> 
          <img class="logo-white" src="{!! asset('assets/frontend/img/logo.png') !!}"></a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
        </button>

         @if (empty(Session::get('roleId')))
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
       

      <ul class="navbar-nav mr-auto">
        <li class="nav-item <?php echo (isset($page_title) && $page_title=='why')?'active':''?>">
        <a class="nav-link" href="{{ route('why') }}">Why  <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item <?php echo (isset($page_title) && $page_title=='howitworks')?'active':''?>">
        <a class="nav-link" href="{{ route('howitworks') }}">How It Works</a>
          </li>
        <li class="nav-item <?php echo (isset($page_title) && $page_title=='enterprise')?'active':''?>">
        <a class="nav-link" href="{{ route('enterprise') }}">Enterprise </a>
        </li>
        <li class="nav-item <?php echo (isset($page_title) && $page_title=='blog')?'active':''?>">
        <a class="nav-link" href="{{ route('blog') }}">Blog </a>
        </li>
        
        <li class="nav-item <?php echo (isset($page_title) && $page_title=='about')?'active':''?>">
          <a class="nav-link" href="{{ route('about-us') }}">About Us</a>
        </li>
      </ul>
      <ul class="navbar-nav ml-auto">
        <li class="nav-item">
        <a class="nav-link" href="#" data-toggle="modal" data-target="#exampleModal3">Apply as a Freelancer </a>
        </li>
        
        <li class=" nav-item "><a class="nav-link" href="javascript:void(0)" data-toggle="modal" data-target="#exampleModal">Log in</a></li>
        <li class="nav-item">
        <a class="nav-link talant-btt" href="{{ route('hire-to-talent') }}">Hire Top Talent</a>
        </li>
      </ul>
    </div>
      @else
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
     
      <ul class="navbar-nav ml-auto right-nav">
        
        <li>
        <div class="dropdown nav">
            <?php
                      $loginuser = loginuserDetail(Session::get('roleId'),Session::get('role'));
                      if(isset($loginuser->image) && !empty($loginuser->image))
                        {
                          $loginuserimg = $loginuser->image;
                      }else{
                          $loginuserimg = 'no-img.png';

                      }
                      ?>
          <button class="btn  dropdown-toggle bttt" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <a class="nav-link" href="#"><img style="width: 5%; height: auto;"  class="img-responsive" src="{{asset('/uploads/user_images/'.$loginuserimg)}}" alt="">@if(session::get('first_name'))  {{ Session::get('first_name') }} @endif</a>
          </button>
          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
          <ul class="nav-dropdown">
            <li>
              <img src="{!! asset('assets/frontend/img/drop1.png') !!}">
              <a href="{{(Session::get('role') == 'client')?route('client-dashboard'):route('user-dashboard')}}">Dashboard</a>
            </li>
            <li>
              <img src="{!! asset('assets/frontend/img/drop2.png') !!}">
              <a href="{{(Session::get('role') == 'client')?route('client-profile'):route('user-profile')}}">Profile</a>
            </li>
            <li>
              <img src="{!! asset('assets/frontend/img/drop3.png') !!}">
              <a href="{{(Session::get('role') == 'client')?route('client-edit-profile'):route('user-edit-profile')}}">Edit Profile</a>
            </li>
             <li>
              <img src="{!! asset('assets/frontend/img/drop3.png') !!}">
              <a href="{{(Session::get('role') == 'client')?route('client-change-password'):route('user-change-password')}}">Change Password</a>
            </li>
           
            <li>
              <img src="{!! asset('assets/frontend/img/drop4.png') !!}">
              <a href="{{(Session::get('role') == 'client')?route('client-payment-request'):route('user-payment-request')}}">Payment Request</a>
            </li>
             <li>
              <img src="{!! asset('assets/frontend/img/drop5.png') !!}">
              <a href="{{route('user-contract-list')}}">Contracts</a>
            </li>
            <!-- <li>
              <img src="{!! asset('assets/frontend/img/drop6.png') !!}">
              <a href="#">Support</a>
            </li> -->
            <li>
              <img src="{!! asset('assets/frontend/img/drop7.png') !!}">
              <a href="{{route('logoutfront')}}">Sign Out</a>
            </li>
          </ul>
          </div>
        </div>
        </li>
      </ul>
      </div>
      @endif
      </nav>
    </div>
 @elseif(Request::segment(1)=='howitworks')

      <div class="header how-it-work">
    <div class="unveiled-navigation">
      <nav class="navbar navbar-expand-lg navbar-light bg-light nav-transparent">
        <a class="navbar-brand" href="{{ route('home') }}"><img class="logo-nav" src="{!! asset('assets/frontend/img/footer-logo.png') !!}"> <img class="logo-white" src="{!! asset('assets/frontend/img/logo.png') !!}"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
        </button>

         @if (empty(Session::get('roleId')))
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
       

      <ul class="navbar-nav mr-auto">
        <li class="nav-item <?php echo (isset($page_title) && $page_title=='why')?'active':''?>">
        <a class="nav-link" href="{{ route('why') }}">Why  <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item <?php echo (isset($page_title) && $page_title=='howitworks')?'active':''?>">
        <a class="nav-link" href="{{ route('howitworks') }}">How It Works</a>
          </li>
        <li class="nav-item <?php echo (isset($page_title) && $page_title=='enterprise')?'active':''?>">
        <a class="nav-link" href="{{ route('enterprise') }}">Enterprise </a>
        </li>
        <li class="nav-item <?php echo (isset($page_title) && $page_title=='blog')?'active':''?>">
        <a class="nav-link" href="{{ route('blog') }}">Blog </a>
        </li>
        
        <li class="nav-item <?php echo (isset($page_title) && $page_title=='about')?'active':''?>">
          <a class="nav-link" href="{{ route('about-us') }}">About Us</a>
        </li>
      </ul>
      <ul class="navbar-nav ml-auto">
        <li class="nav-item">
        <a class="nav-link" href="#" data-toggle="modal" data-target="#exampleModal3">Apply as a Freelancer </a>
        </li>
        
        <li class=" nav-item "><a class="nav-link" href="javascript:void(0)" data-toggle="modal" data-target="#exampleModal">Log in</a></li>
        <li class="nav-item">
        <a class="nav-link talant-btt" href="{{ route('hire-to-talent') }}">Hire Top Talent</a>
        </li>
      </ul>
    </div>
      @else
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
      
      <ul class="navbar-nav ml-auto right-nav">
        
        <li>
        <div class="dropdown nav">
            <?php
                      $loginuser = loginuserDetail(Session::get('roleId'),Session::get('role'));
                      if(isset($loginuser->image) && !empty($loginuser->image))
                        {
                          $loginuserimg = $loginuser->image;
                      }else{
                          $loginuserimg = 'no-img.png';

                      }
                      ?>
          <button class="btn  dropdown-toggle bttt" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <a class="nav-link" href="#"><img style="width: 5%; height: auto;"  class="img-responsive" src="{{asset('/uploads/user_images/'.$loginuserimg)}}" alt="">@if(session::get('first_name'))  {{ Session::get('first_name') }} @endif</a>
          </button>
          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
          <ul class="nav-dropdown">
            <li>
              <img src="{!! asset('assets/frontend/img/drop1.png') !!}">
              <a href="{{(Session::get('role') == 'client')?route('client-dashboard'):route('user-dashboard')}}">Dashboard</a>
            </li>
            <li>
              <img src="{!! asset('assets/frontend/img/drop2.png') !!}">
              <a href="{{(Session::get('role') == 'client')?route('client-profile'):route('user-profile')}}">Profile</a>
            </li>
            <li>
              <img src="{!! asset('assets/frontend/img/drop3.png') !!}">
              <a href="{{(Session::get('role') == 'client')?route('client-edit-profile'):route('user-edit-profile')}}">Edit Profile</a>
            </li>
             <li>
              <img src="{!! asset('assets/frontend/img/drop3.png') !!}">
              <a href="{{(Session::get('role') == 'client')?route('client-change-password'):route('user-change-password')}}">Change Password</a>
            </li>
           
            <li>
              <img src="{!! asset('assets/frontend/img/drop4.png') !!}">
              <a href="{{(Session::get('role') == 'client')?route('client-payment-request'):route('user-payment-request')}}">Payment Request</a>
            </li>
             <li>
              <img src="{!! asset('assets/frontend/img/drop5.png') !!}">
              <a href="{{route('user-contract-list')}}">Contracts</a>
            </li>
            <!-- <li>
              <img src="{!! asset('assets/frontend/img/drop6.png') !!}">
              <a href="#">Support</a>
            </li> -->
            <li>
              <img src="{!! asset('assets/frontend/img/drop7.png') !!}">
              <a href="{{route('logoutfront')}}">Sign Out</a>
            </li>
          </ul>
          </div>
        </div>
        </li>
      </ul>
      </div>
      @endif
      </nav>
    </div>
    @else
  <div class="unveiled-navigation">
    <nav class="navbar navbar-expand-lg navbar-light bg-light" style="background-color:#fff !important;">
      <a class="navbar-brand" href="{{ route('home') }}"><img class="logo-nav" src="{!! asset('assets/frontend/img/logo.png') !!}"> <img class="logo-white" src="{!! asset('assets/frontend/img/logo.png') !!}"></a>

      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
      </button>

       @if (empty(Session::get('roleId')))
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
       

      <ul class="navbar-nav mr-auto">
        <li class="nav-item <?php echo (isset($page_title) && $page_title=='why')?'active':''?>">
        <a class="nav-link" href="{{ route('why') }}">Why  <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item <?php echo (isset($page_title) && $page_title=='howitworks')?'active':''?>">
        <a class="nav-link" href="{{ route('howitworks') }}">How It Works</a>
          </li>
        <li class="nav-item <?php echo (isset($page_title) && $page_title=='enterprise')?'active':''?>">
        <a class="nav-link" href="{{ route('enterprise') }}">Enterprise </a>
        </li>
        <li class="nav-item <?php echo (isset($page_title) && $page_title=='blog')?'active':''?>">
        <a class="nav-link" href="{{ route('blog') }}">Blog </a>
        </li>
        
        <li class="nav-item <?php echo (isset($page_title) && $page_title=='about')?'active':''?>">
          <a class="nav-link" href="{{ route('about-us') }}">About Us</a>
        </li>
      </ul>
      <ul class="navbar-nav ml-auto">
        <li class="nav-item">
        <a class="nav-link" href="#" data-toggle="modal" data-target="#exampleModal3">Apply as a Freelancer </a>
        </li>
        
        <li class=" nav-item "><a class="nav-link" href="javascript:void(0)" data-toggle="modal" data-target="#exampleModal">Log in</a></li>
        <li class="nav-item">
        <a class="nav-link talant-btt" href="{{ route('hire-to-talent') }}">Hire Top Talent</a>
        </li>
      </ul>
    </div>
      @else
   
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
     
      <ul class="navbar-nav ml-auto right-nav">
         
        
        <li>
        <div class="dropdown nav">
            <?php
                      $loginuser = loginuserDetail(Session::get('roleId'),Session::get('role'));
                      if(isset($loginuser->image) && !empty($loginuser->image))
                        {
                          $loginuserimg = $loginuser->image;
                      }else{
                          $loginuserimg = 'no-img.png';

                      }
                      ?>
          <button class="btn  dropdown-toggle bttt" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <a class="nav-link" href="#"><img style="width: 5%; height: auto;"  class="img-responsive" src="{{asset('/uploads/user_images/'.$loginuserimg)}}" alt="">@if(session::get('first_name'))  {{ Session::get('first_name') }} @endif</a>
          </button>
          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
          <ul class="nav-dropdown">
            <li>
              <img src="{!! asset('assets/frontend/img/drop1.png') !!}">
              <a href="{{(Session::get('role') == 'client')?route('client-dashboard'):route('user-dashboard')}}">Dashboard</a>
            </li>
            <li>
              <img src="{!! asset('assets/frontend/img/drop2.png') !!}">
              <a href="{{(Session::get('role') == 'client')?route('client-profile'):route('user-profile')}}">Profile</a>
            </li>
            <li>
              <img src="{!! asset('assets/frontend/img/drop3.png') !!}">
              <a href="{{(Session::get('role') == 'client')?route('client-edit-profile'):route('user-edit-profile')}}">Edit Profile</a>
            </li>
             <li>
              <img src="{!! asset('assets/frontend/img/drop3.png') !!}">
              <a href="{{(Session::get('role') == 'client')?route('client-change-password'):route('user-change-password')}}">Change Password</a>
            </li>
           
            <li>
              <img src="{!! asset('assets/frontend/img/drop4.png') !!}">
              <a href="{{(Session::get('role') == 'client')?route('client-payment-request'):route('user-payment-request')}}">Payment Request</a>
            </li>
             <li>
              <img src="{!! asset('assets/frontend/img/drop5.png') !!}">
              <a href="{{(Session::get('role') == 'client')?route('client-contract-list'):route('user-contract-list')}}">Contracts</a>
            </li>
            <!-- <li>
              <img src="{!! asset('assets/frontend/img/drop6.png') !!}">
              <a href="#">Support</a>
            </li> -->
            <li>
              <img src="{!! asset('assets/frontend/img/drop7.png') !!}">
              <a href="{{route('logoutfront')}}">Sign Out</a>
            </li>
          </ul>
          </div>
        </div>
        </li>
      </ul>
      </div>
      @endif
     
    </nav>
  </div>
@endif