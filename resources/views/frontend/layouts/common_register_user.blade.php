<div class="try-today " id="sign-up" >

    <div class="container">

      <div class="row">

        <div class="col-md-12">

          <h2 class="try-title">Try us today</h2>

          <div class="form-tabs">

            <ul class="nav nav-tabs" id="myTab" role="tablist">

              <li class="nav-item">

              <a class="nav-link" id="home-tab" data-toggle="tab" href="#humnechangekarahai" role="tab" aria-controls="home" aria-selected="true">Client and Agencies</a>

              </li>

              <li class="nav-item">

              <a class="nav-link active" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Freelancers</a>

              </li>

             

            </ul>

            <div class="tab-content" id="myTabContent">

              <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">

              <div class="clients-forms">

                <p>Justzoom only accepts the best freelancers who have years of experience. When working with Justzoom, you'll gain access to a top-tier professional network, great clients, and resources you can leverage to accelerate your career.</p>

                @if ($message = Session::get('success'))

            <div class="alert alert-success alert-block">

                <button type="button" class="close" data-dismiss="alert">×</button>

                    <strong>{{ $message }}</strong>

            </div>

            <br>

            @endif

            <div class="alert alert-danger print-error-msg" style="display:none">

                <ul></ul>

            </div>

              <form action="#" id="registrationform" class="needs-validation" onsubmit="registration(this,event)" novalidate>

              @csrf

              <div class="form-group login-checkbox">

                    <div class="form-check-inline">

                        <label class="form-check-label">

                        <input type="radio" class="form-check-input userRoleUser" name="role" value="user" id="user" >Freelancer

                        </label>

                    </div>

                    <div class="form-check-inline">

                        <label class="form-check-label">

                        <input type="radio" class="form-check-input userRoleClient" name="role" value="client" id="client">Client

                        </label>

                    </div>

                </div>

              <div class="form-group">

                    <label for="uname">First Name:</label>

                    <input type="text" class="form-control" id="fname"  name="fname" required>

                    <span class="text-danger">{{ $errors->first('fname') }}</span>

                  

                    <div class="invalid-feedback">Please fill out this field.</div>

                </div>

                 <div class="form-group">

                    <label for="uname">Last Name:</label>

                    <input type="text" class="form-control" id="lname"  name="lname" required>

                    <span class="text-danger">{{ $errors->first('lname') }}</span>

                  

                    <div class="invalid-feedback">Please fill out this field.</div>

                </div>

                  <div class="form-group">

                    <label for="uname">Email:</label>

                    <input type="email" class="form-control" id="email"  name="email" required>

                    <span class="text-danger">{{ $errors->first('email') }}</span>

                    

                    <div class="invalid-feedback">Please fill out this field.</div>

                </div>

                   <div class="form-group">

                    <label for="pwd">Password:</label>

                    <input type="password" class="form-control" id="password"  name="pswd" required>

                    <span class="text-danger">{{ $errors->first('pswd') }}</span>

                  

                    <div class="invalid-feedback">Please fill out this field.</div>

                </div>

                  <div class="form-group">

                    <label for="pwd">Confirm Password:</label>

                    <input type="password" class="form-control" id="confirmPassword" name="cpswd" required>

                    <span class="text-danger">{{ $errors->first('cpswd') }}</span>

                  

                    <div class="invalid-feedback">Please fill out this field.</div>

                </div>

                  <div class="login-checkbox">

                        <div class="custom-control custom-checkbox">

                            <input type="checkbox" class="custom-control-input" name="remember" required id="agree">

                            <label class="custom-control-label" for="agree">By signing up, you agree to our terms of service and privacy policy.

                         <div class="invalid-feedback">Check this checkbox to continue.</div>



                            </label>

                        </div>

                      </div>

                 

                    <input type="hidden" name="action" value="registration_Form">  

                     

                         <button type="button" class="btn btn-primary join-btt mu-send-btn validate-password registerform btn-regis" value="Register">Join Justzoom</button>



                <div class="form-group msg">

                </div>

                </form>

              </div>

              </div>

              

            </div>

          </div>

        </div>

      </div>

    </div>

  </div>
  <script type="text/javascript">
    


    $(document).ready(function(){
    $(".user").click(function(){
        $("#user").prop("checked", true);
    });
    $(".client").click(function(){
        $("#client").prop("checked", true);
    });
});
  </script>