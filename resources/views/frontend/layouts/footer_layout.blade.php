 <style>
   .error{ color:red; } 
  </style>
<div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <img src=" {!! asset('assets/frontend/img/footer-logo.png') !!}">
                   
                    <h4>Hire the top of freelance talent</h4>
                   <form id="contact_us" method="post" action="javascript:void(0)">
      @csrf
               <input type="text"  name="email" class="form-control" id="email" placeholder="Please enter email id">
        <span id="msg_divv" class="text-danger">{{ $errors->first('email') }}</span>

      <span id="msg_div"></span>
                <button type="submit id="send_form" class="btn btn-success btn-submit">Subscribe Now</button>
                
              </form>
                    <ul class="social-link">
                        <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                        <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <h2>Most In-Demand Talent</h2>
                    <ul class="footer-link">
                        <li><a href="#">iOS Developers</a></li>
                        <li><a href="#">Front-End Developers</a></li>
                        <li><a href="#">UX Designers</a></li>
                        <li><a href="#">UI Designers</a></li>
                        <li><a href="#">Financial Modeling Consultants</a></li>
                        <li><a href="#">Interim CFOs</a></li>
                        <li><a href="#">Digital Project Managers</a></li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <h2>About</h2>
                    <ul class="footer-link">
                       
                         <li><a href="{{route('about-us')}}">About Us</a></li>
                    </ul>
                </div>
                <div class="col-md-2">
                    <h2>Contact</h2>
                     <ul class="footer-link">
                        <li><a href="{{route('contact-us')}}">Contact Us</a></li>
                        <li><a href="#">Press Center</a></li>
                        <li><a href="{{route('faq')}}">FAQ</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="copy-right">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <p>Copyright © 2020 Justzoom. All Right Reserved.</p>
                    </div>
                    <div class="col-md-6">
                        <ul>
                            <li><a href="#">Terms & Conditions   |  </a></li>
                            <li><a href="#">Privacy Policy</a></li> 
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="login-modal">
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="login-form">
                  <h2>Login</h2>
                    <h3>Welcome back!</h3>
                  <div class="alert alert-danger print-error-msg" style="display:none">
                <ul></ul>
            </div>
            <form action="#" class="needs-validation loginauth" onsubmit="logincheck(this,event)" novalidate>
                @csrf
                <div class="form-group msg">
                </div>
                    
                       <div class="form-group">
                    <label for="uname">Email:</label>
                    <input type="text" class="form-control" id="uname" placeholder="Enter email" name="uname" required>
                    <div class="valid-feedback">Valid.</div>
                    <div class="invalid-feedback">Please fill out this field.</div>
                </div>
                      <div class="form-group">
                    <label for="pwd">Password:</label>
                    <input type="password" class="form-control" id="pass" placeholder="Enter password" name="pswd" required>
                    <div class="valid-feedback">Valid.</div>
                    <div class="invalid-feedback">Please fill out this field.</div>
                </div>
                     
                       <div class="form-forget">
                    <a href="javascript:" data-toggle="modal" data-target="#Forgot_pass" data-dismiss="modal">Forgot Password</a>
                </div>
                      <div class="login-form-button">
                         <input type="hidden" name="action" value="login_Form">
                <button type="button" class="btn btn-primary mu-send-btn validate-form registerform btn-regis" value="Login">Login</button>
                       
                       
                         <h4>Don�t have a account? <a class="page-scroll" id="cc" href="#sign-up" >Register here</a></h4>
                      </div>
                    </form>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>

    
    
    <div class="login-modal">
        <div class="modal fade" id="Forgot_pass" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="login_form">
                    <h2>Forgot Password</h2>
                   <form method="post" action="#" class="needs-validation mu-contact-form" onsubmit="forgotcheck(this,event)">
                      @csrf
                      <div class="form-group msg">
                      </div> 
                      <div class="form-group">
                        <label for="exampleInputEmail1">Email</label>
                       <input type="text" class="form-control" id="forgotemail" placeholder="Enter email address" name="email" required>
    <div class="invalid-feedback">Please fill out this field.</div>
                      </div>
                      <input type="hidden" name="action" value="forgot_Form">
                     
                       
                      <div class="login-form-button">
                        <button type="button" class="btn btn-primary btn-regis mu-send-btn validate-forgot-password" value="Submit" >Submit</button>
                        <h4><a href="#" data-toggle="modal" data-target="#exampleModal" data-dismiss="modal">Back to login</a></h4>
                      </div>
                    </form>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
    <div class="login-modal">
        <div class="modal fade" id="register" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="login-form">
                    <h2>Create account</h2>
                   @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
            </div>
            <br>
            @endif
            <div class="alert alert-danger print-error-msg" style="display:none">
                <ul></ul>
            </div>
            <form action="#" id="registrationform" class="needs-validation" onsubmit="registration(this,event)" novalidate>
              @csrf
              <div class="form-group login-checkbox">
                    <div class="form-check-inline">
                        <label class="form-check-label">
                        <input type="radio" class="form-check-input userRoleUser" name="role" value="user" checked>Freelancer
                        </label>
                    </div>
                    <div class="form-check-inline">
                        <label class="form-check-label">
                        <input type="radio" class="form-check-input userRoleClient" name="role" value="client">Client
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="uname">First Name:</label>
                    <input type="text" class="form-control" id="fname"  name="fname" required>
                    <span class="text-danger">{{ $errors->first('fname') }}</span>
                  
                    <div class="invalid-feedback">Please fill out this field.</div>
                </div>
                <div class="form-group">
                    <label for="uname">Last Name:</label>
                    <input type="text" class="form-control" id="lname"  name="lname" required>
                    <span class="text-danger">{{ $errors->first('lname') }}</span>
                  
                    <div class="invalid-feedback">Please fill out this field.</div>
                </div>
                <div class="form-group">
                    <label for="uname">Email:</label>
                    <input type="email" class="form-control" id="email"  name="email" required>
                    <span class="text-danger">{{ $errors->first('email') }}</span>
                    
                    <div class="invalid-feedback">Please fill out this field.</div>
                </div>
                <div class="form-group">
                    <label for="pwd">Password:</label>
                    <input type="password" class="form-control" id="password"  name="pswd" required>
                    <span class="text-danger">{{ $errors->first('pswd') }}</span>
                  
                    <div class="invalid-feedback">Please fill out this field.</div>
                </div>
                <div class="form-group">
                    <label for="pwd">Confirm Password:</label>
                    <input type="password" class="form-control" id="confirmPassword" name="cpswd" required>
                    <span class="text-danger">{{ $errors->first('cpswd') }}</span>
                  
                    <div class="invalid-feedback">Please fill out this field.</div>
                </div>
                      <div class="login-checkbox">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="remember" required id="agree">
                            <label class="custom-control-label" for="agree">By signing up, you agree to our terms of service and privacy policy.
                         <div class="invalid-feedback">Check this checkbox to continue.</div>

                            </label>
                        </div>
                      </div>    
                       <input type="hidden" name="action" value="registration_Form">  
                      <div class="login-form-button">
                         <button type="button"  class="btn btn-primary mu-send-btn validate-password registerform btn-regis" value="Register">Register</button>

                <div class="form-group msg">
                </div>
                        <h4>If you have account? <a href="#"  data-toggle="modal" data-target="#exampleModal" data-dismiss="modal">Login Now</a></h4>
                      </div>
                    </form>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
    <div class="freelance-modal">
        <div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel3" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="star-living">
                    <h2>Start living your work dream</h2>
                    <p>What do you want to do? </p>
                    <div class="freelance-button">
                       <a class="want-green page-scroll user" id="aa" href="#sign-up">Freelancer<br> signup as freelancer </a>
                      
                        <a class="want-blue page-scroll client" id="bb" href="#sign-up">Client<br> signup as client</a>
                    </div>
                   <!--  <div class="creative-account">
                        <button type="button" class="btn btn-primary create-blue">Create Account</button>
                        <button type="button" class="btn btn-warning create-orange">Sign Up with Email</button>
                    </div> -->
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
</div>

     <script type="text/javascript" src= "{!! asset('assets/frontend/js/popper.min.js') !!}"></script>
    <script type="text/javascript" src= "{!! asset('assets/frontend/js/bootstrap.min.js') !!}"></script>
    <script type="text/javascript" src= "{!! asset('assets/frontend/js/custom.js') !!}"></script>

<script type="text/javascript">
      $(document).ready(function() {
  $(".jumper").on("click", function( e ){

    e.preventDefault();

    $("body, html").animate({ 
      scrollTop: $($(this).attr('href') ).offset().top 
    }, 600);

  });
});


            $(document).ready(function () {
             
                $('#cate-search').on('keyup',function() {
                    var query = $(this).val();
                    $.ajax({
                       
                        url:"{{ route('categary-search') }}",
                  
                        type:"GET",
                       
                        data:{'category':query},
                       
                        success:function (data) {
                          
                            $('#category_list').html(data);
                        }
                    })
                    // end of ajax call
                });

                
                $(document).on('click', 'li', function(){
                  
                    var value = $(this).text();
                    var catId = $(this).attr('data-cat-id');
                    var ques = $(this).attr('data-question');
                    $('#cate-id').val(catId);
                    $('#cate-search').val(value);
                    $('#question').val(ques);
                    $('#category_list').html("");
                });
            });
       
    </script>
    
  <script type="text/javascript">


    $(document).ready(function() {

        $(".btn-submit").click(function(e){
            
            e.preventDefault();

            $('#send_form').html('Sending..');
      $.ajax({
         url:'/add-subscriber-email',
        type: "POST",
        data: $('#contact_us').serialize(),
        success: function( response ) {
           if (response['status']==true) {
                
                    $("#msg_div").html('Subscriber Succrssfully!').css("color", "green");
                  }
            $('#send_form').html('Submit');
            
           
            document.getElementById("contact_us").reset(); 
            setTimeout(function(){
           $('#msg_div').hide();

            },8000);
        },
         error: function (request, status, error) {
            $('[name="email"]').next('span').html(request.responseJSON.errors.email);
            setTimeout(function(){
           $('#msg_divv').hide();

            },10000);
        }
      });


        }); 
});
</script>
<script type="text/javascript">
$("#aa").click(function(){
$("#exampleModal3").modal('hide');
});

$("#bb").click(function(){
$("#exampleModal3").modal('hide');
});
$("#cc").click(function(){
$("#exampleModal").modal('hide');
});
</script>
