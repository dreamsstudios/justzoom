
<div class="login-modal">
        <div class="modal fade" id="review" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
              	<div class="alert alert-danger" style="display:none"></div>
	 <div class="alert alert-success" style="display:none"></div>
                <div class="login_form">
                    <h2>Client Reviews</h2>
                    <form action="#" id="reviewsubmit" class="needs-validation" onsubmit="reviewsubmit(this,event)" novalidate>
                      @csrf
                      
                     <input type="hidden" value="ajax_client_review" name="action" />
                      <div class="form-group">
                        <label for="exampleInputEmail1">Rating</label>
                        <select class="form-control" id="rating" name="rating" required>
                          <option value="0">Select Rating</option>
                          <option value="1">1 star</option>
                      <option value="2">2 star</option>
                      <option value="3">3 star</option>
                      <option value="4">4 star</option>
                      <option value="5">5 star</option>
			</select>
    <div class="invalid-feedback">Please fill out this field.</div>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Review</label>
                       <textarea type="text" id="review" name="review" rows="3" class="form-control md-textarea" placeholder="Your Review"></textarea>
    <div class="invalid-feedback">Please fill out this field.</div>
                      </div>
                     <input type="hidden" name="contractId" value="{{$past->contractId}}" id="contractId">
                     <input type="hidden" name="user_id" value="{{$past->userId}}" id="user_id">
                     <input type="hidden" name="client_id" value="{{$past->clientId}}" id="client_id">
                     
                     <input type="hidden" value="" name="review_user_id" id="review_user_id">
                     <input type="hidden" value="" name="review_job_req_id" id="review_job_req_id">
                     
        <div class="login-form-button">
        <button type="submit" class="btn btn-primary" id="reviewButton">Submit</button>
      </div>
	  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>