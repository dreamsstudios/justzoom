<form action="#" id="Editjobsubmit" class="needs-validation" onsubmit="jobsubmit(this,event)" novalidate>
				@csrf
				<input type="hidden" value="ajax_jobrequest" name="action" />
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="job-title">Job Title:</label>
								<input type="text" class="form-control" value="{{$job_detail->title}}" id="title"  name="title" required>
								<div class="invalid-feedback">Please fill out this field.</div>
							</div>
						</div>
						<div class="col-md-6">
						
							<div class="form-group">
								<label for="category">Category:</label>
								<select class="form-control" id="category_id" name="category_id" required>
									@foreach($category_list as $key=>$category)
                                        <option value="{{$key}}" <?php if($job_detail->category_id == $key){ echo "selected"; }?>> {{$category}}</option>
									@endforeach
								</select>
							
								<div class="invalid-feedback">Please Select any Category.</div>
							</div>
						</div>
						<div class="col-md-8">
							<div class="form-group">
								<label for="address">Address:</label>
								<textarea class="form-control"  id="address" name="address" rows="2" required>{{$job_detail->address}}</textarea>
							
								<div class="invalid-feedback">Please fill out this field.</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="country">Country:</label>
								<select class="form-control" id="country" name="country" onchange="state_list(this.value,event)" required>
								<option>Select Country</option>
								@foreach($country_list as $key=>$category)
                                        <option value="{{$key}}"  <?php if($job_detail->country_id == $key){ echo "selected"; }?> > {{$category}}</option>
									@endforeach
								</select>
						
								<div class="invalid-feedback">Please fill out this field.</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="state">State:</label>
								<select class="form-control" id="state" name="state" onchange="city_list(this.value,event)" required>
								
								<option>Select State</option>
								@foreach($state_list as $key=>$state)
                                        <option value="{{$key}}"  <?php if($job_detail->state_id == $key){ echo "selected"; }?> > {{$state}}</option>
									@endforeach
								</select>
							
								<div class="invalid-feedback">Please fill out this field.</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="city">City:</label>
								<select class="form-control" id="city" name="city" required>
								<option>Select City</option>
								@foreach($city_list as $key=>$city)
                                        <option value="{{$key}}"  <?php if($job_detail->city_id == $key){ echo "selected"; }?> > {{$city}}</option>
									@endforeach
								</select>
							
								<div class="invalid-feedback">Please fill out this field.</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="job-title">Pincode:</label>
								<input type="text" value="{{$job_detail->pincode}}" class="form-control" id="pincode"  name="pincode" required>
							
								<div class="invalid-feedback">Please fill out this field.</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label for="JobDes">Job description:</label>
								<textarea class="form-control"  id="JobDes" name="description" rows="6" required>{{$job_detail->description}}</textarea>
							
								<div class="invalid-feedback">Please fill out this field.</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="start_date">Start Date:</label>
								<input onchange="getpriceofuser()" value="<?php echo date('m/d/y',strtotime($job_detail->job_start_on)) ?>" type="text" class="form-control start_date" id="start_date" name="job_start_on" required>
							
								<div class="invalid-feedback">Please Select Start Date.</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="end_date">End Date:</label>
								<input  onchange="getpriceofuser()"  type="text" value="<?php echo date('m/d/y',strtotime($job_detail->job_end_on)) ?>" class="form-control end_date" id="end_date" name="job_end_on" required>
							
								<div class="invalid-feedback">Please Select End Date.</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="pr-type">Working Hour/Day</label>
								<input onchange="getpriceofuser()" value="{{$job_detail->working_hour}}" onkeyup="getpriceofuser()" type="number" min=1 max=24 class="form-control" id="working_hour" name="working_hour" required>
							
								<div class="invalid-feedback">Please fill out this field.</div>
							</div>
						</div>
						<div class="col-md-6">
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="pr-type">Price Type:</label>
								<select class="form-control" id="price_type" name="price_type" onchange="getpriceofuser()" required>
									<option value="0" <?php if($job_detail->job_price_type == '0'){ echo "selected"; } ?>>Fixed</option>
									<option value="1" <?php if($job_detail->job_price_type == '1'){ echo "selected"; } ?>>Open</option>
								</select>
								<div class="invalid-feedback">Please fill out this field.</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="price">Price:</label>
								<input type="hidden" value="{{$job_detail->job_req_id}}" id="job_req_id" name="user_id"/>
								<input type="hidden" value="{{$job_detail->user_id}}" id="user_id" name="user_id"/>
								<input type="hidden" value="{{$job_detail->user_price}}" id="user_price" name="user_price" />
								<input type="text" value="{{$job_detail->job_price}}" @if($job_detail->job_price_type == '0') readOnly="true" @endif class="form-control" id="price" name="price" required>
							
								<div class="invalid-feedback">Please Select Price.</div>
							</div>
						</div>
					</div>
					<button type="submit" class="btn btn-primary">Submit</button>
        </form>
		<div class="alert alert-success" style="display:none"></div>
		<script>
			$(document).ready(function() {
$('.start_date').datepicker({
    multidate: false,		
    startDate: new Date()

}).on('changeDate', function (selected) {
    startDate = new Date(selected.date.valueOf());
    startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
    $('.end_date').datepicker('setStartDate', startDate);
});

$('.end_date').datepicker({
    multidate: false,
    startDate: new Date()
}).on('changeDate', function (selected) {
    FromEndDate = new Date(selected.date.valueOf());
    FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
    $('.start_date').datepicker('setEndDate', FromEndDate);
});
});
		</script>