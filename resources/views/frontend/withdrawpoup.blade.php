
<div class="login-modal">
        <div class="modal fade" id="withdraw" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
              	<div class="alert alert-danger" style="display:none"></div>
	 <div class="alert alert-success" style="display:none"></div>
                <div class="login_form">
                    <h2>Withdraw for Contract</h2>
                      @if(!empty($closedContract) && count($closedContract)>0)
                      <div class="form-group">
                        <label for="exampleInputEmail1">Contract</label>
                        <select class="form-control" id="contractId" name="contractId" required>
                        <option value="">Select contract</option>
                          @foreach($closedContract as $con)
                          <option value="{{$con->contractId}}">#{{$con->contractId}}</option>
                          @endforeach
			                </select> 
                      <br>   
                    <div class="login-form-button">
                    <button type="button" class="btn btn-primary" onclick="releasefund()" id="withdraw">Submit</button>
                  </div>
                  @elseif($escrowAmount>0)
                  <div class="alert alert-warning" role="alert">
                    Already you have sent request for withdraw.wait for client response.
                  </div>
                  @else
                  <div class="alert alert-warning" role="alert">
                    You have already received all the payment.
                  </div>
	                @endif
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>