<!-- Job request Post-->
<div class="modal job-request-popup" id="jobpopup">
	<div class="modal-dialog  modal-dialog-centered ">
		<div class="modal-content">
			  <div class="alert alert-danger" style="display:none"></div>
			  
			<!-- Modal Header -->
			<div class="modal-header">
				<h1 class="modal-title">Job Request</h1>
				<button type="button" class="close" data-dismiss="modal"  title="close popup"><img src="{!! asset('assets/frontend/img/X.png') !!}" alt="close form"></button>
			</div>

			<div class="login_form">
				<form action="#" id="jobsubmit" class="needs-validation" onsubmit="jobsubmit(this,event)" novalidate>
				@csrf
				<input type="hidden" value="ajax_jobrequest" name="action" />
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="job-title">Job Title:</label>
								<input type="text" class="form-control" id="title"  name="title" required>
								<div class="invalid-feedback">Please fill out this field.</div>
							</div>
						</div>
						<div class="col-md-6">
						
							<div class="form-group">
								<label for="category">Category:</label>
								<select class="form-control" id="category_id" name="category_id" required>
									@foreach($category_list as $key=>$category)
                                        <option value="{{$key}}"> {{$category}}</option>
									@endforeach
								</select>
							
								<div class="invalid-feedback">Please Select any Category.</div>
							</div>
						</div>
						<div class="col-md-8">
							<div class="form-group">
								<label for="address">Address:</label>
								<textarea class="form-control" id="address" name="address" rows="2" required></textarea>
							
								<div class="invalid-feedback">Please fill out this field.</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="country">Country:</label>
								<select class="form-control" id="country" name="country" onchange="state_list(this.value,event)" required>
								<option>Select Country</option>
								@foreach($country_list as $key=>$category)
                                        <option value="{{$key}}"> {{$category}}</option>
									@endforeach
								</select>
						
								<div class="invalid-feedback">Please fill out this field.</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="state">State:</label>
								<select class="form-control" id="state" name="state" onchange="city_list(this.value,event)" required>
								<option>Select State</option>
								</select>
							
								<div class="invalid-feedback">Please fill out this field.</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="city">City:</label>
								<select class="form-control" id="city" name="city" required>
								<option>Select City</option>
								</select>
							
								<div class="invalid-feedback">Please fill out this field.</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="job-title">Pincode:</label>
								<input type="text" class="form-control" id="pincode"  name="pincode" required>
							
								<div class="invalid-feedback">Please fill out this field.</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label for="JobDes">Job description:</label>
								<textarea class="form-control" id="JobDes" name="description" rows="6" required></textarea>
							
								<div class="invalid-feedback">Please fill out this field.</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="start_date">Start Date:</label>
								<input @if(isset($gaurd->freelance_id)) onchange="getpriceofuser()" @endif  type="text" class="form-control start_date" id="start_date" name="job_start_on" required>
							
								<div class="invalid-feedback">Please Select Start Date.</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="end_date">End Date:</label>
								<input  @if(isset($gaurd->freelance_id)) onchange="getpriceofuser()" @endif  type="text" class="form-control end_date" id="end_date" name="job_end_on" required>
							
								<div class="invalid-feedback">Please Select End Date.</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="pr-type">Working Hour/Day</label>
								<input @if(isset($gaurd->freelance_id)) onchange="getpriceofuser()"  onkeyup="getpriceofuser()" @endif type="number" min=1 max=24 class="form-control" id="working_hour" name="working_hour" required>
							
								<div class="invalid-feedback">Please fill out this field.</div>
							</div>
						</div>
						
                        @if(isset($gaurd->freelance_id))
                        <div class="col-md-6">
                        </div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="pr-type">Price Type({{Config::get('app.site_currency_symbol')}}): </label>
								<select class="form-control" id="price_type" name="price_type" onchange="getpriceofuser()" required>
									<option value="0">Fixed</option>
									<option value="1">Open</option>
								</select>
							
								<div class="invalid-feedback">Please fill out this field.</div>
							</div>
                        </div>
                        @endif
						<div class="col-md-6">
							<div class="form-group">
								<label for="price">Price({{Config::get('app.site_currency_symbol')}}) :</label>
								<input type="hidden" value="@if(isset($gaurd->freelance_id)){{$gaurd->freelance_id}}@endif" id="user_id" name="user_id"/>
                                <input type="hidden" value="@if(isset($gaurd->price)){{$gaurd->price}}@endif" id="user_price" name="user_price" />
                                <input type="hidden" value="<?php echo  isset($gaurd->freelance_id)? '1':'0' ?>" id="job_type" name="job_type" />
								<input type="text" @if(isset($gaurd->freelance_id))readOnly="true"@endif class="form-control" maxlength="6" id="price" name="price" value="" required >
							
								<div class="invalid-feedback">Please Select Price.</div>
							</div>
						</div>
					</div>
					<button type="submit" class="btn btn-primary">Submit</button>
        </form>
        <div class="alert alert-success" style="display:none"></div>
			</div> 
		</div>
	</div>
</div>