
<div class="login-modal">
        <div class="modal fade" id="releaseFund" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
              	<div class="alert alert-danger" style="display:none"></div>
	              <div class="alert alert-success" style="display:none"></div>
                <div class="login_form">
                    <h2>Card Detail</h2>
                  <div class="row">
									<div class="col-md-12">
										<div class="form-group">
										    <label for="card-name">Pending Amount : <?=$req->currency.$req->freelancerPrice;?></label>
										    
										</div>
									</div>
								</div>
              
                    
                    <div class="payment-heading">
									<h3>Enter Payment Info</h3>
									<img src="<?php echo  asset('assets/frontend/img/payment-icon.png') ?>" alt="payment icon">
								</div>
								<input type="hidden" value="<?php echo $req->freelancerPrice; ?>" name="totalPrice" id="totalPrice" />
								<input type="hidden" value="<?php echo $req->contractId; ?>" name="contractId" id="contractId" />
								<div class="row">
                <div class="col-md-12 payment-errors"></div>
									<div class="col-md-12">
										<div class="form-group">
										    <label for="card-name">Name on Card</label>
										    <input type="text" class="form-control" autocomplete="off" name="cardholder" id="cardholder" placeholder="" required autofocus>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-8">
										<div class="form-group">
										    <label for="card-number">Card Number</label>
										    <input type="text" class="form-control" autocomplete="off" id="cardNumber" placeholder="" name="cardNumber" 
                      						required data-stripe="number" maxlength="20" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" />
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
										    <label for="cvc">CVC</label>
										    <input type="password" maxlength="4" autocomplete="off" class="form-control" id="cvvCode" placeholder="" name="cvvcode" size="4" 
											data-stripe="cvc" required onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')"  />
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
										    <label for="card-expiry">Expiration Month</label>
										    <input type="text" class="form-control"  autocomplete="off" id="expiryMonth" maxlength="2" 
											placeholder="MM" required name="expiryMonth" size="2" 
											data-stripe="exp_month"  onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" />
										</div>
									</div>
									<div class="col-md-6">
									<div class="form-group">
										    <label for="card-expiry">Expiration Year</label>
										    <input type="text" class="form-control" 
									autocomplete="off" maxlength="2" id="expiryYear" placeholder="YY" size="2" data-stripe="exp_year" name="expiryYear" 
									required onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" />
										</div>
									</div>
								</div>
                <div class="payment-notes">
									<div class="pay-msg">
										<h6>Payments are processed securely through</h6>
									</div>
									<div class="pay-img">
										<img src="<?php echo  asset('assets/frontend/img/stripe-icon.png') ?>" alt="stripe icon">
									</div>
								</div>
								<button type="button" class="btn btn-success blue pay" onclick="payByCard()">Pay</button>
	            
                </div>

              </div>
              <div class="alert alert-success" style="display:none"></div>
            </div>
          </div>
        </div>
    </div>
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
	<script>
	
	Stripe.setPublishableKey('<?php echo $setting->stirpePublicKey; ?>');
  function payByCard(){
    Stripe.card.createToken({
  number: $('#cardNumber').val(),
  cvc: $('#cvvCode').val(),
  exp_month: $('#expiryMonth').val(),
  exp_year: $('#expiryYear').val()
}, stripeResponseHandler);

  }
  

  function stripeResponseHandler(status, response) {
	 
    // Grab the form:
   
  
    if (response.error) { 
     // alert(response.error.message);
    $('.payment-errors').text(response.error.message);
    $('.pay').prop('disabled', false).html("process"); // Re-enable submission
  
    } else { // Token was created!

    $('.pay').prop('disabled', false).html('Processing subsciption..');
    // Get the token ID:
    var token = response.id;
    var contractId = $('#contractId').val();
    payTouser(contractId,token);
  
    }
  }

  function cvvCheck() {
  debugger;
  if(!$('#cvvCode').val())
    $('.payment-errors.msg').text('Please enter cvv code.');

} 

	</script>