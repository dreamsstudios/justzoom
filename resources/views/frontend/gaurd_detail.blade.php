@extends('frontend.layouts.layout')

@section('title', 'Justzoom')
@section('body')
 <div class="banner_search">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="user_info">
                                <div class="user_img">
                                     <?php
                                        if(!empty($gaurd->image)){
                                            $img = $gaurd->image;
                                        }else{
                                            $img = 'no-img.png';

                                        }
                                      ?>
                                    <img src="{{asset('/uploads/user_images/'.$img)}}" alt="user image" class="img-fluid">
                                </div>
                                <div class="profile_details">
                                    <h2>{{ucfirst($gaurd->first_name).' '.ucfirst($gaurd->last_name)}}</h2>
                                    @if(!empty($gaurd->category_detail))<h4>{{ $gaurd->category_detail->category_name }}</h4>@endif
                                    @if(isset($total_count->average_rating) && !empty($total_count->average_rating))<h4><b>{{number_format($total_count->average_rating,1)}}</b> ({{$total_count->totalreview}} Feedbacks)</h4>@endif
                                    <h3><span><i class="fas fa-map-marker-alt"></i></span> {{ $gaurd->country }} @if($gaurd->is_user_verified == '0')
                                        <strong> Verified</strong>
                                        @endif </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>              
            </div>
            <div class="profile_main">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="about_me">
                                <h2>About Me</h2>                               
                                <p><?php if(!empty($gaurd->about)){
                                    echo $gaurd->about;
                                }else{
                                    echo "No data found";

                                }

                                ?>

                                </p>
                            </div>
                            <div class="work_history">
                                <h2><i class="far fa-thumbs-up"></i> Work History and Feedback</h2>

                                <?php 
                                if(!empty($reviews)){
                                    foreach($reviews as $review){
                                ?>

                                     <div class="work_post">
                                    <h3><?php 
                                    if(isset($review->category_id)){
                                         $cat_data = user_category_data($review->category_id);
                                        echo  $cat_data->category_name;    
                                    }
                                   
                                    ?></h3>
                                    <!--<h5>Rated as Freelancer</h5>-->
                                    <div class="rating">
                                        <h3>@if(isset($review->rating) && !empty($review->rating)){{number_format($review->rating, 1)}}  @endif</h3>
                                        <ul>
                                           <?php
                                           if(isset($review->rating) && !empty($review->rating)){
                                           for($i=1;$i<=$review->rating;$i++){
                                             echo '<li><i class="fas fa-star"></i></li>';

                                           }
                                          }
                                           ?>
                                        </ul>
                                        <div class="calend"><i class="far fa-calendar-alt"></i> {{$review->created_at}}</div>
                                    </div>
                                    <p>@if(isset($review->rating)){{$review->review}} @endif</p>
                                </div>


                                    <?php   
                                    }?>
                                     <ul class="pagination">
                                        {{ $reviews->links() }}
                                </ul>
                                <?php
                                }else{
                                    echo "No Feedback found";
                                }
                                ?>
                        </div>
                    </div>
                        <div class="col-md-4">
                            <div class="statistics">
                                <h3><strong>{{Config::get('app.site_currency_symbol')}} {{ $gaurd->price}}</strong><br>Hourly Rate</h3>
                                <h3><strong>{{$job_done_count}}</strong><br>Jobs Done</h3>
                                <h3><strong>0</strong><br>Rehired</h3>
                            </div>
                            <!--<h5 class="name_desg"><a href="#" sendOfferModel data-toggle="modal" data-target="#sendOfferModel">Make a Offer</a></h5>-->
							      <h5 class="name_desg">
                    @if(session::get('role') == 'client')
                      <a href="javascript:void(0);" data-toggle="modal" data-target="#jobpopup">Hire</a>@endif</h5>
                            <div class="achieve">
                                <h3>Skills</h3>
                                <ul>
                                    <?php

                                    $skills = user_skill_data($gaurd->freelance_id);
                                   // print_r($skills->skill_data);
                                    if(!empty($skills)){
                                        ?>
                                        <div class="soft_skills">
                                        <ul>
                                        @foreach ($skills as $skill)
                                        <li>{{$skill->skill_data->skill}}</li>
                                        @endforeach
                                        </ul>
                                        </div>  
                                    <?php   
                                    }else{
                                        echo "No data found";
                                    }
                                     ?>
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<!--<div class="modal fade" id="sendOfferModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">New message</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
	   <form action="#" id="jobsubmit" class="needs-validation" onsubmit="jobsubmit(this,event)" novalidate>
       @csrf
	   <div class="modal-body">
        <div class="form-group">
            <label for="recipient-name" class="col-form-label">Job Title:</label>
            <input type="text" value="" name="title" id="title" />
          </div>
		   <div class="form-group">
            <label for="recipient-name" class="col-form-label">Category:</label>
            <select class="form-control" id="category" name="category">
                <option value="">Select Category</option>
				
            </select>
          </div>
		   <div class="form-group">
            <label for="address" class="col-form-label">Address:</label>
           <textarea class="form-control" id="address" name="address"></textarea>
			
          </div>
		   <div class="form-group">
            <label for="country" class="col-form-label">Country:</label>
            <input type="text" value="" name="country" id="country" />
			
          </div>
		   <div class="form-group">
            <label for="state" class="col-form-label">State:</label>
            <input type="text" value="" name="state" id="state" />
			
          </div>
		   <div class="form-group">
            <label for="city" class="col-form-label">City:</label>
            <input type="text" value="" name="city" id="city" />
			
          </div>
          <div class="form-group">
            <label for="message-text" class="col-form-label">Job Description:</label>
            <textarea class="form-control" id="description" name="description"></textarea>
          </div>
      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary jbsubmit">Submit</button>
      </div>
	    </form>
    </div>
  </div>
</div>
job request popup -->

<div class="modal fade" id="sendOfferModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">New message</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Task:</label>
            <select class="form-control" id="task">
                <option value="">Select Task</option>
            </select>
          </div>
          <div class="form-group">
            <label for="message-text" class="col-form-label">Message:</label>
            <textarea class="form-control" id="message-text"></textarea>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Send message</button>
      </div>
    </div>
  </div>
</div>
@include("frontend.job_request_poup")
@endsection
