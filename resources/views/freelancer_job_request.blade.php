@extends('frontend.layouts.user_dashboard_layout')

@section('title', 'Justzoom')
@section('body')

<div class="banner_search">
				<h1><?php echo $pageTitle; ?></h1>				
			</div>	
			<div class="search_main">
				<div class="container">
					<div class="row">
					@include("frontend.layouts.user_dashboard_sidebar")
						 <div class="col-12 col-md-8 col-lg-8 col-sm-7">
						 <div class="alert alert-danger" style="display:none"></div>
			    		<div class="alert alert-success" style="display:none"></div>
							<div class="hire_companies work_history current_task">
								<h2><span><i class="flaticon-task-complete black"></i></span>Pending Request</h2>
								<ul>
								<?php
									if(isset($jobs) && count($jobs) > 0){
										foreach($jobs as $job){
											$countAcceptJob = countAcceptJob($job->job_req_id,$job->job_id);

									?>
									<li>
										<div class="posted">
											<h3><b><a href="{{asset('/user-dashboard/user-job-detail/'.$job->job_id)}}">{{ucfirst($job->title)}}</a></b>  
											{{$job->address}},
											{{$job->city}}, {{$job->state}}, {{$job->country}}
											</h3>
											<time class="calen"><img src="{!! asset('assets/frontend/img/cal.svg') !!}" alt="">{{date('d/m/y', strtotime($job->job_start_on))}} - {{date('d/m/y', strtotime($job->job_end_on))}}
											</time>
										</div>
										<div class="price">Job Type : <span>@if($job->job_type == '0') Open @else Private @endif</span> </div>
										<div class="price">Job Status : 
										<span>
										<?php
										if($countAcceptJob> 0){
											echo "Closed";
										}else if($job->status == 0){
											echo "Pending";
										}else if($job->status == 1){
											echo "Ongoing";	
										}
										?>
										</span> </div>
										<div class="price">Client name : <span>{{ucfirst($job->first_name).' '.ucfirst($job->last_name)}}</span> </div>
										<div class="price">Job Price : <span>{{Config::get('app.site_currency_symbol')}}  {{$job->price}}</span> </div>
										<?php
										if($job->price_type == 1){
											?>
											<div class="price">Offer Price : <span> {{Config::get('app.site_currency_symbol')}} {{$job->offer_price}}</span> </div>
										<?php
										}
										?>
										<div class="price">Offer Given By : <span>@if($job->offer_by == 'user') {{'You'}} @else {{'Client'}} @endif</span> </div>
										<div class="job-status">Offer Status : <span id="jobstatus_{{$job->job_req_id}}">
										<?php
										if($job->job_status == 0){
											echo 'Pending';
											
										}else if($job->job_status == 1){
											echo ($job->offer_by == 'user')?'Accepted by client':'Accepted by You';
										
										}else{
											echo ($job->offer_by == 'user')?'Declined by client':'Declined by You';
										
										}
										?></span> 
										</div>
										<div class="status-report" id="pending-button_{{$job->job_req_id}}">
											<?php
											if($job->job_status == 0 && $job->offer_by != 'user' && $countAcceptJob<1){
												?>
											<button type="button" class="btn btn-success" onclick="change_status('1',{{$job->job_req_id}})">Accept</button>
											<button type="button" class="btn btn-danger" onclick="change_status('2',{{$job->job_req_id}})">Declined</button>
											<?php	
											}else if($job->job_status == '2' && $job->price_type == '1' && $countAcceptJob<1){
											?>
											<button type="button" class="btn btn-warning" onclick="makeanoffer('<?php echo  $job->job_id; ?>','<?php echo session::get('role'); ?>',event)">Make an offer</button>	

											<?php	
											}
											?>	
										</div>
										<p>
										{{$job->description}}
										</p>
									</li>
									<?php
										}
									}else{?>
										<li>No record found.</li>
										<?php
	
										}
									?>
								</ul>
							</div>
							<div class="paginating pagination-section">
								<ul class="pagination">
									{{ $jobs->links() }}	
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>

	@include("frontend.layouts.make_an_offer")

@endsection
