@extends('frontend.layouts.layout')

@section('title', 'Justzoom')
@section('body')
<div class="banner_search">
				<h1>Job Details</h1>
				<ul class="breadcrumb">
					<li class="breadcrumb-item"><a href="{{route('search-job')}}">Open Jobs</a></li>
					<li class="breadcrumb-item active">Job Detail</li>
					
				</ul>
			</div>	
      <div class="search_main">
				<div class="container">
					<div class="job-details">
						<div class="row">
							<div class="col-md-6 col-sm-12">
								<table class="table">
									<tr>
										<th class="job_title">Job Title</th>
										<th class="seperater">:</th>
										<td>{{$job_detail->title}}</td>
                  </tr>
                  <tr>
										<th class="job_title">Price</th>
										<th class="seperater">:</th>
										<td>${{$job_detail->price}}</td>
                  </tr>
                  <tr>
										<th class="job_title">Working Hour</th>
										<th class="seperater">:</th>
										<td>{{$job_detail->working_hour}}</td>
                  </tr>
                  <tr>
										<th class="job_title">Job start on</th>
										<th class="seperater">:</th>
										<td>{{date('d M Y', strtotime($job_detail->job_start_on))}}</td>
									</tr>
									<tr>
										<th class="job_title">Country</th>
										<th class="seperater">:</th>
										<td>{{$job_detail->country}}</td>
                  </tr>
									<tr>
										<th class="job_title">City</th>
										<th class="seperater">:</th>
										<td>{{$job_detail->city}}</td>
                  </tr>
                  <tr>
										<th class="job_title">Address</th>
										<th class="seperater">:</th>
										<td>{{$job_detail->address}}</td>
									</tr> 
									
								</table>
							</div>
							<div class="col-md-6 col-sm-12">
								<table class="table">
                <tr>
										<th class="job_title">Category</th>
										<th class="seperater">:</th>
										<td>{{$job_detail->category_name}}</td>
                  </tr>
                  <tr>
										<th class="job_title">Job Type</th>
										<th class="seperater">:</th>
                    <td><?php
                    if($job_detail->job_type == 0){
                      echo "Open Job";
                    }else{
                      echo "Private Job";
                    }
                    ?></td>
                  </tr>
                  <tr>
										<th class="job_title">Client Name</th>
										<th class="seperater">:</th>
										<td>{{$job_detail->first_name.' '.$job_detail->last_name}}</td>
                  </tr>
									<tr>
										<th class="job_title">Job end on</th>
										<th class="seperater">:</th>
										<td>{{date('d M Y', strtotime($job_detail->job_end_on))}}</td>
                  </tr>
                  <tr>
										<th class="job_title">State</th>
										<th class="seperater">:</th>
										<td>{{$job_detail->state}}</td>
                  </tr>
                  <tr>
										<th class="job_title">Pincode</th>
										<th class="seperater">:</th>
										<td>{{$job_detail->pincode}}</td>
									</tr> 
                  <tr>
										<th class="job_title">Created At</th>
										<th class="seperater">:</th>
										<td>{{date('d M Y', strtotime($job_detail->created_at))}}</td>
									</tr> 
								
								</table>
							</div>
							<div class="col-12 col-md-12 col-lg-12 col-sm-12">
								<div class="details">
									<p><b>Discription</b></p>
									<p>{{$job_detail->description}}</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
@endsection
