@extends('frontend.layouts.layout')

@section('title', 'Justzoom')
@section('body')

<div class="banner_search">
	<h1>Search Result</h1>
	<ul class="breadcrumb">
		<li class="breadcrumb-item"><a href="{{ url('/user-dashboard') }}">Dashboard</a></li>
		<li class="breadcrumb-item active">Job Search</li>
		
	</ul>
</div>	
<div class="search_main">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-4 col-lg-4 col-sm-5">
				
				<div class="filters">
					<h2>Categories</h2>
					<!--<form class="form-inline">
						<input class="form-control" type="search" placeholder="Search Categories" aria-label="Search">
						<a class="submit_btn"><img src="{!! asset('assets/frontend/img/search.png') !!}" width="18px"></a>
					</form>-->
					<?php 
						if(!empty($category_list)){
					?>
					<form class="form-inline" id="searchjob" onsubmit="searchjob(this, event)">
						<input type="hidden" name="action" value="search_job">
						<input type="hidden" name="page" value="1" id="page">

						{!!csrf_field()!!}
					<ul>
						<?php 
							foreach($category_list as $key=>$val){
							?>
							<li>
								<div class="filt_name">
									<input type="checkbox" id="cat_{{$val}}" name="category[]" 
									value="{{$key}}" class="searchjob">
									<label for="cat_{{$val}}">{{$val}}</label>
								</div>
							</li>
							<?php	
							}
						?>
					</ul>
					<?php	
						}
					?>
				</div>
				<!--<div class="filters">
					</div>-->
				<div class="filters">
					<h2>Price</h2>								
					<ul>
						<li>
							<div class="filt_name">
								<input type="checkbox" id="ten_below" name="price[]" value="0-10" class="searchjob">
								<label for="ten_below">{{Config::get('app.site_currency_symbol')}} 10 and Below</label>
							</div>
						</li>
						<li>										
							<div class="filt_name">
								<input type="checkbox" id="10_30" name="price[]" value="10-30" class="searchjob" >
								<label for="10_30">{{Config::get('app.site_currency_symbol')}} 10-30</label>
							</div>
						</li>
						<li>										
							<div class="filt_name">
								<input type="checkbox" id="30_60" name="price[]" value="30-60" class="searchjob">
								<label for="30_60">{{Config::get('app.site_currency_symbol')}} 30-60</label>
							</div>
						</li>
						<li>										
							<div class="filt_name">
								<input type="checkbox" id="60_90" name="price[]" value="60-90" class="searchjob">
								<label for="60_90">{{Config::get('app.site_currency_symbol')}} 60-90</label>
							</div>
						</li>
						<li>										
							<div class="filt_name">
							<input type="checkbox" id="90_above" name="price[]" value="90-900000" class="searchjob">

								<label for="90_above">{{Config::get('app.site_currency_symbol')}} 90 & Above</label>
							</div>
						</li>
					</ul>
				</div>
			
				</form>											
			</div>
			<div class="col-12 col-md-8 col-lg-8 col-sm-7">
				<div class="search-job-data">
				@include('ajax_job_view')
				</div>	
			</div>
		</div>
	</div>
</div>
@include("frontend.layouts.make_an_offer")
@endsection

<style type="text/css">
	.page-item.active .page-link {
		color: #fff !important;
	}

</style>

 


