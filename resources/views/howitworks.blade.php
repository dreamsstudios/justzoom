@extends('frontend.layouts.layout')







@section('title', 'Justzoom')



@section('body')







 <div class="business-banner">



            <div class="container-fluid">



                <div class="row">



                    <div class="col-md-7">



                        <h2>Get the most from Justzoom<br> and live your work dream</h2>



                        <p>Justzoom connects clients to expert freelancers who are available to hire by the hour or project.</p>



                    </div>



                    <div class="col-md-5">



                        <div class="video-img">



                            <img src="assets/frontend/img/video.png">



                        </div>



                    </div>



                </div>



            </div>



        </div>



    </div>



    <div class="how-it-work-tabs">



        <div class="container-fluid">



            <ul class="nav nav-tabs" id="myTab" role="tablist">



          <li class="nav-item">



            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Client</a>



          </li>



          <li class="nav-item">



            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Freelancer</a>



          </li>



        </ul>



        </div>



        <div class="client-tabs">



            <div class="container-fluid">



                <div class="tab-content" id="myTabContent">



                  <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">



                    <div class="row">



                        <div class="col-md-6">



                            <h2>Join Justzoom</h2>



                            <p>It's easy to get started!</p>



                            <p>Simply join for free, post your job, and start receiving Quotes. Use the Find Freelancers tool to get Quotes from quality Freelancers.</p>



                            <ul>



                                <li><span><i class="fas fa-check"></i></span><h3>Join our community - it's free!</h3></li>



                                <li><span><i class="fas fa-check"></i></span><h3>Post your job</h3></li>



                                <li><span><i class="fas fa-check"></i></span><h3>Search for Freelancers</h3></li>



                            </ul>



                        </div>



                        <div class="col-md-6">



                            <div class="join-img">



                                <img src="assets/frontend/img/join1.png">



                            </div>



                        </div>



                    </div>



                    <div class="row">



                        <div class="col-md-6">



                            <div class="phone-img">



                                <img src="assets/frontend/img/motion7-8.gif">

                                

                            </div>



                        </div>



                        <div class="col-md-6 phone-text">



                            <h2>Hire in minutes.</h2>



                            <p>Review, compare and select the best Freelancers for your job.</p>



                            <ul>



                                <li><span><i class="fas fa-check"></i></span><h3>Evaluate Quotes</h3></li>



                                <li><span><i class="fas fa-check"></i></span><h3>Finalize the Agreement</h3></li>



                                <li><span><i class="fas fa-check"></i></span><h3>Hire a Freelancer in one click</h3></li>



                            </ul>



                        </div>



                    </div>



                    <div class="row">



                        <div class="col-md-6 ">



                            <h2>Track your work in real time</h2>



                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br> Nulla aliquam eros et imperdiet luctus.</p>



                            <ul>



                                <li><span><i class="fas fa-check"></i></span><h3>Boost productivity by 30%</h3></li>



                                <li><span><i class="fas fa-check"></i></span><h3>Stay organized</h3></li>



                                <li><span><i class="fas fa-check"></i></span><h3>Manage team members</h3></li>



                            </ul>



                        </div>



                        <div class="col-md-6">



                            <div class="lock-img">

                                <img src="assets/frontend/img/tab.png">

                            </div>



                        </div>



                    </div>



                    <div class="row">



                        <div class="col-md-6 phone-text">



                            <div class="phone-img">



                                <img src="assets/frontend/img/join4.png">



                            </div>



                        </div>



                        <div class="col-md-6">



                            <h2>Pay securely with confidence.</h2>



                            <p>Pay a Freelancer for a job well done through our secure payment system.</p>



                            <ul>



                                <li><span><i class="fas fa-check"></i></span><h3>Use SafePay</h3></li>



                                <li><span><i class="fas fa-check"></i></span><h3>Pay with ease using automatic payments</h3></li>



                                <li><span><i class="fas fa-check"></i></span><h3>Choose your preferred payment method</h3></li>



                            </ul>



                        </div>



                    </div>



                  </div>



                  <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">



                    <div class="row">



                        <div class="col-md-6">



                            <h2>Join Justzoom</h2>



                            <p>It's easy to get started!</p>



                            <p>Simply join for free, post your job, and start receiving Quotes. Use the Find Freelancers tool to get Quotes from quality Freelancers.</p>



                            <ul>



                                <li><span><i class="fas fa-check"></i></span><h3>Join our community - it's free!</h3></li>



                                <li><span><i class="fas fa-check"></i></span><h3>Post your job</h3></li>



                                <li><span><i class="fas fa-check"></i></span><h3>Search for Freelancers</h3></li>



                            </ul>



                        </div>



                        <div class="col-md-6">



                            <div class="join-img">



                                <img src="assets/frontend/img/join1.png">



                            </div>



                        </div>



                    </div>



                    <div class="row">



                        <div class="col-md-6">



                            <div class="phone-img">



                                <img src="assets/frontend/img/join2.png">



                            </div>



                        </div>



                        <div class="col-md-6 phone-text">



                            <h2>Hire in minutes.</h2>



                            <p>Review, compare and select the best Freelancers for your job.</p>



                            <ul>



                                <li><span><i class="fas fa-check"></i></span><h3>Evaluate Quotes</h3></li>



                                <li><span><i class="fas fa-check"></i></span><h3>Finalize the Agreement</h3></li>



                                <li><span><i class="fas fa-check"></i></span><h3>Hire a Freelancer in one click</h3></li>



                            </ul>



                        </div>



                    </div>



                    <div class="row">



                        <div class="col-md-6 ">



                            <h2>Track your work in real time</h2>



                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br> Nulla aliquam eros et imperdiet luctus.</p>



                            <ul>



                                <li><span><i class="fas fa-check"></i></span><h3>Boost productivity by 30%</h3></li>



                                <li><span><i class="fas fa-check"></i></span><h3>Stay organized</h3></li>



                                <li><span><i class="fas fa-check"></i></span><h3>Manage team members</h3></li>



                            </ul>



                        </div>



                        <div class="col-md-6">



                            <div class="lock-img">



                                <img src="assets/frontend/img/join3.png">



                            </div>



                        </div>



                    </div>



                    <div class="row">



                        <div class="col-md-6 phone-text">



                            <div class="phone-img">



                                <img src="assets/frontend/img/join4.png">



                            </div>



                        </div>



                        <div class="col-md-6">



                            <h2>Pay securely with confidence.</h2>



                            <p>Pay a Freelancer for a job well done through our secure payment system.</p>



                            <ul>



                                <li><span><i class="fas fa-check"></i></span><h3>Use SafePay</h3></li>



                                <li><span><i class="fas fa-check"></i></span><h3>Pay with ease using automatic payments</h3></li>



                                <li><span><i class="fas fa-check"></i></span><h3>Choose your preferred payment method</h3></li>



                            </ul>



                        </div>



                    </div>



                  </div>



                  <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">



                    <div class="row">



                        <div class="col-md-6">



                            <h2>Who we are</h2>



                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi sodales fringilla dolor, eget convallis tellus iaculis vel. Aliquam eros libero, varius quis molestie et, aliquam at ipsum. Nulla volutpat sapien vitae nunc tempor luctus. Aenean nunc risus, sodales quis odio id, condimentum lobortis tellus. Aenean sollicitudin at lectus eu viverra. Phasellus ut posuere quam, scelerisque faucibus quam. Nunc hendrerit ultricies mauris.</p>



                        </div>



                        <div class="col-md-6">



                            <div class="tabs-img">



                                <img src="assets/frontend/img/about-girl.png">



                            </div>



                        </div>



                    </div>



                  </div>



                </div>



            </div>



        </div>  



    </div>



    <div class="find-expert">



        <div class="container">



            <div class="row">



                <div class="col-md-12">



                    <h2>Find an expert for anything</h2>



                    <p>Work with curated freelance talent from all over the world. Manage the entire project with Justzoom.<br>Pay securely with confidence.</p>



                   <a href="#sign-up"> <button type="button" class="btn btn-link page-scroll">Join for Free</button></a>



                    <h4>No upfront payments, no hidden fees.</h4>



                </div>



            </div>



        </div>



    </div>



    <div class="testmonial-bg">



        <div class="container">



            <div class="row">



                <div class="col-md-12">



                    <div class="testmonial-title">



                        <h2>Hear From Our Clients</h2>



                        <ul>



                            <li><h3>Our customers say </h3></li>



                            <li><h4>Excellent    <img src="assets/frontend/img/star.png"></h4></li>



                            <li><h4>4.7 out of 5 based on 1,123 reviews<img src="assets/frontend/img/trustpilot.png"></h4></li>



                        </ul>



                    </div>



                </div>



                <div class="col-md-12">



                    <div class="testmonial">



                        <div class="test-box">



                            <span><i class="fas fa-quote-left"></i></span>



                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In molestie neque et tortor pretium, a facilisis justo tempus. Maecenas facilisis maximus vehicula. Proin sem lacus, eleifend sed orci at, fermentum aliquet dolor. Duis ac erat ac augue ullamcorper aliquet nec quis quam. Donec interdum iaculis erat sit amet commodo. Donec eu porttitor magna, id aliquet ligula. </p>



                            <div class="mc-nallay">



                                <img src="assets/frontend/img/aval.png">



                                <div class="testmonial-text">



                                    <h3>Ryan Walker, Director of Product</h3>



                                    <h4>Rand McNally</h4>



                                </div>



                            </div>



                        </div>



                        <div class="test-box">



                            <span><i class="fas fa-quote-left"></i></span>



                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In molestie neque et tortor pretium, a facilisis justo tempus. Maecenas facilisis maximus vehicula. Proin sem lacus, eleifend sed orci at, fermentum aliquet dolor. Duis ac erat ac augue ullamcorper aliquet nec quis quam. Donec interdum iaculis erat sit amet commodo. Donec eu porttitor magna, id aliquet ligula. </p>



                            <div class="mc-nallay">



                                <img src="assets/frontend/img/aval.png">



                                <div class="testmonial-text">



                                    <h3>Ryan Walker, Director of Product</h3>



                                    <h4>Rand McNally</h4>



                                </div>



                            </div>



                        </div>



                        <div class="test-box">



                            <span><i class="fas fa-quote-left"></i></span>



                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In molestie neque et tortor pretium, a facilisis justo tempus. Maecenas facilisis maximus vehicula. Proin sem lacus, eleifend sed orci at, fermentum aliquet dolor. Duis ac erat ac augue ullamcorper aliquet nec quis quam. Donec interdum iaculis erat sit amet commodo. Donec eu porttitor magna, id aliquet ligula. </p>



                            <div class="mc-nallay">



                                <img src="assets/frontend/img/aval.png">



                                <div class="testmonial-text">



                                    <h3>Ryan Walker, Director of Product</h3>



                                    <h4>Rand McNally</h4>



                                </div>



                            </div>



                        </div>



                        <div class="test-box">



                            <span><i class="fas fa-quote-left"></i></span>



                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In molestie neque et tortor pretium, a facilisis justo tempus. Maecenas facilisis maximus vehicula. Proin sem lacus, eleifend sed orci at, fermentum aliquet dolor. Duis ac erat ac augue ullamcorper aliquet nec quis quam. Donec interdum iaculis erat sit amet commodo. Donec eu porttitor magna, id aliquet ligula. </p>



                            <div class="mc-nallay">



                                <img src="assets/frontend/img/aval.png">



                                <div class="testmonial-text">



                                    <h3>Ryan Walker, Director of Product</h3>



                                    <h4>Rand McNally</h4>



                                </div>



                            </div>



                        </div>



                        <div class="test-box">



                            <span><i class="fas fa-quote-left"></i></span>



                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In molestie neque et tortor pretium, a facilisis justo tempus. Maecenas facilisis maximus vehicula. Proin sem lacus, eleifend sed orci at, fermentum aliquet dolor. Duis ac erat ac augue ullamcorper aliquet nec quis quam. Donec interdum iaculis erat sit amet commodo. Donec eu porttitor magna, id aliquet ligula. </p>



                            <div class="mc-nallay">



                                <img src="assets/frontend/img/aval.png">



                                <div class="testmonial-text">



                                    <h3>Ryan Walker, Director of Product</h3>



                                    <h4>Rand McNally</h4>



                                </div>



                            </div>



                        </div>



                        <div class="test-box">



                            <span><i class="fas fa-quote-left"></i></span>



                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In molestie neque et tortor pretium, a facilisis justo tempus. Maecenas facilisis maximus vehicula. Proin sem lacus, eleifend sed orci at, fermentum aliquet dolor. Duis ac erat ac augue ullamcorper aliquet nec quis quam. Donec interdum iaculis erat sit amet commodo. Donec eu porttitor magna, id aliquet ligula. </p>



                            <div class="mc-nallay">



                                <img src="assets/frontend/img/aval.png">



                                <div class="testmonial-text">



                                    <h3>Ryan Walker, Director of Product</h3>



                                    <h4>Rand McNally</h4>



                                </div>



                            </div>



                        </div>



                    </div>



                </div>



            </div>



        </div>



    </div>



     @include("frontend.layouts.common_register_user")



    <div class="Justzoom-Developers">



        <div class="container">



            <div class="row">



                <div class="col-md-12">



                    <h2 class="just-title">Justzoom Developers</h2>



                </div>



               



                  <div class="col-md-3">



                    <ul>



                        @foreach($category_list as $category)



                        <li><h3>{{$category}}</h3></li>



                        @endforeach



                    </ul>



                </div>



            </div>



        </div>



    </div>



@endsection



