@extends('frontend.layouts.layout')

@section('title', 'Justzoom')
@section('body')


 <div class="business-banner">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-7">
                        <h2>Why Justzoom?</h2>
                        <p>Many companies — both large and small — face challenges with finding top talent, from candidate qualifications, to team dynamics, to economics that fit their financial scale. Our unique solution for hiring elite independent contractors addresses all of these concerns.</p>
                    </div>
                    <div class="col-md-5">
                        <div class="video-img">
                            <img src="assets/frontend/img/why.png">
                        </div>
                    </div>
                </div>
            </div>
        </div>
       
    </div>
    <div class="client-tabs">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <h2>It's a system built for you</h2>
                    <p>We make it easy for you to work online, whether you run your own business or work for an agency, small business, or enterprise company.</p>
                </div>
                <div class="col-md-6">
                    <div class="phone-img">
                        <img src="assets/frontend/img/why-icon1.png">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="phone-img">
                        <img src="assets/frontend/img/why-icon2.png">
                    </div>
                </div>
                <div class="col-md-6 phone-text">
                    <h2>You won't find better value <br>anywhere else</h2>
                    <p>We believe in maximizing value and minimizing costs for all of our members so that you have a rewarding experience using the website. Our cost-effective platform provides all the tools and features you need to get work done successfully, while charging you the lowest fees in the freelance marketplace industry.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 ">
                    <h2>Your funds are secure with us</h2>
                    <p>With SafePay payment protection, Freelancers feel confident that they will get paid for their work and Employers feel secure that they can review the work before paying an invoice. We also provide multiple payment and withdrawal options for secure transactions.</p>
                </div>
                <div class="col-md-6">
                    <div class="phone-img">
                        <img src="assets/frontend/img/why-icon3.png">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 phone-text">
                    <div class="phone-img">
                        <img src="assets/frontend/img/why-icon4.png">
                    </div>
                </div>
                <div class="col-md-6">
                    <h2>We are available 24/7</h2>
                    <p>Our dedicated Support team is here to help you navigate our tools and get the most out of the website. You can count on them to work with you in a timely manner to resolve any issues that might arise, no matter where you are located.</p>
                </div>
            </div>
        </div>
    </div>  
    <div class="how-it-work-tabs">
        <div class="container-fluid">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Client</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Freelancer</a>
          </li>
        </ul>
        </div>
        <div class="why-tabs">
            <div class="tab-content" id="myTabContent">
              <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                <div class="row">
                    <div class="col-md-6">
                        <div class="Clients-post">
                            <h2>Clients</h2>
                            <ul>
                                <li>
                                    <span><i class="far fa-check-circle"></i></span>
                                    <div class="Clients-post-text">
                                        <h3>Browse 2 Million Experts</h3>
                                        <p>Identify credible Freelancers with All-Time Transaction Data</p>
                                    </div>
                                </li>
                                <li>
                                    <span><i class="far fa-check-circle"></i></span>
                                    <div class="Clients-post-text">
                                        <h3>Post a Job for Free</h3>
                                        <p>Boost your reach and hire top talent with Featured jobs on <br>Justzoom.</p>
                                    </div>
                                </li>
                                <li>
                                    <span><i class="far fa-check-circle"></i></span>
                                    <div class="Clients-post-text">
                                        <h3>Work in a Flexible Environment</h3>
                                        <p>Use Justzoom to manage multiple Freelancers working on the <br>same job.</p>
                                    </div>
                                </li>
                                <li>
                                    <span><i class="far fa-check-circle"></i></span>
                                    <div class="Clients-post-text">
                                        <h3>Protect Payments with SafePay</h3>
                                        <p>Release payment for an invoice only after reviewing work.</p>
                                    </div>
                                </li>
                                <li>
                                    <span><i class="far fa-check-circle"></i></span>
                                    <div class="Clients-post-text">
                                        <h3>Pay Lowest Fee</h3>
                                        <p>Pay an invoice using eCheck or wire transfer and receive 100% <br>cashback on the 2.9% handling fee.</p>
                                    </div>
                                </li>
                            </ul>
                            <!-- <button type="submit" class="btn btn-primary join-btt">Join Zustzoom</button> -->
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="client-img-tabe">
                            <img src="assets/frontend/img/client-img.png">
                        </div>
                    </div>
                </div>
              </div>
              <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                <div class="row">
                    <div class="col-md-6">
                        <div class="Clients-post">
                            <h2>Clients</h2>
                            <ul>
                                <li>
                                    <span><i class="far fa-check-circle"></i></span>
                                    <div class="Clients-post-text">
                                        <h3>Browse 2 Million Experts</h3>
                                        <p>Identify credible Freelancers with All-Time Transaction Data</p>
                                    </div>
                                </li>
                                <li>
                                    <span><i class="far fa-check-circle"></i></span>
                                    <div class="Clients-post-text">
                                        <h3>Post a Job for Free</h3>
                                        <p>Boost your reach and hire top talent with Featured jobs on <br>Justzoom.</p>
                                    </div>
                                </li>
                                <li>
                                    <span><i class="far fa-check-circle"></i></span>
                                    <div class="Clients-post-text">
                                        <h3>Work in a Flexible Environment</h3>
                                        <p>Use Justzoom to manage multiple Freelancers working on the <br>same job.</p>
                                    </div>
                                </li>
                                <li>
                                    <span><i class="far fa-check-circle"></i></span>
                                    <div class="Clients-post-text">
                                        <h3>Protect Payments with SafePay</h3>
                                        <p>Release payment for an invoice only after reviewing work.</p>
                                    </div>
                                </li>
                                <li>
                                    <span><i class="far fa-check-circle"></i></span>
                                    <div class="Clients-post-text">
                                        <h3>Pay Lowest Fee</h3>
                                        <p>Pay an invoice using eCheck or wire transfer and receive 100% <br>cashback on the 2.9% handling fee.</p>
                                    </div>
                                </li>
                            </ul>
                            <!-- <button type="submit" class="btn btn-primary join-btt">Join Zustzoom</button> -->
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="client-img-tabe">
                            <img src="assets/frontend/img/client-img.png">
                        </div>
                    </div>
                </div>
              </div>
            </div>
        </div>  
    </div>

@endsection
