@extends('frontend.layouts.user_dashboard_layout')

@section('title', 'Justzoom')
@section('body')

	
			<div class="client-page">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="deshbord-title">
						<h2>Dashboard <span>Welcome back, {{ucfirst($user->first_name.' '.$user->last_name)}}</span></h2>
						<h4>Date: <?php echo date('d-m-y'); ?></h4>
					</div>
					<ul class="deshbord-list">
						<li>
							<img src="{!! asset('assets/frontend/img/desh-red.png') !!}">
							<div class="desh-text">
								<h4>Total Income</h4>
								<h5>£<?php echo $totalIncome;?></h5>
							</div>
						</li>
						<li>
							<img src="{!! asset('assets/frontend/img/desh2.png') !!}">
							<div class="desh-text">
								<h4>Ongoing Contract</h4>
								<h5><?php echo $onGoing;?></h5>
							</div>
						</li>
						<li>
							<img src="{!! asset('assets/frontend/img/desh1.png') !!}">
							<div class="desh-text">
								<h4>In Escrow</h4>
								<h5>£0</h5>
							</div>
						</li>
						<li>
							<img src="{!! asset('assets/frontend/img/desh4.png') !!}">
							<div class="desh-text">
								<h4>Client Invitation</h4>
								<h5><?=count($requestData); ?></h5>
							</div>
						</li>
					</ul>
				</div>
			</div>
			<div class="row">
				<?php 
				if(isset($contractData) && !empty($contractData))
				{
				
					if(!empty($contractData->image)){
							$img = $contractData->image;
						} else {	
							$img = 'no-img.png';
						}
				
				?>
				<div class="col-md-12">
					<h2 class="opeing-title">Ongoing Contract </h2>
					<div class="profile-list">
						<div class="profile-list-left">
							<div class="chat-user-img">
								<img src="{{asset('/uploads/user_images/'.$img)}}" alt="user image" >
								<div class="brand-logo">
									<img src="{!! asset('assets/frontend/img/chat-logo.png') !!}" alt="">
								</div>
							</div>
							@if($contractData->is_clients_verified == 1)
							<div class="profile-statics">
								<h6><i class="fa fa-check"></i> Verified</h6>
							</div>
							@endif
						</div>
						<div class="profile-list-right">							
							<div class="chat-user-statics profile-info">
								<ul>
									<li class="profile-name">
										<h2>{{$contractData->first_name .' '.$contractData->last_name}}</h2>
										<h4>{{$contractData->job_title}}</h4>
									</li>
									<li>
									<?php
										if($contractData->chatId>0)
										$unreadMsg = unreadMsgCount($contractData->chatId);
										else
										$unreadMsg = 0;
									?>
										<div class="inbox">

											<img src="{!! asset('assets/frontend/img/envelope.png') !!}" alt="">
											@if($unreadMsg>0)<span class="msg-count">{{$unreadMsg}}</span>@endif
										</div>
										<p>Unread</p>
									</li>
									<li>
									<div class="raiting">
									<?php
									$totalrating = getTotalClientrating($contractData->clientId);
								   ?>
											
											<?php 
											$remain = 5-$totalrating->average_rating;
											for($i=1; $i<=$totalrating->average_rating;$i++){
												echo '<span class="fa fa-star checked"></span>';
											}
											if($remain>0){
												for($i=1; $i<=$remain;$i++){	
													echo '<span class="fa fa-star"></span>';
												}

											}
										?>
									
									<p><span>{{$totalrating->average_rating}}</span> ({{$totalrating->totalreview}})</p>
								</div>
								</li>
									
									<li>
										<h3>#<?= $contractData->contractId; ?>
										<p>Contract ID</p>
									</li>
									<li>
										<h3>£<?= $contractData->freelancerPrice; ?></h3>
										<p>In Escrow</p>
									</li>
									<li>
										<button type="button" class="btn btn-warning Progress-yulo">In Progress</button>
										<p>Work Status</p>
									</li>
								</ul>
							</div>
							<div class="profile-review-section">
								<div class="review-area">
								<?php 
								$chatMessages = lastMessage($contractData->chatId);
								if(!empty($chatMessages)){
									
								?>
									<div class="review-duration">
										<h5>@if($chatMessages->first_name){{ucfirst($chatMessages->first_name)}}@else Me @endif</h5>
										<h5></h5>
									</div>
									<hr>
									<p>{{$chatMessages->message}}</p>
									<div class="reply-btn-area">
									<a href="{{asset('/user-dashboard/chat/'.$contractData->chatId)}}">
									<button type="button" class="btn reply-btn">Reply</button>
									</a>
									</div>
									<?php
								}
								?>
								</div>
								<div class="btn-area">
									<div class="profile-btn-group">
										<a href="{{asset('/user-dashboard/contract-detail/'.$contractData->contractId)}}" class="view-profile-btn">View Contract</a>
										<a href="{{asset('/user-dashboard/chat/'.$contractData->chatId)}}" class="contact-btn">Chat</a>
										<!--<a href="{{asset('/client-detail/'.$contractData->id)}}" class="quote-btn">View Profile</a>-->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php
				
				}?>
			</div>	
			<div class="row">
				<div class="col-md-12">
					<h2 class="opeing-title">Invitation (<?= count($requestData);?>)</h2>
				</div>
				<?php
				if(isset($requestData) && !empty($requestData)){
					foreach($requestData as $req){
						if(!empty($req->image)){
							$img = $req->image;
						} else {	
							$img = 'no-img.png';
						}
						?>
					<div class="col-md-6">
					<div class="hire-web james-web">
						<span class="jasim-bg"><img src="{{asset('/uploads/user_images/'.$img)}}"></span>
						<h6>{{ucfirst($req->first_name.' '.$req->last_name)}}</h6>
						<h3>{{ucfirst($req->job_title)}}</h3>
						<div class="rating-star">
						<div class="raiting">
									<?php
									$totalrating = getTotalClientrating($req->clientId);
								   ?>
											
											<?php 
											$remain = 5-$totalrating->average_rating;
											for($i=1; $i<=$totalrating->average_rating;$i++){
												echo '<span class="fa fa-star checked"></span>';
											}
											if($remain>0){
												for($i=1; $i<=$remain;$i++){	
													echo '<span class="fa fa-star"></span>';
												}

											}
										?>
									
									<p><span>{{$totalrating->average_rating}}</span> ({{$totalrating->totalreview}})</p>
								</div></div>
						<div class="wirter-hire">
							<p>{{$req->message}}</p>
						</div>
						<div class="hire-btt Cancel-btt">
						   @if($req->requestStatus ==0)
							<button type="button" class="btn btn-link bcontact-button no-bg" id="canc_<?php echo $req->reqId; ?>" onclick="handleinvite('2','<?php echo $req->reqId; ?>')">Cancel</button></a>
							<button type="button" class="btn btn-link bcontact-button Hire-blu" id="suc_<?php echo $req->reqId; ?>" onclick="handleinvite('1','<?php echo $req->reqId; ?>')">Accept</button></a>
							@endif
							@if($req->requestStatus ==1)
							<a href="{{asset('/user-dashboard/chat/'.$req->chatId)}}"><button type="button" class="btn btn-link bcontact-button Hire-blu">Chat</button></a>
							@endif
							
						</div>
					</div>
				</div>
					<?php
					}
				
				}
				?>
				
			
		</div>
		<?php
							if(isset($requestData) && !empty($requestData)){
								?>
							<div class="paginating">
								<ul class="pagination searchpagination">
									{{ $requestData->links() }}	
								</ul>
							</div>
							<?php
							}?>
		<div class="row">
				<div class="col-md-12">
					<div class="post-table">
						<h2 class="post-title">Past Work</h2>
						<div class="table-responsive-sm">
							<table class="table table-bordered">
							  <thead>
								<tr style="background-color: #f9f9f9;">
								  <th scope="col" style="text-align: left;"><h4>Client</h4></th>
								  <th scope="col"><h4>Category</h4></th>
								  <th scope="col"><h4>Total Hours</h4></th>
								  <th scope="col"><h4>Paid Amount</h4></th>
								  <th scope="col"><h4>Rating</h4></th>
								  <th scope="col"><h4>Status</h4></th>
								  <th scope="col"><h4>Action</h4></th>
								</tr>
							  </thead>
							  <tbody>
							  <?php
							  if(isset($pastContract) && !empty($pastContract) && count($pastContract) >0){
								  foreach($pastContract as $past){
									if(!empty($past->image)){
										$img = $past->image;
									} else {	
										$img = 'no-img.png';
									}
									?>
									<tr>
									<th scope="row" style="text-align: left;">
									<div class="table-profile">
									
				<img src="{{asset('/uploads/user_images/'.$img)}}" style="width:50px;" class="rounded-circle">
										<div class="post-text">
											<h3 >{{ucfirst($past->first_name.' '.$past->last_name)}}</h3>
											<p>{{ucfirst($past->job_title)}}</p>
										</div>
									</div>
									</th>
									<td><p>{{$past->category_name}}</p></td>
									<td><p>{{$past->duration}} days</p></td>
									<td><p>£{{$past->freelancerPrice}}</p></td>
									<td>
									  <div class="raiting">
									  <?php
											if(isset($past->rating)){
											?>
											<div class="raiting">
											<?php 
											$remain = 5-$past->rating;
											for($i=1; $i<=$past->rating;$i++){
												echo '<span class="fa fa-star checked"></span>';
											}
											if($remain>0){
												for($i=1; $i<=$remain;$i++){	
													echo '<span class="fa fa-star"></span>';
												}

											}
										}else{
										?>
										<a href="javascript:void(0);"  data-toggle="modal" data-target="#review">write review</a>
										@include("frontend.clientreview")  
                                        <?php
										}
									    ?>
											
										</div></td>
									<td><button type="button" class="btn btn-success table-done">Done</button></td>
									<td>
									<div class="dropdown dot-button">
										<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<img src="{!! asset('assets/frontend/img/dot.png') !!}">
										</button>
										<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
										<a class="dropdown-item" href="{{asset('/user-dashboard/contract-detail/'.$past->contractId)}}" >Contract Detail</a>
										</div>
									</div>
									</td>
									</tr>
                                  <?php
								  }

							  }else{
								  echo "<tr ><td colspan='7'>No record Found</td></tr>";

							  }
							  ?>
							  </tbody>
							</table>
							<?php
							if(isset($pastContract) && !empty($pastContract)){
								?>
							<div class="paginating">
								<ul class="pagination searchpagination">
									{{ $pastContract->links() }}	
								</ul>
							</div>
							<?php
							}?>
						</div>
					</div>
				</div>
			</div>
	</div>
	</div>
	

@endsection
