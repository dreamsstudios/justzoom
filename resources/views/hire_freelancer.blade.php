@extends('frontend.layouts.layout')
@section('title', 'Justzoom')
@section('body')

<div class="hire-adrien-page">
		<div class="container">
			<div class="hire-adrien-section">
				<div class="row">
					<div class="col-md-12">					
						<div class="breadcrumb-nav">
							<div class="title">
							<?php
							//echo "<pre>";
							//print_r($requestData);
							?>
								<h2>Hire {{ucfirst($requestData->first_name)}}</h2>
							</div>
							<div class="sub-heading">
								<ul>
									<li>
										<a href="">Hire</a>
									</li>
									<li>/</li>
									<li>
										<a href="#" class="active">Payment</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="hire-adrien-form">
				@if ($errors->any())
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
					@if(session("msg"))
                                        <div class="alert-dismiss">
                                            <div class="alert alert-success alert-dismissible fade show">
                                                <span>{{session("msg")}}</span><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span class="fa fa-times"></span> </button><br/>
                                            </div>
                                        </div>
                            @endif
							 @if(session("errormsg"))
                                        <div class="alert-dismiss">
                                            <div class="alert alert-danger alert-dismissible fade show">
                                                <span>{{session("errormsg")}}</span><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span class="fa fa-times"></span> </button><br/>
                                            </div>
                                        </div>
                            @endif
					<div class="row">
					 
					
						<div class="col-md-6">
						<form class="needs-validation" novalidate="" id="my-form" method="post" action="{{route('savecontract')}}" enctype = "multipart/form-data">
										{!!csrf_field()!!}
								<div class="form-group">
								    <label for="about-project">What the Project About</label>
								    <select class="form-control" id="about-project" name="aboutProject" required>
										<option @if($requestData->aboutProject == "a new project"){{'selected'}} @endif value="a new project">A new project</option>
										<option   @if($requestData->aboutProject == "change in existing project"){{'selected'}} @endif value="change in existing project">Change in Existing Project</option>
										
								    </select>
									<input type="hidden" name="contractId" value="{{$requestData->contractId}}" />
								</div>
								<div class="form-group">
								    <label for="requirement">What do you need to get done</label>
								    <input type="text" class="form-control italic-input" id="requirement" 
									placeholder="e.g. I need a professional website design" name="requirement" required value="{{$requestData->requirement}}">
								</div>
								<div class="form-group desc-textarea">
								    <label for="description">Description</label>
								    <textarea required class="form-control" id="description"  name="description" >{{$requestData->description}}</textarea>
								   
								</div>
								
								<div class="form-group">
									<label>What level of time commitment will you require from the designer?</label>
									<div class="commitment-radio">
										<div class="custom-control custom-radio ">
									    	<input required type="radio" class="custom-control-input commitment" id="hourly" 
											name="priceType" value="hourly" @if($requestData->priceType == "hourly"){{'checked'}} @endif> 
									    	<label class="custom-control-label" for="hourly">Hourly</label>
									    </div>
									    <div class="custom-control custom-radio">
										    <input required type="radio" class="custom-control-input commitment" id="fixed-price" 
											name="priceType" value="fixed" @if($requestData->priceType == "fixed"){{'checked'}} @endif>
										    <label class="custom-control-label" for="fixed-price">Fixed Price</label>
									    </div>
									</div>

								</div>
								
								<div class="form-group" style="@if($requestData->priceType!='fixed')display:none;@endif" id="priceDiv">
								    <label for="price">Price</label>
									<input type="text" class="form-control italic-input" id="freelancerAmount" 
									placeholder="Enter amount here" value="{{$requestData->freelancerPrice}}" name="freelancerAmount" required onkeyup="fixedPrice(this)">
								    
								</div>
								<div class="form-group">
								    <label for="duration">How long do you need the designer(In days)?</label>

									<input type="number" class="form-control italic-input" id="duration" 
									 required name="duration" required value="{{$requestData->duration}}">
								    
									<!--<select class="form-control" id="duration" required name="duration">
										<option value="2">2 Days</option>
										<option value="3">3 Days</option>
										<option value="4">4 Days</option>
								    </select>-->
								</div>
								<div class="form-group">
									<div class="custom-control custom-checkbox">
										<input type="checkbox" class="custom-control-input" id="terms" name="terms" required >
										<label class="custom-control-label form-check-label" for="terms">I agree to the <a href="#">Terms & Conditions</a></label>
									</div>
									<div class="custom-control custom-checkbox">
										<input type="checkbox" class="custom-control-input" id="nondisclosure" name="nondisclosure" required>
										<label class="custom-control-label form-check-label" for="nondisclosure">Protect your information and ideas with a non-disclosure agreement.</label>
									</div>
								</div>
								<input type="hidden" value="{{$requestData->id}}" name="userId" >
								<input type="hidden" value="{{$requestData->categoryId}}" name="categoryId">
								<input type="hidden" value="{{$requestData->reqId}}" name="reqId">
								<input type="hidden" value="{{$requestData->hourly_rate}}" id="hourPrice">
								<input type="hidden" value="" id="totalPrice" name="totalPrice">
								<input type="hidden" value="{{$setting->paltformCharge}}" id="paltformCharge" name="paltformCharge">
								


								<button type="submit" class="btn btn-continue">Continue</button>
							</form>
						</div>
						<div class="col-md-6">
							<div class="hire-profile-section">
								<div class="hire-profile-card">
									<div class="hire-profile">
										<div class="hire-user-img">
										<?php
										if(!empty($requestData->image)){
											$img = $requestData->image;
										}else{
											$img = 'no-img.png';
											}
                                         ?>
						                <img src="{{asset('/uploads/user_images/'.$img)}}" alt="user image">
											
											<div class="brand-logo">
												<img src="{{asset('assets/img/chat-logo.png')}}" alt="">
											</div>
										</div>
										<div class="hire-user-profile">
											<h2>{{$requestData->first_name.' '.$requestData->last_name}}</h2>
											<h4>{{$requestData->category_name}}  @if($requestData->city)in {{$requestData->city}}@endif</h4>
											<div class="raiting">
											<?php 
											$remain = 5-$totalRating->average_rating;
											for($i=1; $i<=$totalRating->average_rating;$i++){
												echo '<span class="fa fa-star checked"></span>';
											}
											if($remain>0){
												for($i=1; $i<=$remain;$i++){	
													echo '<span class="fa fa-star"></span>';
												}

											}
											
										?>
										<p><span>@if($totalRating->average_rating>0){{$totalRating->average_rating}}@else {{0}} @endif</span> ({{$totalRating->totalreview}})</p>
										</div>
				
											
											@if($requestData->is_user_verified==1)
											<h6><i class="fa fa-check"></i> Verified</h6>
											@endif
										</div>
									</div>
									<div class="hire-locate">
									
										<div class="location">@if(!empty($requestData->country))<img src="{{asset('assets/img/locate-icon.png')}}" alt="location"> 
										{{$requestData->city}} {{','.$requestData->country}}@endif</div>
										<div class="hired-price">£{{$requestData->hourly_rate}}<span>/hr</span></div>
									</div>
									<div class="hire-description">
										<p>{{$requestData->about}}</p>
									</div>
								</div>
								<div class="hired-totl-price">
									<h3>Paltform Charge</h3>
									<h3 id="plateform-price">£{{$setting->paltformCharge}}</h3>
								</div>
								<div class="hired-totl-price">
									<h3>Freelancer Charge</h3>
									<h3 id="freelancer-price">@if($requestData->freelancerPrice>0){{$requestData->freelancerPrice}}@endif</h3>
								</div>
							
								<div class="hired-totl-price">
									<h3>Total</h3>
									<h3 id="total-price">@if($requestData->totalPrice>0){{$requestData->totalPrice}}@endif</h3>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
   <script>
   function fixedPrice(vv) {
  var amount = $(vv).val();
  var platform =  $('#paltformCharge').val();
  $('#freelancer-price').html('£'+amount);
  var withpl = +amount + +platform;
	  $("#total-price").html('£'+withpl);
	  $('#totalPrice').val(withpl);
 
}
   $(document).ready(function() {
    //set initial state.
   /// $('#textbox1').val(this.checked);

    $('.commitment').change(function() {
		$('#freelancerAmount').val(0);
		var platform =  $('#paltformCharge').val();
        if(this.checked) {
			if($(this).val() == 'fixed'){
				$('#priceDiv').css('display','block');
				$('#freelancer-price').html('£ 0');
				$('#total-price').html('£'+platform);
				$('#freelancerAmount').val(0);
			}else{
				var duration = $('#duration').val();
				var hourPrice = $('#hourPrice').val();
				var amount = duration*24*hourPrice;
				
				$('#freelancerAmount').val(amount);
				$('#freelancer-price').html('£'+amount);
				
				var withpl = +amount + +platform;
				$('#total-price').html('£'+withpl);
				$('#totalPrice').val(withpl);
				$('#priceDiv').css('display','none');

			}
		
        }
           
    });
	$('#duration').change(function(){
		var commitment = $(".commitment:checked").val();
		var platform =  $('#paltformCharge').val();
		if(commitment == 'hourly'){
			var duration = $(this).val();
			var hourPrice = $('#hourPrice').val();
			var amount = duration*24*hourPrice;
			$('#freelancerAmount').val(amount);
			$('#freelancer-price').html('£'+amount);
             var withpl = +amount + +platform;
			$('#total-price').html('£'+withpl);
			$('#totalPrice').val(withpl);
		}
	});
	
});
   </script>

	@endsection
