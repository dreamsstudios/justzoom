@extends('frontend.layouts.user_dashboard_layout')

@section('title', 'Justzoom')
@section('body')

<div class="profile-page">
		<div class="row">
			<div class="col-md-5" style="background-color: #f9f9f9;">
				<div class="profile-adrien">
					<div class="profile-img">
						<div class="left-aero">
						<a href="{{asset('/user-dashboard/contract-list')}}">
							<img src="{!! asset('assets/frontend/img/left-arrow.png') !!}"></a>
						</div>
						<?php
						if(!empty($contract_detail->image)){
							$img = $contract_detail->image;
						} else {	
							$img = 'no-img.png';
						}
						?>
						<img src="{{asset('/uploads/user_images/'.$img)}}">
						<h2>{{$contract_detail->first_name .' '.$contract_detail->last_name}}</h2>
						<h3>{{$contract_detail->job_title}}</h3>
						<div class="star--list">
							
											<div class="raiting">
											<?php 
											$remain = 5-$totalRating->average_rating;
											for($i=1; $i<=$totalRating->average_rating;$i++){
												echo '<span class="fa fa-star checked"></span>';
											}
											if($remain>0){
												for($i=1; $i<=$remain;$i++){	
													echo '<span class="fa fa-star"></span>';
												}

											}
										
										?>
										</div>
						<h4><b>@if($totalRating->average_rating){{$totalRating->average_rating}}@else 0 @endif</b> ({{$totalRating->totalreview}})</h4>
					     </div>
					
						@if($contract_detail->is_clients_verified == 1)
						<div class="verifi-button">
							<a href="#"><i class="fas fa-check"></i>Verified</a>
							<!-- <h4><b>£30</b>/hr</h4> -->
						</div>
						@endif
					</div>
					
					<div class="contacts-button">
                        <?php
						if($contract_detail->IsContractComplete==1 && $contract_detail->relId==null){
							if(isset($detail) && !empty($detail) && $detail->payouts_enabled==1){
							?>
							<button type="button" class="btn btn-success" onclick="releasefundrequest('<?php echo $contract_detail->contractId; ?>')">Send Release fund Request</button>
							<?php
							}else{?>
							<a href="{{route('user-payment-request')}}"><button type="button" class="btn btn-success" >Add Stripe Detail</button></a>
							<?php
							}
						?>
						
						<?php
						}else if($contract_detail->payStatus==1 &&  $contract_detail->relId > 0){
							?>
						<button type="button" class="btn btn-success">Payment Done</button>
                        <?php
						}else if($contract_detail->payStatus==0 &&  $contract_detail->relId > 0){
							
						?>
						<button type="button" class="btn btn-success">Waiting For payment</button>
						<?php
						}?>
					</div>
				</div>
			</div>
			<div class="col-md-7">
				<div class="profile-tabs view-tabs view-wwww">
					<div class="tab-content" id="myTabContent">
					  <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
						<div class="about-tab-list">
							<div class="tabs-rate">
								<div id="about">
									
									<div class="about-info-t">
										<h2>Contract info</h2>
										<h3>Contract ID: #{{$contract_detail->contractId}}</h3>
									</div>
									<ul class="Projects-rate-list">
										<li>
										<?php
										    if($contract_detail->priceType == 'hourly'){
												$price = $contract_detail->freelancerPrice/($contract_detail->duration*24);
											}else{
												$price = $contract_detail->freelancerPrice;
											}
									    ?>
											<h3>£ {{$price}}</h3>
											<h4>{{$contract_detail->priceType}} Rate</h4>
										</li>
										<li>
											<h3>{{$contract_detail->duration}} days</h3>
											<h4>Hire Duration</h4>
										</li>
										<li>
											<h3>£{{$contract_detail->freelancerPrice}}</h3>
											<h4>In Escrow</h4>
										</li>
										<li>
										@if($contract_detail->IsContractComplete==0)
										<span class="work-progress">In Progress</span>
										
										@else
										<span class="work-done"><i class="fas fa-check"></i>Done</span>
										@endif

											<h4>Work Status</h4>
										</li>
										
									</ul>
									
								</div>
								<div class="view-about">
									<div id="Education" class="projects-text">
										<h3>What the Project About</h3>
										<p>{{$contract_detail->aboutProject}}</p>
									</div>
									<div id="Education" class="projects-text">
										<h3>What do you need to get done</h3>
										<p>{{$contract_detail->requirement}}</p>
									</div>
									<div id="Education" class="projects-text">
										<h3>Project Discription</h3>
										<p>{{$contract_detail->description}}</p>
									</div>
								</div>	
								
							</div>
						</div>
					  </div>
					</div>
				</div>
			</div>
		</div>
	</div>

	

@endsection


