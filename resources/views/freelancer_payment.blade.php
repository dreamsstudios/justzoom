@extends('frontend.layouts.user_dashboard_layout')

@section('title', 'Justzoom')
@section('body')



<div class="client-page">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="deshbord-title">
						<h2>Payment </h2>
						<h4>Date: {{date('d-m-y')}}</h4>
					</div>
				</div>
			</div>
			<div class="row payment-flex">
				
			@if(isset($Stripedetail->payouts_enabled) && $Stripedetail->payouts_enabled==1)
				<div class="alert alert-success col-md-12">
					Your stripe account verified for payout.
				</div>
				@else
				<div class="alert alert-warning col-md-12">
					You need to verified your stripe account for payout.Please click On Account Info Icon.
				</div>
				@endif
				<div class="col-md-6">
					<div class="Total-Earning">
						<h4>Total Earning</h4>
						<h5>£{{$totalIncome}}</h5>
						<ul class="week-list">
							<li>
								<h4>Today</h4>
								<h5>£{{$todayIncome}}</h5>
							</li>
							<li>
								<h4>This Month</h4>
								<h5>£{{$monthlyIncome}}</h5>
							</li>
							<li>
								<h4>This Year</h4>
								<h5>£{{$yearlyIncome}}</h5>
							</li>
						</ul>
					</div>
				</div>
				<div class="col-md-6">
					<div class="Total-Earning two">
						<div class="total-in">
							<h4>In Escrow</h4>
							<h5>£{{$escrowAmount}}</h5>
							@if(isset($Stripedetail->payouts_enabled) && $Stripedetail->payouts_enabled==1)
							<button type="button" data-toggle="modal" data-target="#withdraw" class="btn btn-link bcontact-button con-green">Withdrawal</button>
							@endif
						</div>
						<div class="account-info">
							<div class="accout-title-n">
								<h2>Account Info</h2>
							
								<a href="<?php echo $account_links->url; ?>"><span><i class="far fa-edit"></i></span></a>
							</div>
							<?php if(!empty($Stripedetail)){
								?>
								<ul class="account-member">
								<li>
									<h2>Acc. Name:	</h2>
									<h3>{{$Stripedetail->payout_account_holder_name}}<h3>
								</li>
								<li>
									<h2>Bank Name:</h2>
									<h3>{{$Stripedetail->payout_bank_name}}<h3>
								</li>
								<li>
									<h2>Stripe Acc Id:</h2>
									<h3>{{$Stripedetail->stripeAccId}}<h3>
								</li>
								<li>
									<h2>Acc. Number:</h2>
									<h3>{{$Stripedetail->payout_acc_no}}<h3>
								</li>
								<li>
									<h2>Currency:	</h2>
									<h3>{{$Stripedetail->payout_currency}}<h3>
								</li>
								</ul>
                            <?php
							}
							?>
							
						</div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<div class="post-table">
						<h2 class="post-title">Recent Contract</h2>
						<div class="table-responsive-sm">
							<table class="table table-bordered">
							  <thead>
								<tr style="background-color: #f9f9f9;">
								  <th scope="col" style="text-align: left;"><h4>Client</h4></th>
								  <th scope="col"><h4>Contract ID</h4></th>
								  <th scope="col"><h4>Hourly Rate</h4></th>
								  <th scope="col"><h4>Total Hours</h4></th>
								  <th scope="col"><h4>Paid Amount</h4></th>
								  <th scope="col"><h4>Status</h4></th>
								  <th scope="col"><h4>Action</h4></th>
								</tr>
							  </thead>
							  <tbody>
							  @if(!empty($pastWork) && count($pastWork)>0)
							  		@foreach($pastWork as $past)
									  <?php
									  if(!empty($past->image)){
										$img = $past->image;
									} else {	
										$img = 'no-img.png';
									}
									$hr = $past->duration*24;
									 $perhour = $past->freelancerPrice/$hr;
									?>
									 
								<tr>
								  <th scope="row" style="text-align: left;">
									<div class="table-profile">
										<img src="{{asset('/uploads/user_images/'.$img)}}">
										<div class="post-text">
											<h3 >{{$past->first_name.' '.$past->last_name}}</h3>
											<p>{{$past->job_title}}</p>
										</div>
									</div>
								  </th>
								  <td><p>#{{$past->contractId}}</p></td>
								  <td><p>£{{round($perhour,2)}}</p></td>
								  <td><p>{{$past->duration*24}} hr</p></td>
								  <td><p>£{{$past->freelancerPrice}}</p></td>
								  <td><button type="button" class="btn btn-success table-done">Done</button></td>
								  <td>
									<div class="dropdown dot-button">
									  <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<img src="{!! asset('assets/frontend/img/dot.png') !!}">
									  </button>
									  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
										<a class="dropdown-item" href="{{asset('/user-dashboard/contract-detail/'.$past->contractId)}}">Contract Detail</a>
									
									  </div>
									</div>
								</td>
								</tr>
								@endforeach
								@else
								<tr><td colspan="7">No record Found!</td></tr>
								@endif
								
							  </tbody>
							</table>
						</div>
						<?php
					if(isset($pastWork) && !empty($pastWork)){
								?>
							<div class="paginating">
								<ul class="pagination searchpagination">
									{{ $pastWork->links() }}	
								</ul>
							</div>
							<?php
							}?>
					</div>
				</div>
			</div>
		</div>
		</div>
		@include("frontend.withdrawpoup") 
		<script>
			
	function releasefund(){
		var cc = $('#contractId').val();
		if(cc != ''){
			$('#withdraw').prop('disabled', true).html("Loading..");
    
   
			releasefundrequest(cc);
		}else{
			alert("please select contract Id");
		}


	}
	
		</script>
	
@endsection
