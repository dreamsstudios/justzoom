@extends('frontend.layouts.layout')



@section('title', 'Justzoom')

@section('body')
<div class="webdesign-page">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
				<form class="needs-validation" novalidate="" id="my-form" method="post" action="{{route('setfilter')}}" enctype = "multipart/form-data">
										{!!csrf_field()!!}

					<div class="design-requirement">
						<div class="sero-icon">
							<a href=" {{URL::previous()}}"><span><i class="fas fa-arrow-left"></i></span></a>
							<span>{{$number}}/{{$questionCount}}</span>
						</div>
						<div class="radio-buttons">
							<h2>{{$question->question}}</h2>
							@if(!empty($questionQption))
							 @foreach($questionQption as $option)

							<div class="rdio rdio-primary radio-inline"> 
							  <input name="option" value="{{$option->optionText}}" id="radio{{$option->optionId}}" type="radio">
							  <label for="radio{{$option->optionId}}">{{$option->optionText}}</label>
							</div>
							@endforeach
							@endif
							
							
						</div>
						<input type="hidden" value="{{$question->questionId}}" name="questionId" />
						<input type="hidden" value="{{$question->categoryId}}" name="catId" />
						<input type="hidden" value="{{$next}}" name="next" />
						<input type="hidden" value="{{$number}}" name="step"/>
						<div class="Continue-btt">
								<button type="submit" class="btn btn-primary join-btt">Continue</button>
							</div>

					</div>
					</form>
				</div>
				
			</div>
		</div>
	</div>

@endsection



