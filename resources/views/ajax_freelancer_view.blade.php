
<?php
if(!empty($users) && count($users)>0) {
?>

@foreach($users as $user)
	<?php $img = (!empty($user->image)) ? $user->image: 'no-img.png'; ?>
	<div class="profile-list">

		<!-- User Profile -->
		<div class="profile-list-left">
			<div class="chat-user-img">
				<img src="{{asset('/uploads/user_images/'.$img)}}" alt="user image">
				<div class="brand-logo">
					<img src="{!! asset('assets/img/chat-logo.png') !!}" alt="">
				</div>
			</div>
			<div class="profile-statics">			
				@if ($user->is_user_verified == '1')
					<h6><i class="fa fa-check"></i> Verified</h6>
				@endif 
			</div>
		</div>

		<!-- User Description -->
		<div class="profile-list-right">							
			<div class="chat-user-statics profile-info">
				<ul>
					<!-- User Introduction -->
					<li class="profile-name">
						<h2>{{ ucfirst ($user->first_name).' '. ucfirst ($user->last_name)}}</h2>
						<h4>{{ $user->category_name }} @if($user->city)in {{$user->city}}@endif @if($user->country), {{ $user->country }}@endif </h4>
					</li>

					<!-- User Unread Message -->
					<li>
						<div class="inbox">
							<img src="{!! asset('assets/frontend/img/envelope.png') !!}" alt="">
							<span class="msg-count">{{ $user->unreadMessage }}</span>
						</div>
						<p>Unread</p>
					</li>

					<!-- User Rating and Total Review -->
					<li>
						<div class="raiting">
							<?php
								$totalRatings = 5;
								for($i=1; $i <= $user->totalRatings; $i++) {
									$totalRatings--;
							?>
									<span class="fa fa-star checked"></span>		
							<?php
								}

								for($i=1; $i<= $totalRatings; $i++) {
									echo '<span class="fa fa-star"></span>';
								}
							?>
						</div>				
						<p><span>{{$user->totalRatings}}</span> ({{$user->totalReviews}})</p>						
					</li>

					<!-- Total Customers -->
					<li>
						<h3>{{$user->totalCustomer}}</h3>
						<p>Customers</p>
					</li>

					<!-- Response time -->
					<li>
					@if ($user->response_time!= '')<h3>{{$user->response_time}}hr</h3>@else N/A @endif
						<p>Response Time</p>
					</li>

					<!-- Hourly Rate -->
					<li>
						<h3>{{Config::get('app.site_currency_symbol')}} {{ $user->hourly_rate}}</h3>
						<p>Hourly Rate</p>
					</li>
				</ul>
			</div>
			@if ($user->userLastMessage)
			<div class="profile-review-section">
				<!--   -->
				<div class="review-area">
					<div class="review-area">
						<div class="review-duration">
							<h5>{{$user->userLastMessageSenderName}}</h5>
							<h5>{{$user->userLastMessageDate}}</h5>
						</div>
						<hr>
						<p>{{$user->userLastMessage}}</p>
						<!-- <p>Brian is man of amazing attitude, perserverance and true to his word - everything you need in a great real estate agent. You can't go past his great service if you are needing to sell or buy.</p> -->
					
							<div class="reply-btn-area">
							<?php if(!empty($user->userChatId)){
						    ?>
								<a href="{{asset('/client/chat/'.$user->userChatId)}}"><button type="button" class="btn reply-btn">Reply</button></a>
							<?php
							}?>
							</div>
											
					</div>

				
				</div>
				@endif 	
				<div class="btn-area">
					<div class="profile-btn-group">
						<a href="{{asset('/freelancer-detail/'.$user->id)}}" class="view-profile-btn">View Profile</a>
						<!--<a href="javascript:void(0)" class="contact-btn">Contact Us</a>
						<a href="javascript:void(0)" class="quote-btn">Request Quote</a>-->
					</div>
				</div>
			</div>
		</div>
	</div>

@endforeach

<div class="paginating">
	<ul class="pagination searchpagination">
		{{ $users->links() }}	
	</ul>
</div>

<?php } else { echo '<div class="profile-list">No record found</div>'; } ?>

							
							<style>
.location {
    display: flex;
    text-align: center;
    font-family: 'Roboto', sans-serif;
    width: 90%;
    justify-content: space-between;
    margin: 0 auto;
    flex-direction: column;
}
.loc-box {
    display: flex;
}
.profile .name_loct h4 {
    font-size: 16px;
    color: #7a7a7a;
    margin-right: 23px;
}
</style>