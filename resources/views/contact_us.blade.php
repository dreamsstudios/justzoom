@extends('frontend.layouts.layout')

@section('title', 'JustZoom')

@section('body')

 <div class="Lets-Talk-page">

        <div class="container">

            <div class="row">

                <div class="col-md-12">

                    <h2 class="lets-talk-title">Let’s Talk</h2>

                    @if(session("msg"))

                                        <div class="alert-dismiss">

                                            <div class="alert alert-success alert-dismissible fade show">

                                                <span>{{session("msg")}}</span><button type="button" class="close" data-dismiss="alert" aria-label="Close">

                                                <span class="fa fa-times"></span> </button><br/>

                                            </div>

                                        </div>

                            @endif

                             @if(session("errormsg"))

                                        <div class="alert-dismiss">

                                            <div class="alert alert-danger alert-dismissible fade show">

                                                <span>{{session("errormsg")}}</span><button type="button" class="close" data-dismiss="alert" aria-label="Close">

                                                <span class="fa fa-times"></span> </button><br/>

                                            </div>

                                        </div>

                            @endif

                </div>

                <div class="clients-forms">

                    <h4><strong>Any questions? </strong>Feel free to contact us!</h4>

                    <form id="contact-form" novalidate="" name="contact-form" action="{{route('contact-us')}}" method="POST">

                                {!!csrf_field()!!}

                        <div class="row">

                            <div class="col-md-12">

                              <div class="form-group">

                                <label for="exampleInputEmail1">Full Name</label>

                                <input type="text" id="name" name="name" class="form-control" placeholder="Name">

                                 {!! $errors->first('name', '<p class="help-block">:message</p>') !!}

                                        

                              </div>

                            </div> 

                            

                        </div>

                        <div class="row">

                            <div class="col-md-12">

                                <div class="form-group">

                                    <label for="exampleInputPassword1">Email</label>

                                    <input type="text" id="contactemail" name="contactemail" class="form-control" placeholder="Your Email">

                                     {!! $errors->first('contactemail', '<p class="help-block">:message</p>') !!}

                                </div>

                            </div>

                        </div>

                        <div class="row">

                            <div class="col-md-12">

                                <div class="form-group">

                                    <label for="exampleInputPassword1">Subject</label>

                                    <input type="text" id="subject" name="subject" class="form-control" placeholder="Your subject">

                                    {!! $errors->first('subject', '<p class="help-block">:message</p>') !!}

                                </div>

                            </div>  

                        </div>

                        <div class="row">

                            <div class="col-md-12">

                                <div class="form-group">

                                    <label for="exampleInputPassword1">Message</label>

                                    <textarea type="text" id="message" name="message" rows="7" class="form-control md-textarea" placeholder="Your Message"></textarea>

                                    {!! $errors->first('message', '<p class="help-block">:message</p>') !!}

                                </div>

                            </div>  

                        </div>

                     

                      <button type="submit" class="btn btn-primary join-btt name_desg contact-us-button">Let’s GO!</button>

                    </form>

                </div>

            </div>

        </div>

    </div>

@endsection

