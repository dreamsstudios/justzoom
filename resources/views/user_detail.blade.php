@extends('frontend.layouts.layout')

@section('title', 'Justzoom')
@section('body')
<div class="profile-page">
		<div class="row">
			<div class="col-md-5" style="background-color: #f9f9f9;">
				<div class="profile-adrien">
					<div class="profile-img">
						<?php
									if(!empty($user->image)){
                      $img = $user->image;
                 }else{
                     $img = 'no-img.png';
                    }
                                         ?>
						<img src="{{asset('/uploads/user_images/'.$img)}}">
						<h2>{{ $user->first_name.' '.$user->last_name }}</h2>
						<h3>
							@if(!empty($user->category))
							{{$user->category->category_name}}
							@endif
							@if(!empty($user->address_line_1))
							,{{$user->address_line_1}}
							@endif
							@if(!empty($user->country))
							,
							{{$user->country}}
							@endif
							</h3>
						<div class="star--list">
							<div class="raiting">
								<?php 
									$remain = 5-$totalRating->average_rating;
									for($i=1; $i<=$totalRating->average_rating;$i++){
										echo '<span class="fa fa-star checked"></span>';
										}
									if($remain>0){
									for($i=1; $i<=$remain;$i++){	
										echo '<span class="fa fa-star"></span>';
									}

									}
										
								?></div>
							<h4><b>{{$totalRating->average_rating}}</b> ({{$totalRating->totalreview}})</h4>
						</div>
						<div class="verifi-button">
						@if($user->is_user_verified == 1)
							<a href="#"><i class="fas fa-check"></i>Verified</a>
					    @endif
							<h4><b>£{{$user->hourly_rate}}</b>/hr</h4>
						</div>
					</div>
					<div class="intro-video">
                       @if(!empty($user->video))
                       <video width="220" height="240" controls>
                          <source src="{{asset('/uploads/video/user_video/'.$user->video)}}" type="video/mp4" >
                            Your browser does not support the video tag.
                        </video>           <div class="watch-now">
                         <h2>Watch Now</h2>
                         <h3>An introduction to Justzoom</h3>
                     </div>
                     @endif
                 </div>
                 <div class="skill-list">
                  <ul>
                     
                     @foreach($userskill as $value)
                     <li><a href="#">{{$value->skill_data->skill}}</a></li>
                     @endforeach
                 </ul>
             </div>
					<div class="contacts-button">
					<?php
					if(isset($UserInvite) && !empty($UserInvite)){
						if($UserInvite->requestStatus == 2){
						?>
						<button sendOfferModel type="button" data-toggle="modal" data-target="#sendInviteModel" class="btn btn-success" 
					    onclick="">Send Invitation</button>
                        <?php
						}else if($UserInvite->requestStatus == 0){
							echo '<div class="alert alert-warning" role="alert">You have sent Invitation</div>';

						}else{
						?>
						<a href="\client\chat\{{$UserInvite->chatId}}"><button type="button" data-toggle="modal" data-target="#sendInviteModel" class="btn btn-success" 
					    onclick="">Chat</button></a>
                        <?php
						}
				    ?>
					
                    <?php
					}else{
					?>
					<button sendOfferModel type="button" data-toggle="modal" data-target="#sendInviteModel" class="btn btn-success" 
					onclick="">Send Invitation</button>
                    <?php
					}
					?>
					</div>
				</div>
			</div>
			<div class="col-md-7">
				<div class="profile-tabs">
					<ul class="nav nav-tabs" id="myTab" role="tablist">
					  <li class="nav-item">
						<a class="nav-link page-scroll active" href="#home">About </a>
					  </li>
					   <li class="nav-item">
						<a class="nav-link page-scroll" href="#Experience">Experience</a>
					  </li>
					   <li class="nav-item">
						<a class="nav-link page-scroll" href="#Portfolio">Portfolio</a>
					  </li>
					   <li class="nav-item">
						<a class="nav-link page-scroll" href="#Projects">Projects</a>
					  </li>
					   <li class="nav-item">
						<a class="nav-link page-scroll" href="#Education">Education </a>
					  </li>
					  <li class="nav-item">
						<a class="nav-link page-scroll" href="#Reviews">Reviews</a>
					  </li>
					 
					  
					</ul>
					<div class="tab-content" id="myTabContent">
					  <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
						<div class="about-tab-list">
							<div class="tabs-rate">
								<div id="about">
									<h2>About</h2>
									<p>{{$user->about}}</p>
									<ul class="Projects-rate-list">
										<li>
											<h3>£{{$user->hourly_rate}}</h3>
											<h4>Hourly Rate</h4>
										</li>
										<li>
											<h3>{{$contract}}</h3>
											<h4>Projects worked on</h4>
										</li>
										<li>
											<h3>{{$clients}}</h3>
											<h4>Buyers worked with</h4>
										</li>
										<li>
											<h3>{{$user->response_time}} hr</h3>
											<h4>Response time</h4>
										</li>
									</ul>
								<?php if(!empty($UserInvite) && $UserInvite->requestStatus == 1 && $UserInvite->contractId==null){
									?>
									<div class="hire-box">
										<h2>{{ $user->first_name.' '.$user->last_name }} is now<span> available</span> for hire</h2>
										<a href="{{asset('/hire-freelancer/'.$UserInvite->reqId)}}"><button type="button" class="btn btn-info her-btt">Hire  {{ $user->first_name}}</button></a>
									</div> 
								</div>
                                <?php
								}
								?>
								
								<div id="Experience" class="Experience-lsit">
									<h2>Experience</h2>
									<ul>
										@foreach($UserExperience as $value)
										<li><h3>{{$value->experience}}</h3><h3>{{$value->experience_year}} Years</h3></li>
										@endforeach
									</ul>
								</div>
								<div id="Portfolio" class="portfolio-img">
									<h2>Portfolio</h2>
									<ul>
										@if(!empty($userportfolio))
										@foreach($userportfolio as $value)
										
										<li><img src="{{asset('/uploads/user_portfolio/'.$value->filename)}}"></li>
										@endforeach
										@endif
									</ul>
									
								</div>
								<div id="Projects" class="project-bg">
								<div class="projects-text">
										<h2>Projects</h2>
										
										@foreach($UserProject as $value)
										<h3>{{$value->project_title}}</h3>
										<p>{{$value->project_dis}}</p>
										@endforeach
								</div>
									<div id="Education" class="projects-text">
										<h2>Education</h2>
										@foreach($UserEducation as $value)
										<h3>{{$value->course }}</h3>
										<p>{{$value->university}}</p>
										@endforeach
									</div>
								</div>	
								<div id="Reviews" class="reviews-5">
									<div class="revew-button">
										<h2>Reviews ({{$totalRating->totalreview}}) </h2>
										
									</div>
									@foreach($reviews as $user_review)
						<div class="review-allira">
<?php
if(!empty($user_review->image)){
$img = $user_review->image;
}else{
$img = 'no-img.png';

}
?>
<img style="width: 5%; height: auto;"  class="img-responsive" src="{{asset('/uploads/user_images/'.$img)}}" alt="">

<div class="mark-dobbie">
<div class="mark-dobbie-img">
<div class="mark-dobbie-text">
<h2>{{$user_review->first_name}} {{$user_review->last_name}}</h2>
<?php
if(isset($user_review->rating)){
?>

<?php 
$remain = 5-$user_review->rating;
for($i=1; $i<=$user_review->rating;$i++){
echo '<span class="fa fa-star checked"></span>';
}
if($remain>0){
for($i=1; $i<=$remain;$i++){	
echo '<span class="fa fa-star"></span>';
}

}
}
?>
</div>
<h4>{{ Carbon\Carbon::parse($user_review->created_at)->format('d M Y') }}</h4>
</div>
<p>{{$user_review->review}}</p>
</div>
</div>
										@endforeach
									
									
									
								</div>
							</div>
						</div>
					  </div>
					  <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">2</div>
					  <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">3</div>
					  <div class="tab-pane fade" id="projects" role="tabpanel" aria-labelledby="projects-tab">4</div>
					  <div class="tab-pane fade" id="education" role="tabpanel" aria-labelledby="education-tab">5</div>
					  <div class="tab-pane fade" id="reviews" role="tabpanel" aria-labelledby="reviews-tab">6</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="sendInviteModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Send A Invitiation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
	  <form action="#" id="invite" class="needs-validation" onsubmit="sendInvitation(this,event)" novalidate>
		@csrf
      <div class="modal-body">
	  
		<input type="hidden" value="ajax_sendInvite" name="action" />
		<input type="hidden" value="{{$user->id}}" name="userId" />
		<input type="hidden" value="{{$user->category_id}}" name="categoryId"/>
          <div class="form-group">
            <label for="message-text" class="col-form-label">Message:</label>
            <textarea class="form-control" id="message-text" name="message" id="message"></textarea>
          </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" id="invitebtn" class="btn btn-primary ">Send invitation</button>
      </div>
	  </form>
	  <div class="alert alert-danger" style="display:none"></div>
	  <div class="alert alert-success" style="display:none"></div>
    </div>
  </div>
</div>


@endsection
