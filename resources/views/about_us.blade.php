@extends('frontend.layouts.layout')



@section('title', 'Justzoom')

@section('body')

 <div class="business-banner about-bgg">

            <div class="container-fluid">

                <div class="row">

                    <div class="col-md-7">

                        <h2>About Justzoom</h2>

                        <p>We empower people worldwide to live their work dream building their<br> business from the ground up and becoming financially and professionally <br>independent.</p>

                    </div>

                </div>

            </div>

        </div>

    </div>

    <div class="about-tabs">

        <ul class="nav nav-tabs" id="myTab" role="tablist">

          <li class="nav-item">

            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Who We Are</a>

          </li>

          <li class="nav-item">

            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Our Value system</a>

          </li>

          <li class="nav-item">

            <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Meet the team</a>

          </li>

        </ul>

        <div class="about-list-taab">

            <div class="container-fluid">

                <div class="tab-content" id="myTabContent">

                  <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">

                    <div class="row">

                        <div class="col-md-6">

                            <h2>Who we are</h2>

                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi sodales fringilla dolor, eget convallis tellus iaculis vel. Aliquam eros libero, varius quis molestie et, aliquam at ipsum. Nulla volutpat sapien vitae nunc tempor luctus. Aenean nunc risus, sodales quis odio id, condimentum lobortis tellus. Aenean sollicitudin at lectus eu viverra. Phasellus ut posuere quam, scelerisque faucibus quam. Nunc hendrerit ultricies mauris.</p>

                        </div>

                        <div class="col-md-6">

                            <div class="tabs-img">

                                <img src="assets/frontend/img/about-girl.png">

                            </div>

                        </div>

                    </div>

                  </div>

                  <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">

                    <div class="row">

                        <div class="col-md-6">

                            <h2>Who we are</h2>

                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi sodales fringilla dolor, eget convallis tellus iaculis vel. Aliquam eros libero, varius quis molestie et, aliquam at ipsum. Nulla volutpat sapien vitae nunc tempor luctus. Aenean nunc risus, sodales quis odio id, condimentum lobortis tellus. Aenean sollicitudin at lectus eu viverra. Phasellus ut posuere quam, scelerisque faucibus quam. Nunc hendrerit ultricies mauris.</p>

                        </div>

                        <div class="col-md-6">

                            <div class="tabs-img">

                                <img src="assets/frontend/img/about-girl.png">

                            </div>

                        </div>

                    </div>

                  </div>

                  <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">

                    <div class="row">

                        <div class="col-md-6">

                            <h2>Who we are</h2>

                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi sodales fringilla dolor, eget convallis tellus iaculis vel. Aliquam eros libero, varius quis molestie et, aliquam at ipsum. Nulla volutpat sapien vitae nunc tempor luctus. Aenean nunc risus, sodales quis odio id, condimentum lobortis tellus. Aenean sollicitudin at lectus eu viverra. Phasellus ut posuere quam, scelerisque faucibus quam. Nunc hendrerit ultricies mauris.</p>

                        </div>

                        <div class="col-md-6">

                            <div class="tabs-img">

                                <img src="assets/frontend/img/about-girl.png">

                            </div>

                        </div>

                    </div>

                  </div>

                </div>

            </div>

        </div>  

    </div>

    <div class="high-perfomance">

        <div class="container-fluid">

            <div class="row involis-project">

                <div class="col-md-4">

                    <div class="teams-list client-logo">

                        <span><img src="assets/frontend/img/Clients1.png"></span>

                        <h3>10,000+</h3>

                        <p>Clients</p>

                    </div>

                </div>

                <div class="col-md-4">

                    <div class="teams-list client-logo">

                        <span><img src="assets/frontend/img/Clients2.png"></span>

                        <h3>500+</h3>

                        <p>Employees</p>

                    </div>

                </div>

                <div class="col-md-4">

                    <div class="teams-list client-logo">

                        <span><img src="assets/frontend/img/Clients3.png"></span>

                        <h3>100+</h3>

                        <p>Countries</p>

                    </div>

                </div>

            </div>

        </div>

    </div>

    <div class="our-culture">

        <div class="container-fluid">

            <div class="row">

                <div class="col-md-6">

                    <h2>Our culture</h2>

                    <p>We love what we do and collaborate every day<br> hungry to change the world of work</p>

                </div>

            </div>

        </div>

    </div>

    

    <div class="find-expert">

        <div class="container">

            <div class="row">

                <div class="col-md-12">

                    <h2>Find an expert for anything</h2>

                    <p>Work with curated freelance talent from all over the world. Manage the entire project with Justzoom.<br>Pay securely with confidence.</p>

                    <a href="#sign-up"> <button type="button" class="btn btn-link page-scroll">Join for Free</button></a>

                    <h4>No upfront payments, no hidden fees.</h4>

                </div>

            </div>

        </div>

    </div>

    <div class="testmonial-bg">

        <div class="container">

            <div class="row">

                <div class="col-md-12">

                    <div class="testmonial-title">

                        <h2>Hear From Our Clients</h2>

                        <ul>

                            <li><h3>Our customers say </h3></li>

                            <li><h4>Excellent    <img src="assets/frontend/img/star.png"></h4></li>

                            <li><h4>4.7 out of 5 based on 1,123 reviews<img src="assets/frontend/img/trustpilot.png"></h4></li>

                        </ul>

                    </div>

                </div>

                <div class="col-md-12">

                    <div class="testmonial">

                        <div class="test-box">

                            <span><i class="fas fa-quote-left"></i></span>

                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In molestie neque et tortor pretium, a facilisis justo tempus. Maecenas facilisis maximus vehicula. Proin sem lacus, eleifend sed orci at, fermentum aliquet dolor. Duis ac erat ac augue ullamcorper aliquet nec quis quam. Donec interdum iaculis erat sit amet commodo. Donec eu porttitor magna, id aliquet ligula. </p>

                            <div class="mc-nallay">

                                <img src="assets/frontend/img/aval.png">

                                <div class="testmonial-text">

                                    <h3>Ryan Walker, Director of Product</h3>

                                    <h4>Rand McNally</h4>

                                </div>

                            </div>

                        </div>

                        <div class="test-box">

                            <span><i class="fas fa-quote-left"></i></span>

                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In molestie neque et tortor pretium, a facilisis justo tempus. Maecenas facilisis maximus vehicula. Proin sem lacus, eleifend sed orci at, fermentum aliquet dolor. Duis ac erat ac augue ullamcorper aliquet nec quis quam. Donec interdum iaculis erat sit amet commodo. Donec eu porttitor magna, id aliquet ligula. </p>

                            <div class="mc-nallay">

                                <img src="assets/frontend/img/aval.png">

                                <div class="testmonial-text">

                                    <h3>Ryan Walker, Director of Product</h3>

                                    <h4>Rand McNally</h4>

                                </div>

                            </div>

                        </div>

                        <div class="test-box">

                            <span><i class="fas fa-quote-left"></i></span>

                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In molestie neque et tortor pretium, a facilisis justo tempus. Maecenas facilisis maximus vehicula. Proin sem lacus, eleifend sed orci at, fermentum aliquet dolor. Duis ac erat ac augue ullamcorper aliquet nec quis quam. Donec interdum iaculis erat sit amet commodo. Donec eu porttitor magna, id aliquet ligula. </p>

                            <div class="mc-nallay">

                                <img src="assets/frontend/img/aval.png">

                                <div class="testmonial-text">

                                    <h3>Ryan Walker, Director of Product</h3>

                                    <h4>Rand McNally</h4>

                                </div>

                            </div>

                        </div>

                        <div class="test-box">

                            <span><i class="fas fa-quote-left"></i></span>

                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In molestie neque et tortor pretium, a facilisis justo tempus. Maecenas facilisis maximus vehicula. Proin sem lacus, eleifend sed orci at, fermentum aliquet dolor. Duis ac erat ac augue ullamcorper aliquet nec quis quam. Donec interdum iaculis erat sit amet commodo. Donec eu porttitor magna, id aliquet ligula. </p>

                            <div class="mc-nallay">

                                <img src="assets/frontend/img/aval.png">

                                <div class="testmonial-text">

                                    <h3>Ryan Walker, Director of Product</h3>

                                    <h4>Rand McNally</h4>

                                </div>

                            </div>

                        </div>

                        <div class="test-box">

                            <span><i class="fas fa-quote-left"></i></span>

                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In molestie neque et tortor pretium, a facilisis justo tempus. Maecenas facilisis maximus vehicula. Proin sem lacus, eleifend sed orci at, fermentum aliquet dolor. Duis ac erat ac augue ullamcorper aliquet nec quis quam. Donec interdum iaculis erat sit amet commodo. Donec eu porttitor magna, id aliquet ligula. </p>

                            <div class="mc-nallay">

                                <img src="assets/frontend/img/aval.png">

                                <div class="testmonial-text">

                                    <h3>Ryan Walker, Director of Product</h3>

                                    <h4>Rand McNally</h4>

                                </div>

                            </div>

                        </div>

                        <div class="test-box">

                            <span><i class="fas fa-quote-left"></i></span>

                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In molestie neque et tortor pretium, a facilisis justo tempus. Maecenas facilisis maximus vehicula. Proin sem lacus, eleifend sed orci at, fermentum aliquet dolor. Duis ac erat ac augue ullamcorper aliquet nec quis quam. Donec interdum iaculis erat sit amet commodo. Donec eu porttitor magna, id aliquet ligula. </p>

                            <div class="mc-nallay">

                                <img src="assets/frontend/img/aval.png">

                                <div class="testmonial-text">

                                    <h3>Ryan Walker, Director of Product</h3>

                                    <h4>Rand McNally</h4>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

     @include("frontend.layouts.common_register_user")

    <div class="Justzoom-Developers">

        <div class="container">

            <div class="row">

                <div class="col-md-12">

                    <h2 class="just-title">Justzoom Developers</h2>

                </div>

                <div class="col-md-3">

                    <ul>

                        @foreach($category_list as $category)

                        <li><h3>{{$category}}</h3></li>

                        @endforeach

                    </ul>

                </div>

                

            </div>

        </div>

    </div>   



@endsection

