@extends('frontend.layouts.layout')
@section('title', 'Justzoom')
@section('body')

<div class="hire-adrien-page">
		<div class="container">
			<div class="hire-adrien-section">
				<div class="row">
					<div class="col-md-12">					
						<div class="breadcrumb-nav">
							<div class="title">
								<h2>Hire {{ucfirst($contract->first_name)}}!</h2>
							</div>
							<div class="sub-heading">
								<ul>
									<li>
										<a href="{{asset('/hire-freelancer/'.$contract->reqId)}}">Hire</a>
									</li>
									<li>/</li>
									<li>
										<a href="#">Payment</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				@if(session("msg"))
                                        <div class="alert-dismiss">
                                            <div class="alert alert-success alert-dismissible fade show">
                                                <span>{{session("msg")}}</span><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span class="fa fa-times"></span> </button><br/>
                                            </div>
                                        </div>
                            @endif
							 @if(session("errormsg"))
                                        <div class="alert-dismiss">
                                            <div class="alert alert-danger alert-dismissible fade show">
                                                <span>{{session("errormsg")}}</span><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span class="fa fa-times"></span> </button><br/>
                                            </div>
                                        </div>
                @endif	
				<div class="hire-adrien-form pay-form">
				
					<div class="row">
						<div class="col-md-6">
						<form class="needs-validation" novalidate="" method="post" id="payment-form" action="{{route('savepayment')}}" enctype = "multipart/form-data">
										{!!csrf_field()!!}
								<div class="payment-heading">
									<h3>Enter Payment Info</h3>
									<img src="{!! asset('assets/frontend/img/payment-icon.png') !!}" alt="payment icon">
								</div>
								<input type="hidden" value="{{$contract->totalPrice}}" name="totalPrice" id="totalPrice" />
								<input type="hidden" value="{{$contract->platformPrice}}" name="platformPrice" id="platformPrice" />
								<input type="hidden" value="{{$contract->contractId}}" name="contractId" id="contractId" />
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
										    <label for="card-name">Name on Card</label>
										    <input type="text" class="form-control" autocomplete="off" name="cardholder" id="cardholder" placeholder="" required autofocus>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-8">
										<div class="form-group">
										    <label for="card-number">Card Number</label>
										    <input type="text" class="form-control" autocomplete="off" id="cardNumber" placeholder="" name="cardNumber" 
                      						required data-stripe="number" maxlength="20" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" />
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
										    <label for="cvc">CVC</label>
										    <input type="password" maxlength="4" autocomplete="off" class="form-control" id="cvvCode" placeholder="" name="cvvcode" size="4" 
											data-stripe="cvc" required onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')"  />
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
										    <label for="card-expiry">Expiration Month</label>
										    <input type="text" class="form-control"  autocomplete="off" id="expiryMonth" maxlength="2" 
											placeholder="MM" required name="expiryMonth" size="2" 
											data-stripe="exp_month"  onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" />
										</div>
									</div>
									<div class="col-md-6">
									<div class="form-group">
										    <label for="card-expiry">Expiration Year</label>
										    <input type="text" class="form-control" 
									autocomplete="off" maxlength="2" id="expiryYear" placeholder="YY" size="2" data-stripe="exp_year" name="expiryYear" 
									required onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" />
										</div>
									</div>
								</div>
								<div class="payment-notes">
									<div class="pay-msg">
										<h6>Payments are processed securely through</h6>
									</div>
									<div class="pay-img">
										<img src="{!! asset('assets/frontend/img/stripe-icon.png') !!}" alt="stripe icon">
									</div>
								</div>
								
								<button type="submit" class="btn btn-continue submit" >Pay</button>
							</form>
						</div>
						<div class="col-md-6">
							<div class="hire-profile-section">
								<div class="hire-profile-card">
									<div class="hire-profile">
										<div class="hire-user-img">
										<?php
										if(!empty($contract->image)){
											$img = $contract->image;
										}else{
											$img = 'no-img.png';
											}
                                         ?>
						                <img src="{{asset('/uploads/user_images/'.$img)}}" alt="user image">
											<div class="brand-logo">
												<img src="{!! asset('assets/frontend/img/chat-logo.png') !!}" alt="">
											</div>
										</div>
										<div class="hire-user-profile">
									
											<h2>{{ucfirst($contract->first_name.' '.$contract->last_name)}}</h2>
											<h4>{{ucfirst($contract->category_name)}} @if($contract->city)in {{$contract->city}}@endif</h4>
											<div class="raiting">
											<?php 
											$remain = 5-$totalRating->average_rating;
											for($i=1; $i<=$totalRating->average_rating;$i++){
												echo '<span class="fa fa-star checked"></span>';
											}
											if($remain>0){
												for($i=1; $i<=$remain;$i++){	
													echo '<span class="fa fa-star"></span>';
												}

											}
											
										?>
										<p><span>@if($totalRating->average_rating>0){{$totalRating->average_rating}}@else {{0}} @endif</span> ({{$totalRating->totalreview}})</p>
										</div>
											@if($contract->is_user_verified==1)
											<h6><i class="fa fa-check"></i> Verified</h6>
											@endif
										</div>
									</div>
									<div class="hire-locate">
									<div class="location">@if(!empty($contract->country))<img src="{{asset('assets/img/locate-icon.png')}}" alt="location"> 
										{{$contract->city}} {{','.$contract->country}}@endif</div>
										<div class="hired-price">£{{$contract->hourly_rate}}<span>/hr</span></div>
									</div>
									<div class="hire-description">
										<p>{{$contract->about}}</p>
									</div>
								</div>
								<div class="hired-totl-price">
								<h3>Platform Charge</h3>
								<h3>£ {{$contract->platformPrice}}</h3>
								</div>
									
								<div class="hired-totl-price">
								
									<h3>Freelancer Charge</h3>
									<h3>£ {{$contract->freelancerPrice}} <span>(This Amount will be charged after closure of contract.)</span></h3>
								    
									</div>
									
									
								<div class="hired-totl-price">
									<h3>Total</h3>
									<h3>£ {{$contract->totalPrice}}</h3>
								</div>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
	
	</div>
	<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
	<script>
	Stripe.setPublishableKey('<?php echo $setting->stirpePublicKey; ?>');
  
  $(function() {
    var $form = $('#payment-form');
    $form.submit(function(event){
      debugger;
      // Disable the submit button to prevent repeated clicks:
      $form.find('.submit').prop('disabled', true).html('Checking card information..');
    
      // Request a token from Stripe:
      Stripe.card.createToken($form, stripeResponseHandler);
    
      // Prevent the form from being submitted:
     return false;
    });
  });

  function stripeResponseHandler(status, response) {
	  //alert("ddd");
    // Grab the form:
    var $form = $('#payment-form');
  
    if (response.error) { // Problem!
  
	// Show the errors on the form:
		
    $form.find('.payment-errors').text(response.error.message);
    $form.find('.submit').prop('disabled', false).html(GLOBALLANG.Pay); // Re-enable submission
  
    } else { // Token was created!

    $form.find('.submit').prop('disabled', false).html('Processing subsciption..');
    // Get the token ID:
    var token = response.id;
   
    // Insert the token ID into the form so it gets submitted to the server:
    $form.append($('<input type="hidden" id="stripeToken" name="stripeToken">').val(token));
     // return false;
    // Submit the form:
     $form.get(0).submit();
   // courseSubscription('#payment-form', event);
    }
  }

  function cvvCheck() {
  debugger;
  if(!$('#cvvCode').val())
    $('.payment-errors.msg').text('Please enter cvv code.');

} 

	</script>
	@endsection
