@extends('frontend.layouts.layout')

@section('title', 'Justzoom')
@section('body')
<div class="banner">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <h2>Hire the top of freelance <br>talent</h2>
                    <p>Justzoom expertly connects professionals and agencies to businesses seeking specialized talent.</p>
                    <div class="hear-button">
                        <a href="#"><button type="button" class="btn btn-secondary green-bg">Secondary</button></a>
                        <a href="#"><button type="button" class="btn btn-secondary white-border">Secondary</button></a>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="banner-lapto">
                        <img src="assets/frontend/img/Motion1.gif">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="logo-list">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <ul>
                        <li><h2>TRUSTED BY LEADING <br>BRANDS AND STARTUPS</h2></li>
                        <li><img src="assets/frontend/img/plastic1.png"></li>
                        <li><img src="assets/frontend/img/plastic2.png"></li>
                        <li><img src="assets/frontend/img/plastic3.png"></li>
                        <li><img src="assets/frontend/img/plastic4.png"></li>
                        <li><img src="assets/frontend/img/plastic5.png"></li>
                        <li><img src="assets/frontend/img/plastic6.png"></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    
    <div class="we-talent">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h3>We connect the world's top talent with the world's top organizations.</h3>
                </div>
                <div class="col-md-6">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla aliquam eros et imperdiet luctus. In ullamcorper lorem vel ante interdum, ac blandit dolor volutpat. Phasellus vehicula nunc est, ac egestas diam semper sed. In mollis malesuada venenatis. </p>
                    <a href="{{route('about-us')}}">About</a>
                </div>
            </div>
        </div>
    </div>
    <div class="client-gallrey">
        <div class="popular-course">
            
           @foreach($users as $value)
                    <div class="slide-box">
                        <?php
                                    if(!empty($value->image)){
                                             $img = $value->image;
                                         }else{
                                          $img = 'no-img.png';

                                         }
                                         ?>
                        <img src="{{asset('/uploads/user_images/'.$img)}}">
               
                <div class="slide-tex">
                    <h2>{{ $value->first_name.' '.$value->last_name }}</h2>
                    <p><span><img src="assets/frontend/img/pen.png"></span> @if(!empty($value->category))
                            {{$value->category->category_name}}
                            @else
                            N/A
                            @endif</p>
                </div>
            </div>
                     @endforeach
            
            
        </div>
        <!-- <div class="range-slider">
            <div class="row">
                <div class="col-sm-12">
                  <div id="slider-range"></div>
                </div>
            </div>
            <div class="row slider-labels">
                <div class="col-xs-6 caption">
                  <strong>Min:</strong> <span id="slider-range-value1"></span>
                </div>
                <div class="col-xs-6 text-right caption">
                  <strong>Max:</strong> <span id="slider-range-value2"></span>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                  <form>
                    <input type="hidden" name="min-value" value="">
                    <input type="hidden" name="max-value" value="">
                  </form>
                </div>
            </div>
            <button type="button" class="btn btn-secondary diceover-btt">Decover More</button>
        </div> -->
    </div>
    <div class="connect-demand">
        <div class="container">
            <div class="row">
                <div class="col-md-6" style="background-color: #f9f9f9;padding: 100px 100px;">
                    <h2>Connect with in-demand professionals</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla aliquam eros et imperdiet luctus.</p>
                    <ul class="demant-list">
                        <li><span><i class="fas fa-check"></i></span><h3>Complex projects</h3></li>
                        <li><span><i class="fas fa-check"></i></span><h3>Long-term contracts</h3></li>
                        <li><span><i class="fas fa-check"></i></span><h3>Short-term jobs</h3></li>
                    </ul>
                    <button type="button" class="btn btn-secondary Get-Started red">Get Started</button>
                </div>
                <div class="col-md-6" style="background-color: #003043;padding: 100px 100px;">
                    <img src="assets/frontend/img/motion2.gif">
                </div>
            </div>
            <div class="row two">
                <div class="col-md-6" style="background-color: #f9f9f9;padding: 100px 100px;">
                    <h2>Hiring Made Easy</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla aliquam eros et imperdiet luctus.</p>
                    <button type="button" class="btn btn-secondary Get-Started green">Get Started</button>
                </div>
                <div class="col-md-6" style="background-color: #003043;padding: 100px 100px;">
                    <div class="curcle-banner">
                    <img src="assets/frontend/img/motion3.gif">
                    </div>
                </div>
            </div>
            <div class="row three">
                <div class="col-md-6" style="background-color: #f9f9f9;padding: 100px 100px;">
                    <h2>Hire professional for any <br>industry</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla aliquam eros et imperdiet luctus.</p>
                    <ul class="demant-list">
                        <li><span><i class="fas fa-check"></i></span><h3>Complex projects</h3></li>
                        <li><span><i class="fas fa-check"></i></span><h3>Long-term contracts</h3></li>
                        <li><span><i class="fas fa-check"></i></span><h3>Short-term jobs</h3></li>
                    </ul>
                    <button type="button" class="btn btn-secondary Get-Started yello">Get Started</button>
                </div>
                <div class="col-md-6" style="background-color: #003043;padding: 100px 100px;">
                    <div class="curcle-img">
                        <img src="assets/frontend/img/motion4.gif">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="hire-project">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="hare-left">
                        <h2>Hire the Top of <br>Project Managers</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla aliquam eros et imperdiet luctus. In ullamcorper lorem vel ante interdum, ac blandit dolor volutpat. Phasellus vehicula nunc est, ac egestas diam semper sed.</p>
                        <button type="button" class="btn btn-secondary green-bttt ">Hire Top Project Manager</button>
                        <h4>No Risk Trial Pay Only if Satisfied</h4>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="crwford-bg">
                        <ul>
                            <li>
                                <img src="assets/frontend/img/croud1.png">
                                <div class="crwford-box">
                                    <h2>Carole Crawford, CFA</h2>
                                    <h3><span><img src="assets/frontend/img/pen.png"></span>Interim CFO</h3>
                                    <h5>Previously at</h5>
                                    <h4>Morgan Stanley</h4>
                                </div>
                            </li>
                            <li>
                                <img src="assets/frontend/img/croud2.png">
                                <div class="crwford-box">
                                    <h2>Carole Crawford, CFA</h2>
                                    <h3><span><img src="assets/frontend/img/pen.png"></span>Interim CFO</h3>
                                    <h5>Previously at</h5>
                                    <h4>Morgan Stanley</h4>
                                </div>
                            </li>
                            <li>
                                <img src="assets/frontend/img/croud3.png">
                                <div class="crwford-box">
                                    <h2>Carole Crawford, CFA</h2>
                                    <h3><span><img src="assets/frontend/img/pen.png"></span>Interim CFO</h3>
                                    <h5>Previously at</h5>
                                    <h4>Morgan Stanley</h4>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="demand-talent">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <h2>In-demand talent on demand.<br>Justzoom is how.</h2>
                    <p>Justzoom expertly connects professionals and agencies to businesses seeking <br>specialized talent.</p>
                </div>
            </div>
        </div>
        <div class="clinet-say">
            <ul>
                <li>
                    <h3>CLIENT AND AGENCIES</h3>
                    <h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h4>
                    <a href="#">Find out more<span><i class="fas fa-chevron-right"></i></span></a>
                </li>
                <li>
                    <h3>FREELANCER</h3>
                    <h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h4>
                    <a href="#">Find out more<span><i class="fas fa-chevron-right"></i></span></a>
                </li>
            </ul>
        </div>
    </div>
    <div class="testmonial-bg">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="testmonial-title">
                        <h2>Hear From Our Clients</h2>
                        <ul>
                            <li><h3>Our customers say </h3></li>
                            <li><h4>Excellent    <img src="assets/frontend/img/star.png"></h4></li>
                            <li><h4>4.7 out of 5 based on 1,123 reviews<img src="assets/frontend/img/trustpilot.png"></h4></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="testmonial">
                        <div class="test-box">
                            <span><i class="fas fa-quote-left"></i></span>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In molestie neque et tortor pretium, a facilisis justo tempus. Maecenas facilisis maximus vehicula. Proin sem lacus, eleifend sed orci at, fermentum aliquet dolor. Duis ac erat ac augue ullamcorper aliquet nec quis quam. Donec interdum iaculis erat sit amet commodo. Donec eu porttitor magna, id aliquet ligula. </p>
                            <div class="mc-nallay">
                                <img src="assets/frontend/img/aval.png">
                                <div class="testmonial-text">
                                    <h3>Ryan Walker, Director of Product</h3>
                                    <h4>Rand McNally</h4>
                                </div>
                            </div>
                        </div>
                        <div class="test-box">
                            <span><i class="fas fa-quote-left"></i></span>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In molestie neque et tortor pretium, a facilisis justo tempus. Maecenas facilisis maximus vehicula. Proin sem lacus, eleifend sed orci at, fermentum aliquet dolor. Duis ac erat ac augue ullamcorper aliquet nec quis quam. Donec interdum iaculis erat sit amet commodo. Donec eu porttitor magna, id aliquet ligula. </p>
                            <div class="mc-nallay">
                                <img src="assets/frontend/img/aval.png">
                                <div class="testmonial-text">
                                    <h3>Ryan Walker, Director of Product</h3>
                                    <h4>Rand McNally</h4>
                                </div>
                            </div>
                        </div>
                        <div class="test-box">
                            <span><i class="fas fa-quote-left"></i></span>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In molestie neque et tortor pretium, a facilisis justo tempus. Maecenas facilisis maximus vehicula. Proin sem lacus, eleifend sed orci at, fermentum aliquet dolor. Duis ac erat ac augue ullamcorper aliquet nec quis quam. Donec interdum iaculis erat sit amet commodo. Donec eu porttitor magna, id aliquet ligula. </p>
                            <div class="mc-nallay">
                                <img src="assets/frontend/img/aval.png">
                                <div class="testmonial-text">
                                    <h3>Ryan Walker, Director of Product</h3>
                                    <h4>Rand McNally</h4>
                                </div>
                            </div>
                        </div>
                        <div class="test-box">
                            <span><i class="fas fa-quote-left"></i></span>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In molestie neque et tortor pretium, a facilisis justo tempus. Maecenas facilisis maximus vehicula. Proin sem lacus, eleifend sed orci at, fermentum aliquet dolor. Duis ac erat ac augue ullamcorper aliquet nec quis quam. Donec interdum iaculis erat sit amet commodo. Donec eu porttitor magna, id aliquet ligula. </p>
                            <div class="mc-nallay">
                                <img src="assets/frontend/img/aval.png">
                                <div class="testmonial-text">
                                    <h3>Ryan Walker, Director of Product</h3>
                                    <h4>Rand McNally</h4>
                                </div>
                            </div>
                        </div>
                        <div class="test-box">
                            <span><i class="fas fa-quote-left"></i></span>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In molestie neque et tortor pretium, a facilisis justo tempus. Maecenas facilisis maximus vehicula. Proin sem lacus, eleifend sed orci at, fermentum aliquet dolor. Duis ac erat ac augue ullamcorper aliquet nec quis quam. Donec interdum iaculis erat sit amet commodo. Donec eu porttitor magna, id aliquet ligula. </p>
                            <div class="mc-nallay">
                                <img src="assets/frontend/img/aval.png">
                                <div class="testmonial-text">
                                    <h3>Ryan Walker, Director of Product</h3>
                                    <h4>Rand McNally</h4>
                                </div>
                            </div>
                        </div>
                        <div class="test-box">
                            <span><i class="fas fa-quote-left"></i></span>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In molestie neque et tortor pretium, a facilisis justo tempus. Maecenas facilisis maximus vehicula. Proin sem lacus, eleifend sed orci at, fermentum aliquet dolor. Duis ac erat ac augue ullamcorper aliquet nec quis quam. Donec interdum iaculis erat sit amet commodo. Donec eu porttitor magna, id aliquet ligula. </p>
                            <div class="mc-nallay">
                                <img src="assets/frontend/img/aval.png">
                                <div class="testmonial-text">
                                    <h3>Ryan Walker, Director of Product</h3>
                                    <h4>Rand McNally</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
   @include("frontend.layouts.common_register_user")
    <div class="Justzoom-Developers">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="just-title">Justzoom Developers</h2>
                </div>
                <div class="col-md-3">
                    <ul>
                        @foreach($category_list as $category)
                        <li><h3>{{$category}}</h3></li>
                        @endforeach
                    </ul>
                </div>
                
                
                
            </div>
        </div>
    </div>
    <style>
    .connect-demand img {
    width: 100%;
}
    </style>
      
@endsection