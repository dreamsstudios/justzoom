@extends('client.layouts.client_dashboard_layout')

@section('title', 'Justzoom')
@section('body')
<!-- comma seperated input css -->
	<link rel="stylesheet" href="{!! asset('assets/frontend/css/bootstrap-tagsinput.css') !!}">
	 <script type="text/javascript" src= "{!! asset('assets/frontend/js/bootstrap-tagsinput.min.js') !!}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>

    <!-- comma seperated input css -->
	
			<div class="edit-profile-page">
		<div class="container">
			<div class="row profile-information">
				<div class="col-md-12">
					<div class="edit-title">
						<h2>Edit your Profile</h2>
						<h3>Fill in your profile information</h3>
					</div>
					 
					 @if(session("msg"))
                                        <div class="alert-dismiss">
                                            <div class="alert alert-success alert-dismissible fade show">
                                                <span>{{session("msg")}}</span><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span class="fa fa-times"></span> </button><br/>
                                            </div>
                                        </div>
                            @endif
							 @if(session("errormsg"))
                                        <div class="alert-dismiss">
                                            <div class="alert alert-danger alert-dismissible fade show">
                                                <span>{{session("errormsg")}}</span><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span class="fa fa-times"></span> </button><br/>
                                            </div>
                                        </div>
                            @endif		
				</div>
				<div class="col-md-7">
					<div class="profile-bg">
						<div class="profile-photo">
							<h5>Profile Photo</h5>
							<div class="image-upload">
								<?php
									if(!empty($user->image)){
                                             $img = $user->image;
                                         }else{
                                          $img = 'no-img.png';

                                         }
                                         ?>
								<img id="preview_image" src="{{asset('/uploads/user_images/'.$img)}}" alt="profile img"  class="img-responsive" style="width: 20%;"> 
								<i id="loading" class="fa fa-spinner fa-spin fa-3x fa-fw" style="position: absolute;left: 40%;top: 40%;display: none"></i>	
								<label><a class="edit_imgicon" for="file-input" href="javascript:changeProfile()" >
									<span>Change Photo</span>
								</a></label>
								
								
							</div>
							
						</div>
						<div class="profile-form">
							<form class="needs-validation" novalidate="" id="my-form" method="post" action="{{route('saveclientdata')}}" enctype = "multipart/form-data">
										{!!csrf_field()!!}

                             <div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label for="first_name">First Name</label>
											<input type="text" name="first_name" placeholder="Your First Name" value="{{$user->first_name}}" class="form-control">
													{!! $errors->first('first_name', '<p class="help-block">:message</p>') !!}
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label for="last_name">Last Name</label>
											<input type="text" name="last_name" placeholder="Your Last Name" value="{{$user->last_name}}" class="form-control">
													{!! $errors->first('last_name', '<p class="help-block">:message</p>') !!}
										</div>
									</div>
								</div>

                                <div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label for="job_title">Job Title</label>
											<input type="text" name="job_title" class="form-control" id="job_title" placeholder="Enter job title"  value="{{$user->job_title}}">
											{!! $errors->first('job_title', '<p class="help-block">:message</p>') !!}
										</div>
									</div>
									<div class="col-md-6">
										
											<div class="form-group">
												<label for="exampleFormControlSelect2" class="qustion-icon">Phone <span><i class="fas fa-question-circle"></i></span></label>
													
													
													<div class="form-group">
													<input type="text" name="phone" placeholder="Your Number" value="{{$user->phone}}" class="form-control">
													{!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
												</div>
												</div>	
											
										</div>
								</div>

                              <div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<label for="email">Email</label>
											<input type="email" name="email" placeholder="Your Email Id" value="{{$user->email}}"class="form-control">
													{!! $errors->first('email', '<p class="help-block">:message</p>') !!}
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label for="address_line_1">Address</label>
											<input type="text" id="address_line_1" name="address_line_1" placeholder="Enter Address Line 1" value="{{ $user->address_line_1 }} " class="form-control">
													 {!! $errors->first('address_line_1', '<p class="help-block">:message</p>') !!}
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label for="exampleInputPassword3">Country</label>
											<select class="form-control" id="country" name="country" onchange="state_list(this.value,event)" >
													<option  value="">Select Country</option>
													@foreach($country_list as $key=>$category)
															<option {{ $user->country == $key ? 'selected' : '' }} value="{{$key}}"> {{$category}}</option>
														@endforeach
												</select>
													
													 {!! $errors->first('country', '<p class="help-block">:message</p>') !!}
										</div>
									</div>
								</div>
								<div class="row">
									
									<div class="col-md-4">
										<div class="form-group">
											<label for="exampleInputPassword3">State</label>
											<select class="form-control" id="state" name="state" onchange="city_list(this.value,event)" >
												<option  value="">Select State</option>
												@foreach($state_list as $key=>$state)
															<option {{ $user->state == $key ? 'selected' : '' }} value="{{$key}}"> {{$state}}</option>
														@endforeach
												</select>
													 {!! $errors->first('state', '<p class="help-block">:message</p>') !!}
										</div>
										</div>

										<div class="col-md-4">
										<div class="form-group">
											<label for="exampleInputPassword3">City</label>
											<select class="form-control" id="city" name="city" >
													<option value="">Select City</option>
													@foreach($city_list as $key=>$city)
															<option {{ $user->city == $key ? 'selected' : '' }} value="{{$key}}"> {{$city}}</option>
														@endforeach
												</select>
													 {!! $errors->first('city', '<p class="help-block">:message</p>') !!}
												
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label for="exampleInputPassword3">Pincode</label>
											<input type="text" class="form-control" id="pincode" name="pincode" placeholder="Enter pincode" value="{{ $user->pincode }}" >
													 {!! $errors->first('pincode', '<p class="help-block">:message</p>') !!}
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<label for="about">About You</label>
											<textarea class="form-control" id="about" rows="3" name="about" >
												{{ $user->about }}	</textarea>

													 {!! $errors->first('about', '<p class="help-block">:message</p>') !!}
										</div>
									</div>
								</div>
								

									<div class="row">
									<div class="col-md-12">
										<div class="form-cncle-button">

											<button type="submit" class="btn btn-dark save-blue">Update</button>
										</div>
									</div>
								</div>
								
									
								
							</form>
						</div>
					</div>
				</div>
				<div class="col-md-5" style="background-color: #f9f9f9;">

			
			   @if (!empty($user->job_title))
			   	       <?php $hasCompletedJobTitle = 10;?>
			   	@else
			   	      <?php $hasCompletedJobTitle = 0;?>
			    @endif 
			    @if (!empty($user->image))
			   	       <?php $hasCompletedImage = 10;?>
			   	@else
			   	      <?php $hasCompletedImage = 0;?>
			    @endif
			    @if (!empty($user->address_line_1))
			   	       <?php $hasCompletedAddress = 10;?>
			   	@else
			   	      <?php $hasCompletedAddress = 0;?>
			    @endif
			     @if (!empty($user->country))
			   	       <?php $hasCompleteCountry = 10;?>
			   	@else
			   	      <?php $hasCompleteCountry = 0;?>
			    @endif
			    @if (!empty($user->about))
			   	       <?php $hasCompletedAbout = 10;?>
			   	@else
			   	      <?php $hasCompletedAbout = 0;?>
			    @endif
			    @if (!empty($user->first_name))
			   	       <?php $hasCompletedFirstNmae = 10;?>
			   	@else
			   	      <?php $hasCompletedFirstNmae = 0;?>
			    @endif 
			    @if (!empty($user->phone))
			   	       <?php $hasCompletedPhone = 10;?>
			   	@else
			   	      <?php $hasCompletedPhone = 0;?>
			    @endif
			     @if (!empty($user->email))
			   	       <?php $hasCompletedEmail = 10;?>
			   	@else
			   	      <?php $hasCompletedEmail = 0;?>
			    @endif
			     @if (!empty($user->pincode))
			   	       <?php $hasCompletedPincode = 10;?>
			   	@else
			   	      <?php $hasCompletedPincode = 0;?>
			    @endif
			  @if (!empty($user->state))
			   	       <?php $hasCompletedState = 10;?>
			   	@else
			   	      <?php $hasCompletedState = 0;?>
			    @endif
			  
			
			    <?php  $maximumPoints  = 100;

                 $percentage = ($hasCompletedImage+$hasCompletedJobTitle+$hasCompletedAddress+$hasCompletedAbout+$hasCompletedPhone+$hasCompleteCountry+$hasCompletedFirstNmae+$hasCompletedEmail+$hasCompletedPincode+$hasCompletedState)*$maximumPoints/100;

			    ?>
					<div class="profile-lavle">
						<div class="completion-bg">
							<h2>Profile Completion Level: </h2>
							<div class="circle" id="circle-b">
								<strong>{{$percentage}}%<br><span>Complete</span></strong>
							</div>
							<h3>Improve Profile Tips</h3>
							<ul>
								<li>
									<h4>Add your Image</h4><h4><h4>10%</h4></h4>
								</li>
								<li>
									<h4>Add your Title</h4><h4><h4>10%</h4></h4>
								</li>
								<li>
									<h4>Add your About</h4><h4><h4>10%</h4></h4>
								</li>
							</ul>
						</div>
					</div>
				</div>

			
			</div>
		</div>
	</div>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
<script>
    $(function () {
    $('.selectpicker').selectpicker();
});
$(document).on('keyup keypress', 'form input[type="text"]', function(e) {
  if(e.keyCode == 13) {
    e.preventDefault();
    return false;
  }
});
</script>
			<script>
				$('.date').datepicker({
				  multidate: true,
					format: 'dd-mm-yyyy'
				});
			</script>
			<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery-circle-progress/1.1.3/circle-progress.min.js'></script>
  <script>
	/**
*Exampe from https://kottenator.github.io/jquery-circle-progress/
*/
var progressBarOptions = {
	startAngle: -1.50,
	size: 200,
    value: 0.75,
    fill: {
		color: '#ffa500'
	}
}

$('.circle').circleProgress(progressBarOptions).on('circle-animation-progress', function(event, progress, stepValue) {
	//$(this).find('strong').text(String(stepValue.toFixed(2)).substr(1));
});

$('#circle-b').circleProgress({
	value : 0.50,
	fill: {
		color: '#00ab21'
	}
});


  </script>
  <input type="file" id="file" style="display: none"/>
    <input type="hidden" id="file_name"/>
					<script>
   function changeProfile() {
        $('#file').click();
    }
    $('#file').change(function () {
        if ($(this).val() != '') {
            upload(this);

        }
    });
    function upload(img) {
        var form_data = new FormData();
        form_data.append('file', img.files[0]);
        form_data.append('_token', '{{csrf_token()}}');
        $('#loading').css('display', 'block');
        $.ajax({
            url: "{{url('ajax-client-image-upload')}}",
            data: form_data,
            type: 'POST',
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.fail) {
                    alert(data.errors['images']);
                }
                else {
                    $('#file_name').val(data);
                    $('#preview_image').attr('src', '{{asset('/uploads/user_images/')}}/' + data);
                }
                $('#loading').css('display', 'none');
            },
            error: function (xhr, status, error) {
                alert(xhr.responseText);
                $('#preview_image').attr('src', '{{asset('images/noimage.jpg')}}');
            }
        });
    }

   
</script>







@endsection
