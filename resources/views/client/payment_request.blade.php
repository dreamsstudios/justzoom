@extends('client.layouts.client_dashboard_layout')

@section('title', 'Justzoom')
@section('body')

<div class="client-page">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="deshbord-title">
						<h2>Payment Request (<?= $countReq ?>)</h2>
						<h4>Date: <?= date('d-m-y')?></h4>
					</div>
					
				</div>
			</div>
			<div class="payments-reqest Management-bottom">
			<div class="alert alert-danger" style="display:none"></div>
	              <div class="alert alert-success" style="display:none"></div>
				<div class="row">
				    @if(!empty($pendingRequest) && count($pendingRequest)>0)
					@foreach($pendingRequest as $req)
					<?php
					if(!empty($req->image)){
								$img = $req->image;
							} else {	
								$img = 'no-img.png';
							}
					
					?>
					<div class="col-md-6">
						<div class="sheman-box">
							<div class="sheman-profile">
								<div class="table-profile">
									<img src="{{asset('/uploads/user_images/'.$img)}}">
									<div class="post-text">
										<h3>{{ucfirst($req->first_name.' '.$req->last_name)}}</h3>
										<p>{{ucfirst($req->category_name)}}</p>
									</div>
								</div>
								<div class="sheman-hr">
									<h4></h4>
								</div>
							</div>
							<div class="admin-bg">
								<p>Hi {{$user->first_name}}</p>
								<p>Hope you’re doing well. This is just to remind you that the Contract ID #{{$req->contractId}} is done so please release fund.</p>
							</div>
							<div class="resaled-btt">	
								<a href="{{asset('/client/contract-detail/'.$req->contractId)}}"><button type="button" class="btn btn-success blue">VIEW CONTRACT</button></a>
								@if(!empty($req->cardSource))
								<button type="button" class="btn btn-success green" id="payBysource" onclick="payTouser('<?php echo $req->contractId; ?>')">RELEASE FUND</button>
							    @else
								<button type="button" class="btn btn-success green" data-toggle="modal" data-target="#releaseFund">RELEASE FUND</button>
								
								@endif
								@include("frontend.cardDetailPopup") 
							</div>
						</div>
					</div>
					@endforeach
					@else
					<div class="col-md-12">No Request Found.</div>
					@endif

					<?php
					if(isset($pendingRequest) && !empty($pendingRequest)){
								?>
							<div class="paginating">
								<ul class="pagination searchpagination">
									{{ $pendingRequest->links() }}	
								</ul>
							</div>
							<?php
							}?>
					
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<div class="post-table">
						<h2 class="post-title">Past Work</h2>
						<div class="table-responsive-sm">
							<table class="table table-bordered">
							  <thead>
								<tr style="background-color: #f9f9f9;">
								  <th scope="col" style="text-align: left;"><h4>Freelancer</h4></th>
								  <th scope="col"><h4>Date</h4></th>
								  <th scope="col"><h4>Contract ID</h4></th>
								  <th scope="col"><h4>Paid Amount</h4></th>
								  <th scope="col"><h4>Payment Method</h4></th>
								  <th scope="col"><h4>Status</h4></th>
								  <th scope="col"><h4>Action</h4></th>
								</tr>
							  </thead>
							  <tbody>
							  	@if(!empty($pastWork) && count($pastWork) > 0)
								   @foreach($pastWork as $past)
								<?php
									if(!empty($req->image)){
												$img = $req->image;
											} else {	
												$img = 'no-img.png';
											}
									
								?>
								<tr>
								  <th scope="row" style="text-align: left;">
									<div class="table-profile">
										<img src="{{asset('/uploads/user_images/'.$img)}}">
										<div class="post-text">
										<h3>{{ucfirst($past->first_name.' '.$past->last_name)}}</h3>
										<p>{{ucfirst($past->category_name)}}</p>
										</div>
									</div>
								  </th>
								  <td><p>{{date('d-m-y',strtotime($past->created_at))}}</p></td>
								  <td><p>#{{$past->contractId}}</p></td>
								  <td><p>£{{$past->amount}}</p></td>
								  <td><p>Stripe</p></td>
								  <td><button type="button" class="btn btn-success table-done">Done</button></td>
								  <td>
									<div class="dropdown dot-button">
									  <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<img src="{!! asset('assets/frontend/img/dot.png') !!}">
									  </button>
									  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
										<a class="dropdown-item" href="{{asset('/client/contract-detail/'.$past->contractId)}}">Contract detail</a>
										
									  </div>
									</div>
								</td>
								</tr>
								
								@endforeach
								@else
								<tr><td colspan="7">No record Found!</td></tr>
								@endif
							  </tbody>
							</table>
						</div>
						<?php
					if(isset($pastWork) && !empty($pastWork)){
								?>
							<div class="paginating">
								<ul class="pagination searchpagination">
									{{ $pastWork->links() }}	
								</ul>
							</div>
							<?php
							}?>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
