@extends('client.layouts.client_dashboard_layout')

@section('title', 'Justzoom')
@section('body')

<div class="banner_search">
				<h1><?php echo $pageTitle; ?></h1>				
			</div>	
			<div class="search_main">
				<div class="container">
					<div class="row">
						 @include("client.layouts.client_dashboard_sidebar")
						 <div class="col-12 col-md-8 col-lg-8 col-sm-7">
						 @if(session("msg"))
                                        <div class="alert-dismiss">
                                            <div class="alert alert-success alert-dismissible fade show">
                                                <span>{{session("msg")}}</span><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span class="fa fa-times"></span> </button><br/>
                                            </div>
                                        </div>
                            @endif
							 @if(session("errormsg"))
                                        <div class="alert-dismiss">
                                            <div class="alert alert-danger alert-dismissible fade show">
                                                <span>{{session("errormsg")}}</span><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span class="fa fa-times"></span> </button><br/>
                                            </div>
                                        </div>
                            @endif
							<div class="hire_companies work_history current_task">
								<h2><span><i class="flaticon-task-complete black"></i></span>Job Request</h2>
								<ul>
								<?php
									if(isset($jobs) && count($jobs) > 0){
										foreach($jobs as $job){
											$jobRequestData = jobrequestData($job->id);
									?>
									<li>
										<div class="posted">
											<h3><b><a href="{{asset('/client/job/job-detail/'.$job->id)}}">{{ucfirst($job->title)}}</a>
											
										
											</b>{{$job->address}},
											{{$job->city}}, {{$job->state}}, {{$job->country}}</h3>
											<time class="calen"><img src="{!! asset('assets/frontend/img/cal.svg') !!}" alt="">{{date('d/m/y', strtotime($job->job_start_on))}} - {{date('d/m/y', strtotime($job->job_end_on))}}
											</time>
										</div>
										<div class="price">Price : <span> {{Config::get('app.site_currency_symbol')}}  {{$job->price}}</span> </div>
										<div class="price">Job Type : <span> @if($job->job_type == '0') Open @else Private @endif </span> </div>
										
										<p>
										{{$job->description}}
										</p>
									</li>
									<?php
										}
									}else{
										?>
										<li>No record found.</li>
										<?php
										}
										?>
								</ul>
							</div>
							<div class="paginating pagination-section">
								<ul class="pagination">
									{{ $jobs->links() }}	
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			
@endsection
