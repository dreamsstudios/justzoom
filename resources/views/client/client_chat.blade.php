@extends('client.layouts.chatlayout')

@section('title', 'Justzoom')
@section('body')

<div class="chatting-page">
		<div class="chat-profile">
			
	        <div id="sidebar">
	        	<div class="sidebar-btn">
				<button type="button" id="sidebarbtn" class="btn btn-info">
	                <i class="fa fa-angle-left"></i>
	            </button>
	        </div>
	        <div class="sidebar-inner">
			<div class="chat-user-details">
				<div class="chat-user-img">
				<?php
										if(!empty($chat->image)){
											$img = $chat->image;
										}else{
											$img = 'no-img.png';
											}
                                         ?>
					 <img src="{{asset('/uploads/user_images/'.$img)}}" alt="user image">
					<div class="brand-logo">
						<img src="{!! asset('assets/frontend/img/chat-logo.png') !!}" alt="">
					</div>
				</div>
				<div class="chat-user-profile">
				<h2>{{ucfirst($chat->first_name.' '.$chat->last_name)}}
				<span><i class="fa fa-check"></i> Verified</span>
				</h2>
				<h4>{{$chat->category_name}} @if($chat->city)in {{$chat->city}}@endif</h4>
					<h4><span><i class="fa fa-location"></i></span> {{$chat->country}}</h4>
					<h4 class="active-time">Active 2 hours ago</h4>
					<div class="hired-profile-btn">
						@if($chat->contractId==null ||$chat->contractId<=0)
						<a href="{{asset('/hire-freelancer/'.$chat->reqId)}}"><button type="button" class="btn user-hired-btn">HIRED {{$chat->first_name}}</button></a>
					     @elseif($chat->paymentId==null ||$chat->paymentId<=0)
						 <a href="{{asset('/payment/'.$chat->contractId)}}"><button type="button" class="btn user-hired-btn">Pay Platform charge</button></a>
						 @else
						 <a href="{{asset('/client/contract-detail/'.$chat->contractId)}}"><button type="button" class="btn user-hired-btn">VIew Contract</button></a>

						@endIf

						<a href="{{asset('/freelancer-detail/'.$chat->userId)}}" class="full-profile-btn">View Full Profile</a>
					</div>
				</div>
			</div>
			<div class="chat-user-statics">
				<ul>
											
				    <li>
						<div class="raiting">
						<?php 
											$remain = 5-$totalRating->average_rating;
											for($i=1; $i<=$totalRating->average_rating;$i++){
												echo '<span class="fa fa-star checked"></span>';
											}
											if($remain>0){
												for($i=1; $i<=$remain;$i++){	
													echo '<span class="fa fa-star"></span>';
												}

											}
						?>
						</div>
						<p><span>{{$totalRating->average_rating}}</span> ({{$totalRating->totalreview}})</p>
					</li>
					<li>
						<h3>{{$clients}}</h3>
						<p>Customers</p>
					</li>
					<li>
						<h3>{{$chat->response_time}} hr</h3>
						<p>Response Time</p>
					</li>
					<li>
						<h3>£{{$chat->hourly_rate}} hr</h3>
						<p>Hourly Rate</p>
					</li>
				</ul>
			</div>
			<div class="chat-user-gallery">
				<h3 class="title">Gallery</h3>
				<ul>
					<li>
						<img src="{!! asset('assets/frontend/img/gallery1.png') !!}" alt="gallery1">
					</li>
					<li>
						<img src="{!! asset('assets/frontend/img/gallery2.png') !!}" alt="gallery2">
					</li>
				</ul>
			</div>
			<div class="about-chat-user">
				<h3 class="title">About</h3>
				<p>@if($chat->about){{$chat->about}}@else N/A @endif</p>
			</div>
			<div class="chat-user-review">
				<h3 class="title">Total Reviews <span>(<?php echo $reviewCount; ?>)</span></h3>
				<?php
				if(isset($reviews) && !empty($reviews))
				{
					foreach($reviews as $rew){
						if(!empty($rew->image)){
						$img = $rew->image;
						}else{
						$img = 'no-img.png';
						}
				    ?>
					<div class="user-review-details">
					<div class="review-img">
						<img src="{{asset('/uploads/user_images/'.$img)}}" alt="">
					</div>
					<div class="review-details">
						<h4>{{$rew->first_name}} {{$rew->last_name}}</h4>
						<div class="raiting">
						<?php
							if(isset($rew->rating)){
							
							$remain = 5-$rew->rating;
							for($i=1; $i<=$rew->rating;$i++){
							echo '<span class="fa fa-star checked"></span>';
							}
							if($remain>0){
							for($i=1; $i<=$remain;$i++){	
							echo '<span class="fa fa-star"></span>';
							}

							}
							}
							?>
						</div>
						<p>{{$rew->review}}</p>
					</div>
				</div>

                    <?php
					}

				}else{
					echo "<div>No review Found</div>";

				}
				?>
				
			</div>
		</div>
		</div>
	</div>
	<div  id="msg123">
						
			            	
			          	</div>
		<div class="chatbox" id="content12">
			<div class="messaging">
			    <div class="inbox_msg">
			        <div class="mesgs">
			          	<div class="msg_history" >
						  <?php
							if(isset($chatDetail) && !empty($chatDetail)){
								$previousDate = date('d M Y');
								foreach($chatDetail as $chatm){
									$date = date('d M Y',strtotime($chatm->created_at));
									if($previousDate != $date){
										$previousDate = $date;
										?>
									<div class="chat-date">{{date("l",strtotime($chatm->created_at)).' '.$previousDate}}</div>
									<?php	
									}
									

							?>
							
							
							@if($chatm->senderType == 'user')
							<div class="incoming_msg">
				              <div class="incoming_msg_img"> <img src="{!! asset('assets/frontend/img/chat.png') !!}" alt="sunil"> </div>
				              <div class="received_msg">
				                <div class="received_withd_msg">
				                	<div class="chat-time-detail">
					                	<p>{{$chat->first_name.' '.$chat->last_name}}</p>
					                	<p>{{date('h:i A',strtotime($chatm->created_at))}}</p>
					                </div>
				                	<p>{{$chatm->message}}</p>
				                  </div>
				              </div>
							</div>
							@endif
							@if($chatm->senderType == 'client')
				            <div class="outgoing_msg">
				              	<div class="sent_msg">
					              	<div class="chat-time-detail">
					              		<p>{{date('h:i A',strtotime($chatm->created_at))}}</p>
					                	<p>You</p>                	
					                </div>
				                	<p>{{$chatm->message}}</p>
				            	</div>
				                <div class="incoming_msg_img"> 
				                	<img src="{!! asset('assets/frontend/img/chat.png') !!}" alt="sunil"> 
				                </div>
				            </div>
							@endif

							<?php
								}
							}else{

							}
							?>
						
			            	
			          	</div>
					</div>
					<input type="hidden" value="<?php echo (isset($lastId->id)?$lastId->id:0);?>" id="lastId" />
					  <form action="#" id="msgSend" class="needs-validation" onsubmit="messageSend(this,event)" novalidate>
						@csrf
				        <div class="type_msg">
				            <div class="input_msg_write">
								<input type="hidden" value="ajax_sendMessage" name="action" /> 
								<input type="hidden" value="{{$chat->chatId}}" name="chatId" id="chatId"/>
								<input type="hidden" name="type" value="msg" id="type"/>
				            	<input type="text" class="write_msg" name="message" id="message" placeholder="Type your message" />
				            	<button class="msg_send_btn" type="submit"><i class="fa fa-paper-plane" aria-hidden="true"></i>Send</button>
				            </div>
						</div>
						</form>
			    </div> 
			</div>
		</div>
	</div>
	<script type="text/javascript">

  $(document).ready(function() {
   
    $(".mesgs").animate({ scrollTop: $('.mesgs').prop("scrollHeight")}, 1000);
    setInterval(function () { if(isAjaxEngaged == 0) getMessages('<?php echo  $chat->chatId;?>') } , 4000);
});
function getMessages(chatId){
	var lastChId = $('#lastId').val();
   
    $.ajax({
        url: COMMONURL,
        type: "POST",
        data: {"action": "getMessages",_token:_token , "chatId": chatId,'lastChId':lastChId},
        success:function(response){
			
			$('#lastId').val(response.lastId);
			if(response.data !==''){
				$('.msg_history').append(response.data);
                $(".mesgs").animate({ scrollTop: $('.mesgs').prop("scrollHeight")}, 1000);

			}
        
         
        },
        error:function(response){
        },
        beforeSend: function( xhr ) {
            isAjaxEngaged = 1;
        },
        complete:function(){
             isAjaxEngaged = 0;
        },
    });
}
</script>	
	
@endsection
