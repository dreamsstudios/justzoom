<div class="deshbord-footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <p>Copyright © 2020 Justzoom. All Right Reserved.</p>
                </div>
                <div class="col-md-6">
                    <ul class="social-link">
                        <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                        <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src= "{!! asset('assets/frontend/js/popper.min.js') !!}"></script>
    <script type="text/javascript" src= "{!! asset('assets/frontend/js/bootstrap.min.js') !!}"></script>
        <script type="text/javascript" src= "{!! asset('assets/frontend/js/custom.js') !!}"></script>

<script type="text/javascript">
      $(document).ready(function() {
  $(".jumper").on("click", function( e ) {

    e.preventDefault();

    $("body, html").animate({ 
      scrollTop: $( $(this).attr('href') ).offset().top 
    }, 600);

  });
});
    </script>