@extends('client.layouts.client_dashboard_layout')

@section('title', 'Justzoom')
@section('body')

<div class="client-page">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="deshbord-title">
						<h2>Dashboard <span>Welcome back, {{ucfirst($user->first_name.' '.$user->last_name)}}</span></h2>
						<a href="{{asset('/hire-to-talent')}}"><button type="button" class="btn btn-link">Hired Freelancer</button></a>
					</div>
					<ul class="deshbord-list">
						<li>
							<img src="assets/frontend/img/desh1.png">
							<div class="desh-text">
								<h4>Hired Freelancer</h4>
								<h5>{{$hired_freelancer}}</h5>
							</div>
						</li>
						<li>
							<img src="assets/frontend/img/desh2.png">
							<div class="desh-text">
								<h4>Ongoing Work</h4>
								<h5>{{$ongingWork}}</h5>
							</div>
						</li>
						<li>
							<img src="assets/frontend/img/desh3.png">
							<div class="desh-text">
								<h4>In Escrow</h4>
								<h5>£0</h5>
							</div>
						</li>
						<li>
							<img src="assets/frontend/img/desh4.png">
							<div class="desh-text">
								<h4>Payments Request</h4>
								<h5>{{$paymentRequest}}</h5>
							</div>
						</li>
					</ul>
				</div>
			</div>
			<div class="row">
			<?php
				if(isset($request) && !empty($request)){
					foreach($request as $req){
						if($req->contractId>0){
							if(!empty($req->image)){
								$img = $req->image;
							} else {	
								$img = 'no-img.png';
							}

					?>
				<div class="col-md-6">
					<div class="hire-web">
						<div class="hire-img">
							<h5>Hire</h5>
						</div>
						<h6>{{$req->category_name}}</h6>
						<h3></h3>
						<div class="wirter-hire">
						<div class="hire-profile">
							<div class="hire-user-img">
								<img src="{{asset('/uploads/user_images/'.$img)}}" alt="user image">
								<div class="brand-logo">
									<img src="assets/frontend/img/chat-logo.png" alt="">
								</div>
							</div>
							<div class="hire-user-profile">
								<div class="mon-profilr">
								<h2>{{ucfirst($req->first_name.' '.$req->last_name)}}</h2>
								@if($req->is_user_verified==1)
								<span><i class="fa fa-check"></i> Verified</span>
								@endif
								</div>
								<h4>{{$req->category_name}}</h4>
								<div class="raiting">
								<?php
									$totalrating = getUserTotalRating($req->userId);
								   ?>
											
											<?php 
											$remain = 5-$totalrating->average_rating;
											for($i=1; $i<=$totalrating->average_rating;$i++){
												echo '<span class="fa fa-star checked"></span>';
											}
											if($remain>0){
												for($i=1; $i<=$remain;$i++){	
													echo '<span class="fa fa-star"></span>';
												}

											}
										?>
									
									<p><span>{{$totalrating->average_rating}}</span> ({{$totalrating->totalreview}})</p>
								</div>
								<div class="view-contract">
								   @if($req->paymentId >0)
									<a href="{{asset('/client/chat/'.$req->chatId)}}">
										<button type="button" class="btn btn-link bcontact-button con-green">Contact</button>
									</a>
									<div class="view-img-con">
										<span><img src="assets/frontend/img/pad.png"></span>
										<a href="{{asset('/client/contract-detail/'.$req->contractId)}}">View Contract</a>
									</div>
									@else
									<a href="{{asset('/payment/'.$req->contractId)}}">
										<button type="button" class="btn btn-link bcontact-button con-green">Pay Platform charge</button>
									</a>
									@endif
								</div>
							</div>
							</div>
							@if(empty($req->paymentId))
							<div class="alert alert-warning" role="alert">To process Contract firstly pay platform Charge.</div>
							@endif
						</div>
					</div>
				</div>
				<?php
				}else{
					
					if($req->chatId>0)
					$unreadMsg = unreadMsgCount($req->chatId);
					else
					$unreadMsg = 0;
					
					
				?>
				<div class="col-md-6">
					<div class="hire-web">
						<div class="red-bg-1"><h6>{{$req->category_name}}@if($req->chatId>0 && $unreadMsg>0)
						<span>{{$unreadMsg}}</span>@endif</h6>
						</div>
						<h3></h3>
							<div class="wirter-hire">
								<p>{{$req->message}}</p>
							</div>

							<div class="hire-btt">
							@if($req->chatId>0)
							<a href="{{asset('/client/chat/'.$req->chatId)}}">
								<button type="button" class="btn btn-link bcontact-button Hire-blu">Chat</button></a>
							@endif
							@if($req->requestStatus==1)
							<a   href="{{asset('/hire-freelancer/'.$req->reqId)}}"><button type="button" class="btn btn-link bcontact-button Hire-blu">Hire Now</button></a>
							@elseif($req->requestStatus==0)
							<div class="alert alert-warning" role="alert">Please Wait for client Respond</div>
							
							@else
							<button type="button" class="btn btn-link bcontact-button Hire-blu">Dcliened Request</button>
							@endif
							</div>
					</div>
				</div>
				<?php
				}?>
				<?php
					}
				?>
				
				
				<?php
				}?>
			</div>
			     @if(!empty($request))
				<div class="paginating">
								<ul class="pagination searchpagination">
									{{ $request->links() }}	
								</ul>
				</div>
				@endif
				

			<div class="row">
				<div class="col-md-12">
					<div class="post-table">
						<h2 class="post-title">Past Work</h2>
						<div class="table-responsive-sm">
							<table class="table table-bordered">
							  <thead>
							  <tr style="background-color: #f9f9f9;">
								  <th scope="col" style="text-align: left;"><h4>Freelancer</h4></th>
								  <th scope="col"><h4>Contract ID</h4></th>
								  <th scope="col"><h4>category Name</h4></th>
								  <th scope="col"><h4>Duration</h4></th>
								  <th scope="col"><h4>Paid Amount</h4></th>
								  <th scope="col"><h4>Ratings</h4></th>
								  <th scope="col"><h4>Status</h4></th>
								  <th scope="col"><h4>Action</h4></th>
								</tr>
							  </thead>
							  <tbody>
							  <?php
			 					if(isset($pastWork) && !empty($pastWork) && count($pastWork) >0){
			 				?>
								 <?php
							   foreach($pastWork as $past){
								if(!empty($past->image)){
									$img = $past->image;
								} else {	
									$img = 'no-img.png';
								}
								   
							   ?>
							   <tr>
								  <th scope="row" style="text-align: left;">
									<div class="table-profile">
										<img src="{{asset('/uploads/user_images/'.$img)}}">
										<div class="post-text">
								<h3 >{{$past->first_name.' '.$past->last_name}}</h3>
											<p>{{$past->user_category_name}} @if($past->city_name)in {{$past->city_name}}@endif {{$past->state_name}}</p>
										</div>
									</div>
								  </th>
								  <td><p>#{{$past->contractId}}</p></td>

								  <td><p>{{ucfirst($past->category_name)}}</p></td>
								  <td><p>{{$past->duration}} days</p></td>
								  <td><p>£{{$past->totalPrice}}</p></td>

								  <td>
									  <div class="raiting">
									  <?php
											if(isset($past->rating)){
											?>
											<div class="raiting">
											<?php 
											$remain = 5-$past->rating;
											for($i=1; $i<=$past->rating;$i++){
												echo '<span class="fa fa-star checked"></span>';
											}
											if($remain>0){
												for($i=1; $i<=$remain;$i++){	
													echo '<span class="fa fa-star"></span>';
												}

											}
										}else{
										?>
										<a href="javascript:void(0);"  data-toggle="modal" data-target="#review">write review</a>
										@include("frontend.review")    
                                        <?php
										}
									    ?>
											
										</div></td>
								  <td><button type="button" class="btn btn-success table-done">Done</button></td>
								  <td>
									<div class="dropdown dot-button">
									  <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<img src="assets/frontend/img/dot.png">
									  </button>
									  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
										<a class="dropdown-item" href="{{asset('/client/contract-detail/'.$past->contractId)}}">contract Detail</a>
										
									  </div>
									</div>
								</td>
								</tr>
								
							   <?php
							   }
							   ?>
								<?php
								}else{
									echo "<tr><td colspan='8'>No Record Found</td></tr>";
								}
								?>
							  </tbody>
							</table>
							<?php
					if(isset($pastWork) && !empty($pastWork)){
								?>
							<div class="paginating">
								<ul class="pagination searchpagination">
									{{ $pastWork->links() }}	
								</ul>
							</div>
							<?php
							}?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
