@extends('client.layouts.client_dashboard_layout')

@section('title', 'Justzoom')
@section('body')

<div class="banner_search">
				<h1><?php echo $pageTitle; ?></h1>				
			</div>	
			<div class="search_main">
				<div class="container">
					<div class="row">
						 @include("client.layouts.client_dashboard_sidebar")
						 <div class="col-12 col-md-8 col-lg-8 col-sm-7">
						 @if(session("msg"))
                                        <div class="alert-dismiss">
                                            <div class="alert alert-success alert-dismissible fade show">
                                                <span>{{session("msg")}}</span><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span class="fa fa-times"></span> </button><br/>
                                            </div>
                                        </div>
                            @endif
							 @if(session("errormsg"))
                                        <div class="alert-dismiss">
                                            <div class="alert alert-danger alert-dismissible fade show">
                                                <span>{{session("errormsg")}}</span><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span class="fa fa-times"></span> </button><br/>
                                            </div>
                                        </div>
                            @endif
							<div class="hire_companies work_history task-details">
								<div>
								<h2 style="float:left;"> {{$job_detail->title}}</h2>
								<span style="float:right;">
								@if($job_detail->status == 0 &&  $countAccetJob < 1)
									<!--<a href="javascript:void(0);" onclick="editjobrequest('<?php echo  $job_detail->id; ?>',event)"><i class="fa fa-pen"></i></a>
									<a href="javascript:void(0);" onclick="deletejob('<?php echo  $job_detail->id; ?>',event)"><i class="fa fa-trash"></i></a>-->
								@endif
								</span>
								</div>
								<h3>Category Name : {{$job_detail->category_name}}</h3>
								<h3>Job Type : @if($job_detail->job_type == 1) Private @else Open @endif</h3>
								<?php if(isset($job_detail->first_name) && !empty($job_detail->last_name))
								{
								?>
								<p>Freelancer Name: <b>
								<a href="{{asset('/gaurd-detail/'.$job_detail->user_id)}}">{{ $job_detail->first_name.' '.$job_detail->last_name}}</a></b></p>
									
								<?php
								}else if($job_detail->job_type == 1){
									
								?>
									<p>Freelancer Name: <b>
									<a href="{{asset('/gaurd-detail/'.$joBRequests->user_id)}}">{{ $joBRequests->first_name.' '.$joBRequests->last_name}}</a></b></p>
								<?php
								}
								?>
								<p>{{$job_detail->address}}, {{$job_detail->city}},
								{{$job_detail->state}}, 
								{{$job_detail->country}}</p>
								<p>Pincode:  {{$job_detail->pincode}}</p>
								<time class="calen"><img src="{!! asset('assets/frontend/img/cal.svg') !!}" alt="">{{date('d/m/Y', strtotime($job_detail->job_start_on))}} - {{date('d/m/Y', strtotime($job_detail->job_end_on))}}</time>
								<p>Working Hour : {{$job_detail->working_hour}} hour
								</p>
								<p>Offer Type: @if($job_detail->price_type == 0)
								{{'Fixed'}}
								@else
								{{'Open'}}
								@endif
								</p>
								<p>Price : {{Config::get('app.site_currency_symbol')}} {{$job_detail->price}}</p>
								<p>Job Created Date : {{date('d/m/Y', strtotime($job_detail->job_created_date))}}</p>
								<p>Job Status : <?php
									if($job_detail->status == 0){
										echo "Pending";
									}else if($job_detail->status == 1){
										echo "Ongoing";
									}
								?></p>
								@if(isset($job_detail->payment_date))	
								<p>Payment Status : 
								{{'Done'}}
								</p>
								<p>
								Payment Date: {{$job_detail->payment_date}}
								</p>
								<p>
								Payment Token: {{$job_detail->token}}
								</p>
								@endif


							<p class="discription">
								{{$job_detail->description}}
							</p>
							</div>
							@if($job_detail->job_type == 0) 					
							<!-- Bid List -->
							<div class="alert alert-success" style="display:none"></div>
                            <div class="alert alert-danger" style="display:none"></div>
							<div class="bid-details">
								<h3>Requests</h3>
								<?php 
								if(isset($joBRequests) && !empty($joBRequests)){
									foreach($joBRequests as $job){
										if(!empty($job->image)){
											$img = $job->image;
										} else {	
											$img = 'no-img.png';
										}
									?>
										<div class="freelancer_list">
									<div class="profile">
										<div class="img_fP">
											<img src="{{asset('/uploads/user_images/'.$img)}}" alt="">
										</div>
										<div class="name_loct">
											<h3> <a href="{{asset('/gaurd-detail/'.$job->user_id)}}">{{ucfirst($job->first_name).' '.ucfirst($job->last_name)}}</a></h3>
											<h4>Private Security</h4>
												<h4>
													<span>Offer Price</span>
													<b>{{Config::get('app.site_currency_symbol')}}  {{$job->offer_price}}</b>
												</h4>
												<h4>
												<span>Offer given By:</span>
													<b>@if($job->offer_by == 'client') You @else  User @endif</b>
											</h4>
												<h4>
												<span>Offer Status:</span>
													<b>
													<?php
													if($job->job_status == '0'){
														echo "Pending";
													}else if($job->job_status == 1){
														echo ($job->offer_by == 'client')?'Accepted by User':'Accepted by You';
													
													}else{
														echo ($job->offer_by == 'client')?'Declined by User':'Declined by You';
													
													}
													?>
													

													</b>
											</h4>	
											
										</div>
										<div class="star_rate">
										<?php
											if($job->job_status == 1 && $job->is_payment != 1 && !isset($job_detail->user_id)){
										?>
												<form method="POST" action="{{ route('payment') }}">
												{!!csrf_field()!!}
												<input type="hidden" name="job_req_id" value="{{$job->id}}">
											    <input type="hidden" name="price" value="{{$job->offer_price}}">
												<input type="hidden" name="job_id" value="{{$job->job_id}}">
												<input type="hidden" name="payment_user_id" value="{{$job->user_id}}">
												<input type="hidden" name="client_id" value="{{$job->client_id}}">
											<button type="submit" class="btn btn-primary">Pay Now</button>
											</form>
										<?php	
										}else if($job->job_status == 0 && $job->offer_by == 'user'  && !isset($job_detail->user_id) && $countAccetJob < 1){
											?>
											<button type="button" class="btn btn-success" onclick="change_status('1',{{$job->id}})">Accept</button>
											<button type="button" class="btn btn-danger" onclick="change_status('2',{{$job->id}})">Declined</button>
										
										<?php
										}else if($job->job_status == '2' && $job->price_type == '1' && !isset($job_detail->user_id) && $countAccetJob < 1){
											?>
											<button type="button" class="btn btn-warning" onclick="makeanoffer('<?php echo  $job->job_id; ?>','<?php echo session::get('role'); ?>',event,'<?php echo $job->user_id; ?>')">Make an offer</button>	

										<?php	
										}
										?>	
										</div>
									</div>
								</div>
									<?php
									}

								}
								?>
								<div class="paginating pagination-section">
								<ul class="pagination">
									{{ $joBRequests->links() }}	
								</ul>
							</div>
							</div>
						@else

                        <div class="bid-details">
								<h3>Request</h3>
								<?php 
								if(isset($joBRequests) && !empty($joBRequests)){
									$job = $joBRequests;
									if(!empty($job->image)){
										$img = $job->image;
									} else {	
										$img = 'no-img.png';
									}
									?>
										<div class="freelancer_list">
									<div class="profile">
										<div class="img_fP">
											<img src="{{asset('/uploads/user_images/'.$img)}}" alt="">
										</div>
										<div class="name_loct">
											<h3> <a href="{{asset('/gaurd-detail/'.$job->user_id)}}">{{ucfirst($job->first_name).' '.ucfirst($job->last_name)}}</a></h3>
											<h4>Private Security</h4>
											
												<h4>
													<span>Offer Price: </span>
													<b>{{Config::get('app.site_currency_symbol')}}  {{$job->offer_price}}</b>
												</h4>
											<h4>
												<span>Offer given By:</span>
													<b>@if($job->offer_by == 'client') You @else  User @endif</b>
											</h4>
											<h4>
												<span>Offer Status:</span>
													<b>
													<?php
													if($job->job_status == '0'){
														echo "Pending";
													}else if($job->job_status == 1){
														echo ($job->offer_by == 'user')?'Accepted by User':'Accepted by You';
													
													}else{
														echo ($job->offer_by == 'user')?'Declined by User':'Declined by You';
													
													}
													?>
													

													</b>
											</h4>
										
										
										</div>
										<div class="star_rate">
										<?php
											if($job->job_status == 1 && $job->is_payment != 1 && !isset($job_detail->user_id)){
										?>
												<form method="POST" action="{{ route('payment') }}">
												{!!csrf_field()!!}
												<input type="hidden" name="job_req_id" value="{{$job->id}}">
											    <input type="hidden" name="price" value="{{$job->offer_price}}">
												<input type="hidden" name="job_id" value="{{$job->job_id}}">
												<input type="hidden" name="payment_user_id" value="{{$job->user_id}}">
												<input type="hidden" name="client_id" value="{{$job->client_id}}">
											<button type="submit" class="btn btn-primary">Pay Now</button>
											</form>
										<?php	
										}else if($job->job_status == 0 && $job->offer_by == 'user'  && !isset($job_detail->user_id)){
											?>
											<button type="button" class="btn btn-success" onclick="change_status('1',{{$job->id}})">Accept</button>
											<button type="button" class="btn btn-danger" onclick="change_status('2',{{$job->id}})">Declined</button>
										
										<?php
										}else if($job->job_status == '2' && $job->price_type == '1' && !isset($job_detail->user_id)){
											?>
											<button type="button" class="btn btn-warning" onclick="makeanoffer('<?php echo  $job->job_id; ?>','<?php echo session::get('role'); ?>',event,'<?php echo $job->user_id; ?>')">Make an offer</button>	

										<?php	
										}
										?>	
										</div>
									</div>
								</div>
									<?php
									}
								?>
							</div>
						@endif	

						</div>
					</div>
				</div>
			</div>
			@include("frontend.layouts.make_an_offer")
@endsection
