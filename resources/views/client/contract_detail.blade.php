@extends('client.layouts.client_dashboard_layout')

@section('title', 'Justzoom')
@section('body')


<div class="profile-page">
		<div class="row">
			<div class="col-md-5" style="background-color: #f9f9f9;">
				<div class="profile-adrien">
					<div class="profile-img">
						<div class="left-aero">
						<a href="{{route('client-contract-list')}}">
							<img src="{!! asset('assets/frontend/img/left-arrow.png') !!}">
							</a>
						</div>
						<?php
						if(!empty($contract_detail->image)){
							$img = $contract_detail->image;
						} else {	
							$img = 'no-img.png';
						}
						?>
						<img src="{{asset('/uploads/user_images/'.$img)}}">
						<h2>{{ucfirst($contract_detail->first_name .' '.$contract_detail->last_name)}}</h2>
						<h3>{{ucfirst($contract_detail->user_category_name)}}</h3>
						<div class="star--list">
							
											<div class="raiting">
											<?php 
											$remain = 5-$totalRating->average_rating;
											for($i=1; $i<=$totalRating->average_rating;$i++){
												echo '<span class="fa fa-star checked"></span>';
											}
											if($remain>0){
												for($i=1; $i<=$remain;$i++){	
													echo '<span class="fa fa-star"></span>';
												}

											}
										
										?>
										</div>
						<h4><b>{{$totalRating->average_rating}}</b> ({{$totalRating->totalreview}})</h4>
					     </div>
						@if($contract_detail->is_users_verified == 1)
						<div class="verifi-button">
							<a href="#"><i class="fas fa-check"></i>Verified</a>
							<h4><b>£{{$contract_detail->hourly_rate}}</b>/hr</h4> 
						</div>
						@endif
					</div>
					
					<div class="contacts-button">
					<?php
						if($contract_detail->IsContractComplete==0){
						?>
					   <button type="button" class="btn btn-success close-blue"  onclick="closeContract('<?php echo $contract_detail->contractId;?>')">Close this contract</button>
					   <?php
						}else{
							?>
							<button type="button" class="btn btn-success close-blue"  >Closed contract</button>
                        <?php
						}
						?>
					</div>
				</div>
			</div>
			<div class="col-md-7">
				<div class="profile-tabs view-tabs view-wwww">
					<div class="tab-content" id="myTabContent">
					  <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
						<div class="about-tab-list">
							<div class="tabs-rate">
								<div id="about">
								<div class="downlod-pdf">
										<a href="{{route('generate-pdf', $contract_detail->contractId)}}"><span><img src="{!! asset('assets/frontend/img/pdf.png') !!}"></span>Download PDF</a>
									</div>
									<div class="about-info-t">
										<h2>Contract info</h2>
										<h3>Contract ID: #{{$contract_detail->contractId}}</h3>
									</div>
									<ul class="Projects-rate-list">
										<li>
										<?php
										    if($contract_detail->priceType == 'hourly'){
												$price = $contract_detail->freelancerPrice/($contract_detail->duration*24);
											}else{
												$price = $contract_detail->freelancerPrice;
											}
									    ?>
											<h3>£{{$price}}</h3>
											<h4>{{$contract_detail->priceType}} Rate</h4>
										</li>
										<li>
											<h3>{{$contract_detail->duration}} days</h3>
											<h4>Hire Duration</h4>
										</li>
										<li>
											<h3>£{{$contract_detail->freelancerPrice}}</h3>
											<h4>In Escrow</h4>
										</li>
										<li>
										@if($contract_detail->IsContractComplete==0)
										<span class="work-progress">In Progress</span>
										
										@else
										<span class="work-done"><i class="fas fa-check"></i>Done</span>
										@endif

											<h4>Work Status</h4>
										</li>
										
									</ul>
									
								</div>
								<div class="view-about">
									<div id="Education" class="projects-text">
										<h3>What the Project About</h3>
										<p>{{$contract_detail->aboutProject}}</p>
									</div>
									<div id="Education" class="projects-text">
										<h3>What do you need to get done</h3>
										<p>{{$contract_detail->requirement}}</p>
									</div>
									<div id="Education" class="projects-text">
										<h3>Project Discription</h3>
										<p>{{$contract_detail->description}}</p>
									</div>
								</div>
								<div class="contact-detals-view">
									<h5>Contact Detail</h5>
									<ul>
										<li>
											<h2>Email</h2>
											<h3>{{$contract_detail->email}}</h3>
										</li>
										<li>
											<h2>Phone</h2>
											<h3>{{$contract_detail->phone}}</h3>
										</li>
										
									</ul>
								</div>	
								
							</div>
						</div>
					  </div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
