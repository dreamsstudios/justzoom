@extends('client.layouts.client_dashboard_layout')

@section('title', 'Justzoom')
@section('body')

<div class="profile-page">
		<div class="row">
			<div class="col-md-5" style="background-color: #f9f9f9;">
				<div class="profile-adrien">
					<div class="profile-img">
						<?php
									if(!empty($user->image)){
                                             $img = $user->image;
                                         }else{
                                          $img = 'no-img.png';

                                         }
                                         ?>
						<img src="{{asset('/uploads/user_images/'.$img)}}">
						<h2>{{ $user->first_name.' '.$user->last_name }}</h2>
						<h3>{{$user->job_title}},{{$user->address_line_1}}, 
                           @if(!empty($user->country_data))
							{{$user->country_data->name}}
							@endif
							</h3>
						
						<div class="verifi-button">
							@if($user->is_clients_verified==1 )
							<a href="#"><i class="fas fa-check"></i>Verified</a>
							@endif
						</div>
					</div>
					<div class="contacts-button">
						<a href="{{route('contact-us')}}">
						<button type="button" class="btn btn-success">Contact Us</button></a>
					</div>
				</div>
			</div>
			<div class="col-md-7">
				<div class="profile-tabs">
					<div class="tab-content" id="myTabContent">
					  <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
						<div class="about-tab-list">
							<div class="tabs-rate">
								<div id="about">
									<h2>About</h2>
									<p>{{$user->about}}</p>
									<ul class="Projects-rate-list">
										
										<li>
											<h3>{{$completeWork}}</h3>
											<h4>Contract</h4>
										</li>
										<li>
											<h3>{{$hired_freelancer}}</h3>
											<h4>Hired Freelancer</h4>
										</li>
										<li>
											<h3>{{$ongingWork}}</h3>
											<h4>On going Contract</h4>
										</li>
										
									</ul>
								</div>
								<div id="Experience" class="Experience-lsit">
									<h2>Job Title</h2>
									<ul>
										{{$user->job_title}}
									</ul>
								</div>
								<div id="Portfolio" class="portfolio-img">
									<h2>Phone</h2>
									<ul>
										{{$user->phone}}
									</ul>
									
								</div>
								<div id="Projects" class="project-bg">
								<div class="projects-text">
										<h2>Email</h2>
										<ul>
											{{$user->email}}
										</ul>
										
										
								</div>
									<div id="Education" class="projects-text">
										<h2>Address</h2>
										<ul>{{$user->address_line_1}}</ul>
										
									</div>
								</div>	
								
							</div>
						</div>
					  </div>
					  <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">2</div>
					  <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">3</div>
					  <div class="tab-pane fade" id="projects" role="tabpanel" aria-labelledby="projects-tab">4</div>
					  <div class="tab-pane fade" id="education" role="tabpanel" aria-labelledby="education-tab">5</div>
					  <div class="tab-pane fade" id="reviews" role="tabpanel" aria-labelledby="reviews-tab">6</div>
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection
