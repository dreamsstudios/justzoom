@extends('frontend.layouts.layout')

@section('title', 'Justzoom')
@section('body')

	
<div class="client-page">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="deshbord-title">
						<h2>Contract</h2>
						<h4>Date: <?= date('d-m-y');?></h4>
					</div>
					<ul class="deshbord-list freelance-openig">
						<li>
							<img src="{!! asset('assets/frontend/img/img/book.png') !!}">
							<div class="desh-text">
								<h4>Complete Contract</h4>
								<h5><?= count($pastContract); ?></h5>
							</div>
						</li>
						<li>
							<img src="{!! asset('assets/frontend/img/img/book.png') !!}">
							<div class="desh-text">
								<h4>Ongoing Contract</h4>
								<h5><?= count($contractData); ?></h5>
							</div>
						</li>
						<li>
							<img src="{!! asset('assets/frontend/img/img/book.png') !!}">
							<div class="desh-text">
								<h4>Pending Invitation</h4>
								<h5><?= $pendingInvitation; ?></h5>
							</div>
						</li>
					</ul>
				</div>
			</div>
			<div class="row">
			<?php 
				if(isset($contractData) && !empty($contractData))
				{
				?>
				<h2 class="opeing-title">Ongoing Contract </h2>
				<?php
				foreach($contractData as $contract){
				if(!empty($contract->image)){
							$img = $contract->image;
						} else {	
							$img = 'no-img.png';
						}
				?>
				<div class="col-md-12">
					
					<div class="profile-list">
						<div class="profile-list-left">
							<div class="chat-user-img">
								<img src="{{asset('/uploads/user_images/'.$img)}}" alt="user image">
								<div class="brand-logo">
									<img src="{!! asset('assets/frontend/img/chat-logo.png') !!}" alt="">
								</div>
							</div>
							@if($contract->is_users_verified == 1)
							<div class="profile-statics">
								<h6><i class="fa fa-check"></i> Verified</h6>
							</div>
							@endif
						</div>
						<div class="profile-list-right">							
							<div class="chat-user-statics profile-info">
								<ul>
									<li class="profile-name">
										<h2>{{ucfirst($contract->first_name .' '.$contract->last_name)}}</h2>
										<h4>{{ucfirst($contract->user_category_name)}}</h4>
									</li>
									<li>
									<?php
										if($contract->chatId>0)
										$unreadMsg = unreadMsgCount($contract->chatId);
										else
										$unreadMsg = 0;
									?>
										<div class="inbox">

											<img src="{!! asset('assets/frontend/img/envelope.png') !!}" alt="">
											@if($unreadMsg>0)<span class="msg-count">{{$unreadMsg}}</span>@endif
										</div>
										<p>Unread</p>
									</li>
									<li>
									<div class="raiting">
									<?php
									$totalrating = getUserTotalRating($contract->userId);
								   ?>
											
											<?php 
											$remain = 5-$totalrating->average_rating;
											for($i=1; $i<=$totalrating->average_rating;$i++){
												echo '<span class="fa fa-star checked"></span>';
											}
											if($remain>0){
												for($i=1; $i<=$remain;$i++){	
													echo '<span class="fa fa-star"></span>';
												}

											}
										?>
									
									<p><span>{{$totalrating->average_rating}}</span> ({{$totalrating->totalreview}})</p>
								</div>
								</li>
									
									<li>
										<h3>#<?= $contract->contractId; ?>
										<p>Contract ID</p>
									</li>
									<li>
										<h3>£<?= $contract->freelancerPrice; ?></h3>
										<p>In Escrow</p>
									</li>
									<li>
										<button type="button" class="btn btn-warning Progress-yulo">In Progress</button>
										<p>Work Status</p>
									</li>
								</ul>
							</div>
							<div class="profile-review-section">
								
								<div class="review-area">
								<?php 
								$chatMessages = lastMessage($contract->chatId);
								if(!empty($chatMessages)){
									
								?>
									<div class="review-duration">
										<h5>@if($chatMessages->first_name){{ucfirst($chatMessages->first_name)}}@else Me @endif</h5>
										<h5></h5>
									</div>
									<hr>
									<p>{{$chatMessages->message}}</p>
									<div class="reply-btn-area">
									<a href="{{asset('/client/chat/'.$contract->chatId)}}">
									<button type="button" class="btn reply-btn">Reply</button>
									</a>
									</div>
									<?php
								}
								?>
								</div>
								
								<div class="btn-area">
								     
									<div class="profile-btn-group">
									    @if($contract->paymentId>0)
									    <a href="{{asset('/client/contract-detail/'.$contract->contractId)}}" class="view-profile-btn">Contract Detail</a>
										<a href="{{asset('/client/chat/'.$contract->chatId)}}" class="contact-btn">Chat</a>
										<a href="{{asset('/freelancer-detail/'.$contract->id)}}" class="quote-btn">View Profile</a>
									     @else
										 <a href="{{asset('/payment/'.$contract->contractId)}}" class="view-profile-btn">Pay Platform charge</a>
										 @endif
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php
				}
				
				}?>
				<?php
					if(isset($contractData) && !empty($contractData)){
								?>
							<div class="paginating">
								<ul class="pagination searchpagination">
									{{ $contractData->links() }}	
								</ul>
							</div>
							<?php
							}?>
				
			</div>	
			<div class="row">
				<div class="col-md-12">
					<div class="post-table Freelancer-table">
						<h2 class="post-title">Past Work</h2>
						<div class="table-responsive-sm">
							<table class="table table-bordered">
							  <thead>
								<tr style="background-color: #f9f9f9;">
								  <th scope="col" style="text-align: left;"><h4>Freelancer</h4></th>
								  <th scope="col"><h4>Contract ID</h4></th>
								  <th scope="col"><h4>category Name</h4></th>
								  <th scope="col"><h4>Duration</h4></th>
								  <th scope="col"><h4>Paid Amount</h4></th>
								  <th scope="col"><h4>Ratings</h4></th>
								  <th scope="col"><h4>Status</h4></th>
								  <th scope="col"><h4>Action</h4></th>
								</tr>
							  </thead>
							  <tbody>
							  <?php
							  if(isset($pastContract) && !empty($pastContract) &&  count($pastContract) >0){
								  foreach($pastContract as $past){
									if(!empty($past->image)){
										$img = $past->image;
									} else {	
										$img = 'no-img.png';
									}
									?>
									<tr>
									<th scope="row" style="text-align: left;">
									<div class="table-profile">
									
										<img src="{{asset('/uploads/user_images/'.$img)}}">
										<div class="post-text">
											<h3 >{{ucfirst($past->first_name.' '.$past->last_name)}}</h3>
											<p>{{ucfirst($past->user_category_name)}}</p>
										</div>
									</div>
									</th>
									<td><p>{{$past->category_name}}</p></td>
									<td><p>{{ucfirst($past->category_name)}}</p></td>
									<td><p>{{$past->duration}} days</p></td>
									<td><p>£{{$past->freelancerPrice}}</p></td>
									<td>
									  <div class="raiting">
									  <?php
											if(isset($past->rating)){
											?>
											<div class="raiting">
											<?php 
											$remain = 5-$past->rating;
											for($i=1; $i<=$past->rating;$i++){
												echo '<span class="fa fa-star checked"></span>';
											}
											if($remain>0){
												for($i=1; $i<=$remain;$i++){	
													echo '<span class="fa fa-star"></span>';
												}

											}
										}else{
										?>
										<a href="javascript:void(0);"  data-toggle="modal" data-target="#review">write review</a>
										@include("frontend.review")  
										<?php
										

										}
									    ?>
											
										</div></td>
									<td><button type="button" class="btn btn-success table-done">Done</button></td>
									<td>
									<div class="dropdown dot-button">
										<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<img src="{!! asset('assets/frontend/img/dot.png') !!}">
										</button>
										<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
										<a class="dropdown-item" href="{{asset('/client/contract-detail/'.$past->contractId)}}">Contract Detail</a>
										
										</div>
									</div>
									</td>
									</tr>
                                  <?php
								  }

							  }else{
								  echo "<tr><td colspan='8'>No record Found</td></tr>";
							  }
							  ?>
							  </tbody>
							</table>
							
						</div>
						<?php
					if(isset($pastWork) && !empty($pastWork)){
								?>
							<div class="paginating">
								<ul class="pagination searchpagination">
									{{ $pastWork->links() }}	
								</ul>
							</div>
							<?php
							}?>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
	
@endsection
