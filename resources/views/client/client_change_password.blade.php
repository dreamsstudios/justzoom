@extends('client.layouts.client_dashboard_layout')

@section('title', 'Justzoom')
@section('body')
<!-- comma seperated input css -->
<link rel="stylesheet" href="{!! asset('assets/frontend/css/bootstrap-tagsinput.css') !!}">
<script type="text/javascript" src= "{!! asset('assets/frontend/js/bootstrap-tagsinput.min.js') !!}"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
<!-- comma seperated input css -->
<div class="edit-profile-page">
		<div class="container">
			<div class="row profile-information">
				<div class="col-md-12">
					<div class="edit-title">
						<h2>Change your Password</h2>
						<h3>Fill in your  information</h3>
					</div>
				</div>
				<div class="col-md-7">
					<div class="profile-bg">
						
						<div class="profile-form">
							@if(session("msg"))
							<div class="alert-dismiss">
								<div class="alert alert-success alert-dismissible fade show">
									<span>{{session("msg")}}</span><button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span class="fa fa-times"></span> </button><br/>
								</div>
							</div>
				@endif
					@if(session("errormsg"))
							<div class="alert-dismiss">
								<div class="alert alert-danger alert-dismissible fade show">
									<span>{{session("errormsg")}}</span><button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span class="fa fa-times"></span> </button><br/>
								</div>
							</div>
				@endif
				@if(count($errors)>0)
							<div class="alert-dismiss">
								<div class="alert alert-danger alert-dismissible fade show">
									@foreach($errors->all()  as $k=>$error)
										<span>{{$error}}</span><br/>
									@endforeach
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span class="fa fa-times"></span>
									</button>
								</div>
							</div>
							@endif
							<form class="needs-validation" novalidate="" id="my-form" method="post"  enctype = "multipart/form-data">
							{!!csrf_field()!!}
								<div class="row">
									<div class="col-md-12">
									<label for="place">Old Password</label>
									<div class="form-group">
										<input type="password" name="old_password" placeholder="Your old password" value="" class="form-control">
										{!! $errors->first('old_password', '<p class="help-block">:message</p>') !!}
									</div>
								</div>
									
								</div>

								<div class="row">
									<div class="col-md-12">
									<label for="place">New Password</label>
									<div class="form-group">
										<input type="password" name="new_password" placeholder="Your new password" value="" class="form-control">
										{!! $errors->first('new_password', '<p class="help-block">:message</p>') !!}
									</div>
								</div>
									
								</div>

								<div class="row">
									<div class="col-md-12">
									<label for="place">Confirm Password</label>
									<div class="form-group">
										<input type="password" name="confirm_password" placeholder="Your confirm password" value="" class="form-control">
										{!! $errors->first('confirm_password', '<p class="help-block">:message</p>') !!}
									</div>
								</div>
									
								</div>
								</div>
								
								<div class="row">
									<div class="col-md-12">
										<div class="form-cncle-button">
											<button type="submit" class="formsubmit_btn btn btn-dark save-blue">Submit</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
	

@endsection
