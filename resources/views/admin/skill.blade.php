@extends("admin.layouts.layout")

@section("title","Admin Dashboard | JustZoom")

@section("header-page-script")  
    
   <!-- Start datatable css -->
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/jquery.dataTables.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/dataTables.bootstrap4.min.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/responsive.bootstrap.min.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/jqueryui.min.css') !!}">
    
    
@endsection

@section("content")   

<div id="content">

            <div class="client-page">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="deshbord-title">
                        <h2>Skill Management</h2>
                    </div>
                    
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-12">
                  @if(session("msg"))
                            <div class="alert-dismiss">
                                <div class="alert alert-success alert-dismissible fade show">
                                    <span>{{session("msg")}}</span><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span class="fa fa-times"></span> </button><br/>
                                </div>
                            </div>
                            @endif
                            @if(count($errors)>0)
                            <div class="alert-dismiss">
                                <div class="alert alert-danger alert-dismissible fade show">
                                    @foreach($errors->all()  as $error)
                                        <span>{{$error}}</span><br/>
                                    @endforeach
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span class="fa fa-times"></span>
                                    </button>
                                </div>
                            </div>
                            @endif
                    <div class="post-table">
                        <div class="add-title">
                            <a href="{{ route('add-skill') }}" ><button type="button" class="btn btn-success">Add New Skills</button></a>
                            
                            
                        </div>
                        <div class="table-responsive-lg">
                           <table class="table table-bordered" id="example1">
                              <thead>
                                <tr style="background-color: #f9f9f9;">
                                  
                                  <th scope="col"><h4>Skill</h4></th>
                                 <th scope="col"><h4>Create Date</h4></th>
                                 <th scope="col">Status</th>
                                  <th scope="col"><h4>Action</h4></th>
                                </tr>
                              </thead>
<tbody>

@foreach($queryData  as $queryData)

<tr>

<td><p>{{$queryData->skill}}</p></td>

<td><p>{{ Carbon\Carbon::parse($queryData->created_at)->format('d M Y') }}</p></td>       

<td><P>@if($queryData->status==0)

<a class="UpdateSectionStatus" id="Skill-{{$queryData->id}}" skill_id="{{$queryData->id}}" href="javascript:Void(0)" style="color: green"> Active</a>

@else

<a class="UpdateSectionStatus" id="Skill-{{$queryData->id}}" skill_id="{{$queryData->id}}" href="javascript:Void(0)" style="color: red"> Inactive</a>

@endif

</P></td>              

<td><div class="dtable-done-btt">

<a href="{{ route('editskill',$queryData->id) }}"><button type="button" class="btn btn-success blue">Edit</button></a>
<button type="button" class="btn btn-success red btn-delete" data-id="{{$queryData->id}}}">Delete</button></div></td>

</tr>

@endforeach

</tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
        </div>

@endsection

@section("footer-page-script")
    <!-- Start datatable js -->
    <script src="{!! asset('assets/js/jquery.dataTables.js') !!}"></script>
    <script src="{!! asset('assets/js/jquery.dataTables.min.js') !!}"></script>
    <script src="{!! asset('assets/js/dataTables.bootstrap4.min.js') !!}"></script>
    <script src="{!! asset('assets/js/dataTables.responsive.min.js') !!}"></script>
    <script src="{!! asset('assets/js/responsive.bootstrap.min.js') !!}"></script>
    <script>
         $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
    </script>
     

    <script>
        $(function () {

            $(document).on("click", ".btn-delete", function () {

                var conf = confirm("Are you sure want to delete ?");

                if (conf) {

                    // ajax call functions
                    var delete_id = $(this).attr("data-id"); // delete id of delete button

                    var postdata = {
                        "_token": "{{ csrf_token() }}",
                        "hiddenval": delete_id
                    }

                    $.post("{{ route('deleteskill') }}", postdata, function (response) {
                              debugger;
                        var data = $.parseJSON(response);

                        if (data.status == 1) {

                            location.reload();
                        } else {

                            alert(data.message);
                        }
                    })
                }
            });
        });

        $(".UpdateSectionStatus").click(function(){
              var status = $(this).text();
              var skill_id = $(this).attr("skill_id");
             $.ajax({
                   type: 'post',
                   url : '/dashboard/update-skill-status',
                   data :{status:status,skill_id:skill_id},
                   success:function(resp){
                  if (resp['status']==0) {
                    $("#Skill-"+skill_id).html('Active').css("color", "green");
                  }else if(resp['status']==1){
                    $("#Skill-"+skill_id).html('Inactive').css("color", "red");
                  }
                   },error: function(){
                    alert('ERRRRr');
                   }
             });

        });
    </script>
@endsection