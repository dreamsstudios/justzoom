@extends("admin.layouts.layout")

@section("title","Admin Dashboard | JustZoom")

@section("header-page-script")   
    
    <!-- Start datatable css -->
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/jquery.dataTables.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/dataTables.bootstrap4.min.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/responsive.bootstrap.min.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/jqueryui.min.css') !!}">

  
   
@endsection

@section("content")      
<div id="content">

            <div class="client-page">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="deshbord-title">
                        <h2>Querys</h2>
                    </div>
                    
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-12">
                  @if(session("msg"))
                            <div class="alert-dismiss">
                                <div class="alert alert-success alert-dismissible fade show">
                                    <span>{{session("msg")}}</span><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span class="fa fa-times"></span> </button><br/>
                                </div>
                            </div>
                            @endif
                            @if(count($errors)>0)
                            <div class="alert-dismiss">
                                <div class="alert alert-danger alert-dismissible fade show">
                                    @foreach($errors->all()  as $error)
                                        <span>{{$error}}</span><br/>
                                    @endforeach
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span class="fa fa-times"></span>
                                    </button>
                                </div>
                            </div>
                            @endif
                    <div class="post-table">
                       
                        <div class="table-responsive-lg">
                           <table class="table table-bordered" id="example1">
                              <thead>
                                <tr style="background-color: #f9f9f9;">
                                  
                                  <th scope="col"><h4>Name</h4></th>
                                 <th scope="col"><h4>Email Address</h4></th>
                                 <th scope="col">Subject</th>
                                 <th scope="col">Created Date</th>
                                  <th scope="col"><h4>Action</h4></th>
                                </tr>
                              </thead>
<tbody>

@foreach($queryData  as $queryData)

<tr>

<td><p>{{$queryData->name}}</p></td>
<td><p>{{$queryData->email}}</p></td>
<td><p>{{$queryData->subject}}</p></td>

<td><p>{{ Carbon\Carbon::parse($queryData->created_at)->format('d M Y') }}</p></td>       

              

<td><div class="dtable-done-btt">

<a href="{{ route('viewquery',$queryData->id) }}"><button type="button" class="btn btn-success green">Views</button></a>

</div></td>


</tr>

@endforeach

</tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
        </div>

@endsection


@section("footer-page-script")
    <!-- Start datatable js -->
    <script src="{!! asset('assets/js/jquery.dataTables.js') !!}"></script>
    <script src="{!! asset('assets/js/jquery.dataTables.min.js') !!}"></script>
    <script src="{!! asset('assets/js/dataTables.bootstrap4.min.js') !!}"></script>
    <script src="{!! asset('assets/js/dataTables.responsive.min.js') !!}"></script>
    <script src="{!! asset('assets/js/responsive.bootstrap.min.js') !!}"></script>
  
      <script>
         $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
    </script>

    
@endsection