@extends("admin.layouts.layout")

@section("title","Admin Dashboard | JustZoom")

@section("header-page-script")   
    
    <!-- Start datatable css -->
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/jquery.dataTables.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/dataTables.bootstrap4.min.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/responsive.bootstrap.min.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/jqueryui.min.css') !!}">

    
@endsection

@section("content")            
   <div id="content">

            <div class="profile-page">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        
                        <div class="profile-title">
                            <!-- <h3>Profile <a href="{{ route('approvel-user') }}"><span></span>Back</a></h3> -->
                        <p>{{$user->about}}.</p>
                        </div>
                        <div class="jeremy-ros">
                            <div class="peofile-text">
                                <div class="contact-ros">
                                 <h5>Full Name:</h5>&nbsp; &nbsp;  <h2>{{$user->first_name}} {{$user->last_name}}</h2>
                                    
                                </div>
                                <div class="personal-information">
                                    <h3>Job Information</h3>
                                    <ul>
                                        <li><h4>Title</h4><h5>@if(!empty($user->job_title)){{$user->job_title}}
                                        @else
                                        N/A
                                        @endif
                                    </h5></li>
                                        
                                         <li><h4>Complete Contract</h4><h5>{{$complete}}
                                    </h5></li>
                                    <li><h4>Ongoing Contract</h4><h5>{{$onGoing}}
                                    </h5></li>
                                    <li><h4>Hired Freelancer</h4><h5>{{$hired_freelancer}}
                                    </h5></li>
                                        
                                        
                                    </ul>
                                </div>
                                
                                <div class="personal-information">
                                    <h3>Personal Information</h3>
                                    <ul>
                                        <li><h4>Phone No.</h4><h5>{{$user->phone}}</h5></li>
                                        <li><h4>Email</h4><h5>{{$user->email}}</h5></li>
                                        
                                        <li><h4>Address</h4><h5>@if(!empty($user->address_line_1)){{$user->address_line_1}}
                                        @else
                                        N/A
                                        @endif
                                    </h5></li>
                                        
                                        <li><h4>City</h4><h5>@if(!empty($user->city_data)){{$user->city_data->name}}
                                        @else
                                        N/A
                                        @endif</h5>
                                    </li>
                                        <li><h4>State</h4><h5>@if(!empty($user->state_data)){{$user->state_data->name}}
                                        @else
                                        N/A
                                        @endif</h5>
                                    </li>


                                       <li><h4>Country</h4><h5>@if(!empty($user->country_data)){{$user->country_data->name}}
                                        @else
                                        N/A
                                        @endif</h5>
                                    </li>

                                    </ul>
                                </div>
                                <div class="personal-information">
                                    <h3>Billing Information</h3>
                                    <ul>
                                        <li><h4>Phone No.</h4><h5>{{$user->phone}}</h5></li>
                                        <li><h4>Email</h4><h5>{{$user->email}}</h5></li>
                                        
                                        <li><h4>Address</h4><h5>@if(!empty($user->address_line_1)){{$user->address_line_1}}
                                        @else
                                        N/A
                                        @endif
                                    </h5></li>
                                        
                                        <li><h4>City</h4><h5>@if(!empty($user->city_data)){{$user->city_data->name}}
                                        @else
                                        N/A
                                        @endif</h5>
                                    </li>
                                        <li><h4>State</h4><h5>@if(!empty($user->state_data)){{$user->state_data->name}}
                                        @else
                                        N/A
                                        @endif</h5>
                                    </li>


                                       <li><h4>Country</h4><h5>@if(!empty($user->country_data)){{$user->country_data->name}}
                                        @else
                                        N/A
                                        @endif</h5>
                                    </li>

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>        

@endsection


@section("footer-page-script")
    <!-- Start datatable js -->
    <script src="{!! asset('assets/js/jquery.dataTables.js') !!}"></script>
    <script src="{!! asset('assets/js/jquery.dataTables.min.js') !!}"></script>
    <script src="{!! asset('assets/js/dataTables.bootstrap4.min.js') !!}"></script>
    <script src="{!! asset('assets/js/dataTables.responsive.min.js') !!}"></script>
    <script src="{!! asset('assets/js/responsive.bootstrap.min.js') !!}"></script>
    
    
@endsection