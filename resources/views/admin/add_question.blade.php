@extends("admin.layouts.layout")

@section("title","Admin Dashboard | Just Zoom")

@section("header-page-script")  
    
    <!-- Start datatable css -->
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/jquery.dataTables.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/dataTables.bootstrap4.min.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/responsive.bootstrap.min.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/jqueryui.min.css') !!}">

@endsection

@section("content")   

<div id="content">
      <div class="client-page">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="deshbord-title">
                <h2>Category</h2>
                
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="cata-setaps">
              @include("admin.layouts.cate_header")
             
                <form class="needs-validation" novalidate="" id="my-form" method="post" action="{{ route('savequestion') }}">
                 @if(session("msg"))
                            <div class="alert-dismiss">
                                <div class="alert alert-success alert-dismissible fade show">
                                    <span>{{session("msg")}}</span><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span class="fa fa-times"></span> </button><br/>
                                </div>
                            </div>
                            @endif
                            @if(count($errors)>0)
                            <div class="alert-dismiss">
                                <div class="alert alert-danger alert-dismissible fade show">
                                    @foreach($errors->all()  as $error)
                                        <span>{{$error}}</span><br/>
                                    @endforeach
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span class="fa fa-times"></span>
                                    </button>
                                </div>
                            </div>
                            @endif
                            
                            {!!csrf_field()!!}
                  <div class="radio-buttons">
                    <h2>Add Category Questions</h2>
                    <div class="form-group question-input">
                      <label for="exampleInputEmail1">Question</label>
                      <input type="text" name="question" class="form-control" 
                      id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Wich type Designer you find? "
                       value="{{$questionValue}}">
                    </div>
                    <div id="option">
                    @if(isset($questionOptions) && !empty($questionOptions))
                    @foreach($questionOptions as $opt)
                    <div class="form-group opt"><label for="exampleInputEmail1">Options</label>
                    <div class="rdio rdio-primary radio-inline">
                    <input  value="{{$opt->optionText}}" id="radio1" type="radio"><label for="radio1">{{$opt->optionText}}</label></div>
                    <input type="hidden" name="option[]" value="{{$opt->optionText}}"></div>
                    @endforeach
                    @endif
                    </div>
                    <div class="add-new-op">
                      <a href="javascript:void(0);" data-toggle="modal" data-target="#myModal">Add New Option</a>
                    </div>
                  </div>

                  <input type="hidden" name="hiddenval" class="hiddenval" value="<?php echo $id; ?>">
                  <input type="hidden" name="step" class="hiddenval" value="<?php echo $step; ?>">
                  <input type="hidden" name="categoryId" class="hiddenval" value="<?php echo $category->id; ?>">
                  <input type="hidden" name="questionId" class="hiddenval" value="<?php echo (isset($question->questionId))?$question->questionId:''; ?>">
                  <div class="back-button">
                    @if($backStep != "")
                    <a style="padding: 0.375rem 2.75rem" href="{{'/dashboard/add-category/'.$category->id.'/'.$backStep.'?backStep='. $backStep}}" class="btn btn-light light-back"><span><i class="fas fa-long-arrow-alt-left"></i></span> Back</a>
                    @else
                    <a style="padding: 0.375rem 2.75rem" href="{{'/dashboard/add-category/'.$category->id}}" class="btn btn-light light-back"><span><i class="fas fa-long-arrow-alt-left"></i></span> Back</a>
                    @endif
                    
                    <button type="submit" class="btn btn-dark dark-next" >Next<span><i class="fas fa-long-arrow-alt-right"></i></span></button>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div>
        </div>
        <!-- The Modal -->
<div class="modal" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Add Option</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
       <input type="text" id="add-option" value="" />
       <div class="invalid-feedback" id="inval">
       </div>
       <button type="button" class="btn btn-success" onclick="addedOption()">Added</button>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>

@endsection

<script>

function addedOption(){
   var opt = $('#add-option').val();
  
   if(opt){
     
      var Html = '<div class="form-group opt"><label for="exampleInputEmail1">Options</label><div class="rdio rdio-primary radio-inline">'; 
      Html += '<input  value="'+opt+'" id="radio1" type="radio"><label for="radio1">'+opt+'</label></div>';
      Html += '<input type="hidden" name="option[]" value="'+opt+'"></div>';
      $('#option').append(Html);

   }else{
     
       $("#inval").html("please enter option!");
       $("#inval").show();
   }
    

}


</script>

@section("footer-page-script")
