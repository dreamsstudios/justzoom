@extends("admin.layouts.layout")

@section("title","Admin Dashboard | Market Place")

@section("header-page-script")
<!-- Start datatable css -->
<link rel="stylesheet" type="text/css" href="{!! asset('assets/css/jquery.dataTables.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/dataTables.bootstrap4.min.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/responsive.bootstrap.min.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/jqueryui.min.css') !!}">

    <!-- others css -->
    <link rel="stylesheet" href="{!! asset('assets/css/typography.css') !!}">
    <link rel="stylesheet" href="{!! asset('assets/css/default-css.css') !!}">
    <link rel="stylesheet" href="{!! asset('assets/css/styles.css') !!}">
    <link rel="stylesheet" href="{!! asset('assets/css/responsive.css') !!}">
    <!-- modernizr css -->
    <script src="{!! asset('assets/js/vendor/modernizr-2.8.3.min.js') !!}"></script>
@endsection

@section("content")            <!-- page title area start -->
            <div class="page-title-area">
                <div class="row align-items-center">
                    <div class="col-sm-12">
                        <div class="breadcrumbs-area clearfix">
                            <h4 class="page-title pull-left">Dashboard</h4>
                            <ul class="breadcrumbs pull-left">
                                <li><a href="{{ route('dashboard') }}">Home</a></li>
                                <li><a href="{{ route('listjob') }}">Job List</a></li>
                                <li><span>View Job</span></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- page title area end -->
            <div class="main-content-inner">
                <div class="row">

                    <!-- Server side start -->
                    <div class="col-12">
                        <div class="card mt-5">
                            <div class="card-body">
                               
                                <div class="form-row">
                                        <div class="col-md-6 mb-3">
                                            <label for="city">Title: </label>
                                             {{$jobData->title}}
                                            
                                        </div> 
                                        <div class="col-md-6 mb-3">
                                            <label for="city">Category: </label>
                                            {{$jobData->category}}
                                            
                                        </div> 
                                        <div class="col-md-6 mb-3">
                                            <label for="city">Job Type: </label>
                                            @if($jobData->job_type == 1)
                                            {{"Private"}}
                                            @else
                                            {{"Open"}}
                                            @endif
                                        </div>
                                        <div class="col-md-6 mb-3">
                                            <label for="city">Job Price: </label>
                                             $ {{$jobData->price}}
                                            
                                        </div>
                                        <div class="col-md-6 mb-3">
                                            <label for="city">Job Start On: </label>
                                            {{date('d M Y', strtotime($jobData->job_start_on))}}
                                            
                                        </div>
                                        <div class="col-md-6 mb-3">
                                            <label for="city">Job End On: </label>
                                            {{date('d M Y', strtotime($jobData->job_end_on))}}
                                        </div>
                                        <div class="col-md-6 mb-3">
                                            <label for="city">Job Status: </label>
                                           <?php 
                                            if($jobData->status == 0){
                                                echo "Pending";
            
                                            }else if($jobData->status == 1){
                                            if(date('Y-m-d', strtotime($jobData->job_end_on)) <= date('Y-m-d')){
                                                echo "Completed";
                                            }else{
                                                echo "Ongoing";
            
                                            }
                                            }else if($jobData->status == 2){
                                                echo "Completed";
                                            }
                                           ?>
                                        </div>
                                        <?php 
                                        if(isset($jobData->first_name) && !empty($jobData->first_name)){
                                            ?>
                                            <div class="col-md-6 mb-3">
                                            <label for="city">Freelancer: </label>
                                                {{ucfirst($jobData->first_name).' '.$jobData->last_name}}
                                           
                                            </div>

                                            <div class="col-md-6 mb-3">
                                            <label for="city">Payment Status: </label>
                                            Done
                                            </div>
                                            <div class="col-md-6 mb-3">
                                            <label for="city">Payment Token: </label>
                                            {{$jobData->token}}
                                            </div>
                                            <div class="col-md-6 mb-3">
                                            <label for="city">Payment Date: </label>
                                            {{date('d M Y', strtotime($jobData->payment_Date))}}
                                            </div>
                                            <?php
                                        }
                                        ?>
                                        
                                        
                                        <div class="col-md-6 mb-3">
                                            <label for="city">Created at: </label>
                                            {{date('d M Y', strtotime($jobData->created_at))}}
                                           
                                        </div>
                            </div>
                        </div>
                    </div>
                    <!-- Dark table start -->
                    <div class="col-12 mt-5">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="header-title">Jobs</h4>
                                <div class="data-tables datatable-dark">
                                    <table id="my-dataTable" class="text-center">
                                        <thead class="text-capitalize">
                                            <tr>
                                                <th>Freelancer Name</th>
                                                <th>Offer Price</th>
                                                <th>Offer Given By</th>
                                                <th>Offer Status</th>
                                                <th>Create Date</th>
                                               <!-- <th>Action</th>-->
                                            </tr>
                                        </thead>
                                        <tbody>
                                                                                        
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Dark table end -->


                    <!-- Server side end -->
                </div>
            </div>
            

@endsection


@section("footer-page-script")
    <!-- Start datatable js -->
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>
    <!-- others plugins -->
    <script src="{!! asset('assets/js/plugins.js') !!}"></script>
    <script src="{!! asset('assets/js/scripts.js') !!}"></script>
    <script>
        $(function() {
            $('#my-dataTable').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{ url('/dashboard/job_req_data/'.$jobData->id) }}',
                columns: [
                { data: 'name', name: 'name' },
                { data: 'offer_price', name: 'offer_price' },
                { data: 'offer_by', name: 'offer_by' },
                { data: 'offer_status', name: 'offer_status' },
                { data: 'created_at', name: 'created_at' }
                /*{ data: 'action', name: 'action' }*/
                ]
            });
        });
    </script>
@endsection