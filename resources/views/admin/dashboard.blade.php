@extends("admin.layouts.layout")



@section("title","Admin Dashboard | JustZoom")

@section("header-page-script")   

@endsection

@section("content")



<div id="content">



  <div class="client-page">

    <div class="container-fluid">

      <div class="row">

        <div class="col-md-12">

          <div class="deshbord-title">

            <h2>Dashboard <span>Welcome back, Admin</span></h2>



          </div>

          <ul class="deshbord-list">

            <li>

              <img src="assets/img/desh-red.png">

              <div class="desh-text">

                <h4>Total Revanue</h4>

                <h5>£{{$platformPrice}}</h5>

              </div>

            </li>

            <li>

              <img src="assets/img/home.png">

              <div class="desh-text">

                <h4> Approved Freelancers</h4>

                <h5>{{$user_verified}}</h5>

              </div>

            </li>

            <li>

              <img src="assets/img/man.png">

              <div class="desh-text">

                <h4>Total Clients</h4>

                <h5>{{$client_count}}</h5>

              </div>

            </li>

            <li>

              <img src="assets/img/desh4.png">

              <div class="desh-text">

                <h4>Payments Request</h4>

                <h5>0</h5>

              </div>

            </li>

            <li>

              <img src="assets/img/check.png">

              <div class="desh-text">

                <h4>Approval Waiting</h4>

                <h5>0</h5>

              </div>

            </li>



          </ul>

        </div>

      </div>



      <div class="row">

        <div class="col-md-12">

          <div class="post-table">

            <h2 class="post-title">Waiting for Approval</h2>

            <div class="table-responsive-lg">

              <table class="table table-bordered">

                <thead>

                  <tr style="background-color: #f9f9f9;">

                    <th scope="col" style="text-align: left;"><h4>Freelancer</h4></th>

                    <th scope="col"><h4>Category</h4></th>

                    <th scope="col"><h4>Hourly Rate</h4></th>

                    <th scope="col"><h4>Action</h4></th>

                  </tr>

                </thead>

                <tbody>



                  @foreach($queryData  as $query)



                  <tr>

                    <th scope="row" style="text-align: left;">

                      <?php if(!empty($query->image)){

                        $img = $query->image;

                      }else{

                        $img = 'no-img.png';



                      }

                      ?>

                      <div class="table-profile">

                        <img src="{{asset('/uploads/user_images/'.$img)}}" style="width:50px;" class="rounded-circle">

                        <div class="post-text">

                          <h3>{{$query->first_name}}</h3>

                        </div>

                      </div>

                    </th>



                    <td><p>

                      @if(!empty($query->category))

                      {{$query->category->category_name}}

                      @else

                      N/A

                      @endif



                    </p></td>



                    <td><p>

                      @if(!empty($query->hourly_rate))

                      £{{$query->hourly_rate}}

                      @else

                      N/A



                      @endif



                    </p></td>


                    <td>

                      <span id="delmsgvdo_<?php echo $query->id; ?>">

                        <div class="dtable-done-btt"><button type="button" class="btn btn-success green ApproveStatus " user_id="{{$query->id}}">Approve</button>

                          <a href="{{ route('userprofile',$query->id) }}"><button type="button" class="btn btn-success blue">View Profile</button></a></div></span>





                        </td>



                      </tr>

                      @endforeach


                    </tbody>

                  </table>

                  {{ $queryData->links() }} 



                </div>

              </div>

            </div>



          </div>

          <div class="payments-reqest">

           <div class="row">

            <div class="col-md-12">

              <h2 class="post-title">Payments Request</h2>

            </div>
            <?php
            if(isset($paymentRequest) && !empty($paymentRequest) && count($paymentRequest)>0){
              foreach($paymentRequest as $req){
                
                ?>
                 <?php if(!empty($req->image)){

                        $img = $req->image;

                      }else{

                        $img = 'no-img.png';



                      }

                      ?>
                <div class="col-md-6">

                  <div class="sheman-box">

                    <div class="sheman-profile">

                      <div class="table-profile">

                       <img src="{{asset('/uploads/user_images/'.$img)}}" style="width:50px;" class="rounded-circle">

                        <div class="post-text">

                          <h3>{{ucfirst($req->first_name.' '.$req->last_name)}}</h3>

                          <p>{{$req->category}} @if($req->city)in {{$req->city_name}},{{$req->state_name}} @endif</p>

                        </div>

                      </div>

                      <div class="sheman-hr">

                         <h4>{{ Carbon\Carbon::parse($req->created_at)->format('d M Y') }}</h4>

                      

                      </div>

                    </div>

                    <div class="admin-bg">

                      <p>Hi Admin</p>

                      <p>Please realease fund of this contract ID #{{$req->contractId}}</p>

                    </div>
                    @if(!empty($req->stripeAccId) && $req->payouts_enabled == 1 && !empty($req->cardSource))
                    <button type="button" class="btn btn-success resaled-btt" id="payoutAdmin" onclick="releaseFundByAdmin('<?php echo $req->relId;  ?>')">Release Fund</button>
                    @elseif(empty($req->stripeAccId) || $req->payouts_enabled !=1)
                    <div class="alert alert-warning">Freelancer not verified stripe detail yet.</div>
<<<<<<< HEAD
                   @elseif(empty($req->cardSource))
=======
                    @elseif(empty($req->cardSource))
>>>>>>> 5740178d965d187dae8f4e4fa54f96a199afb729
                    <div class="alert alert-warning">Client Card Detail not available.</div>
                    @endif

                  </div>

                </div>
                <?php

              }


            }else{
              echo  '<div class="col-md-12">No record found!</div>';

            }
         
            ?>			<div class="col-md-12 pagination-admin">
        {{ $paymentRequest->links() }}
</div>


          </div>

        </div>

      </div>

    </div>

  </div>



  @endsection





  @section("footer-page-script")

  <script type="text/javascript">



   $(".ApproveStatus").click(function(){



    var user_id = $(this).attr("user_id");

    $.ajax({

     type: 'post',

     url : '/dashboard/approve-user-status',

     data :{status:status,user_id:user_id},

     success:function(resp){

      if (resp['status']==1) {

        $('#delmsgvdo_'+user_id).html('Successfully Approve.').css("color", "green");

        setTimeout(function(){ location.reload(); }, 100);

      }

    },error: function(){

      alert('ERRRRr');

    }

  });



  });





</script>



@endsection



