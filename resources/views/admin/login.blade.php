@extends('admin.layouts.auth_layout')
@section('title', 'Admin Dashboard | Just Zoom')

@section('content')

    <!-- login area start -->
    <div class="page-wraper">
    
    <div class="login-page">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="login-form-fr">
                        <h1>Sign in</h1>
                        <p>Hello there, Sign in to Just Zoom </p>
                        <form action="{{ route('checklogin') }}" method="post">
                            <div class="form-gp">
                            @if(session("msg"))
                            <div class="alert-dismiss">
                                <div class="alert alert-success alert-dismissible fade show">
                                    <span>{{session("msg")}}</span><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span class="fa fa-times"></span> </button><br/>
                                </div>
                            </div>
                            @endif
                            @if(count($errors)>0)
                            <div class="alert-dismiss">
                                <div class="alert alert-danger alert-dismissible fade show">
                                    @foreach($errors->all()  as $error)
                                        <span>{{$error}}</span><br/>
                                    @endforeach
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span class="fa fa-times"></span>
                                    </button>
                                </div>
                            </div>
                            @endif
                            {!!csrf_field()!!}
                        </div>
                          <div class="form-group">
                           
                            <input type="email" class="form-control" name="email" id="exampleInputEmail1" placeholder="Email address">
                          </div>
                          <div class="form-group">
                            
                            <input type="password" name="password" id="exampleInputPassword1" placeholder="Password" class="form-control">
                          </div>
                          <div class="for-rember">
                                <div>
                                  <!-- Remember me -->
                                  <div class="custom-control custom-checkbox">
                                      <input type="checkbox" class="custom-control-input" id="defaultLoginFormRemember" name="remember_me">
                                      <label class="custom-control-label" for="defaultLoginFormRemember">Remember me</label>
                                  </div>
                                </div>
                               <!--  -->
                            </div>
                           
                         
                          <button id="form_submit" class="btn btn-default loginbt" type="submit">Submit </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
</div>
    <!-- login area end -->

@endsection