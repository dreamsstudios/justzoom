@extends("admin.layouts.layout")

@section("title","Admin Dashboard | JustZoom")

@section("header-page-script")   
    
    <!-- Start datatable css -->
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/jquery.dataTables.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/dataTables.bootstrap4.min.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/responsive.bootstrap.min.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/jqueryui.min.css') !!}">

    
@endsection

@section("content")            
   <div id="content">

            <div class="client-page">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="deshbord-title">
                         <h2>Waiting for Approvel</h2>
                         <a href="{{ route('approvel-client') }}" class="btn btn-primary"> Back</a>

                       
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                   @if(session("msg"))
                            <div class="alert-dismiss">
                                <div class="alert alert-success alert-dismissible fade show">
                                    <span>{{session("msg")}}</span><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span class="fa fa-times"></span> </button><br/>
                                </div>
                            </div>
                            @endif
                            @if(count($errors)>0)
                            <div class="alert-dismiss">
                                <div class="alert alert-danger alert-dismissible fade show">
                                    @foreach($errors->all()  as $error)
                                        <span>{{$error}}</span><br/>
                                    @endforeach
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span class="fa fa-times"></span>
                                    </button>
                                </div>
                            </div>
                            @endif
                    <div class="post-table">
                        <div class="table-responsive-lg">
                            <table class="table table-bordered" id="example1">
                              <thead>

                                <tr style="background-color: #f9f9f9;">
                                  <th scope="col" style="text-align: left;"><h4>Client</h4></th>
                                 <th scope="col"><h4>Title</h4></th>
                                  <th scope="col"><h4>Complete Contract</h4></th>
                                  
                                  <th scope="col"><h4>Ongoing Contract</h4></th>
                                  <th scope="col"><h4>Action</h4></th>
                                </tr>
                              </thead>
                              <tbody>

@foreach($queryData  as $queryData)

<tr>
<th scope="row" style="text-align: left;">
<?php if(!empty($queryData->image)){
$img = $queryData->image;
}else{
$img = 'no-img.png';

}
?>
                                    <div class="table-profile">
                                        <img src="{{asset('/uploads/user_images/'.$img)}}" style="width:50px;" class="rounded-circle">
                                        <div class="post-text">
                                            <h3>{{$queryData->first_name}}</h3>
                                        </div>
                                    </div>
                                  </th>

<td><p>
@if(!empty($queryData->job_title))
  {{$queryData->job_title}}
@else
N/A
@endif

</p></td>

<td><p>
3

</p></td>

      

<td><p>
5

</p></td>


<td>
  
<div class="dtable-done-btt">




            @if($queryData->trashed())

      <form action="{{ route('restore-client',$queryData->id) }}" method="POST"
              style="display: inline" >
           @method('PUT')
           @csrf
            <button class="btn btn-primary">Restore</button>
        </form>
           
             @endif


             
            <form action="{{ route('deleteclient',$queryData->id) }}" method="POST"
              style="display: inline"
              onsubmit="return confirm('Are you sure you wish to delete ?');">
          
            @csrf
            <button class="btn btn-danger">
           {{ $queryData->trashed()?'Delete':'Trash' }}
            </button>
        </form>
        
</td>

</tr>

@endforeach

</tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
        </div>        

@endsection


@section("footer-page-script")
    <!-- Start datatable js -->
    <script src="{!! asset('assets/js/jquery.dataTables.js') !!}"></script>
    <script src="{!! asset('assets/js/jquery.dataTables.min.js') !!}"></script>
    <script src="{!! asset('assets/js/dataTables.bootstrap4.min.js') !!}"></script>
    <script src="{!! asset('assets/js/dataTables.responsive.min.js') !!}"></script>
    <script src="{!! asset('assets/js/responsive.bootstrap.min.js') !!}"></script>
    <script src="{!! asset('assets/js/admin.js') !!}"></script>
    <script>
         $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
    </script>
    
    <script type="text/javascript">
      
       $(".UpdateSectionStatus").click(function(){
              var status = $(this).text();
              var user_id = $(this).attr("user_id");
             $.ajax({
                   type: 'post',
                   url : '/dashboard/update-user-status',
                   data :{status:status,user_id:user_id},
                   success:function(resp){
                  if (resp['status']==0) {
                    $("#User-"+user_id).html('Active').css("color", "green");
                  }else if(resp['status']==1){
                    $("#User-"+user_id).html('Inactive').css("color", "red");
                  }
                   },error: function(){
                    alert('ERRRRr');
                   }
             });

        });

       
    </script>

    <script type="text/javascript">
      
       $(".ApproveStatus").click(function(){
              var user_id = $(this).attr("user_id");
             $.ajax({
                   type: 'post',
                   url : '/dashboard/approve-user-status',
                   data :{status:status,user_id:user_id},
                   success:function(resp){
                  if (resp['status']==1) {
                    $('#delmsgvdo_'+user_id).html('Successfully Approve.').css("color", "green");
                    setTimeout(function(){ location.reload(); }, 100);
                  }
                   },error: function(){
                    alert('ERRRRr');
                   }
             });

        });

       
    </script>
@endsection