@extends("admin.layouts.layout")

@section("title","Admin Dashboard | JustZoom")

@section("header-page-script")   
    
    <!-- Start datatable css -->
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/jquery.dataTables.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/dataTables.bootstrap4.min.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/responsive.bootstrap.min.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/jqueryui.min.css') !!}">

  
   
@endsection

@section("content")      
<div id="content">

            <div class="client-page">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="deshbord-title">
                        <h2>Payments Management</h2>
                        <div class="historay-box">
                           <h4>Date: <?php echo date('d-m-y'); ?></h4>
                            <button type="button" class="btn btn-link">Payment History</button>
                        </div>
                    </div>
                    <ul class="deshbord-list admin-Management">
                        <li>
                            <img src=" {!! asset('assets/img/desh-red.png') !!}">

                         
                            <div class="desh-text">
                                <h4>Total Revanue</h4>
                                <h5>$83400.35</h5>
                            </div>
                        </li>
                        <li>
                            <img src=" {!! asset('assets/img/doler1.png') !!}">
                            <div class="desh-text">
                                <h4> Today Transaction</h4>
                                <h5>$800.35</h5>
                            </div>
                        </li>
                        <li>
                           <img src=" {!! asset('assets/img/doler2.png') !!}">
                            <div class="desh-text">
                                <h4>Refund Transaction</h4>
                                <h5>6</h5>
                            </div>
                        </li>
                        <li>
                           <img src=" {!! asset('assets/img/desh4.png') !!}">
                            <div class="desh-text">
                                <h4>Payments Request</h4>
                                <h5>{{count($paymentRequest)}}</h5>
                            </div>
                        </li>
                        
                        
                    </ul>
                </div>
            </div>
            <div class="payments-reqest Management-bottom">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="post-title">Payments Request</h2>
                    </div>
                    
                    <?php
            if(isset($paymentRequest) && !empty($paymentRequest)){
              foreach($paymentRequest as $req){
                ?>
                <?php if(!empty($req->image)){

                        $img = $req->image;

                      }else{

                        $img = 'no-img.png';



                      }

                      ?>
                <div class="col-md-6">

                  <div class="sheman-box">

                    <div class="sheman-profile">

                      <div class="table-profile">

                        <img src="{{asset('/uploads/user_images/'.$img)}}" style="width:50px;" class="rounded-circle">

                        <div class="post-text">

                          <h3>{{ucfirst($req->first_name.' '.$req->last_name)}}</h3>

                          <p>{{$req->category}} @if($req->city)in {{$req->city_name}},{{$req->state_name}} @endif</p>
                        </div>

                      </div>

                      <div class="sheman-hr">

                        <h4>{{ Carbon\Carbon::parse($req->created_at)->format('d M Y') }}</h4>

                      

                      </div>

                    </div>

                    <div class="admin-bg">

                      <p>Hi Admin</p>

                      <p>Please realease fund of this contract ID #{{$req->contractId}}</p>

                    </div>

                    @if(!empty($req->stripeAccId) && $req->payouts_enabled == 1 && !empty($req->cardSource))
                    <button type="button" class="btn btn-success resaled-btt" id="payoutAdmin" onclick="releaseFundByAdmin('<?php echo $req->relId;  ?>')">Release Fund</button>
                    @elseif(empty($req->stripeAccId) || $req->payouts_enabled !=1)
                    <div class="alert alert-warning">Freelancer not verified stripe detail yet.</div>
                    @elseif(empty($req->cardSource))
                    <div class="alert alert-warning">Client Card Detail not available.</div>
                    @endif

                  </div>

                </div>
                <?php
              }


            }else{
              echo  '<div class="col-md-12">No record found!</div>';

            }

            ?>
             {{ $paymentRequest->links() }}
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="post-table">
                        <h2 class="post-title">Recent Transaction</h2>
                        <div class="table-responsive-lg">
                            <table class="table table-bordered">
                              <thead>
                                <tr style="background-color: #f9f9f9;">
                                  <th scope="col" style="text-align: left;"><h4>Freelancer</h4></th>
                                  <th scope="col"><h4>Category</h4></th>
                                  <th scope="col"><h4>Complete Contract</h4></th>
                                  <th scope="col"><h4>Ongoing Contract</h4></th>
                                  <th scope="col"><h4>Hourly Rate</h4></th>
                                  <th scope="col"><h4>Action</h4></th>
                                </tr>
                              </thead>
                              <tbody>
                                
                               
                               @foreach($Recentpayment  as $queryData)

                                 <?php if(!empty($queryData->image)){

                        $img = $req->image;

                      }else{

                        $img = 'no-img.png';



                      }

                      ?>
                                <tr>
                                  <th scope="row" style="text-align: left;">
                                    <div class="table-profile">
                                <img src="{{asset('/uploads/user_images/'.$img)}}" style="width:50px;" class="rounded-circle">
                                        <div class="post-text">
                                            <h3>{{ucfirst($queryData->first_name.' '.$queryData->last_name)}}</h3>
                                        </div>
                                    </div>
                                  </th>
                                  <td><p>{{$queryData->category}} </p></td>
                                  <td><p>
                                       
                                    {{$queryData->complete}}
                                    
                                   
                                  </p></td>
                                  <td><p>{{$queryData->onGoing}}</p></td>
                                  <td><p>£{{$queryData->hourly_rate}}</p></td>
                                 
                                  <td>
                                    <div class="dropdown dot-button">
                                      <button class="btn btn-secondary menu-bt-tb" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                         <img src=" {!! asset('assets/img/dot.png') !!}">
                                      </button>
                                      <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="#">Action</a>
                                        <a class="dropdown-item" href="#">Another action</a>
                                        <a class="dropdown-item" href="#">Something else here</a>
                                      </div>
                                    </div>
                                    </td>
                                </tr>

                              @endforeach
                              </tbody>

                            </table>
                             {{ $Recentpayment->links() }}

                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="pagination-table">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
        </div>

@endsection


@section("footer-page-script")
    <!-- Start datatable js -->
    <script src="{!! asset('assets/js/jquery.dataTables.js') !!}"></script>
    <script src="{!! asset('assets/js/jquery.dataTables.min.js') !!}"></script>
    <script src="{!! asset('assets/js/dataTables.bootstrap4.min.js') !!}"></script>
    <script src="{!! asset('assets/js/dataTables.responsive.min.js') !!}"></script>
    <script src="{!! asset('assets/js/responsive.bootstrap.min.js') !!}"></script>
  
     
    
@endsection