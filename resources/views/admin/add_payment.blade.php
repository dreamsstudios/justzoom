@extends("admin.layouts.layout")

@section("title","Admin Dashboard | Just Zoom")

@section("header-page-script")  
    
    <!-- Start datatable css -->
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/jquery.dataTables.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/dataTables.bootstrap4.min.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/responsive.bootstrap.min.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/jqueryui.min.css') !!}">

@endsection

@section("content")   

<div id="content">
      <div class="client-page">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="deshbord-title">
                <h2>Admin comission</h2>
                
              </div>
            </div>
          </div>
           <form class="needs-validation" novalidate="" id="my-form" method="post" action="{{ route('save-payment-info') }}">
                 @if(session("msg"))
                            <div class="alert-dismiss">
                                <div class="alert alert-success alert-dismissible fade show">
                                    <span>{{session("msg")}}</span><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span class="fa fa-times"></span> </button><br/>
                                </div>
                            </div>
                            @endif
                            @if(count($errors)>0)
                            <div class="alert-dismiss">
                                <div class="alert alert-danger alert-dismissible fade show">
                                    @foreach($errors->all()  as $error)
                                        <span>{{$error}}</span><br/>
                                    @endforeach
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span class="fa fa-times"></span>
                                    </button>
                                </div>
                            </div>
                            @endif
                
          <div class="row">
                  <div class="col-md-8">
                  
                  <h4> Paltform charge</h4>
                  <div class="form-group ">
                    <input type="hidden" name="hiddenval" class="hiddenval" value="0">
                            {!!csrf_field()!!}
                  <input type="text" class="form-control"  name="paltformCharge" placeholder="Paltform Charge" value="" required="">
                                <div class="valid-feedback">
                                    Looks good!
                                </div>
                                <div class="invalid-feedback">
                                    Please provide a valid category.
                                </div>
                  </div>
                </div>
                </div>

                <div class="row">
                  <div class="col-md-8">
                  <h4>Stirpe Public Key</h4>
                  <div class="form-group ">
                            {!!csrf_field()!!}
                  <input type="text" class="form-control"  name="stirpePublicKey" placeholder="Stirpe Public Key" value="" required="">
                                <div class="valid-feedback">
                                    Looks good!
                                </div>
                                <div class="invalid-feedback">
                                    Please provide a valid category.
                                </div>
                  </div>
                </div>
                </div>

                 <div class="row">
                  <div class="col-md-8">
                  <h4>Secrete Key</h4>
                  <div class="form-group ">
                            {!!csrf_field()!!}
                  <input type="text" class="form-control"  name="secreteKey" placeholder="Secrete Key" value="" required="">
                                <div class="valid-feedback">
                                    Looks good!
                                </div>
                                <div class="invalid-feedback">
                                    Please provide a valid category.
                                </div>
                  </div>
                </div>
                </div>
                <div class="row">
                  <div class="back-button content-btt">
                  
                  <button class="btn btn-dark dark-next" type="submit">Submit</button>
                    </form>
                </div>
                </div>
        </div>
      </div>
        </div>

@endsection


@section("footer-page-script")

 <script src="https://cdn.tiny.cloud/1/un00cy8c82z2t4eq7lu1sg11nejnz6wunjnha1vqzavpuqut/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script type="text/javascript">
    tinymce.init({
        selector : "textarea",
        plugins : ["advlist autolink lists link image charmap print preview anchor", "searchreplace visualblocks code fullscreen", "insertdatetime media table contextmenu paste jbimages"],
        toolbar : "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image jbimages",
    });
</script>
    
@endsection