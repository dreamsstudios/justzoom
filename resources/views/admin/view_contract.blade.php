@extends("admin.layouts.layout")

@section("title","Admin Dashboard | JustZoom")

@section("header-page-script")  
    
   <!-- Start datatable css -->
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/jquery.dataTables.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/dataTables.bootstrap4.min.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/responsive.bootstrap.min.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/jqueryui.min.css') !!}">
    
    
@endsection

@section("content")   

<div id="content">

            <div class="client-page">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="deshbord-title">
                        <h2>Contract</h2>
                          <a href="{{ route('listcontract') }}" ><button type="button" class="btn btn-success blue">Back</button></a></a>
                    </div>
                    
                </div>
            </div>
            
            <div class="row">

                    <!-- Server side start -->
                    <div class="col-12">
                        <div class="card mt-5">
                            <div class="card-body">
                               
                                <div class="form-row">
                                        <div class="col-md-12 mb-3">
                                            <label for="city">Freelancer Name: </label>
                                            {{$queryData->freelancer_firstName}}
                                            
                                        </div> 
                                        <div class="col-md-12 mb-3">
                                            <label for="city">contractId: </label>
                                            {{$queryData->contractId}}
                                            
                                        </div> 
                                         <div class="col-md-12 mb-3">
                                            <label for="city">Category: </label>
                                            {{$queryData->category_name}}
                                            
                                        </div>
                                         <div class="col-md-12 mb-3">
                                            <label for="city">Client Name: </label>
                                            {{$queryData->client_firstName}}
                                            
                                        </div>
                                        <div class="col-md-12 mb-3">
                                            <label for="city">About Project: </label>
                                            {{$queryData->aboutProject}}
                                            
                                        </div>
                                        <div class="col-md-12 mb-3">
                                            <label for="city">Description: </label>
                                            {{$queryData->description}}
                                            
                                        </div>  
                                         
 <div class="col-md-12 mb-3">
                                            <label for="city">Price Type: </label>
                                            {{$queryData->priceType}}
                                            
                                        </div>  
 <div class="col-md-12 mb-3">
                                            <label for="city">Duration: </label>
                                            {{$queryData->duration}}hr
                                            
                                        </div>  
 <div class="col-md-12 mb-3">
                                            <label for="city">Platform Price: </label>
                                            {{$queryData->platformPrice}}
                                            
                                        </div>  
 <div class="col-md-12 mb-3">
                                            <label for="city">Preelancer Price: </label>
                                            £ {{$queryData->freelancerPrice}}
                                            
                                        </div>  
<div class="col-md-12 mb-3">
<label for="city">Total Price: </label>
£ {{$queryData->totalPrice}}

</div>  
<div class="col-md-12 mb-3">
<label for="city">Created Date: </label>
{{ Carbon\Carbon::parse($queryData->created_at)->format('d M Y') }}
</div> 
<div class="col-md-12 mb-3">
<label for="city">requirement: </label>
{{$queryData->requirement}}

</div> 
<div class="col-md-12 mb-3">
<label for="city">Contract Complete Status: </label>
@if($queryData->IsContractComplete ==1)
Done
@else
Ongoing
@endif
</div> 


                                
                            </div>
                        </div>
                    </div>
                    <!-- Server side end -->
                </div>
            </div>
            
        </div>
    </div>
        </div>

@endsection

@section("footer-page-script")
    <!-- Start datatable js -->
    <script src="{!! asset('assets/js/jquery.dataTables.js') !!}"></script>
    <script src="{!! asset('assets/js/jquery.dataTables.min.js') !!}"></script>
    <script src="{!! asset('assets/js/dataTables.bootstrap4.min.js') !!}"></script>
    <script src="{!! asset('assets/js/dataTables.responsive.min.js') !!}"></script>
    <script src="{!! asset('assets/js/responsive.bootstrap.min.js') !!}"></script>
   

    
