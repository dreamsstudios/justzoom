</div>
<div class="deshbord-footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <p>Copyright © 2020 Justzoom. All Right Reserved.</p>
                </div>
                <div class="col-md-6">
                    <ul class="social-link">
                        <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                        <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- jQuery CDN - Slim version (=without AJAX) -->

   <script type="text/javascript" src=" {!! asset('assets/js/jquery.min.js') !!}"></script>
   <script type="text/javascript" src=" {!! asset('assets/js/popper.min.js') !!}"></script>
   <script type="text/javascript" src=" {!! asset('assets/js/bootstrap.min.js') !!}"></script>
   <script type="text/javascript" src=" {!! asset('assets/js/custom.js') !!}"></script>
  
    <script type="text/javascript">
       function releaseFundByAdmin(relId){
        $("#payoutAdmin").prop('disabled', true).html("Loading");
          
        $.ajax({
            headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  },

            type: 'post',

            url : '/dashboard/releasedFund',

            data :{relId:relId,action:'releasedFund'},

            success:function(resp){

            if (resp.valid) {
            alert("Payment Done successfully.");
            setTimeout(function(){ location.reload(); }, 100);

            }

            },error: function(resp){
                console.log(resp);
                $.each(resp['errors'], function(key, value){
                    alert(value);
                      $('.alert-danger').show();
                      $('.alert-danger').append('<p>'+value+'</p>');
  
            });

            alert('Error');

            }

        });

       }
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
            });
        });
    </script>
