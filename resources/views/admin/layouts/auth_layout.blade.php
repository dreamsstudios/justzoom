<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Just Zoom -@yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/x-icon" href="{!! asset('assets/img/Favicon.png') !!}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
     <link rel="stylesheet" href="{!! asset('assets/css/bootstrap.min.css') !!}">
     <link rel="stylesheet" href="{!! asset('assets/css/Dashboard.css') !!}">
     <link rel="stylesheet" href="{!! asset('assets/css/slick.css') !!}">
     <link rel="stylesheet" href="{!! asset('assets/css/slick-theme.css') !!}">
     <link rel="stylesheet" href="{!! asset('assets/css/style.css') !!}">
     <link rel="stylesheet" href="{!! asset('assets/css/responsive.css') !!}">
  
</head>

<body>
   
    <div id="preloader">
        <div class="loader"></div>
    </div>
    <!-- preloader area end -->
    @section('content')
    @show
    
     <script type="text/javascript" src=" {!! asset('assets/js/jquery.min.js') !!}"></script>
   <script type="text/javascript" src=" {!! asset('assets/js/popper.min.js') !!}"></script>
   <script type="text/javascript" src=" {!! asset('assets/js/bootstrap.min.js') !!}"></script>
   <script type="text/javascript" src=" {!! asset('assets/js/custom.js') !!}"></script>
  
    <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
            });
        });
    </script>

</body>

</html>