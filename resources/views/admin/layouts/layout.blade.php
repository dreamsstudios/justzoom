<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Just Zoom - @yield('title')</title>

        {{-- include styles --}}
        @include("admin.layouts.header_script")

        {{-- dynamic content --}}
        @section("header-page-script")
        @show
    </head>
    <body>
      
        <div id="preloader">
            <div class="loader"></div>
        </div>
        <!-- preloader area end -->
        <!-- page container area start -->
        <div class="page-container">
             {{-- include header --}}
            @include("admin.layouts.header")

            {{-- include left sidebar panel --}}
            @include("admin.layouts.sidebar")

                     

            {{-- dynamic content --}}
            @section("content")
            @show
            
            {{-- include footer --}}
            @include("admin.layouts.footer")

            {{-- dynamic content --}}
            @section("footer-page-script")
            @show
            
    </body>
</html>
