
<!-- sidebar menu area start -->
<div class="aside">
<nav id="sidebar">
<?php
$segment = Request::segment(2);
//echo $segment; exit();
?> 
            <ul class="list-unstyled components">
                 
                   <li class="{{ request()->is('dashboard') ? 'active' : '' }}"> 
                   <a href="{{ route('dashboard') }}"><span><img class="gray-icon" src="{!! asset('assets/img/deshbord1.png') !!}"><img class="white-icon" src="{!! asset('assets/img/deshbord1-white.png') !!}"></span> Dashboard</a>
                </li>
               <li class="@if($segment=='approvel-client')active
                     @elseif($segment=='approved-client')active    
                   @endif">
                    <a href="#pageSubmenu1" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><span><img class="gray-icon" src="{!! asset('assets/img/deshbord2.png') !!}"> <img class="white-icon" src="{!! asset('assets/img/deshbord2-white.png') !!}"></span>Clients</a>
                    <ul class="collapse list-unstyled" id="pageSubmenu1">
                    <li class="@if($segment=='approvel-client')active
                   @endif ">
                            <a href="{{ route('approvel-client') }}">Waiting for Approvel</a>
                        </li>
                        <li class="@if($segment=='approved-client')active
                   @endif ">
                            <a href="{{ route('approved-client') }}">Approved Clients</a>
                        </li>
                    </ul>
                </li>

                

                <li class="@if($segment=='approvel-user')active
                     @elseif($segment=='approved-user')active    
                   @endif">
                    <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><span><img class="gray-icon" src="{!! asset('assets/img/deshbord3.png') !!}"> <img class="white-icon" src="{!! asset('assets/img/deshbord3-white.png') !!}"></span>Freelancer</a>
                    <ul class="collapse list-unstyled" id="pageSubmenu">
                    <li class="@if($segment=='approvel-user')active
                   @endif ">
                            <a href="{{ route('approvel-user') }}">Waiting for Approvel</a>
                        </li>
                        <li class="@if($segment=='approved-user')active
                   @endif ">
                            <a href="{{ route('approved-user') }}">Approved Freelancer</a>
                        </li>
                    </ul>
                </li>
                     <li class="@if($segment=='payments')
              active
              @endif
              ">
                  <a href="{{ route('payments') }}"> <span><img class="gray-icon" src="{!! asset('assets/img/deshbord5.png') !!}"><img class="white-icon" src="{!! asset('assets/img/deshbord5-white.png') !!}"></span> Payment Management</a>
                </li>
               <li class="@if($segment=='category')
              active
              @endif
              ">
                  <a href="{{ route('category') }}"> <span><img class="gray-icon " src="{!! asset('assets/img/deshbord4.png') !!}"><img class="white-icon" src="{!! asset('assets/img/deshbord4-white.png') !!}"></span> Category Management</a>
                </li>
               <li class="@if($segment=='blog')
              active
              @endif
              ">
                  <a href="{{ route('admin-blogs') }}"> <span><img class="gray-icon" src="{!! asset('assets/img/deshbord6.png') !!}"><img class="white-icon" src="{!! asset('assets/img/deshbord6-white.png') !!}"></span> Blog Management</a>
                </li>
                 <li class="@if($segment=='skills')
              active
              @endif
              ">
                  <a href="{{ route('skills') }}"> <span><img class="gray-icon" src="{!! asset('assets/img/deshbord6.png') !!}"><img class="white-icon" src="{!! asset('assets/img/deshbord6-white.png') !!}"></span> Skills Management</a>
                </li>

                 

       <li class="@if($segment=='query-list')
              active
              @endif
              ">
                  <a href="{{ route('listquery') }}"><span><img class="gray-icon" src="{!! asset('assets/img/deshbord7.png') !!}"><img class="white-icon" src="{!! asset('assets/img/deshbord7-white.png') !!}"></span> Messages</a>
                </li>
                
        <li class="@if($segment=='contract-list')
              active
              @endif
              ">
                  <a href="{{ route('listcontract') }}"> <span><img class="gray-icon" src="{!! asset('assets/img/deshbord4.png') !!}"><img class="white-icon" src="{!! asset('assets/img/deshbord4-white.png') !!}"></span> Contracts</a>
                </li>

                   <li class="@if($segment=='newsletter-subscribers')
              active
              @endif
              ">
                  <a href="{{ route('newsletter-subscribers') }}"> <span><img class="gray-icon" src="{!! asset('assets/img/deshbord4.png') !!}"><img class="white-icon" src="{!! asset('assets/img/deshbord4-white.png') !!}"></span> Newsletter subscribers</a>
                </li>
                <li class="@if($segment=='settings')
              active
              @endif
              ">
                  <a href="{{ route('settings') }}"> <span><img class="gray-icon" src="{!! asset('assets/img/deshbord8.png') !!}"><img class="white-icon" src="{!! asset('assets/img/deshbord8-white.png') !!}"></span> Settings</a>
                </li>
                  <li>
                  <a href="{{ route('logout') }}"> <span><img class="gray-icon" src="{!! asset('assets/img/deshbord8.png') !!}"><img class="white-icon" src="{!! asset('assets/img/deshbord8-white.png') !!}"></span> Sign Out</a>
                </li>
            </ul>
        </nav>

<!-- sidebar menu area end -->

