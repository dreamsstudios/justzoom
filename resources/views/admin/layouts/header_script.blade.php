
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
      <!-- stylesheet -->
     <link rel="icon" type="image/x-icon" href="{!! asset('assets/img/Favicon.png') !!}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
     <link rel="stylesheet" href="{!! asset('assets/css/bootstrap.min.css') !!}">
     <link rel="stylesheet" href="{!! asset('assets/css/Dashboard.css') !!}">
 <meta name="csrf-token" content="{{ csrf_token() }}" />
    <script type="text/javascript">
        var BASEURL = "{{route('home')}}";
    </script>