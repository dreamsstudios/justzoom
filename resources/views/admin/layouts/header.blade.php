
<nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">
          <a class="navbar-brand" href="{{ route('dashboard') }}"><img src="{!! asset('assets/img/logo.png') !!}"></a>
            <div class="toggle-button">
                 <button type="button" id="sidebarCollapse" class="btn btn-info toggle">
                    <i class="fas fa-align-justify"></i>
                </button>
                <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fas fa-align-justify"></i>
                </button>
            </div>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto right-nav">
                   
                    <li>
                        <div class="dropdown nav">
                          <button class="btn  dropdown-toggle bttt" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <a class="nav-link" href="#"><img src="{!! asset('assets/img/avatar.png') !!}">Admin</a>
                          </button>
                          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <ul class="nav-dropdown">
                                {{-- <li>
                                    <img src="assets/img/drop1.png">
                                    <a href="{{ route('dashboard') }}">Dashboard</a>
                                </li>
                                <li>
                                    <img src="assets/img/drop2.png">
                                    <a href="#">Profile</a>
                                </li>
                                <li>
                                    <img src="assets/img/drop3.png">
                                    <a href="#">Edit Profile</a>
                                </li> --}}
                               {{--  <li>
                                    <img src="assets/img/drop4.png">
                                    <a href="#">Payment Request</a>
                                </li>
                                <li>
                                    <img src="assets/img/drop5.png">
                                    <a href="#">Contracts</a>
                                </li>
                                <li>
                                    <img src="assets/img/drop6.png">
                                    <a href="#">Support</a>
                                </li> --}}
                                <li>
                                    <img src="{!! asset('assets/img/drop7.png') !!}">
                                    <a href="{{ route('logout') }}">Sign Out</a>
                                </li>
                            </ul>
                          </div>
                        </div>
                    </li>
                    
                </ul>
            </div>
        </div>
    </nav>
    <!-- header area end