@extends("admin.layouts.layout")

@section("title","Admin Dashboard | Justzoom")

@section("header-page-script")   
    
    <!-- Start datatable css -->
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/jquery.dataTables.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/dataTables.bootstrap4.min.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/responsive.bootstrap.min.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/jqueryui.min.css') !!}">

    
@endsection

@section("content")            
   <div id="content">

            <div class="client-page">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="deshbord-title">
                         <h2>Contract List</h2>
                        
                       
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                   @if(session("msg"))
                            <div class="alert-dismiss">
                                <div class="alert alert-success alert-dismissible fade show">
                                    <span>{{session("msg")}}</span><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span class="fa fa-times"></span> </button><br/>
                                </div>
                            </div>
                            @endif
                            @if(count($errors)>0)
                            <div class="alert-dismiss">
                                <div class="alert alert-danger alert-dismissible fade show">
                                    @foreach($errors->all()  as $error)
                                        <span>{{$error}}</span><br/>
                                    @endforeach
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span class="fa fa-times"></span>
                                    </button>
                                </div>
                            </div>
                            @endif
                    <div class="post-table">
                        <div class="table-responsive-lg">
                            <table class="table table-bordered" id="example1">
                              <thead>

                                <tr style="background-color: #f9f9f9;">
                                  <th scope="col" style="text-align: left;"><h4>Freelancer</h4></th>
                                  <th scope="col"><h4>Contract ID</h4></th>
                                  <th scope="col"><h4>Category</h4></th>
                                   <th scope="col" style="text-align: left;"><h4>Client</h4></th>
                                  <th scope="col"><h4>Total Amount</h4></th>
                                  <th scope="col"><h4>Freelancer Amount</h4></th>
                                  <th scope="col"><h4>Action</h4></th>
                                </tr>
                              </thead>
                              <tbody>

@foreach($queryData  as $query)

<tr>
<th scope="row" style="text-align: left;">
<?php if(!empty($query->freelancer_image)){
$img = $query->freelancer_image;
}else{
$img = 'no-img.png';

}
?>
                                    <div class="table-profile">
                                        <img src="{{asset('/uploads/user_images/'.$img)}}" style="width:50px;" class="rounded-circle">
                                         <div class="post-text">
                                            <h3>{{$query->freelancer_firstName}}</h3>
                                        </div>
                                    </div>
                                  </th>

<td><p>
@if(!empty($query->contractId))
  {{$query->contractId}}
@else
N/A
@endif

</p></td>
<td><p>
@if(!empty($query->category_name))
  {{$query->category_name}}
@else
N/A
@endif

</p></td>
<th scope="row" style="text-align: left;">
<?php if(!empty($query->client_image)){
$img = $query->client_image;
}else{
$img = 'no-img.png';

}
?>
                                    <div class="table-profile">
                                        <img src="{{asset('/uploads/user_images/'.$img)}}" style="width:50px;" class="rounded-circle">
                                         <div class="post-text">
                                            <h3>{{$query->client_firstName}}</h3>
                                        </div>
                                    </div>
                                  </th>
<td><p>
@if(!empty($query->totalPrice))
  £{{$query->totalPrice}}
@else
N/A

@endif

</p></td>

      

<td><p>
@if(!empty($query->freelancerPrice))
  £{{$query->freelancerPrice}}
@else
N/A

@endif

</p></td>
           

<td>

<div class="dtable-done-btt">
  <a href="{{ route('contract-details',$query->contractId) }}" ><button type="button" class="btn btn-success green">views</button></a>
  </div>

</td>

</tr>

@endforeach

</tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
        </div>        

@endsection


@section("footer-page-script")
    <!-- Start datatable js -->
    <script src="{!! asset('assets/js/jquery.dataTables.js') !!}"></script>
    <script src="{!! asset('assets/js/jquery.dataTables.min.js') !!}"></script>
    <script src="{!! asset('assets/js/dataTables.bootstrap4.min.js') !!}"></script>
    <script src="{!! asset('assets/js/dataTables.responsive.min.js') !!}"></script>
    <script src="{!! asset('assets/js/responsive.bootstrap.min.js') !!}"></script>
    <script src="{!! asset('assets/js/admin.js') !!}"></script>
    <script>
         $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
    </script>
    
    <script type="text/javascript">
      
       $(".UpdateSectionStatus").click(function(){
              var status = $(this).text();
              var user_id = $(this).attr("user_id");
             $.ajax({
                   type: 'post',
                   url : '/dashboard/update-user-status',
                   data :{status:status,user_id:user_id},
                   success:function(resp){
                  if (resp['status']==0) {
                    $("#User-"+user_id).html('Verified').css("color", "green");
                  }else if(resp['status']==1){
                    $("#User-"+user_id).html('Unverified').css("color", "red");
                  }
                   },error: function(){
                    alert('ERRRRr');
                   }
             });

        });

       
    </script>

    <script type="text/javascript">
      
       $(".ApproveStatus").click(function(){
       
              var user_id = $(this).attr("user_id");
             $.ajax({
                   type: 'post',
                   url : '/dashboard/approve-user-status',
                   data :{status:status,user_id:user_id},
                   success:function(resp){
                  if (resp['status']==1) {
                    $('#delmsgvdo_'+user_id).html('Successfully Approve.').css("color", "green");
                    setTimeout(function(){ location.reload(); }, 100);
                  }
                   },error: function(){
                    alert('ERRRRr');
                   }
             });

        });

       
    </script>
@endsection