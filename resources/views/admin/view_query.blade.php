@extends("admin.layouts.layout")



@section("title","Admin Dashboard | JustZoom")



@section("header-page-script")  

    

   <!-- Start datatable css -->

    <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/jquery.dataTables.css') !!}">

    <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/dataTables.bootstrap4.min.css') !!}">

    <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/responsive.bootstrap.min.css') !!}">

    <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/jqueryui.min.css') !!}">

    

    

@endsection



@section("content")   



<div id="content">



            <div class="client-page">

        <div class="container-fluid">

            <div class="row">

                <div class="col-md-12">

                    <div class="deshbord-title">

                        <h2>Querys</h2>

                    </div>

                    

                </div>

            </div>

            

            <div class="row">



                    <!-- Server side start -->

                    <div class="col-12">

                        <div class="card mt-5">

                            <div class="card-body">

                               

                                <div class="form-row">

                                        <div class="col-md-12 mb-3">

                                            <label for="city">Name: </label>

                                            {{$queryData->name}}

                                            

                                        </div> 

                                        <div class="col-md-12 mb-3">

                                            <label for="city">Email: </label>

                                            {{$queryData->email}}

                                            

                                        </div> 

                                        <div class="col-md-12 mb-3">

                                            <label for="city">Subject: </label>

                                            {{$queryData->subject}}

                                            

                                        </div>

                                        <div class="col-md-12 mb-3">

                                            <label for="city">Query: </label>

                                            {{$queryData->message}}

                                            

                                        </div>  



                                

                            </div>

                        </div>

                    </div>

                    <!-- Server side end -->

                </div>

            </div>

            

        </div>

    </div>

        </div>



@endsection



@section("footer-page-script")

    <!-- Start datatable js -->

    <script src="{!! asset('assets/js/jquery.dataTables.js') !!}"></script>

    <script src="{!! asset('assets/js/jquery.dataTables.min.js') !!}"></script>

    <script src="{!! asset('assets/js/dataTables.bootstrap4.min.js') !!}"></script>

    <script src="{!! asset('assets/js/dataTables.responsive.min.js') !!}"></script>

    <script src="{!! asset('assets/js/responsive.bootstrap.min.js') !!}"></script>

   



    

