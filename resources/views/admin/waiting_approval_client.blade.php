@extends("admin.layouts.layout")

@section("title","Admin Dashboard | Market Place")

@section("header-page-script")   
    
    <!-- Start datatable css -->
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/jquery.dataTables.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/dataTables.bootstrap4.min.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/responsive.bootstrap.min.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/jqueryui.min.css') !!}">

    
@endsection

@section("content")            
   <div id="content">

            <div class="client-page">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="deshbord-title">
                         <h2>Waiting for Approvel</h2>
                         <a href="{{ route('trashed-client') }}"><button type="button" class="btn btn-success" style="background-color: #2caf97;    border-color: #2caf97;">Trashed Client</button></a>
                        
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                   @if(session("msg"))
                            <div class="alert-dismiss">
                                <div class="alert alert-success alert-dismissible fade show">
                                    <span>{{session("msg")}}</span><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span class="fa fa-times"></span> </button><br/>
                                </div>
                            </div>
                            @endif
                            @if(count($errors)>0)
                            <div class="alert-dismiss">
                                <div class="alert alert-danger alert-dismissible fade show">
                                    @foreach($errors->all()  as $error)
                                        <span>{{$error}}</span><br/>
                                    @endforeach
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span class="fa fa-times"></span>
                                    </button>
                                </div>
                            </div>
                            @endif
                    <div class="post-table">
                        <div class="table-responsive-lg">
                            <table class="table table-bordered" id="example1">
                              <thead>

                                <tr style="background-color: #f9f9f9;">
                                  <th scope="col" style="text-align: left;"><h4>Clients</h4></th>
                                  <th scope="col"><h4>Title</h4></th>
                                  <th scope="col"><h4>Complete Contract</h4></th>
                                  
                                  <th scope="col"><h4>Ongoing Contract</h4></th>
                                  <th scope="col"><h4>Status</h4></th>
                                  <th scope="col"><h4>Action</h4></th>
                                </tr>
                              </thead>
                              <tbody>

@foreach($queryData  as $queryData)

<tr>
<th scope="row" style="text-align: left;">
<?php if(!empty($queryData->image)){
$img = $queryData->image;
}else{
$img = 'no-img.png';

}
?>
                                    <div class="table-profile">
                                        <img src="{{asset('/uploads/user_images/'.$img)}}" style="width:50px;" class="rounded-circle">
                                        <div class="post-text">
                                            <h3>{{$queryData->first_name}}</h3>
                                        </div>
                                    </div>
                                  </th>

<td><p>
@if(!empty($queryData->job_title))
  {{$queryData->job_title}}
@else
N/A
@endif

</p></td>

<td><p>

{{$queryData->complete}}


</p></td>

      

<td><p>
{{$queryData->onGoing}}

</p></td>
<td><P>@if($queryData->status==0)

<a class="UpdateSectionStatus" id="Client-{{$queryData->id}}" client_id="{{$queryData->id}}" href="javascript:Void(0)" style="color: green"> Verified</a>

@else

<a class="UpdateSectionStatus" id="Client-{{$queryData->id}}" client_id="{{$queryData->id}}" href="javascript:Void(0)" style="color: red"> Unverified</a>

@endif

</P></td>              

<td>
  <span id="delmsgvdo_<?php echo $queryData->id; ?>">
<div class="dtable-done-btt"><button type="button" class="btn btn-success green ApproveStatus " client_id="{{$queryData->id}}">Approve</button>
  <a href="{{ route('clientprofile',$queryData->id) }}" ><button type="button" class="btn btn-success blue">View Profile</button></a>
  



            @if($queryData->trashed())

      <form action="{{ route('restore-client',$queryData->id) }}" method="POST"
              style="display: inline" >
           @method('PUT')
           @csrf
            <button class="btn btn-primary">Restore</button>
        </form>
           
             @endif

            <form action="{{ route('deleteclient',$queryData->id) }}" method="POST"
              style="display: inline"
              onsubmit="return confirm('Are you sure you wish to delete ?');">
          
            @csrf
            <button class="btn btn-danger">
           {{ $queryData->trashed()?'Delete':'Trash' }}
            </button>
        </form>
        
</td>

</tr>

@endforeach

</tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
        </div>        

@endsection


@section("footer-page-script")
    <!-- Start datatable js -->
    <script src="{!! asset('assets/js/jquery.dataTables.js') !!}"></script>
    <script src="{!! asset('assets/js/jquery.dataTables.min.js') !!}"></script>
    <script src="{!! asset('assets/js/dataTables.bootstrap4.min.js') !!}"></script>
    <script src="{!! asset('assets/js/dataTables.responsive.min.js') !!}"></script>
    <script src="{!! asset('assets/js/responsive.bootstrap.min.js') !!}"></script>
    
    <script>
         $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
    </script>
    
    <script type="text/javascript">
      
       $(".UpdateSectionStatus").click(function(){
              var status = $(this).text();
              var client_id = $(this).attr("client_id");
             $.ajax({
                   type: 'post',
                   url : '/dashboard/update-client-status',
                   data :{status:status,client_id:client_id},
                   success:function(resp){
                  if (resp['status']==0) {
                    $("#Client-"+client_id).html('Verified').css("color", "green");
                  }else if(resp['status']==1){
                    $("#Client-"+client_id).html('Unverified').css("color", "red");
                  }
                   },error: function(){
                    alert('ERRRRr');
                   }
             });

        });

       
    </script>

    <script type="text/javascript">
      
       $(".ApproveStatus").click(function(){
              var client_id = $(this).attr("client_id");
             $.ajax({
                   type: 'post',
                   url : '/dashboard/approve-client-status',
                   data :{status:status,client_id:client_id},
                   success:function(resp){
                  if (resp['status']==1) {
                    $('#delmsgvdo_'+client_id).html('Successfully Approve.').css("color", "green");
                    setTimeout(function(){ location.reload(); }, 100);
                  }
                   },error: function(){
                    alert('ERRRRr');
                   }
             });

        });

       
    </script>
@endsection