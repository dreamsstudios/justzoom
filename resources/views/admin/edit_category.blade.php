@extends("admin.layouts.layout")

@section("title","Admin Dashboard | Just Zoom")

@section("header-page-script")  
    
    <!-- Start datatable css -->
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/jquery.dataTables.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/dataTables.bootstrap4.min.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/responsive.bootstrap.min.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/jqueryui.min.css') !!}">

@endsection

@section("content")   

<div id="content">
      <div class="client-page">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="deshbord-title">
                <h2>Category</h2>
                
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="cata-setaps">
                <div class="arrow-steps clearfix">
                  <div class="step"> <span>Category</span> </div>
                  <div class="step current"> <a href="{{ route('category') }}"><span>&nbsp;&nbsp;&nbsp;List Categoryes</span></a> </div>
                   
                </div>
                <form class="needs-validation" novalidate="" id="my-form" method="post" action="{{ route('savecategory') }}">
    
      
                 @if(session("msg"))
                            <div class="alert-dismiss">
                                <div class="alert alert-success alert-dismissible fade show">
                                    <span>{{session("msg")}}</span><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span class="fa fa-times"></span> </button><br/>
                                </div>
                            </div>
                            @endif
                            @if(count($errors)>0)
                            <div class="alert-dismiss">
                                <div class="alert alert-danger alert-dismissible fade show">
                                    @foreach($errors->all()  as $error)
                                        <span>{{$error}}</span><br/>
                                    @endforeach
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span class="fa fa-times"></span>
                                    </button>
                                </div>
                            </div>
                            @endif
                <div class="radio-buttons catre-name">
                  <h3>Category Name</h3>
                  <div class="form-group question-input">
                    <input type="hidden" name="hiddenval" class="hiddenval" value="{{$category->id}}">
                            {!!csrf_field()!!}
                    <input type="text" class="form-control" id="category_name" name="category_name"   required="" value="{{ $category->category_name }}">
                                <div class="valid-feedback">
                                    Looks good!
                                </div>
                                <div class="invalid-feedback">
                                    Please provide a valid skill.
                                </div>
                            </div>
                  </div>
                </div>
                <div class="back-button content-btt">
                  
                  <button class="btn btn-dark dark-next" type="submit">Update</button>
                    </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
        </div>

@endsection


@section("footer-page-script")
    
    
   

    

@endsection