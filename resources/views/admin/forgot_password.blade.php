@extends('admin.layouts.auth_layout')
@section('title', 'Admin Dashboard | JustZoom')

@section('content')

    <!-- forgot area start -->
    <div class="page-wraper">
  
  <div class="login-page">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="login-form-fr">
            <h1>Forgot Password</h1>
            <form action="/action_page.php">
              <div class="form-group">
              <input type="text" class="form-control" id="Username" placeholder="New password">
              </div>
              <div class="form-group">
              <input type="password" class="form-control" id="pwd" placeholder="Verify password">
              </div>
             
              <button type="button" class="btn btn-default loginbt">Reset My Password</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  
</div>
    <!-- login area end -->

@endsection