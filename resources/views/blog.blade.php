@extends('frontend.layouts.layout')



@section('title', 'Justzoom')

@section('body')

 <div class="blog-bg">

        <div class="container-fluid">

            <div class="row">

                <div class="col-md-7">

                    <h2>Justzoom Blog</h2>

                    <p>From freelancer advice to small business stories, you'll find the<br> most up to date content right here, right now.</p>

                </div>

            </div>

        </div>

    </div>

    <div class="blog-listing">

        <div class="container-fluid">

            

            

            

            

            <div class="row">

                @foreach($queryData as $blog)

                <div class="col-md-6">

                    <div class="blog-box">

                        <div class="blog-img">

                            <img src="{{asset('/uploads/blogs/'.$blog->image)}}">

                            <h2>{{$blog->title}}</h2>

                        </div>

                        <h3>{!!str_limit($blog->content,70)!!}</h3>

                        <a href="{{ route('blog-details',$blog->slug) }}">Read More <span><i class="fas fa-chevron-right"></i></span></a>

                    </div>

                </div>

                @endforeach

                

            </div>

            <div class="row">

                <div class="pagination-list">

                {{ $queryData->links() }}

                </div>

            </div>

        </div>

    </div>

     @include("frontend.layouts.common_register_user")

    <div class="Justzoom-Developers">

        <div class="container">

            <div class="row">

                <div class="col-md-12">

                    <h2 class="just-title">Justzoom Developers</h2>

                </div>

               <div class="col-md-3">

                    <ul>

                        @foreach($category_list as $category)

                        <li><h3>{{$category}}</h3></li>

                        @endforeach

                    </ul>

                </div>

                

                

            </div>

        </div>

    </div>



@endsection

