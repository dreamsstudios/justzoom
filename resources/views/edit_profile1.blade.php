@extends('frontend.layouts.user_dashboard_layout')

@section('title', 'Justzoom')
@section('body')
<!-- comma seperated input css -->
	<link rel="stylesheet" href="{!! asset('assets/frontend/css/bootstrap-tagsinput.css') !!}">
	 <script type="text/javascript" src= "{!! asset('assets/frontend/js/bootstrap-tagsinput.min.js') !!}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>

    <!-- comma seperated input css -->
	
			<div class="edit-profile-page">
		<div class="container-fluid">
			<div class="row profile-information">
				<div class="col-md-12">
					<div class="edit-title">
						<h2>Edit your Profile</h2>
						<h3>Fill in your profile information</h3>
					</div>
					 
					 @if(session("msg"))
                                        <div class="alert-dismiss">
                                            <div class="alert alert-success alert-dismissible fade show">
                                                <span>{{session("msg")}}</span><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span class="fa fa-times"></span> </button><br/>
                                            </div>
                                        </div>
                            @endif
							 @if(session("errormsg"))
                                        <div class="alert-dismiss">
                                            <div class="alert alert-danger alert-dismissible fade show">
                                                <span>{{session("errormsg")}}</span><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span class="fa fa-times"></span> </button><br/>
                                            </div>
                                        </div>
                            @endif		
				</div>
				<div class="col-md-7">
					<div class="profile-bg">
						<div class="profile-photo">
							<h5>Profile Photo</h5>
							<div class="image-upload">
								<?php
									if(!empty($user->image)){
                                             $img = $user->image;
                                         }else{
                                          $img = 'no-img.png';

                                         }
                                         ?>
								<img id="preview_image" src="{{asset('/uploads/user_images/'.$img)}}" alt="profile img"  class="img-responsive" style="width: 20%;"> 
								<i id="loading" class="fa fa-spinner fa-spin fa-3x fa-fw" style="position: absolute;left: 40%;top: 40%;display: none"></i>	
								<label><a class="edit_imgicon" for="file-input" href="javascript:changeProfile()" >
									<span>Change Photo</span>
								</a></label>
								
								
							</div>
							
						</div>
						<div class="profile-form">
							<form class="needs-validation" novalidate="" id="my-form" method="post" action="{{route('saveuserdata')}}" enctype = "multipart/form-data">
										{!!csrf_field()!!}

                             <div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label for="first_name">First Name</label>
											<input type="text" name="first_name" placeholder="Your First Name" value="{{$user->first_name}}" class="form-control">
													{!! $errors->first('first_name', '<p class="help-block">:message</p>') !!}
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label for="last_name">Last Name</label>
											<input type="text" name="last_name" placeholder="Your Last Name" value="{{$user->last_name}}" class="form-control">
													{!! $errors->first('last_name', '<p class="help-block">:message</p>') !!}
										</div>
									</div>
								</div>

                                <div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<label for="category_id">Job Category</label>
											<select name="category_id" class="form-control" >
     @if (!$category_data->isEmpty())
     <option  value="">Select Category</option>
     @foreach($category_data as $value)
         
       <option 
   
         @if ($value->id == $user->category_id)
         {{'selected'}}
       @endif 

      
     
          value="{{$value->id}}" >{{$value->category_name}}
       </option>
         @endforeach
       @endif 

   </select>
											
										</div>
									</div>
								</div>
<div class="row">
									
									<div class="col-md-4">
										<div class="form-group">
											<label for="exampleInputPassword3">Projects worked </label>
											<input type="text" class="form-control" id="projects_worked" name="projects_worked" placeholder="Enter Projects worked" value="{{ $user->projects_worked }}" >
													 
										</div>
									</div>

										<div class="col-md-4">
										<div class="form-group">
											<label for="exampleInputPassword3">Buyers worked</label>
											<input type="text" class="form-control" id="buyers_worked" name="buyers_worked" placeholder="Enter Buyers worked" value="{{ $user->buyers_worked }}">
													
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label for="">Response time</label>
											<input type="text" class="form-control" id=" 	response_time" name="response_time" placeholder="Enter Response time" value="{{$user->response_time }}" >
													 
										</div>
									</div>
								</div>
                              <div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<label for="email">Email</label>
											<input type="email" name="email" placeholder="Your Email Id" value="{{$user->email}}"class="form-control">
													{!! $errors->first('email', '<p class="help-block">:message</p>') !!}
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label for="address_line_1">Address</label>
											<input type="text" id="address_line_1" name="address_line_1" placeholder="Enter Address Line 1" value="{{ $user->address_line_1 }} " class="form-control">
													 {!! $errors->first('address_line_1', '<p class="help-block">:message</p>') !!}
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label for="exampleInputPassword3">Country</label>
											<select class="form-control" id="country" name="country" onchange="state_list(this.value,event)" >
										<option  value="">Select Country</option>
													@foreach($country_list as $key=>$category)
															<option {{ $user->country == $key ? 'selected' : '' }} value="{{$key}}"> {{$category}}</option>
														@endforeach
												</select>
													
													 {!! $errors->first('country', '<p class="help-block">:message</p>') !!}
										</div>
									</div>
								</div>
								<div class="row">
									
									<div class="col-md-4">
										<div class="form-group">
											<label for="exampleInputPassword3">State</label>
											<select class="form-control" id="state" name="state" onchange="city_list(this.value,event)" >
												<option  value="">Select State</option>
												@foreach($state_list as $key=>$state)
															<option {{ $user->state == $key ? 'selected' : '' }} value="{{$key}}"> {{$state}}</option>
														@endforeach
												</select>
													 {!! $errors->first('state', '<p class="help-block">:message</p>') !!}
										</div>
										</div>

										<div class="col-md-4">
										<div class="form-group">
											<label for="exampleInputPassword3">City</label>
											<select class="form-control" id="city" name="city" >
													<option value="">Select City</option>
													@foreach($city_list as $key=>$city)
															<option {{ $user->city == $key ? 'selected' : '' }} value="{{$key}}"> {{$city}}</option>
														@endforeach
												</select>
													 {!! $errors->first('city', '<p class="help-block">:message</p>') !!}
												
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label for="exampleInputPassword3">Pincode</label>
											<input type="text" class="form-control" id="pincode" name="pincode" placeholder="Enter pincode" value="{{ $user->pincode }}" >
													 {!! $errors->first('pincode', '<p class="help-block">:message</p>') !!}
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<label for="about">About You</label>
											<textarea class="form-control" id="about" rows="3" name="about" placeholder="About You">{{ $user->about }}
													</textarea>

													 {!! $errors->first('about', '<p class="help-block">:message</p>') !!}
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										
											<div class="form-group">
												<label for="exampleFormControlSelect2">Hourly Rate</label>
													
									<div class="form-group">
								<input type="text" class="form-control" id="hourly_rate" placeholder="Enter Your Hourly Rate" name="hourly_rate" value="{{ $user->hourly_rate }}">
								{!! $errors->first('hourly_rate', '<p class="help-block">:message</p>') !!}
													</div>
													
													
											</div>
										
									</div>
									<div class="col-md-6">
										
											<div class="form-group">
												<label for="exampleFormControlSelect2" class="qustion-icon">Phone <span><i class="fas fa-question-circle"></i></span></label>
													
													
													<div class="form-group">
													<input type="text" name="phone" placeholder="Your Number" value="{{$user->phone}}" class="form-control">
													{!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
												</div>
												</div>	
											
										</div>
									</div>
									<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											
											<label >Select Skills</label>
   <select name="skills[]" class="form-control" multiple>
     @if (!$skill_data->isEmpty())
     <option  value="">Select Skills</option>
     @foreach($skill_data as $value)
         
       <option 
      @foreach($userskill as $value1)
         @if ($value->id == $value1->skill_id)
         {{'selected'}}
       @endif 

       @endforeach
     
          value="{{$value->id}}" >{{$value->skill}}
       </option>
         @endforeach
       @endif 

   </select>
											{!! $errors->first('skills', '<p class="help-block">:message</p>') !!}
										</div>
									</div>
								</div>

								<div class="row">
									<div class="sep_box">
                                        
                                        <?php $i=0; foreach ($userportfolio as $key => $value) { $i++; ?>
                                            <div class="col-lg-12">
                                           
                                                <label>Portfolio - <?php echo $i; ?></label>
                                                    
                                               
                                               
                                                   <div class="tbl_input">
                      <span id="delmsg_<?php echo $value->id; ?>">
       <img src="{{asset('/uploads/user_portfolio/'.$value->filename)}}" width="50" height="50" > 
                                                        <!--  <p style="word-break: break-all;"><?php echo $value->cm_url; ?></p>  -->
                                                        <button type="button" class="btn btn-secondary btn-sm deleteportfolio" image_id="{{$value->id}}" href="javascript:Void(0)"><span class="delet-ioc"><i class="far fa-trash-alt"></i></span>Delete</button>
                                                        
                                                        
                                                        </span>
                                                    </div>
                                               
                                           
                                        </div>
                                        <?php } ?>
                                    </div>

								</div>
								<div class="append">
                                    <div class="sep_box">
                                        <input type="hidden" name="count[]" value="1">
                                        <div class="row ">
                                        <div class="col-lg-12">
                                            <label>Portfolio </label>
                                              
                                                   <div class="tbl_input">
                                                        <input id="simage" type="file" name="simage_1" value="" class="form-control"/>
                                                    </div>
                                                    {!! $errors->first('simage_1', '<p class="help-block">:message</p>') !!}
                                                
                                            </div>
                                        </div>
                                        
                                    </div>
                                    </div>
                                    
								
								<div class="row">
										<div class="add-exprens">
											<button type="button" class="addMore btn btn-sm btn-dark"><i class="fas fa-plus-circle"></i> Add More</button>
										</div>
								</div>@if(!empty($user->video))
									<div class="row">
			<span id="delmsgvdo_<?php echo $user->id; ?>">
				<video width="110" height="130" controls>
      <source src="{{asset('/uploads/video/user_video/'.$user->video)}}" type="video/mp4" >
    Your browser does not support the video tag.
</video><button type="button" class="btn btn-secondary btn-sm deletevideo" video_id="{{$user->id}}" href="javascript:Void(0)"><span class="delet-ioc"><i class="far fa-trash-alt"></i></span>Delete</button></span></div>@endif
									
									<div class="row ">
										<div class="col-md-12"><h5>Upload Intro Video</h5></div>
                                        <div class="col-lg-12">
                                            
                                              
                                                   <div class="tbl_input">
                                                        <input id="simage" type="file" name="video" value="" class="form-control"/>
                                                    </div>
                                                 {!! $errors->first('video', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div><br>
<div class="row">
									<div class="sep_box">
                                        
                                        <?php $i=0; foreach ($UserExperience as $key => $value) { $i++; ?>
                                            <div class="col-lg-12">
                                           
                                                <label>Experience - <?php echo $i; ?></label>
                                                    
                                               
                                               
                                                   <div class="tbl_input">
                                           <span id="delmsgexp_<?php echo $value->id; ?>">
                                                        
        <p ><bold>Subject:</bold>   {{$value->experience}} <bold>Year:</bold>   {{$value->experience_year}} 
       
                                    <button type="button" class="btn btn-secondary btn-sm deleteexperience" experience_id="{{$value->id}}" href="javascript:Void(0)"><span class="delet-ioc"><i class="far fa-trash-alt"></i></span>Delete</button>
                                                        </p>
                                                        
                                                        </span>
                                                    </div>
                                               
                                           
                                        </div>
                                        <?php } ?>
                                    </div>

								</div>
<div class="expappend">
<div class="sep_box">
<input type="hidden" name="expcount[]" value="1">
<div class="row">
<div class="col-md-12"><h5>Experience</h5></div>

<div class="col-md-9">
<div class="form-group">
<label for="Experience">Subject</label>
<input type="text" class="form-control"  name="experience_1" value="" placeholder="User Experience">
</div>

</div>
<div class="col-md-3">
<div class="form-group">
<label for="exampleFormControlSelect2">Year</label>
<select class="form-control"  name="experience_year_1">
<option value="">Select Year</option>
<option>1</option>
<option>2</option>
<option>3</option>
<option>4</option>
<option>5</option>

</select>

</div>
</div>

</div>
</div>
</div>
								<div class="row">
										<div class="add-exprens">
											<button type="button" class="addMoreExp btn btn-sm btn-dark"><i class="fas fa-plus-circle"></i> Add More</button>
										</div>
									</div>

<div class="row">
									<div class="sep_box">
                                        
                                        <?php $i=0; foreach ($UserProject as $key => $value) { $i++; ?>
                                            <div class="col-lg-12">
                                           
                                                <label>Project - <?php echo $i; ?></label>
                                                    
                                               
                                               
                                                   <div class="tbl_input">
                                           <span id="delmsgpro_<?php echo $value->id; ?>">
                                                        
        <p ><bold> Title:</bold>   {{$value->project_title}} </p><p><bold> Discription:</bold>   {{$value->project_dis}} 
       
                                    <button type="button" class="btn btn-secondary btn-sm deleteproject" project_id="{{$value->id}}" href="javascript:Void(0)"><span class="delet-ioc"><i class="far fa-trash-alt"></i></span>Delete</button>
                                                        </p>
                                                        
                                                        </span>
                                                    </div>
                                               
                                           
                                        </div>
                                        <?php } ?>
                                    </div>

								</div>
<div class="proappend">
<div class="sep_box">
<input type="hidden" name="procount[]" value="1">
<div class="row">
<div class="col-md-12"><h4>Project</h4></div>
<div class="col-md-12">
<div class="form-group">
<label for="project_title">Title</label>
<input type="text" class="form-control"  name="project_title_1" placeholder="Type project title">

</div>
</div>
</div>
<div class="row">
<div class="col-md-12">
<div class="form-group">
<label for="project_dis">Discription</label>
<textarea class="form-control" id="project_dis" name="project_dis_1" rows="3" placeholder="Project Discription" style="height: 82px;"></textarea>


</div>
</div>
</div>
</div>
</div>
<div class="row">
<div class="add-exprens">
<button type="button" class="addMorePro btn btn-sm btn-dark"><i class="fas fa-plus-circle"></i> Add More</button>
</div>
</div>
<div class="row">
									<div class="sep_box">
                                        
                                        <?php $i=0; foreach ($UserEducation as $key => $value) { $i++; ?>
                                            <div class="col-lg-12">
                                           
                                                <label> 	Education  - <?php echo $i; ?></label>
                                                    
                                               
                                               
                                                   <div class="tbl_input">
                                           <span id="delmsgedu_<?php echo $value->id; ?>">
                                                        
        <p ><bold>Course :</bold>   {{$value->course}} </p><p><bold>University :</bold>   {{$value->university }} 
       
                                    <button type="button" class="btn btn-secondary btn-sm deleteeducation" education_id="{{$value->id}}" href="javascript:Void(0)"><span class="delet-ioc"><i class="far fa-trash-alt"></i></span>Delete</button>
                                                        </p>
                                                        
                                                        </span>
                                                    </div>
                                               
                                           
                                        </div>
                                        <?php } ?>
                                    </div>

								</div>
<div class="eduappend">
<div class="sep_box">
<input type="hidden" name="educount[]" value="1">
<div class="row">
<div class="col-md-12">
<h4>Education</h4>
</div>
<div class="col-md-5">
<div class="form-group">
<label for="course">Course</label>
<input type="text" class="form-control" name="course_1"placeholder="Enter course name">
{!! $errors->first('course', '<p class="help-block">:message</p>') !!}

</div>
</div>
<div class="col-md-7">
<div class="form-group">
<label for="university">University</label>
<input type="text" class="form-control" name="university_1"  placeholder="Enter University name">
{!! $errors->first('university', '<p class="help-block">:message</p>') !!}
</div>
</div>
</div>
</div>
</div>
<div class="row">
<div class="add-exprens">
<button type="button" class="addMoreEdu btn btn-sm btn-dark"><i class="fas fa-plus-circle"></i> Add More</button>
</div>
</div>





									<div class="row">
									<div class="col-md-12">
										<div class="form-cncle-button">

											<button type="submit" class="btn btn-dark save-blue">Update</button>
										</div>
									</div>
								</div>
								
									
								
							</form>
						</div>
					</div>
				</div>
				<div class="col-md-5" style="background-color: #f9f9f9;">

			
			   @if (!empty($user->job_title))
			   	       <?php $hasCompletedJobTitle = 5;?>
			   	@else
			   	      <?php $hasCompletedJobTitle = 0;?>
			    @endif 
			    @if (!empty($user->image))
			   	       <?php $hasCompletedImage = 10;?>
			   	@else
			   	      <?php $hasCompletedImage = 0;?>
			    @endif
			    @if (!empty($user->address_line_1))
			   	       <?php $hasCompletedAddress = 10;?>
			   	@else
			   	      <?php $hasCompletedAddress = 0;?>
			    @endif
			    @if (!empty($user->about))
			   	       <?php $hasCompletedAbout = 10;?>
			   	@else
			   	      <?php $hasCompletedAbout = 0;?>
			    @endif
			    @if (!empty($user->hourly_rate))
			   	       <?php $hasCompletedHourlyRate = 10;?>
			   	@else
			   	      <?php $hasCompletedHourlyRate = 0;?>
			    @endif
			    @if (!empty($user->phone))
			   	       <?php $hasCompletedPhone = 10;?>
			   	@else
			   	      <?php $hasCompletedPhone = 0;?>
			    @endif
			   @if (!$userskill->isEmpty())
			   	       <?php $hasCompletedSkills = 10;?>
			   	@else
			   	      <?php $hasCompletedSkills = 0;?>
			    @endif
			   @if (!$userportfolio->isEmpty())
			   	       <?php $hasCompletedFileName = 10;?>
			   	@else
			   	      <?php $hasCompletedFileName = 0;?>
			    @endif
			   @if (!$UserExperience->isEmpty())
			   	       <?php $hasCompletedExperience = 5;?>
			   	@else
			   	      <?php $hasCompletedExperience = 0;?>
			    @endif
			   @if (!$UserEducation->isEmpty())
			   	       <?php $hasCompletedEducation = 5;?>
			   	@else
			   	      <?php $hasCompletedEducation = 0;?>
			    @endif
			    @if (!$UserProject->isEmpty())

			   	       <?php  $hasCompletedProject = 10;?>
			   	@else
			   	      <?php  $hasCompletedProject = 0;?>
			    @endif
			    @if (!empty($user->video))
			   	       <?php $hasCompletedVideo = 5;?>
			   	@else
			   	      <?php $hasCompletedVideo = 0;?>
			    @endif
			    <?php  $maximumPoints  = 100;

                 $percentage = ($hasCompletedImage+$hasCompletedJobTitle+$hasCompletedAddress+$hasCompletedAbout+$hasCompletedHourlyRate+$hasCompletedPhone+$hasCompletedSkills+$hasCompletedFileName+$hasCompletedExperience+$hasCompletedProject+$hasCompletedVideo+$hasCompletedEducation)*$maximumPoints/100;

			    ?>
					<div class="profile-lavle">
						<div class="completion-bg">
							<h2>Profile Completion Level: </h2>
							<div class="circle" id="circle-b">
								<strong>{{$percentage}}%<br><span>Complete</span></strong>
							</div>
							<h3>Improve Profile Tips</h3>
							<ul>
								<li>
									<h4>Add your Portfolio</h4><h4><h4>10%</h4></h4>
								</li>
								<li>
									<h4>Add your Experience</h4><h4><h4>10%</h4></h4>
								</li>
								<li>
									<h4>Add your Projects</h4><h4><h4>10%</h4></h4>
								</li>
							</ul>
						</div>
					</div>
				</div>

			
			</div>
		</div>
	</div>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
<script>
    $(function () {
    $('.selectpicker').selectpicker();
});
$(document).on('keyup keypress', 'form input[type="text"]', function(e) {
  if(e.keyCode == 13) {
    e.preventDefault();
    return false;
  }
});
</script>
			<script>
				$('.date').datepicker({
				  multidate: true,
					format: 'dd-mm-yyyy'
				});
			</script>
			<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery-circle-progress/1.1.3/circle-progress.min.js'></script>
  <script>
	/**
*Exampe from https://kottenator.github.io/jquery-circle-progress/
*/
var progressBarOptions = {
	startAngle: -1.50,
	size: 200,
    value: 0.75,
    fill: {
		color: '#ffa500'
	}
}

$('.circle').circleProgress(progressBarOptions).on('circle-animation-progress', function(event, progress, stepValue) {
	//$(this).find('strong').text(String(stepValue.toFixed(2)).substr(1));
});

$('#circle-b').circleProgress({
	value : 0.50,
	fill: {
		color: '#00ab21'
	}
});


  </script>
  <input type="file" id="file" style="display: none"/>
    <input type="hidden" id="file_name"/>
					<script>
   function changeProfile() {
        $('#file').click();
    }
    $('#file').change(function () {
        if ($(this).val() != '') {
            upload(this);

        }
    });
    function upload(img) {
        var form_data = new FormData();
        form_data.append('file', img.files[0]);
        form_data.append('_token', '{{csrf_token()}}');
        $('#loading').css('display', 'block');
        $.ajax({
            url: "{{url('ajax-user-image-upload')}}",
            data: form_data,
            type: 'POST',
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.fail) {
                    alert(data.errors['images']);
                }
                else {
                    $('#file_name').val(data);
                    $('#preview_image').attr('src', '{{asset('/uploads/user_images/')}}/' + data);
                }
                $('#loading').css('display', 'none');
            },
            error: function (xhr, status, error) {
                alert(xhr.responseText);
                $('#preview_image').attr('src', '{{asset('images/noimage.jpg')}}');
            }
        });
    }

   
</script>
<script type="text/javascript">
    
    $(document).ready(function(){
      var maxField = 10; 
     // var addButton = $('.add_row'); 
      var z = 1; 
      var wrapper1 = $('.append'); 
      $(".addMore").click(function(){ 


        if(z < maxField){
            z++;
              $(".append").append(' <div class="sep_box"><input type="hidden" name="count[]" value="'+z+'"><div class="row "><div class="col-lg-12"><label>Portfolio</label><div class="tbl_input"><input id="simage" multiple type="file" name="simage_'+z+'" value="" class="form-control"/></div></div></div><div class="col-lg-6"><button type="button" class="remove_row"><i class="fas fa-times"></i></button></div></div>');
        }
      });
      $(wrapper1).on('click', '.remove_row', function(e){
          e.preventDefault();
          $(this).parent('div').parent('div').remove();
          z--;
      });
    });

</script>
<script type="text/javascript">
    
    $(document).ready(function(){
      var maxField = 10; 
     // var addButton = $('.add_row'); 
      var p = 1; 
      var wrapper1 = $('.expappend'); 
      $(".addMoreExp").click(function(){ 


        if(p < maxField){
            p++;
              $(".expappend").append(' <div class="sep_box"><input type="hidden" name="expcount[]" value="'+p+'"><div class="row "><div class="col-md-9"><div class="form-group"><label for="Experience">Subject</label><input type="text" class="form-control"  name="experience_'+p+'" value="" placeholder="User Experience"></div></div><div class="col-md-3"><div class="form-group"><label for="exampleFormControlSelect2">Year</label><select class="form-control"  name="experience_year_'+p+'"><option value="">Select Year</option><option>1</option><option>2</option><option>3</option><option>4</option><option>5</option></select></div></div><div class="col-lg-4"><button type="button" class="remove_row"><i class="fas fa-times"></i></button></div></div>');
        }
      });
      $(wrapper1).on('click', '.remove_row', function(e){
          e.preventDefault();
          $(this).parent('div').parent('div').remove();
          p--;
      });
    });

</script>

<script type="text/javascript">
    
    $(document).ready(function(){
      var maxField = 10; 
     // var addButton = $('.add_row'); 
      var c = 1; 
      var wrapper1 = $('.proappend'); 
      $(".addMorePro").click(function(){ 


        if(c < maxField){
            c++;
              $(".proappend").append(' <div class="sep_box"><input type="hidden" name="procount[]" value="'+c+'"><div class="row"><div class="col-md-12"><h4>Project</h4></div><div class="col-md-12"><div class="form-group"><label for="project_title">Title</label><input type="text" class="form-control"  name="project_title_'+c+'" placeholder="Type project title"></div></div></div><div class="row"><div class="col-md-12"><div class="form-group"><label for="project_dis">Discription</label><textarea class="form-control" id="project_dis" name="project_dis_'+c+'" rows="3" placeholder="Project Discription" style="height: 82px;"></textarea></div></div></div><div class="col-lg-4"><button type="button" class="remove_row"><i class="fas fa-times"></i></button></div></div>');
        }
      });
      $(wrapper1).on('click', '.remove_row', function(e){
          e.preventDefault();
          $(this).parent('div').parent('div').remove();
          c--;
      });
    });

</script>

<script type="text/javascript">
    
    $(document).ready(function(){
      var maxField = 10; 
     // var addButton = $('.add_row'); 
      var e = 1; 
      var wrapper1 = $('.eduappend'); 
      $(".addMoreEdu").click(function(){ 


        if(e < maxField){
            e++;
              $(".eduappend").append(' <div class="sep_box"><input type="hidden" name="educount[]" value="'+e+'"><div class="row"><div class="col-md-5"><div class="form-group"><label for="course">Course</label><input type="text" class="form-control" name="course_'+e+'" placeholder="Enter course name"></div></div><div class="col-md-7"><div class="form-group"><label for="university">University</label><input type="text" class="form-control" name="university_'+e+'"  placeholder="Enter University name"></div></div></div><div class="col-lg-4"><button type="button" class="remove_row"><i class="fas fa-times"></i></button></div></div>');
        }
      });
      $(wrapper1).on('click', '.remove_row', function(e){
          e.preventDefault();
          $(this).parent('div').parent('div').remove();
          e--;
      });
    });
    

      $(".deleteportfolio").click(function(){
             
              var image_id = $(this).attr("image_id");
           if (confirm("Are you sure?")) {
             $.ajax({
                   type: 'post',
                   url : '/user/delete-portfolio',
                   data :{image_id:image_id},
                   success:function(resp){
                   
                    if (resp==1) {
                    $('#delmsg_'+image_id).html('Successfully Deleted.').css("color", "green");
                  }else if(resp ==0){
                   $('#delmsg_'+image_id).html('Image not avilable .').css("color", "red");
                  }
                   },error: function(){
                    alert('ERRRRr');
                   }
             });
              }
          return false;
        });

       $(".deleteexperience").click(function(){
             
              var experience_id = $(this).attr("experience_id");
           if (confirm("Are you sure?")) {
             $.ajax({
                   type: 'post',
                   url : '/user/delete-experience',
                   data :{experience_id:experience_id},
                   success:function(resp){
                   
                    if (resp==1) {
                    $('#delmsgexp_'+experience_id).html('Successfully Deleted.').css("color", "green");
                  }else if(resp ==0){
                   $('#delmsgexp_'+experience_id).html('Experience not avilable .').css("color", "red");
                  }
                   },error: function(){
                    alert('ERRRRr');
                   }
             });
              }
          return false;
        });
        $(".deleteproject").click(function(){
             
              var project_id = $(this).attr("project_id");
           if (confirm("Are you sure?")) {
             $.ajax({
                   type: 'post',
                   url : '/user/delete-project',
                   data :{project_id:project_id},
                   success:function(resp){
                   
                    if (resp==1) {
                    $('#delmsgpro_'+project_id).html('Successfully Deleted.').css("color", "green");
                  }else if(resp ==0){
                   $('#delmsgpro_'+project_id).html('Project not avilable .').css("color", "red");
                  }
                   },error: function(){
                    alert('ERRRRr');
                   }
             });
              }
          return false;
        });

        $(".deletevideo").click(function(){
             
              var video_id = $(this).attr("video_id");
           if (confirm("Are you sure?")) {
             $.ajax({
                   type: 'post',
                   url : '/user/delete-video',
                   data :{video_id:video_id},
                   success:function(resp){
                   
                    if (resp==1) {
                    $('#delmsgvdo_'+video_id).html('Successfully Deleted.').css("color", "green");
                  }else if(resp ==0){
                   $('#delmsgvdo_'+video_id).html('Video not avilable .').css("color", "red");
                  }
                   },error: function(){
                    alert('ERRRRr');
                   }
             });
              }
          return false;
        });
        $(".deleteeducation").click(function(){
             
              var education_id = $(this).attr("education_id");
           if (confirm("Are you sure?")) {
             $.ajax({
                   type: 'post',
                   url : '/user/delete-education',
                   data :{education_id:education_id},
                   success:function(resp){
                   
                    if (resp==1) {
                    $('#delmsgedu_'+education_id).html('Successfully Deleted.').css("color", "green");
                  }else if(resp ==0){
                   $('#delmsgedu_'+education_id).html('Education not avilable .').css("color", "red");
                  }
                   },error: function(){
                    alert('ERRRRr');
                   }
             });
              }
          return false;
        });
</script>

@endsection
