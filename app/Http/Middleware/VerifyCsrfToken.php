<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * Indicates whether the XSRF-TOKEN cookie should be set on the response.
     *
     * @var bool
     */
    protected $addHttpCookie = true;

    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        "dashboard/update-skill-status","client/delete-portfolio","client/delete-experience","client/delete-project","client/delete-video","client/delete-project","client/delete-education","user/delete-portfolio","user/delete-experience","user/delete-project","user/delete-video","user/delete-project","user/delete-education", "dashboard/update-category-status", "dashboard/update-user-status", "dashboard/approve-user-status","dashboard/unapprove-user-status" , "dashboard/update-client-status", "dashboard/approve-client-status","dashboard/unapprove-client-status" ,"dashboard/update-blog-status","check-subscriber-email","add-subscriber-email","dashboard/update-newsletter-status"
    ];
}
