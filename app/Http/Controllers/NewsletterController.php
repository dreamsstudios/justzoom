<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\NewsletterSubscriber;
use App\Exports\subscribersExport;
use Maatwebsite\Excel\Facades\Excel;


class NewsletterController extends Controller
{
   

    public function addSubscriber(Request $request){
     
       request()->validate([
       
        'email' => 'required|email|unique:fp_newsletter_subscribers'
       
        ]);
         
        $data = request()->except(['_token']);
        $data['created_at'] = \Carbon\Carbon::now();
        $data['updated_at'] = \Carbon\Carbon::now();
        $check = NewsletterSubscriber::insert($data);
        $arr = array('msg' => 'Something goes to wrong. Please try again lator', 'status' => false);
        if($check){ 
        $arr = array('msg' => 'subscribers Successfully!', 'status' => true);
        }
        return Response()->json($arr);
    }

    public function viewNewsletterSubscribers(){
        $newsletters = NewsletterSubscriber::get();
        return view('admin.newsletters')->with(compact('newsletters'));
    }

   public function update_status(Request $request){
    
        if ($request->ajax()) {
            $data= $request->all();
           // echo "<pre>"; print_r($data);
            if ($data['status']=="Active") {
                 $status = 0;
            }else{
                $status = 1;
            }
            NewsletterSubscriber::where('id',$data['Newsletter_id'])->update(['status'=>$status]);
            return response()->json(['status'=>$status, 'Newsletter_id'=>$data['Newsletter_id']]);

        }
    }

    public function delete(Request $request) {
        $id = $request->hiddenval;
        $queryData = NewsletterSubscriber::find($id);

        if (isset($queryData->id)) {
          
            $queryData->delete();

            echo json_encode(array("status" => 1, "message" => "Newsletter Subscriber deleted successfully"));
        } else {
            echo json_encode(array("status" => 0, "message" => "Newsletter Subscriber does not exists"));
        }
        die();
    }

    

    public function exportNewsletterEmails(){
        return Excel::download(new subscribersExport,'subscribers.xlsx');
    }
}
