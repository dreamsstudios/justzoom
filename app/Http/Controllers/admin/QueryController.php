<?php

namespace App\Http\Controllers\admin;

use App\Models\Query;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Datatables;
use DB;

class QueryController extends Controller
{
    public function index(){
        $queryData  = DB::table("fp_queries")
                ->get();
        return view("admin.list_query",compact('queryData'));
    }

  
     function view_query($id){
     
        $queryData  = DB::table("fp_queries")
                ->where('id','=',$id)
                ->first();
        return view("admin.view_query",compact('queryData'));
      
    }


}
