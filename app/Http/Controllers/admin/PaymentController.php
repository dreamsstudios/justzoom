<?php

namespace App\Http\Controllers\admin;
use App\Models\User;
use App\Models\Category;
use App\Models\Contract;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class PaymentController  extends Controller
{
    public function index() {
        /*$user_verified =  DB::table("fp_users")->where('is_user_verified', '=', 1)->get()->count();
         // echo "<pre>"; print_r($user_verified); exit();
        $client_count = DB::table("fp_clients")->get()->count();
          $queryData = User::with(['category'])
           ->where('status', '<>', 2)
            ->where('is_user_verified', '=', 0)
            ->paginate(5);*/  
          $platformPrice =  DB::table("fp_contract")->get()->sum('platformPrice');
         $paymentRequest = DB::table("fp_release_payment")
         ->select("fp_release_payment.*","fp_users.*",
         "fp_contract.*","fp_category.category_name as category", 
         "fp_stripe_details.stripeAccId","fp_stripe_details.payouts_enabled","fp_clients.stripeCustomerId",
         "fp_countries.name as country_name","fp_cities.name as city_name","fp_states.name as state_name")  
         ->join("fp_contract",'fp_contract.contractId', '=', 'fp_release_payment.contractId')
         ->leftjoin('fp_clients', 'fp_clients.id', '=', 'fp_contract.clientId')
         ->leftjoin('fp_users', 'fp_users.id', '=', 'fp_release_payment.userId')
         ->leftjoin('fp_countries', 'fp_countries.id', '=', 'fp_users.country')
          ->leftjoin('fp_cities', 'fp_cities.id', '=', 'fp_users.city')
           ->leftjoin('fp_states', 'fp_states.id', '=', 'fp_users.state')
         ->leftjoin('fp_stripe_details', 'fp_users.id', '=', 'fp_stripe_details.userId')
         ->leftjoin('fp_category', 'fp_category.id', '=', 'fp_users.category_id')
         ->where("fp_release_payment.status",  0)
        ->paginate(2); 
          $Recentpayment =  DB::table("fp_release_payment")
                            ->select("fp_release_payment.*","fp_users.*","fp_category.category_name as category")
                            ->leftjoin('fp_users', 'fp_users.id', '=', 'fp_release_payment.userId')
                            ->leftjoin('fp_category', 'fp_category.id', '=', 'fp_users.category_id')
                             ->where('fp_release_payment.status', '=', 1)
                            ->paginate(10); 
                          
                            foreach ($Recentpayment as  $value) {
                              $value->onGoing = Contract::where('fp_contract.userId', '=', $value->userId)->where('IsContractComplete','=',0)
                                              ->count();
                                              $value->complete = Contract::where('fp_contract.userId', '=', $value->userId)->where('IsContractComplete','=',1)
                                              ->count();
                           
                            }
           //echo "<pre>"; print_r($complete); exit();
       
        return view("admin.payment", compact('platformPrice','paymentRequest','Recentpayment'));
    }
}
