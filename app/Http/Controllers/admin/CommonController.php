<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ReleasePayment;
use App\Models\Setting;
use App\Models\Payout;
use Validator;
use DB;
use Stripe;
use Illuminate\Support\Facades\Notification;
use App\Notifications\SendPaymentSuccess;

class CommonController extends Controller
{
    public function index(Request $request)
    {

		$action = $request->action;

		$return = array("valid" => false, "data" => array(), "msg" => "UnAuthorised Access!");

		if( $action == "changeStatus" )
			$return = $this->changeStatus($request->status, $request);
		if( $action == "releasedFund" )
			$return = $this->releasedFund($request);
		else if( $action == "deleteRecord" )
			$return = $this->changeStatus(2, $request);

		return response()->json($return);
    }

    private function changeStatus($status = 0, $request){

		
		$updateStatus = 0;
      
		$table = 'fp_'.$request->tab;
		$queryData = array('status' => $status);
		$cond = array('id' => $request->id);

		if( $request->tab  == 'category'){
			
			$cond = "categoryId =".$request->id;
		}

		if(!empty($table) && !empty($queryData) && !empty($cond)){
			if($request->tab == 'users' || $request->tab == 'clients')
			{	
			
				if($request->tab == 'users'){
					$rolcond = array('role_id' => $request->id,'role'=>'user');
				}else{
					$rolcond = array('role_id' => $request->id,'role'=>'client');
				}
				$updateStatus = DB::table('fp_auths')->where($rolcond)->update($queryData);
			}
			$updateStatus = DB::table($table)->where($cond)->update($queryData);
			
		}
      
		if( $updateStatus )
			return array("valid" => true, "msg" => "Record updated successfully!");
		else
			return array("valid" => false, "msg" => "Something Went Wrong");
		
	}

	public function releasedFund(Request $request){

		$response = array('valid' => false, 'msg'=>'Invalid Request'); 
		$paymentRequest =  DB::table("fp_release_payment")
		->select("fp_release_payment.*","fp_users.*",
		"fp_contract.*","fp_category.category_name as category", "fp_stripe_details.stripeAccId",
		"fp_stripe_details.payouts_enabled","fp_clients.stripeCustomerId","fp_clients.email as clientemail")  
		->join("fp_contract",'fp_contract.contractId', '=', 'fp_release_payment.contractId')
		->leftjoin('fp_clients', 'fp_clients.id', '=', 'fp_contract.clientId')
		->leftjoin('fp_users', 'fp_users.id', '=', 'fp_release_payment.userId')
		->leftjoin('fp_stripe_details', 'fp_users.id', '=', 'fp_stripe_details.userId')
		->leftjoin('fp_category', 'fp_category.id', '=', 'fp_users.category_id')
		->where("fp_release_payment.status",  0)
		->where("fp_release_payment.relId",  $request->relId)
		->first();
		$setting  = Setting::first();
		$stripe = Stripe\Stripe::setApiKey($setting->secreteKey);

		if(!empty($paymentRequest->stripeCustomerId) && !empty($paymentRequest->stripeAccId) && 
		!empty($paymentRequest->cardSource) && $paymentRequest->payouts_enabled){
			$cc = \Stripe\Customer::retrieveSource(
				$paymentRequest->stripeCustomerId,
				$paymentRequest->cardSource,
			  );
				try {
					/*$charge = \Stripe\Charge::create([
						"amount" => $paymentRequest->amount*100,
						"currency" => "GBP",
						"customer" => $cc->customer,
						"source" => $paymentRequest->cardSource,
						"transfer_data" => [
						"amount" => $paymentRequest->amount*100,
						"destination" => $paymentRequest->stripeAccId,
					],
					]);*/
					 $charge = \Stripe\Charge::create([
						"amount" => $paymentRequest->amount*100,
						"currency" => "GBP",
						"customer" => $cc->customer,
						"source" => $paymentRequest->cardSource,
						"on_behalf_of" => $paymentRequest->stripeAccId,
					]);
			  } catch (Exception $e) {
				$response['errors'] = $e->getMessage();
				return $response;
			  }
			  if(!isset($response['errors']) || empty($response['errors'])){
				$con = new Payout;
				$con->userId = $paymentRequest->userId;
				$con->amount = $paymentRequest->amount;
				$con->relId = $paymentRequest->relId;
				$con->response = $charge;
				$con->currency = $paymentRequest->currency;
				$con->paymentId = $charge->id;
				$con->contractId = $paymentRequest->contractId;
				$con->created_at = date('Y-m-d H:i:s');
				$con->save();
				ReleasePayment::where('relId',$paymentRequest->relId)->update(['status'=>1]);
				Notification::route('mail' , $paymentRequest->clientemail) 
				->notify(new SendPaymentSuccess($con));
	  
				Notification::route('mail' , $paymentRequest->email) 
				->notify(new SendPaymentSuccess($con));
				$response['valid'] = true;
				$response['msg'] = "Payment has been done successfully.";
			}else{
			  $response['msg'] = "Something went wrong!";
			}

		}
		
		return $response;
	}




}
