<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Blog;
use App\Helpers\Common_helper;
use Validator;
use Datatables;
use DB;

class BlogController extends Controller
{
    public function index()
    {
        $queryData  = DB::table("fp_blogs")
                ->where('status', '<>', 2)
                ->get();
        
        return view("admin.blog",compact('queryData'));
    } 

        public function add_blog()
    {
        return view("admin.add_blog");
    }
     public function save(Request $request){

        $id = $request->hiddenval;
  
       if ($id>0) {
       	   $validator = Validator::make(
            array(
                "title"=>$request->title,
                "content"=>$request->content,
                "description"=>$request->description,
               
            ),
            array(
                "title"=>"required".(($id > 0)?'':"|unique:fp_blogs"),
                 "content"=>"required",
                 "description"=>"required",
                  
            )
        );
       }else{
       	   $validator = Validator::make(
            array(
                "title"=>$request->title,
                "content"=>$request->content,
                "description"=>$request->description,
                "image"=>$request->image,
            ),
            array(
                "title"=>"required".(($id > 0)?'':"|unique:fp_blogs"),
                 "content"=>"required",
                 "description"=>"required",
                  'image' => 'mimes:jpeg,jpg,png,gif|max:10000|required'.(($id > 0)?'':"|unique:fp_blogs"), 
            )
        );
       }
        if ($validator->fails())
            return redirect("/dashboard/add-blog")->withErrors($validator)->withInput();
        
        if($id > 0){
            $blog = Blog::find($id);
            $action = "updated";
            
        }
        else{
            $blog = new Blog;
            $action = "created";
           
        }
          if(!empty($request->file('image'))){
            $file = $request->file('image');
            $new_name = time().rand().'.'.$file->getClientOriginalExtension();
            $file->move(public_path('uploads/blogs'), $new_name);
            $blog->image = $new_name;
        }
        $common_lib = new Common_helper();
        $slug =  $common_lib->createSlug($request->title,$id,'fp_blogs');
        $blog->title = $request->title;
        $blog->content = $request->content;
        $blog->description = $request->description;
       
        $blog->user_id = 1;
        $blog->slug =  $slug;
        $blog->save();
        $request->session()->flash("msg", "title has been $action successfully.");
        return redirect("/dashboard/blog");
        
    
     }
     public function edit($id) {
        $blog = ($id > 0)?Blog::find($id):array();
        if(empty($blog)){
            $request->session()->flash("msg", "Invalid request.");
            return redirect("/dashboard/blog");
        }
        return view("admin.edit_blog",compact('blog'));
    
  }
     public function delete(Request $request) {
        $id = $request->hiddenval;
        $queryData = Blog::find($id);

        if (isset($queryData->id)) {
          
            $queryData->delete();

            echo json_encode(array("status" => 1, "message" => "Blog deleted successfully"));
        } else {
            echo json_encode(array("status" => 0, "message" => "Blog does not exists"));
        }
        die();
    }

    public function update_status(Request $request){
        if ($request->ajax()) {
            $data= $request->all();
           // echo "<pre>"; print_r($data);
            if ($data['status']=="Published") {
                 $status = 1;
            }else{
                $status = 0;
            }
            Blog::where('id',$data['blog_id'])->update(['status'=>$status]);
            return response()->json(['status'=>$status, 'blog_id'=>$data['blog_id']]);

        }
    }
}
