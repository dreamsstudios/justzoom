<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Contract;
use App\Helpers\Common_helper;
use DB;

class ContractController extends Controller
{
    public function index()
    {
          $queryData = Contract::with([])
             ->select('fp_contract.contractId','fp_contract.aboutProject','fp_contract.description','fp_contract.priceType','fp_contract.duration','fp_contract.platformPrice','fp_contract.freelancerPrice','fp_contract.totalPrice','fp_contract.currency','fp_contract.created_at','fp_contract.requirement','fp_contract.status','fp_contract.paymentId','fp_contract.IsContractComplete','fp_users.first_name as freelancer_firstName','fp_users.image as freelancer_image','fp_clients.first_name as client_firstName','fp_clients.image as client_image',
             'fp_category.category_name')
            ->leftjoin('fp_users', 'fp_users.id', '=', 'fp_contract.userId')
            ->leftjoin('fp_clients', 'fp_clients.id', '=', 'fp_contract.clientId')
            ->leftjoin('fp_category', 'fp_category.id', '=', 'fp_contract.categoryId')
              ->get();
        return view("admin.contract",compact('queryData'));
    } 

    function contractDetails($id){
     
        $queryData  = Contract::with([])
                     ->select('fp_contract.contractId','fp_contract.aboutProject','fp_contract.description','fp_contract.priceType','fp_contract.duration','fp_contract.platformPrice','fp_contract.freelancerPrice','fp_contract.totalPrice','fp_contract.currency','fp_contract.created_at','fp_contract.requirement','fp_contract.status','fp_contract.paymentId','fp_contract.IsContractComplete','fp_users.first_name as freelancer_firstName','fp_users.image as freelancer_image','fp_clients.first_name as client_firstName','fp_clients.image as client_image',
                         'fp_category.category_name')
            ->leftjoin('fp_users', 'fp_users.id', '=', 'fp_contract.userId')
            ->leftjoin('fp_clients', 'fp_clients.id', '=', 'fp_contract.clientId')
            ->leftjoin('fp_category', 'fp_category.id', '=', 'fp_contract.categoryId')
                ->where('contractId','=',$id)
                ->first();
                //echo "pre"; print_r($queryData); exit();
        return view("admin.view_contract",compact('queryData'));
      
    }
}
