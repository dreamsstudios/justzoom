<?php

namespace App\Http\Controllers\admin;

use App\Models\Skill;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Datatables;
use DB;
use Carbon;

class SkillController extends Controller
{
    public function index(){
        $queryData  = DB::table("fp_skills")
                ->where('status', '<>', 2)
                ->get();
        return view("admin.skill",compact('queryData'));
    }

public function add_skill(){
        return view("admin.add_skill");
    }
    

    public function save(Request $request){
        $id = $request->hiddenval;
        $validator = Validator::make(
            array(
                "skill"=>$request->skill
            ),
            array(
                "skill"=>"required".(($id > 0)?'':"|unique:fp_skills")
            )
        );
        if ($validator->fails())
            return redirect("/dashboard/add_skill")->withErrors($validator)->withInput();
        
        if($id > 0){
            $skill = Skill::find($id);
            $action = "updated";
        }
        else{
            $skill = new Skill;
            $action = "created";
        }

        $skill->skill = $request->skill;
        $skill->save();
        $request->session()->flash("msg", "Skill has been $action successfully.");
        return redirect("/dashboard/skills");
        
    }
  
  public function edit($id) {
  
         

        $skill = ($id > 0)?Skill::find($id):array();
       
           

        if(empty($skill)){
            $request->session()->flash("msg", "Invalid request.");
            return redirect("/dashboard/skills");
        }
        return view("admin.edit_skill",compact('skill'));
    
  }
    public function delete(Request $request) {

        $id = $request->hiddenval;

        $queryData = Skill::find($id);

        if (isset($queryData->id)) {
          
            $queryData->delete();

            echo json_encode(array("status" => 1, "message" => "Skill deleted successfully"));
        } else {
            echo json_encode(array("status" => 0, "message" => "Skill doesnot exists"));
        }

        die();
    }
    public function update_status(Request $request){
        if ($request->ajax()) {
            $data= $request->all();
           // echo "<pre>"; print_r($data);
            if ($data['status']=="Active") {
                 $status = 1;
            }else{
                $status = 0;
            }
            Skill::where('id',$data['skill_id'])->update(['status'=>$status]);
            return response()->json(['status'=>$status, 'skill_id'=>$data['skill_id']]);

        }
    }

}
