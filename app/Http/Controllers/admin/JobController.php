<?php

namespace App\Http\Controllers\admin;

use App\Models\Job;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Datatables;
use DB;

class JobController extends Controller
{
    public function index(){
        return view("admin.list_job");
    }

    public function jobdata(){
        // return Datatables::of(Query::query())->make(true);
        $queryData  = DB::table("fp_jobs")
                ->select("fp_jobs.*","fp_category.category_name as category")
                ->leftjoin('fp_category', 'fp_category.id', '=', 'fp_jobs.category_id')
                ->where('fp_jobs.status','!=',3)
                ->orderBy('fp_jobs.created_at', 'DESC')
                ->get();
                
        return Datatables::of($queryData)
                        ->editColumn("job_type", function($queryData) {
                              return ($queryData->job_type == 1)?'Private':'Open';
                        })
                        ->editColumn("job_status", function($queryData) {
                            if($queryData->status == 0){
                                return "Pending";

                            }else if($queryData->status == 1){
                                if(date('Y-m-d', strtotime($queryData->job_end_on)) <= date('Y-m-d')){
                                    return "Completed";
                                }else{
                                    return "Ongoing";

                                }
                            }else if($queryData->status == 2){
                                return "Completed";
                            }
                            return ($queryData->job_type == 1)?'Private':'Open';
                      })
                        ->editColumn("action", function($queryData) {

                            return '<a href="/dashboard/view_job/'.$queryData->id.'" class="btn btn-rounded btn-xs btn-default mb-1 mr-1" data-id="' . $queryData->id . '"><i class="fa fa-eye"></i>';
                        })
                        ->rawColumns(["action"])
                        ->make(true);
    }
    public function view_job($id){
        $jobData  = DB::table("fp_jobs")
                  ->select("fp_jobs.*","fp_category.category_name as category",
                  "fp_users.first_name","fp_users.last_name","fp_payment.token","fp_payment.created_at as payment_Date")
                 ->leftjoin('fp_category', 'fp_category.id', '=', 'fp_jobs.category_id')
                 ->leftjoin('fp_payment', 'fp_payment.job_id', '=', 'fp_jobs.id')
                 ->leftjoin('fp_user_hire', 'fp_user_hire.payment_id', '=', 'fp_payment.id')
                 ->leftjoin('fp_users', 'fp_payment.user_id', '=', 'fp_users.id')
                ->where('fp_jobs.id','=',$id)
                ->first();
        return view("admin.view_job",compact('jobData'));
      
    }
    public function job_req_data($id){
        $queryData  = DB::table("fp_job_requests")
                  ->select("fp_job_requests.*","fp_users.first_name","fp_users.last_name")
                 ->leftjoin('fp_users', 'fp_users.id', '=', 'fp_job_requests.user_id')
                ->where('fp_job_requests.job_id','=',$id)
                ->orderBy('fp_job_requests.updated_at', 'DESC')
                ->get();
                
        return Datatables::of($queryData)
                        ->editColumn("name", function($queryData) {
                              return $queryData->first_name.' '.$queryData->last_name;
                        })
                        ->editColumn("offer_status", function($queryData) {
                            if($queryData->job_status == 0){
                                return "Pending";
                            }else if($queryData->job_status == 1){
                                return "Accepted";
                            }else if($queryData->job_status == 2){
                                return "Declined";
                            }
                           
                      })
                       /* ->editColumn("action", function($queryData) {

                            return '<a href="/dashboard/view_job/'.$queryData->id.'" class="btn btn-rounded btn-xs btn-default mb-1 mr-1" data-id="' . $queryData->id . '"><i class="fa fa-eye"></i>';
                        })*/
                        ->rawColumns(["action"])
                        ->make(true);
      
    }


}
