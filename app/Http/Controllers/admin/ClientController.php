<?php

namespace App\Http\Controllers\admin;

use App\Models\Client;
use App\Models\Contract;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use DB;
use App\Helpers\Common_helper;
use App\Notifications\Verifymail;

class ClientController extends Controller
{
     public function approvel_client(){
         $queryData = Client::with([])
           ->where('status', '<>', 2)
            ->where('is_clients_verified', '=', 0)
            ->get();
            
             foreach ($queryData as  $value) {
                             $value->onGoing = Contract::where('fp_contract.clientId', '=', $value->id)->where('IsContractComplete','=',0)
                                              ->count();
                            $value->complete = Contract::where('fp_contract.clientId', '=', $value->id)->where('IsContractComplete','=',1)
                                              ->count();
                           
                            }
            //echo "<pre>"; print_r($queryData); exit();
        return view("admin.waiting_approval_client",compact('queryData'));
    }
    
    public function approved_client(){
         $queryData = Client::with([])
           ->where('status', '<>', 2)
            ->where('is_clients_verified', '=', 1)
            ->get();
            foreach ($queryData as  $value) {
                             $value->onGoing = Contract::where('fp_contract.clientId', '=', $value->id)->where('IsContractComplete','=',0)
                                              ->count();
                            $value->complete = Contract::where('fp_contract.clientId', '=', $value->id)->where('IsContractComplete','=',1)
                                              ->count();
                           
                            }
                //echo "<pre>"; print_r($queryData); exit();
        return view("admin.approved_client",compact('queryData'));
    }
    public function client_profile($id){
        
        $user = ($id > 0)?Client::with(['country_data','state_data','city_data'])->find($id):array();

                $onGoing = Contract::where('fp_contract.clientId', '=', $id)->where('IsContractComplete','=',0)
                                       ->count();
              $complete = Contract::where('fp_contract.clientId', '=', $id)->where('IsContractComplete','=',1)
                                         ->count();
               $hired_freelancer =Contract::where('fp_contract.clientId', '=', $id) ->count();        
                          

      //echo "<pre>";  print_r($hired_freelancer); exit();
        return view("admin.client_profile",compact('user','onGoing','complete','hired_freelancer'));
    } 
    
   public function update_status(Request $request){
        if ($request->ajax()) {
            $data= $request->all();
            
            if ($data['status']=="Verified") {
                 $status = 1;
            }else{
                $status = 0;
            }
            Client::where('id',$data['client_id'])->update(['status'=>$status]);
            return response()->json(['status'=>$status, 'client_id'=>$data['client_id']]);

        }
    }

       public function approve_status(Request $request){
        if ($request->ajax()) {
            $data= $request->all();
            
            Client::where('id',$data['client_id'])->update(['is_clients_verified'=>1]);
            return response()->json(['status'=>1, 'client_id'=>$data['client_id']]);

        }
    }

     public function unapprove_status(Request $request){
        if ($request->ajax()) {
            $data= $request->all();
            
            Client::where('id',$data['client_id'])->update(['is_clients_verified'=>0]);
            return response()->json(['status'=>0, 'client_id'=>$data['client_id']]);

        }
    }

    public function delete_client($id)
    {  
        $user = Client::withTrashed()->where('id',$id)->firstOrFail();
       if ($user->trashed())
       {   
          $user->forceDelete();
           return redirect()->route('approvel-client')->with('msg','Client Deleted Successfully');
       }else
       {

        $user->delete();
       return redirect()->route('approvel-client')->with('msg','Client trashed Successfully');
       }

    }

      public function delete_approvedclient($id)
    {  
        $user = Client::withTrashed()->where('id',$id)->firstOrFail();
       if ($user->trashed())
       {   
          $user->forceDelete();
           return redirect()->route('approved-client')->with('msg','Client Deleted Successfully');
       }else
       {

        $user->delete();
       return redirect()->route('approved-client')->with('msg','Client trashed Successfully');
       }

    }

     public function trashed_client(){

    $queryData = Client::onlyTrashed()->get();
   // echo "<pre>"; print_r($queryData); exit();
    
     return view("admin.trashed_client",compact('queryData'));
   }

   public function trashed_approvedclient(){

    $queryData = Client::onlyTrashed()->get();
    
     return view("admin.trashed_approvedclient",compact('queryData'));
   }
  
  public function restore_client($id){
    $user = Client::withTrashed()->where('id',$id)->firstOrFail();
      $user->restore();
    
      return redirect()->route('approvel-client')->with('msg','Client Restore Successfully');
}

 public function restore_approved_client($id){
    $user = Client::withTrashed()->where('id',$id)->firstOrFail();
      $user->restore();
    
      return redirect()->route('approved-client')->with('msg','Client Restore Successfully');
}
    

    
}
