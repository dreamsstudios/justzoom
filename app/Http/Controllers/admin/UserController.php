<?php

namespace App\Http\Controllers\admin;

use App\Models\User;
use App\Models\Contract;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Helpers\Common_helper;
use App\Notifications\Verifymail;

class UserController extends Controller
{

     public function approvel_user(){
         $queryData = User::with(['category'])
           ->where('status', '<>', 2)
            ->where('is_user_verified', '=', 0)
            ->get();
                //echo "<pre>"; print_r($queryData); exit();
        return view("admin.waiting_approval_user",compact('queryData'));
    }
   
     public function approved_user(){
         $queryData = User::with(['category'])
           ->where('status', '<>', 2)
            ->where('is_user_verified', '=', 1)
            ->get();
             foreach ($queryData as  $value) {
                             $value->onGoing = Contract::where('fp_contract.userId', '=', $value->id)->where('IsContractComplete','=',0)
                                              ->count();
                            $value->complete = Contract::where('fp_contract.userId', '=', $value->id)->where('IsContractComplete','=',1)
                                              ->count();
                           
                            }
            //echo "<pre>"; print_r($queryData); exit();
        return view("admin.approved_user",compact('queryData'));
    }
    public function user_profile($id){
        
        $user = ($id > 0)?User::with(['category','country_data','state_data','city_data'])->find($id):array();
      //echo "<pre>";  print_r($user); exit();
        return view("admin.user_profile",compact('user'));
    } 
    
   public function update_status(Request $request){
        if ($request->ajax()) {
            $data= $request->all();
            
            if ($data['status']=="Verified") {
                 $status = 1;
            }else{
                $status = 0;
            }
            User::where('id',$data['user_id'])->update(['status'=>$status]);
            return response()->json(['status'=>$status, 'user_id'=>$data['user_id']]);

        }
    }

       public function approve_status(Request $request){
        if ($request->ajax()) {
            $data= $request->all();
            
            User::where('id',$data['user_id'])->update(['is_user_verified'=>1]);
            return response()->json(['status'=>1, 'user_id'=>$data['user_id']]);

        }
    }

     public function unapprove_status(Request $request){
        if ($request->ajax()) {
            $data= $request->all();
            
            User::where('id',$data['user_id'])->update(['is_user_verified'=>0]);
            return response()->json(['status'=>0, 'user_id'=>$data['user_id']]);

        }
    }

    public function delete_user($id)
    {  
        $user = User::withTrashed()->where('id',$id)->firstOrFail();
       if ($user->trashed())
       {   
          $user->forceDelete();
           return redirect()->route('approvel-user')->with('msg','Freelancer Deleted Successfully');
       }else
       {

        $user->delete();
       return redirect()->route('approvel-user')->with('msg','Freelancer trashed Successfully');
       }

    }

      public function delete_approveduser($id)
    {  
        $user = User::withTrashed()->where('id',$id)->firstOrFail();
       if ($user->trashed())
       {   
          $user->forceDelete();
           return redirect()->route('approved-user')->with('msg','Freelancer Deleted Successfully');
       }else
       {

        $user->delete();
       return redirect()->route('approved-user')->with('msg','Freelancer trashed Successfully');
       }

    }

     public function trashed_user(){

    $queryData = User::onlyTrashed()->get();
   // echo "<pre>"; print_r($queryData); exit();
    
     return view("admin.trashed_user",compact('queryData'));
   }

   public function trashed_approveduser(){

    $queryData = User::onlyTrashed()->get();
   // echo "<pre>"; print_r($queryData); exit();
    
     return view("admin.trashed_approveduser",compact('queryData'));
   }
  
  public function restore_user($id){
    $user = User::withTrashed()->where('id',$id)->firstOrFail();
      $user->restore();
    
      return redirect()->route('approvel-user')->with('msg','Freelancer Restore Successfully');
}

 public function restore_approved_user($id){
    $user = User::withTrashed()->where('id',$id)->firstOrFail();
      $user->restore();
    
      return redirect()->route('approved-user')->with('msg','Freelancer Restore Successfully');
}
}
