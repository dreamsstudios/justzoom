<?php

namespace App\Http\Controllers\admin;
use App\Models\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Datatables;
use DB;

class SettingController extends Controller
{
    public function index() {
        
        $setting = Setting::first();
        if(empty($setting)){
            $request->session()->flash("msg", "Invalid request.");
            
        }
           
        return view("admin.edit_payment",compact('setting'));
    }

        public function add_payment()
    {
        return view("admin.add_payment");
    }

    public function save(Request $request){

        $id = $request->hiddenval;
  
      
           $validator = Validator::make(
            array(
                "paltformCharge"=>$request->paltformCharge,
                "stirpePublicKey"=>$request->stirpePublicKey,
                "secreteKey"=>$request->secreteKey,
               
            ),
            array(
                 "paltformCharge"=>"required",
                 "stirpePublicKey"=>"required",
                 "secreteKey"=>"required",
                  
            )
        );
       
        if ($validator->fails())
            return redirect("/dashboard/add-payment-info")->withErrors($validator)->withInput();
        
        if($id > 0){
            $setting = Setting::find($id);
            $action = "updated";
            
        }
        else{
            $setting = new Setting;
            $action = "created";
           
        }
        
        $setting->paltformCharge = $request->paltformCharge;
        $setting->stirpePublicKey = $request->stirpePublicKey;
        $setting->secreteKey = $request->secreteKey;
        $setting->save();
        $request->session()->flash("msg", "Information has been $action successfully.");
        return redirect("/dashboard/settings");
        
    
     }
     public function edit($id) {
        $setting = ($id > 0)?Setting::find($id):array();
        if(empty($setting)){
            $request->session()->flash("msg", "Invalid request.");
            return redirect("/dashboard/settings");
        }
        return view("admin.edit_payment",compact('setting'));
    
  }
   public function delete(Request $request) {
        $id = $request->hiddenval;
        $queryData = Setting::find($id);

        if (isset($queryData->id)) {
          
            $queryData->delete();

            echo json_encode(array("status" => 1, "message" => "Information deleted successfully"));
        } else {
            echo json_encode(array("status" => 0, "message" => "Information does not exists"));
        }
        die();
    }
}
