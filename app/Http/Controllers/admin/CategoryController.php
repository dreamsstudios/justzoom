<?php

namespace App\Http\Controllers\admin;

use App\Models\Category;
use App\Models\CategoryQuestion;
use App\Models\QuestionOption;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Datatables;
use DB;
use App\Helpers\Common_helper;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        $queryData  = DB::table("fp_category")
                ->where('status', '<>', 2)
                ->get();

        return view("admin.category",compact('queryData'));
    } 
    public function add_category($id=0,Request $request)
    {
        // if(!empty($request->session()->get('category'))){
        //     $category = $request->session()->get('category');
        //     $request->session()->put('category', $category);        
        // }

        $categoryValue = "";
        if ($id > 0) {
            $getCategory = Category::find($id);
            $categoryValue = !empty($getCategory) ? $getCategory['category_name'] : "";
        }

        $step = 'step-1';
        return view("admin.add_category",compact('id', 'step', 'categoryValue'));
    }


    public function save(Request $request){
        $id = $request->hiddenval;
        $validator = Validator::make(
            array(
                "category_name"=>$request->category_name
            ),
            array(
                "category_name"=>"required".(($id > 0)?'':"|unique:fp_category")
            )
        );

        if ($validator->fails())
            return redirect("/dashboard/add-category/".$id)->withErrors($validator)->withInput();
        
        $backStep = "";
        if ($id > 0) {
            $category = Category::find($id);
            $action = "updated";
            $backStep = "step-2";
        }
        else {
            $category = new Category;
            $action = "created";            
        }

        $common_lib = new Common_helper();
        $slug =  $common_lib->createSlug($request->category_name,$id,'fp_category');
        $category->category_name = $request->category_name;
        $category->slug =  $slug;
        $category->save();
        
        $request->session()->put('category', $category);
        // if (empty($request->session()->get('category'))) {
        //     $request->session()->put('category', $category);
        // } else {
        //     $category = $request->session()->get('category');
        //     $request->session()->put('category', $category);
        // }

        $request->session()->flash("msg", "Category has been $action successfully.");

        return redirect('/dashboard/add-category/'. $category->id .'/step-2?backStep='.$backStep);       
    }

    public function save_question(Request $request) {

        $getQuestion = $request->session()->get('getQuestion');

        $id = $request->hiddenval;
        $step = $request->step;
        $quest = $request->questionId;
        //print_r($_POST);exit;
        $validator = Validator::make(
            array(
                "question"=>$request->question
            ),
            array(
                "question"=>"required".(($id > 0)?'':"|unique:fp_category_question")
            )
        );

        if ($validator->fails())
            return redirect('/dashboard/add-category/'.$id.'/'.$step)->withErrors($validator)->withInput();
        
       if (!empty($getQuestion)) {
            $ques = CategoryQuestion::where("questionId",$getQuestion->questionId)->first();
            $action = "updated";
        } else {
            $ques = new CategoryQuestion;
            $action = "created";
        }
        $ques->question = $request->question;
        $ques->categoryId = $request->categoryId;
        $ques->created_at = date('Y-m-d H:i:s');
        $ques->save();

        $questionId = $ques->questionId;

        
        $option = $request->option;
      if(!empty($option)){
          if(!empty($getQuestion)){
            QuestionOption::where('questionId',$getQuestion->questionId)->delete();
          }
       
        foreach($option as $opt) {
            $qestOpt =  new  QuestionOption;
            $qestOpt->optionText = $opt;
            $qestOpt->questionId = $questionId;
            $qestOpt->created_at = date('Y-m-d H:i:s');
            $qestOpt->save();
           
        }

      }

        if(empty($request->session()->get('category'))){
            $request->session()->put('category', $category);
        }else{
            $category = $request->session()->get('category');
            $request->session()->put('category', $category);
        }

        $request->session()->flash("msg", "Question has been $action successfully.");
        if($step == 'step-2'){
            return redirect('/dashboard/add-category/'. $request->categoryId .'/step-3?backStep=step-3');

        }else if($step == 'step-3'){
            return redirect('/dashboard/add-category/'. $request->categoryId .'/step-4?backStep=step-4');

        }else if($step == 'step-4'){
            return redirect('/dashboard/add-category/'. $request->categoryId .'/step-5?backStep=step-5');

        }else{
            return redirect('/dashboard/category');

        }
        
        
        
    }
    
    public function add_question($id,$step,Request $request){
        $category = $request->session()->get('category');
        
        $questionValue = "";
        $request->session()->put('getQuestion', []);
        if (isset($_GET['backStep'])) {
            if ($_GET['backStep'] == "step-2") {
                $getQuestion = CategoryQuestion::where('categoryId', $id)->first();
                $questionValue = !empty($getQuestion) ? $getQuestion['question'] : "";
                $questionOptions = array();
                if(!empty($questionValue)){
                     $questionOptions  = QuestionOption::where('questionId', $getQuestion['questionId'])->get();

                }
                $request->session()->put('getQuestion', $getQuestion);
            } else if($_GET['backStep'] == 'step-3') {
                $getQuestion = CategoryQuestion::where('categoryId', $id)->skip(1)->take(1)->first();
                $questionValue = !empty($getQuestion) ? $getQuestion['question'] : "";
                $questionOptions = array();
                if(!empty($questionValue)){
                    $questionOptions  = QuestionOption::where('questionId', $getQuestion['questionId'])->get();
                    
               }
                $request->session()->put('getQuestion', $getQuestion);
            } else if($_GET['backStep'] == 'step-4'){
                $getQuestion = CategoryQuestion::where('categoryId', $id)->skip(2)->take(1)->first();
                $questionValue = !empty($getQuestion) ? $getQuestion['question'] : "";
                $questionOptions = array();
                if(!empty($questionValue)){
                    $questionOptions  = QuestionOption::where('questionId', $getQuestion['questionId'])->get();
                    
               }
                $request->session()->put('getQuestion', $getQuestion);
            } else if($_GET['backStep'] == 'step-5'){
                $getQuestion = CategoryQuestion::where('categoryId', $id)->skip(3)->take(1)->first();
                $questionValue = !empty($getQuestion) ? $getQuestion['question'] : "";
                $questionOptions = array();
                if(!empty($questionValue)){
                    $questionOptions  = QuestionOption::where('questionId', $getQuestion['questionId'])->get();
                    
               }
                $request->session()->put('getQuestion', $getQuestion);
            }

        }

        if ($step == 'step-2') {
            $backStep = '';
        } else if($step == 'step-3') {
            $backStep = 'step-2';
        } else if($step == 'step-4'){
            $backStep = 'step-3';
        } else if($step == 'step-5'){
            $backStep = 'step-4';
        }

        return view("admin.add_question",compact('category','id', 'step', 'backStep', 'questionValue','questionOptions'));
    
  }

    public function edit($id) {
        $category = ($id > 0)?Category::find($id):array();
        if(empty($category)){
            $request->session()->flash("msg", "Invalid request.");
            return redirect("/dashboard/category");
        }
        return view("admin.edit_category",compact('category'));
    
  }
     public function delete(Request $request) {
        $id = $request->hiddenval;
        $queryData = Category::find($id);

        if (isset($queryData->id)) {
          
            $queryData->delete();

            echo json_encode(array("status" => 1, "message" => "Category deleted successfully"));
        } else {
            echo json_encode(array("status" => 0, "message" => "Category does not exists"));
        }
        die();
    }

    public function update_status(Request $request){
        if ($request->ajax()) {
            $data= $request->all();
           // echo "<pre>"; print_r($data);
            if ($data['status']=="Active") {
                 $status = 1;
            }else{
                $status = 0;
            }
            Category::where('id',$data['category_id'])->update(['status'=>$status]);
            return response()->json(['status'=>$status, 'category_id'=>$data['category_id']]);

        }
    }
}
