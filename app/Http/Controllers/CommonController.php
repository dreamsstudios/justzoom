<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hash;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Client;
use App\Models\Job;
use App\Models\UserReview;
use App\Models\JobRequest;
use App\Models\Category;
use App\Models\ContractRequest;
use App\Models\Contract;
use App\Models\StripeDetail;
use App\Models\Chat;
use App\Models\ChatDetail;
use App\Models\ReleasePayment;
use App\Models\ClientReview;
use App\Models\Payout;
use App\Models\Setting;


use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Auth;
use Validator;
use DB;
use App\Helpers\Common_helper;
use Session;
use Input;
use Mail;
use view;
use Stripe;
use App\Notifications\ApproveInvitationRequest;
use App\Notifications\InvitationFreelacerRequest;
use App\Notifications\PasswordResetRequest;
use App\Notifications\Verifymail;
use App\Notifications\SendPaymentRequest;
use App\Notifications\SendPaymentSuccess;


class CommonController extends Controller
{
  public function __construct()
  {
  parent::__construct();

  }
  public function index(Request $request){
    $return = array('valid' => false, 'msg'=>'Invalid Request');  
   
    $action=$request->action;
    $data=$request;
    if($action=="search_gaurd")
      $return = $this->search_gaurd($data);
      if($action=="search_job")
      $return = $this->search_job($data);
    else if($action=="registration_Form")
      $return =  $this->registration_Form($data);
    else if($action=="login_Form")
      $return = $this->login_Form($data);
    else if($action=="forgot_Form")
      $return = $this->forgot_Form($data);
    else if($action=="password_mail")
      $return = $this->password_mail($data);
    else if($action=="ajaximage")
      $return = $this->ajaximage($data); 
    else if($action=="ajax_jobrequest")
      $return = $this->ajax_jobrequest($data);

    else if($action=="ajax_sendInvite")
      $return = $this->ajax_sendInvite($data);
    
    else if($action=="ajax_sendMessage")
      $return = $this->ajax_sendMessage($data); 
    else if($action=="getstate")
      $return = $this->getstate($data);
    else if($action=="getcity")
      $return = $this->getcity($data);
      else if($action=="getMessages")
      $return = $this->getMessage($data);
      
    else if($action=="change_status")
      $return = $this->change_status($data);
    else if($action=="ajax_review")
      $return = $this->ajax_review($data);
    else if($action=="ajax_client_review")
      $return = $this->ajax_client_review($data);
   
    else if($action=="getJob")
      $return = $this->getJob($data);
    else if($action=="deletejob")
      $return = $this->deletejob($data);
    else if($action=="add_offer")
      $return = $this->add_offer($data);
    else if($action=="close_contract")
      $return = $this->close_contract($data);
    else if($action=="send_release_fund")
     $return = $this->send_release_fund($data);
    else if($action=="payout")
     $return = $this->payout($data);
      
      
      
    return response()->json($return);

  }

  public function registerationEmail ($userId, $data) {
    $isExist = [];
    if($data->role == 'user') {
      $emailTemplates = 'email_templates.freelancer_registeration';
      $isExist = DB::select("SELECT * FROM fp_users WHERE status != 2 AND id = '".$userId."' ",[1]);
    } else if($data->role == 'client') {
      $emailTemplates = 'email_templates.client_registeration';
      $isExist = DB::select("SELECT * FROM fp_clients WHERE status != 2 AND id = '".$userId."' ",[1]);
    }

    if (!empty($isExist)) {
      $user = $isExist[0]; 
      // set-new-password
      $token = base64_encode ($data->email);
      if($data->role == 'user'){
        $userq = User::where('email', $user->email)->where('status' , 0)->first();
     
      }else if($data->role == 'client'){
        $userq = Client::where('email',$user->email)->where('status' , 0)->first();
       
      }
      $userq->notify(
              new Verifymail($token)
          );
     /* Mail::send($emailTemplates, ['url'=> asset('/verify-email').'/'.$token], function ($m) use ($user) {
        $m->to($user->email, 'User')->from('vivek@thedreamsteps.com', 'Justzoom')->subject('Verify Email!');
      });*/

      return true;
    }  

    return false;
  }

  // User Signup
  public function registration_Form($data) {
    $response = array ('valid' => false, 'msg'=>'Invalid Request');  

    $id = 0;
    if ($data->has('password')) {
      try {
        
        DB::beginTransaction();
        $isExist = DB::select("SELECT * FROM fp_auths WHERE status != 2 AND email = '".$data->email."' ",[1]);
        
        if (empty($isExist)) {
          $common_lib = new Common_helper();
          
          if($data->role == 'user') {
            $slug = $common_lib->createSlug($data->firstname,$id,'fp_users');

            $userdata = array (
              'first_name' => $data->firstname, 
              'last_name'=>$data->lastname, 
              'email' => $data->email, 
              'slug' => $slug
            );
            $id = DB::table('fp_users')->insertGetId($userdata);

          } else if ($data->role == 'client') {            
            $slug = $common_lib->createSlug($data->firstname,$id,'fp_clients');

            $clientData = array (
              'first_name' => $data->firstname, 
              'last_name'  => $data->lastname, 
              'email'      => $data->email, 
              'slug' => $slug
            );

        $id   = DB::table('fp_clients')->insertGetId($clientData);
          }
     
          if($id > 0) {

            $userauthdata = array('role'=>$data->role,'role_id'=>$id,'email'=>$data->email,'password'=>bcrypt($data->password),'created_at'=>date('Y-m-d H:i:s'));
            $success= DB::table('fp_auths')->insert($userauthdata);

            if ($this-> registerationEmail($id,$data)) {
              DB::commit();
            } else {
              DB::rollback();
              $response['msg'] = 'Error, Error in sending verification email.';
            }
            
             Session::put(['email'=>$data->email,'roleId'=>$id,'role'=>$data->role,'first_name'=>$data->firstname,'last_name'=>$data->lastname]);

            $response = array('valid' => true, 'msg' => 'You are registered succesfully. Your account is all set.');
          } else {
            $response['msg'] = 'Something went wrong.';
          }           
        } else {
          $response['msg'] = 'This Email address has already been registered.';
        }          
      } 
      catch (\Exception $e) {
        DB::rollback();
        $response['msg'] = 'Error : '.$e->getMessage();
      }
    } 
    return $response;

  }
        
  public function login_Form($data){
    $response = array('valid' => false, 'msg'=>'Invalid Request');
    if($data->has('username')){

      $isExist = DB::select("SELECT * FROM fp_auths WHERE status != 2 AND (role = 'user' or role = 'client') AND email = '".$data->username."' limit 0, 1");

      if (!isset ($isExist[0]->email_verified_at) && isset($isExist[0]->password)) {
        $response['msg'] = 'Sorry! This email is not verified.';
      }  else if (isset($isExist[0]->password)){
        if(Hash::check($data->password,$isExist[0]->password)){
          if($isExist[0]->status == 0){
            $userData = '';

            if($isExist[0]->role == 'user'){
                $userData = DB::select("SELECT * FROM fp_users WHERE status != 2 AND id = '".$isExist[0]->role_id."' limit 0, 1");
            }else if($isExist[0]->role == 'client'){
                $userData = DB::select("SELECT * FROM fp_clients WHERE status != 2 AND id = '".$isExist[0]->role_id."' limit 0, 1");
            }
            if(isset($userData[0]->id) && $userData[0]->id > 0){
              Session::put(['email'=>$userData[0]->email,'roleId'=>$userData[0]->id,'role'=>$isExist[0]->role,'first_name'=>$userData[0]->first_name,'last_name'=>$userData[0]->last_name]);
              $response = array('valid' => true, 'msg'=>'Logged in successfully.',  'role'=>$isExist[0]->role, 'first_name'=>$userData[0]->first_name, 'lastname'=>$userData[0]->last_name, 'mobile'=>$userData[0]->phone);
            }else
              $response['msg'] = 'Something is wrong with your profile. Please contact to admin.'; 
          }else{
            Session::forget(['email'=>$isExist[0]->email,'roleId'=>$isExist[0]->role_id,'role'=>'user','first_name'=>'','last_name'=>'']);
            $response['msg'] = 'Sorry! Your profile is inactive, Please Contact to admin';
          }
        }else
          $response['msg'] = 'Password is not correct.';   
        
      }else
        $response['msg'] = 'Sorry! This email is not registered with us.';
    }
    return $response;
  }

  public function forgot_Form($data) {

    $response = array('valid' => false, 'data'=>'Invalid Request');
    
    if($data->has('email')) {
       $isExist = DB::select("SELECT * FROM fp_auths WHERE status = 0 AND email = '".$data->email."'",[1]);
       if (!empty($isExist[0])) {
              if ($isExist[0]->email == $data->email) {
                $common_lib = new Common_helper();
                $token = $common_lib->generateStrongPassword(20,false,'lud');
                $code = date('Ymdhis').$token;
               
                $match = array('email' => $isExist[0]->email, 'status' => 0);
               /* $isInsert = DB ::table('fp_validcoupon')->updateOrInsert($match,['validtoken' => $code,'status'=>0]);*/
                $headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
 
               
                  if(!empty($isExist[0]->email)){
                      $emailId = '';
                      $user = $isExist[0]; 
                      $url =  url('/set-new-password').'/'.$code;
                      $msg = '<a href="'.$url.'">click</a>';
                      $user->url = $code;
                    
                      // use wordwrap() if lines are longer than 70 characters
                        $msg = wordwrap($msg,70);
                        if($user->role == 'user'){
                          $userq = User::where('email', $user->email)->where('status' , 0)->first();
                       
                        }else if($user->role == 'client'){
                          $userq = Client::where('email',$user->email)->where('status' , 0)->first();
                         
                        }
                       // print_R($userq);exit;
                        $userq->notify(
                                new PasswordResetRequest($code)
                            );
                            
                      // send email
                    // mail($user->email,"My subject",$msg,$headers);
                 /*  Mail::send('email_templates.forgot_password', ['user' => $user], function ($m) use ($user) {
                      $m->to($user->email, "xxx")->subject('Your Reminder!')
                      ->getSwiftMessage()
                      ->getHeaders()
                      ->addTextHeader('x-mailgun-native-send', 'true');
                  }); */
                      /*Mail::send('email_templates.forgot_password', ['url'=> url("/set-new-password/{$code}")], function ($m) use ($user) {
                          $m->to($user->email, 'User')->from('vivek@thedreamsteps.com', 'Justzoom')->subject('Password reset!');
                      });*/
                  }
                  $response = array('valid' => true, 'data'=>'Password reset link has been sent to your email');
                
              }
              else
                $response['data'] = 'Sorry! Your profile is inactive, Please Contact to admin';
       } else {
        $response['data'] = 'Sorry! This email is not registered with us.';
       }         
    }  
    return $response;
  }
  
  public function password_mail($data)
  { 
    
    $response = array('valid' => false, 'data'=>'Invalid Request');
    if($data->has('email') && $data->has('password')){
      // $match = array('email' => $data->email, 'status' => 0);
       $userauthdata = array('password'=>bcrypt($data->password),'updated_at'=>date('Y-m-d H:i:s'));
       
      $IsUpdate=  DB::table('fp_auths')
                ->where('email' ,'=', $data->email)
                ->where('status' ,'=', 0)
                ->update( $userauthdata);
      if($IsUpdate){
         $match = array('email' => $data->email, 'status' => 0);
         $isInsert = DB ::table('fp_validcoupon')->updateOrInsert($match,['status'=>1]);
           $response['valid'] = true;
        $response['data'] = 'Password Updated Successfully'; 

      }else{

        $response['data'] = 'Password not Updated'; 
      }
      
    }else{
      $response['data'] = 'Something is wrong';
    }
    return $response; 
  }

  public function search_gaurd($data){

    $response = array('valid' => false, 'data'=>'Invalid Request');
    $category_list = Category::all()
         ->where('status', '=', '0')   
         ->pluck('category_name', 'id');

    $weapon_list  = DB::table("fp_weapons")
         ->where('status', '=', '0')   
         ->pluck('weapon_name', 'id');

     $where = array();
     $orWhere = array();
     $where[0] = ['fp_users.status', '=', '0'];
     $where[1] = ['fp_users.price', '>', 0];
      if($data->ajax())
     { 
       $users = User::with(['category_detail'])
       ->select('fp_users.*', 'fp_cities.name as city','fp_countries.name as country',
       'fp_states.name as state')
              ->leftjoin('fp_countries', 'fp_countries.id', '=', 'fp_users.country')
              ->leftjoin('fp_cities', 'fp_cities.id', '=', 'fp_users.city')
              ->leftjoin('fp_states', 'fp_states.id', '=', 'fp_users.state')
             ->where($where);
      if(isset($data->price) && !empty($data->price)){
          $price = $data->price;
          $i = 0;
        $users = $users->where(function($query2)  use ($price){
              foreach($price as $p){
                $ary = explode('-', $p);
                $variable1 = $ary[0];
                $variable2 = $ary[1];
               $query2->orWhere(function($query3) use ($variable1,$variable2){
                    $query3->where('fp_users.price','>=',$variable1)
                   ->where('fp_users.price','<=',$variable2);
              });
              }
          });
        }
        if(isset($data->weapon) && !empty($data->weapon)){
          $users =  $users->WhereIn('fp_users.weapon_id',$data->weapon);
        }
        if(isset($data->category) && !empty($data->category)){
             $users = $users->WhereIn('fp_users.category_id',$data->category);
            
        }
        if(isset($data->gender) && !empty($data->gender)){
            $users =  $users->WhereIn('fp_users.gender',$data->gender);
        }

        $users =  $users->paginate(5);
     
        

        $view = view("ajax_gaurd_view",compact('users','category_list','weapon_list'))->render();
        $response['html'] = $view;     

      }
    return $response;
  }

  public function search_job($data){

    $response = array('valid' => false, 'data'=>'Invalid Request');
    $category_list = Category::all()
         ->where('status', '=', '0')   
         ->pluck('category_name', 'id');

    $jobIDs = JobRequest::where('status','!=' ,'3')
         ->where('user_id', session::get('roleId'))
         ->pluck('job_id');

     $where = array();
     $orWhere = array();
     $where[0] = ['fp_jobs.status', '=', '0'];
     $where[1] = ['fp_jobs.job_type', '=', '0'];
     $where[2] = ['fp_jobs.job_start_on', '>=',  date('Y-m-d')];
      if($data->ajax())
     { 
       
      $jobs = Job::with([])
             ->select('fp_jobs.*', 'fp_cities.name as city','fp_countries.name as country',
             'fp_states.name as state','fp_category.category_name')
            ->leftjoin('fp_countries', 'fp_countries.id', '=', 'fp_jobs.country_id')
            ->leftjoin('fp_cities', 'fp_cities.id', '=', 'fp_jobs.city_id')
            ->leftjoin('fp_states', 'fp_states.id', '=', 'fp_jobs.state_id')
            ->leftjoin('fp_category', 'fp_category.id', '=', 'fp_jobs.category_id')
            ->where($where);
            if(!empty($jobIDs) && count($jobIDs) > 0){
              $jobs->whereNotIn('fp_jobs.id', $jobIDs);
            }
            
      if(isset($data->price) && !empty($data->price)){
          $price = $data->price;
          $i = 0;
        $jobs = $jobs->where(function($query2)  use ($price){
              foreach($price as $p){
                $ary = explode('-', $p);
                $variable1 = $ary[0];
                $variable2 = $ary[1];
               $query2->orWhere(function($query3) use ($variable1,$variable2){
                    $query3->where('fp_jobs.price','>=',$variable1)
                   ->where('fp_jobs.price','<=',$variable2);
              });
              }
          });
        }
        if(isset($data->category) && !empty($data->category)){
             $jobs = $jobs->WhereIn('fp_jobs.category_id',$data->category);
            
        }
        $jobs =  $jobs->paginate(5);
     
        

        $view = view("ajax_job_view",compact('jobs','category_list'))->render();
        $response['html'] = $view;     

      }
    return $response;
  }

  public function ajax_jobrequest($data){
    $response = array('valid' => false, 'data'=>'Invalid Request');
   if(isset($_POST) && !empty($_POST)){
     $validationOn =  array(
       "title"=>$data->title,
       "category_id"=>$data->category_id,
       "address" => $data->address,
       "country" => $data->country,
       "state" => $data->state,
       "city" => $data->city,
       "pincode" => $data->pincode,
       "description" => $data->description,
       "job_start_on" => $data->job_start_on,
       "job_end_on" => $data->job_end_on,
       "working_hour" => $data->working_hour,
       "price" => $data->price
       );
       $validateRule=  array(
         "title"=>"required",
         "category_id"=>"required",
         "address" => "required",
         "country" => "required",
         "state" => "required",
         "city" => "required",
         "pincode" => "required",
         "description" => "required",
         "job_start_on" => "required",
         "job_end_on" => "required",
         "working_hour" => "required",
         "price" => "required",
         "price_type" => "required"
         );
     if($data->job_type == 0){
       $validationOn["price_type"] = '1';
       
     }else{
       $validationOn["user_id"] = $data->user_id;
       $validationOn["price_type"] = $data->price_type;
       $validateRule["user_id"] = "required";
     }
   
     
        
         $validator = Validator::make($validationOn,$validateRule);
         if ($validator->fails()){
       
       $response['errors'] = $validator->errors();
             //return redirect("/contact-us")->withErrors($validator)->withInput();
   
         }else{
 
           
       $job = new Job;
       $job->title = $data->title;
       $job->description = $data->description;
       $job->category_id = $data->category_id;
       $job->client_id = session::get('roleId');
       if(isset($data->price_type)){
         $job->price_type = $data->price_type;
       }else{
         $job->price_type = '1';
       }
      
       $job->working_hour = $data->working_hour;
       $job->job_end_on = date('Y-m-d', strtotime($data->job_end_on));
       $job->job_start_on = date('Y-m-d', strtotime($data->job_start_on));
       $job->price = $data->price;
       $job->country_id = $data->country;
       $job->state_id = $data->state;
       $job->city_id = $data->city;
       $job->pincode = $data->pincode;
       $job->address = $data->address;
       $job->job_type = $data->job_type; 
       
      if($job->save()){
        if(isset($data->user_id) && !empty($data->user_id)){
             $job_request = new JobRequest;
             $job_request->user_id = $data->user_id;
             $job_request->client_id = session::get('roleId');
             $job_request->job_id = $job->id;
             $job_request->offer_price = $data->price;
             $job_request->offer_by =  session::get('role');
             $job_request->price_type = $data->price_type;
             $job_request->save();
        }
     
       
       $response = array('valid' => true,'message' => 'Job Post successfully.');
      }else{
        $response = array('valid' => false,'message' => 'Invalid Request.');
      }
     }
     return $response;
   }
 }
  
  public function ajax_sendInvite($data){
   
   $response = array('valid' => false, 'data'=>'Invalid Request');
  if(isset($_POST) && !empty($_POST)){
    $validationOn =  array(
      "userId"=>$data->userId,
      "categoryId"=>$data->categoryId,
      "message" => $data->message
      );
      $validateRule=  array(
        "userId"=>"required",
        "categoryId"=>"required",
        "message" => "required"
        );
    
  
    
       
        $validator = Validator::make($validationOn,$validateRule);
        if ($validator->fails()){
      
      $response['errors'] = $validator->errors();
            //return redirect("/contact-us")->withErrors($validator)->withInput();
  
        }else{

          
      $con = new ContractRequest;
      $con->userId = $data->userId;
      $con->categoryId = $data->categoryId;
      $con->message = $data->message;
      $con->clientId = session::get('roleId');
     if($con->save()){
      
     
       $user_data = User::with([])
       ->where('id', '=', $data->userId)
       ->first();
       $client_data = Client::with([])
       ->where('id', '=', session::get('roleId'))
       ->first();
     
       $Invitation=[
          'first_name'=>$user_data->first_name,
          'last_name'=>$user_data->last_name,
          'email'=>$user_data->email,
          'client_first_name'=>$client_data->first_name,
          'client_last_name'=>$client_data->last_name,
          'client_email'=>$user_data->email,
          'message'=>$data->message,
       ];
     
     
       Mail::send('email_templates.Invitation_freelancer', ['invitation'=>$Invitation], function ($m) use ($Invitation) {
                          $m->to($Invitation['email'], 'Freelancer')->subject('Thank You for sending Invitation');
            });
     
      $response = array('valid' => true,'message' => 'Invitation send successfully.');
     }else{
       $response = array('valid' => false,'message' => 'Invalid Request.');
     }
    }
    return $response;
  }
}

public function ajax_approveInvite($data){

  $response = array('valid' => false, 'data'=>'Invalid Request');
 if(isset($_POST) && !empty($_POST)){
   $validationOn =  array(
     "reqId"=>$data->userId,
     "status"=>$data->status
     );
     $validateRule=  array(
       "userId"=>"required",
       "status"=>"required"
       );
       $validator = Validator::make($validationOn,$validateRule);
       if ($validator->fails()){
     
          $response['errors'] = $validator->errors();
         
       }else{

        $req = ContractRequest::find($data->reqId);
        $msg = "Invitation declined Successfully.";
      if($data->status==1){
        $con = new Chat;
        $con->clientId = $req->clientId;
        $con->userId = session::get('roleId');
        $con->created_at = date('Y-m-d H:i:s');
        $con->save();
         $con->notify(
                    new ApproveInvitationRequest()
                            );
        $req->chatId = $con->chatId;
        $msg = "Invitation accept Successfully.";
      }
      $req->requestStaus = $data->status;
      if($req->save()){
       
     $response = array('valid' => true,'message' => $msg);
    }else{
      $response = array('valid' => false,'message' => 'Invalid Request.');
    }
   }
   return $response;
 }
}


public function ajax_sendMessage($data){
  $response = array('valid' => false, 'data'=>'Invalid Request');
 if(isset($_POST) && !empty($_POST)){
   $validationOn =  array(
     "chatId"=>$data->chatId,
     "type"=>$data->type,
     "message" => $data->message
     );
     $validateRule=  array(
       "chatId"=>"required",
       "type"=>"required",
       "message" => "required"
       );
       $validator = Validator::make($validationOn,$validateRule);
       if ($validator->fails()){
          $response['errors'] = $validator->errors();
         
       }else{

         
     $con = new ChatDetail;
     $con->type = $data->type;
     $con->chatId = $data->chatId;
     $con->message = $data->message;
     $con->senderId = session::get('roleId');
     $con->senderType = session::get('role');
    if($con->save()){
     $response = array('valid' => true,'message' => 'Message send successfully.');
    }else{
      $response = array('valid' => false,'message' => 'Invalid Request.');
    }
   }
   return $response;
 }
}

public function add_offer($data){
  $response = array('valid' => false, 'data'=>'Invalid Request');
 if(isset($_POST) && !empty($_POST)){
   $validationOn =  array(
     "offer_price" => $data->offer_price,
     "offer_by" => $data->offer_by
     );
   $validateRule=  array(
       "offer_price" => "required",
       "offer_by" => "required"
    );
    $validator = Validator::make($validationOn,$validateRule);
    if ($validator->fails()){
     $response['errors'] = $validator->errors();
    }else{

        if($data->offer_by == 'user'){
            $job_request = JobRequest::with([])
            ->where('job_id','=',$data->offer_job_id)
            ->where('user_id','=',session::get('roleId'))
            ->first();
        }else{
            $job_request = JobRequest::with([])
            ->where('job_id','=',$data->offer_job_id)
            ->where('user_id','=',$data->user_id)
            ->first();
        } 
     
      
    $jobData = Job::find($data->offer_job_id);
    if(isset($job_request->id) && !empty($job_request->id)){
      $offer = JobRequest::find($job_request->id);
      $offer->offer_price = $data->offer_price;
      $offer->offer_by = $data->offer_by;
      $offer->job_status = '0';
    }else{
        $offer = new JobRequest;
        if($data->offer_by == 'user'){
          $offer->user_id = session::get('roleId');
        }else{
          $offer->user_id = $data->user_id;
        }
        $offer->job_id = $data->offer_job_id;
        $offer->client_id = $jobData->client_id;
        $offer->job_status = '0';
        $offer->price_type = '1';
        $offer->offer_price = $data->offer_price;
        $offer->offer_by = $data->offer_by;
    }
    if($offer->save()){
     $response = array('valid' => true,'message' => 'Offer added successfully.');
    }else{
      $response = array('valid' => false,'message' => 'Invalid Request.');
    }
   }
   return $response;
 }
}


public function editjobrequest($data){
  $response = array('valid' => false, 'data'=>'Invalid Request');
 if(isset($_POST) && !empty($_POST)){
 
   $validationOn =  array(
       "title"=>$data->title,
               "category_id"=>$data->category_id,
               "address" => $data->address,
       "country" => $data->country,
       "state" => $data->state,
       "city" => $data->city,
       "pincode" => $data->pincode,
       "description" => $data->description,
       "job_start_on" => $data->job_start_on,
       "job_end_on" => $data->job_end_on,
       "working_hour" => $data->working_hour,
       "price_type" => $data->price_type,
       "user_id" => $data->user_id,
       "price" => $data->price
           );
     $validateRule=  array(
       "title"=>"required",
               "category_id"=>"required",
               "address" => "required",
       "country" => "required",
       "state" => "required",
       "city" => "required",
       "pincode" => "required",
       "description" => "required",
       "job_start_on" => "required",
       "job_end_on" => "required",
       "working_hour" => "required",
       "price_type" => "required",
       "user_id" => "required",
       "price" => "required"
               
           );
      
          $validator = Validator::make($validationOn,$validateRule);
       if ($validator->fails()){
     
     $response['errors'] = $validator->errors();
           //return redirect("/contact-us")->withErrors($validator)->withInput();
 
       }else{
        $job = Job::find($data->job_id);
        $job->title = $data->title;
        $job->description = $data->description;
        $job->category_id = $data->category_id;
        $job->client_id = session::get('roleId');
        $job->price_type = $data->price_type;
        $job->working_hour = $data->working_hour;
        $job->job_end_on = date('Y-m-d', strtotime($data->job_end_on));
        $job->job_start_on = date('Y-m-d', strtotime($data->job_start_on));
        $job->price = $data->price;
        $job->country_id = $data->country;
        $job->state_id = $data->state;
        $job->city_id = $data->city;
        $job->pincode = $data->pincode;
        $job->address = $data->address;
     //$job->user_id = $data->user_id;
    if($job->save()){
     $job_request = new JobRequest;
     $job_request->user_id = $data->user_id;
     $job_request->client_id = session::get('roleId');
     $job_request->job_id = $job->id;
     $job_request->save();
     $response = array('valid' => true,'message' => 'Job Post successfully.');
    }else{
      $response = array('valid' => false,'message' => 'Invalid Request.');
    }
   }
   return $response;
 }
}
  
  public function ajax_review($data){

    $response = array('valid' => false, 'data'=>'Invalid Request');
   if(isset($_POST) && !empty($_POST)){
   
     $validationOn =  array(
          "rating"=>$data->rating,
          "review"=>$data->review,
          /*"review_user_id" => $data->review_user_id,
          "review_job_req_id" => $data->review_job_req_id*/
      );
       $validateRule=  array(
        "rating"=>"required",
        "review"=>"required",
       /* "review_user_id" => "required",
        "review_job_req_id" => "required"*/
        );
        
        $validator = Validator::make($validationOn,$validateRule);
         if ($validator->fails()){
       
       $response['errors'] = $validator->errors();
        
   
         }else{
          //$Req = JobRequest::find($data->review_job_req_id);
       $reviews = new UserReview;
       $reviews->rating = $data->rating;
       $reviews->review = $data->review;
       $reviews->user_id = $data->user_id;
       $reviews->contractId =  $data->contractId;
       $reviews->client_id = $data->client_id;
      // $reviews->job_req_id = $data->review_job_req_id;
      if($reviews->save()){
      /* $jobdata =  Job::find($Req->job_id);
       $jobdata->status = 2;*/
       //$jobdata->save();
       
       $response = array('valid' => true,'message' => 'Review submit Successfully.');
      }else{
        $response = array('valid' => false,'message' => 'Invalid Request.');
      }
     }
     return $response;
   }
    
  }

  public function ajax_client_review($data){

    $response = array('valid' => false, 'data'=>'Invalid Request');
   if(isset($_POST) && !empty($_POST)){
   
     $validationOn =  array(
          "rating"=>$data->rating,
          "review"=>$data->review,
          /*"review_user_id" => $data->review_user_id,
          "review_job_req_id" => $data->review_job_req_id*/
      );
       $validateRule=  array(
        "rating"=>"required",
        "review"=>"required",
       /* "review_user_id" => "required",
        "review_job_req_id" => "required"*/
        );
        
        $validator = Validator::make($validationOn,$validateRule);
         if ($validator->fails()){
       
       $response['errors'] = $validator->errors();
        
   
         }else{
          //$Req = JobRequest::find($data->review_job_req_id);
       $reviews = new ClientReview;
       $reviews->rating = $data->rating;
       $reviews->review = $data->review;
       $reviews->user_id = $data->user_id;
       $reviews->contractId =  $data->contractId;
       $reviews->client_id = $data->client_id;
      // $reviews->job_req_id = $data->review_job_req_id;
      if($reviews->save()){
      /* $jobdata =  Job::find($Req->job_id);
       $jobdata->status = 2;*/
       //$jobdata->save();
       
       $response = array('valid' => true,'message' => 'Review submit Successfully.');
      }else{
        $response = array('valid' => false,'message' => 'Invalid Request.');
      }
     }
     return $response;
   }
    
  }
 public function getstate($data){
   $response = array('valid' => false, 'msg'=>'Invalid Request');  
    $state_list = DB::table('fp_states')
      ->where('country_id', '=', $_POST['country_id'])   
      ->pluck('name', 'id');
     $response['valid'] = true;
     $response['data'] = $state_list; 
     return $response;
  
  }
  public function getMessage($data){
    $response = array('valid' => false, 'msg'=>'Invalid Request'); 
    if(session::get('role') == 'client'){
      $chatM = ContractRequest::where('chatId', '=', $data->chatId)
                  ->select("fp_users.*","fp_contract_request.reqId","fp_contract_request.categoryId","fp_contract_request.contractId",
                  "fp_category.category_name",'fp_cities.name as city','fp_countries.name as country','fp_contract_request.userId',
                  'fp_states.name as state')
                 ->leftjoin('fp_users', 'fp_users.id', '=', 'fp_contract_request.userId')
                 ->leftjoin('fp_category', 'fp_category.id', '=', 'fp_contract_request.categoryId')
                 ->leftjoin('fp_countries', 'fp_countries.id', '=', 'fp_users.country')
                 ->leftjoin('fp_cities', 'fp_cities.id', '=', 'fp_users.city')
                 ->leftjoin('fp_states', 'fp_states.id', '=', 'fp_users.state')
                 ->where('clientId', '=', session::get('roleId'))
                  ->first();

    }else{
      $chatM = ContractRequest::where('chatId', '=', $data->chatId)
                  ->select("fp_clients.*","fp_contract_request.reqId","fp_contract_request.categoryId","fp_contract_request.contractId",
                  "fp_category.category_name",'fp_cities.name as city','fp_countries.name as country','fp_contract_request.userId',
                  'fp_states.name as state')
                 ->leftjoin('fp_clients', 'fp_clients.id', '=', 'fp_contract_request.clientId')
                 ->leftjoin('fp_category', 'fp_category.id', '=', 'fp_contract_request.categoryId')
                 ->leftjoin('fp_countries', 'fp_countries.id', '=', 'fp_clients.country')
                 ->leftjoin('fp_cities', 'fp_cities.id', '=', 'fp_clients.city')
                 ->leftjoin('fp_states', 'fp_states.id', '=', 'fp_clients.state')
                 ->where('userId', '=', session::get('roleId'))
                  ->first();

    }
      $chatData = ChatDetail::where('id','>',$data->lastChId) 
      ->where('chatId', '=', $data->chatId)  
       ->get();
       $dt = '';
       $lastId = $data->lastChId;
      if(!empty( $chatData)){
        foreach($chatData as $chat){
          if($chat->senderType==session::get('role') && $chat->senderId==session::get('roleId')){
            $dt.= '<div class="outgoing_msg"><div class="sent_msg"><div class="chat-time-detail">';
            $dt.= '<p>19:47</p><p>You</p> </div><p>'.$chat->message.'</p></div>';
            $dt.=  '<div class="incoming_msg_img"><img src="'.asset("assets/frontend/img/chat.png").'" alt="sunil">';
            $dt.=   '</div></div>';
            $lastId = $chat->id;


          }else{
            $dt.= '<div class="incoming_msg">';
            $dt.= '<div class="incoming_msg_img"><img src="'.asset("assets/frontend/img/chat.png").'" alt="sunil"> </div>';
            $dt.='<div class="received_msg">';
            $dt.= '<div class="received_withd_msg"><div class="chat-time-detail"><p>'.$chatM->first_name.' '.$chatM->last_name.'</p>';
            $dt.= '<p>19:45</p></div><p>'.$chat->message.'</p></div></div></div>';
            $lastId = $chat->id;

          }
          
        }

      }
     
      $response['valid'] = true;
      $response['data'] = $dt; 
      $response['lastId'] = $lastId;
      return $response;
   
   } 
  public function getcity($data){
    $response = array('valid' => false, 'msg'=>'Invalid Request');  
    $city_list = DB::table('fp_cities')
      ->where('state_id', '=', $_POST['state_id'])   
      ->pluck('name', 'id');
     $response['valid'] = true;
     $response['data'] = $city_list; 
     return $response;
  }
 
  
  public function change_status($data){
  
    $response = array('valid' => false, 'msg'=>'Invalid Request');  
    $ContractRequest = ContractRequest::find($data->reqId);
   
    $user_data = User::with([])
       ->where('id', '=', $ContractRequest->userId)
       ->first();
     $client_data = Client::with([])
       ->where('id', '=', $ContractRequest->clientId)
       ->first();
     $Invitation=[
          'first_name'=>$user_data->first_name,
          'last_name'=>$user_data->last_name,
          'email'=>$user_data->email,
          'client_first_name'=>$client_data->first_name,
          'client_last_name'=>$client_data->last_name,
          'client_email'=>$client_data->email,
          'message'=>$data->message,
       ];
       //print_r($Invitation); exit();
    if($data->status == 1){
      $chat = Chat::where("clientId",$ContractRequest->clientId)->where("userId",$ContractRequest->userId)->first();
      if(!empty($chat)){
        $ContractRequest->chatId = $chat->chatId;

      }else{
        $con = new Chat;
        $con->clientId = $ContractRequest->clientId;
        $con->userId = session::get('roleId');
        $con->created_at = date('Y-m-d H:i:s');
        $con->save();
        $ContractRequest->chatId = $con->chatId;
        
      }
    }
    $ContractRequest->requestStatus = $data->status;
    if($ContractRequest->save()){
      $response['valid'] = true;
      if($data->status == 1){
       //print_r($Invitation['client_email']); exit();
       Mail::send('email_templates.accept_invitation_client', ['invitation'=>$Invitation], function ($m) use ($Invitation) {
                          $m->to($Invitation['client_email'], 'Client')->subject('Job Accepted Successfully');
            });
        $response['msg'] = 'Job Accepted Successfully'; 
      }else{
         Mail::send('email_templates.reject_invitation_client', ['invitation'=>$Invitation], function ($m) use ($Invitation) {
                          $m->to($Invitation['client_email'], 'Client')->subject('Job Declined.');
            });
        $response['msg'] = 'Job Declined Successfully'; 
      }
     

    }else{
      $response['msg'] = 'Something wrong.';
    }
     return $response;
  }
  public function close_contract($data){

    $response = array('valid' => false, 'msg'=>'Invalid Request');  
    $ContractRequest = ContractRequest::where("contractId",$data->contractId)->first();
    //print_R($ContractRequest);exit;
   
    $contract = Contract::find($data->contractId);
   
    $ContractRequest->isRequestClose = 1;
    if($ContractRequest->save()){
      $contract->IsContractComplete = 1;
      $contract->save();
      $response['valid'] = true;
      $response['msg'] = "contract closed successfully.";
    }else{
      $response['msg'] = 'Something wrong.';
    }
     return $response;
  }

  public function send_release_fund($data){

    $response = array('valid' => false, 'msg'=>'Invalid Request');  
    $Contract = Contract::where("contractId",$data->contractId)->where('userId',session::get('roleId'))->first();
    if(!empty($Contract)){
      $client = Client::where("id",$Contract->clientId)->first();
      $con = new ReleasePayment;
      $con->userId = session::get('roleId');
      $con->amount = $Contract->freelancerPrice;
      $con->currency = $Contract->currency;
      $con->contractId = $Contract->contractId;
   
      $con->save();
      Notification::route('mail' , $client->email) //Sending mail to subscriber
      ->notify(new SendPaymentRequest($con));
     
      $response['valid'] = true;
      $response['msg'] = "Send release fund request successfully.";

    }else{
      $response['msg'] = 'You have not permisson to access this page.';

    }
     return $response;
  }

  public function payout($data){

    $response = array('valid' => false, 'msg'=>'Invalid Request');  
    $Contract = Contract::where("fp_contract.contractId",$data->contractId)->where('clientId',session::get('roleId'))
                ->select("fp_release_payment.*","fp_contract.*","fp_clients.*","fp_users.email as useremail")
                ->leftjoin('fp_clients', 'fp_clients.id', '=', 'fp_contract.clientId')
                ->leftjoin('fp_users','fp_users.id', '=', 'fp_contract.userId')
                ->leftJoin("fp_release_payment","fp_release_payment.contractId", "=","fp_contract.contractId")
                ->first();

    $stripeDetail = StripeDetail::where("userId",$Contract->userId)->first();
    if($stripeDetail){
      $setting  = Setting::first();
    $stripe = Stripe\Stripe::setApiKey($setting->secreteKey);
    if(!empty($Contract)){
        if(!empty($data->stripetoken)){
          $cc = \Stripe\Customer::createSource(
            $Contract->stripeCustomerId,
            ['source' => $_POST['stripetoken']]
          );
          
          try {

            $charge = \Stripe\Charge::create([
              "amount" => $Contract->freelancerPrice*100,
              "currency" => "GBP",
              "customer" => $cc->customer,
              "source" => $cc->id,
              "on_behalf_of" => $stripeDetail->stripeAccId,
            ]);
            
        } catch (Exception $e) {
          $response['errors'] = $e->getMessage();
          return $response;
        }
        }else{
          
          $cc = \Stripe\Customer::retrieveSource(
            $Contract->stripeCustomerId,
            $Contract->cardSource
          );
            try {
            
                $charge = \Stripe\Charge::create([
                    "amount" => $Contract->freelancerPrice*100,
                    "currency" => "GBP",
                    "customer" => $cc->customer,
                    "source" => $Contract->cardSource,
                    "on_behalf_of" => $stripeDetail->stripeAccId,
                    
                ]);
          } catch (Exception $e) {
            $response['errors'] = $e->getMessage();
            return $response;
          }
        }
      if(!isset($response['errors']) || empty($response['errors'])){
          $con = new Payout;
          $con->userId = $Contract->userId;
          $con->amount = $Contract->freelancerPrice;
          $con->relId = $Contract->relId;
          $con->response = $charge;
          $con->currency = $Contract->currency;
          $con->paymentId = $charge->id;
          $con->contractId = $Contract->contractId;
          $con->created_at = date('Y-m-d H:i:s');
          $con->save();
          ReleasePayment::where('relId',$Contract->relId)->update(['status'=>1]);
          Notification::route('mail' , $Contract->email) 
          ->notify(new SendPaymentSuccess($con));

          Notification::route('mail' , $Contract->useremail) 
          ->notify(new SendPaymentSuccess($con));
          $response['valid'] = true;
          $response['msg'] = "Payment has been done successfully.";
      }else{
        $response['msg'] = "Something went wrong!";
      }

     
    }else{
      $response['msg'] = 'You have not permisson to access this page.';

    }
  }else{
    $response['msg'] = 'User not set Stripe detail yet.';

  }
     return $response;
  }


/*new*/
public function categary_search(Request $request){
        
  if($request->ajax()) {
    
      $data = Category::where('category_name', 'LIKE', $request->category.'%')
      ->leftJoin('fp_category_question', 'fp_category.id', '=', 'fp_category_question.categoryId')
      ->select('fp_category.*',DB::raw('count(fp_category_question.questionId) as question'))
      ->groupBy(['fp_category.id'])
      ->get();
     
      $output = '';
     
      if (count($data)>0) {
        
          $output = '<ul class="list-group" style="display: block; position: relative; z-index: 1">';
        
          foreach ($data as $row){
             
              $output .= '<li class="list-group-item" data-cat-id="'.$row->id.'" data-question="'.$row->question.'">'.$row->category_name.'</li>';
          }
        
          $output .= '</ul>';
      }
      else {
       
          $output .= '<li class="list-group-item">'.'No results'.'</li>';
      }
     
      return $output;
  }
}
  
  
 


}