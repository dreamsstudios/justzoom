<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Client;
use App\Models\Query;
use App\Models\Category;
use App\Models\UserProfileView;
use App\Models\UserReview;
use App\Models\UserSkill;
use App\Models\Blog;
use App\Models\UserExperience;
use App\Models\UserEducation;
use App\Models\ContractRequest;
use App\Models\Contract;
use App\Models\UserProject;
use App\Models\UserPortfolio;
use App\Models\Chat;
use App\Models\Setting;
use App\Models\ChatDetail;
use App\Models\UserHire;
use App\Models\CategoryQuestion;
use App\Models\QuestionOption;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Common_helper;
use Validator;
use Session;
use DB;
use Mail;
use View;
use PDF;
use App\Notifications\PasswordResetRequest;
use App\Notifications\ContactusRequest;

class HomeController extends Controller
{
	
	public function __construct()
    {
    parent::__construct();
   
    
    }
  public function index(){
    
       $user_id =  Session::get('roleId');
  
   // $users = DB::select('select user.id, user.*, (SELECT skill from fp_skills where fp_skills.id  = (SELECT skill_id from fp_user_skills where fp_user_skills.user_id  = user.id limit 0,1)  limit 0,1) as skill from fp_users as user where user.status =0');
   $common_lib = new Common_helper();
      $gaurd_count = User::where(['status' => 0])->get()->count();
      $client_count = Client::where(['status' => 0])->get()->count();

       $users = User::with(['category_detail'])
       ->select('fp_users.*', 'fp_cities.name as city','fp_countries.name as country',
       'fp_states.name as state')
       ->leftjoin('fp_countries', 'fp_countries.id', '=', 'fp_users.country')
       ->leftjoin('fp_cities', 'fp_cities.id', '=', 'fp_users.city')
       ->leftjoin('fp_states', 'fp_states.id', '=', 'fp_users.state')
       ->where('fp_users.status', '=', '0')
       ->where('fp_users.hourly_rate', '>', 0)
       ->get();
       //echo "<pre>"; print_r($users); exit();
        return view("welcome",compact('users','client_count','gaurd_count','common_lib'));
  }
  public function generatePDF($contractId)
    {
      $client_id = session::get('roleId');
      $user = Client::with([])
      ->where('id', '=', $client_id)
      ->first();
      $pageTitle = "fff";
     
      $contract_detail = Contract::where('fp_contract.clientId', '=', $client_id)
      ->select("fp_users.*","fp_contract.*","fp_contract.categoryId",
      "fp_category.category_name",'fp_contract_request.chatId',"uc.category_name as user_category_name","fp_contract_request.userId")
      ->leftjoin('fp_users', 'fp_users.id', '=', 'fp_contract.userId')
     
      ->leftjoin('fp_contract_request', 'fp_contract.contractId', '=', 'fp_contract_request.contractId')
      ->leftjoin('fp_category', 'fp_category.id', '=', 'fp_contract.categoryId')
      ->leftjoin('fp_category as uc', 'uc.id', '=', 'fp_users.category_id')
      ->leftjoin('fp_countries', 'fp_countries.id', '=', 'fp_users.country')
      ->leftjoin('fp_cities', 'fp_cities.id', '=', 'fp_users.city')
      ->leftjoin('fp_states', 'fp_states.id', '=', 'fp_users.state')
      ->where('fp_contract.contractId','=',$contractId)
      ->first();
     
        $data = ['contract_detail' => $contract_detail];
        $pdf = PDF::loadView('pdfView', $data);
  
        return $pdf->download('contract.pdf');
    }
  public function hiretotalent(){
    $category_list = Category::all()
    ->where('status', '=', '0')   
    ->pluck('category_name', 'id');
    return view("hiretotalent",compact('category_list'));
  }
  

  public function hireFreelancer($reqId){
   $common_lib = new Common_helper();
    $requestData = ContractRequest::where('reqId', '=', $reqId)
                  ->select("fp_users.*",'fp_contract.*',"fp_contract_request.reqId",
                  "fp_contract_request.categoryId as categoryId",
                  "fp_category.category_name",'fp_cities.name as city','fp_countries.name as country',
                  'fp_states.name as state','fp_contract_request.userId')
                 ->leftjoin('fp_users', 'fp_users.id', '=', 'fp_contract_request.userId')
                 ->leftjoin('fp_contract', 'fp_contract.contractId', '=', 'fp_contract_request.contractId')
                 ->leftjoin('fp_category', 'fp_category.id', '=', 'fp_contract_request.categoryId')
                 ->leftjoin('fp_countries', 'fp_countries.id', '=', 'fp_users.country')
                 ->leftjoin('fp_cities', 'fp_cities.id', '=', 'fp_users.city')
                 ->leftjoin('fp_states', 'fp_states.id', '=', 'fp_users.state')
                 ->where('fp_contract_request.clientId', '=', session::get('roleId'))
                  ->first();
      
      $setting = Setting::first();
      $totalRating = $common_lib->getTotalRating($requestData->userId);
             
    return view("hire_freelancer",compact('requestData','totalRating','setting'));  
    
  }


  

  public function searchResult($catId) {
    $categoryDeatil = Category::where('id', '=', $catId)->first();
 
    $users = User::with(['category'])
            ->select('fp_users.*', 'fp_cities.name as city','fp_countries.name as country', 'fp_states.name as state',"fp_category.category_name")
            ->leftjoin('fp_countries', 'fp_countries.id', '=', 'fp_users.country')
            ->leftjoin('fp_cities', 'fp_cities.id', '=', 'fp_users.city')
            ->leftjoin('fp_states', 'fp_states.id', '=', 'fp_users.state')
            ->leftjoin('fp_category', 'fp_category.id', '=', 'fp_users.category_id')
            ->where('fp_users.status', '=', '0')
            ->where('fp_users.category_id', '=', $catId)
            ->where('fp_users.hourly_rate', '>', 0);
    $search  = Session::get('getFilter'.$catId);
    if (isset($_GET) && !empty($_GET)) {
      if (!isset($_GET['search'])) {
          $_GET['search'] = $_GET;
      }
      foreach($search as $key=>$v){
        
      }

      if (isset($_GET['search']['location']) && !empty($_GET['search']['location'])) {          
        $users = $users->where('fp_countries.name', 'LIKE', '%' . $_GET['search']['location'] . '%');  
        $users = $users->orWhere('fp_cities.name', 'LIKE', '%' . $_GET['search']['location'] . '%');
        $users = $users->orWhere('fp_states.name', 'LIKE', '%' . $_GET['search']['location'] . '%');
        $users = $users->orWhere('fp_users.available_cities', 'LIKE', '%' . $_GET['search']['location'] . '%');
        $users = $users->orWhere('fp_users.available_pincodes', 'LIKE', '%' . $_GET['search']['location'] . '%');
      }

      if (isset($_GET['search']['keyword']) && !empty($_GET['search']['keyword'])) {
        if (isset($_GET['search']['location']) && !empty($_GET['search']['location'])) {
          $users = $users->orWhere('fp_users.first_name', 'LIKE', '%' . $_GET['search']['keyword'] . '%');
          $users = $users->orWhere('fp_users.last_name', 'LIKE', '%' . $_GET['search']['keyword'] . '%');
          $users = $users->orWhere('fp_category.category_name', 'LIKE', '%' . $_GET['search']['keyword'] . '%');
          $users = $users->orWhere('fp_users.gender',"="  ,$_GET['search']['keyword']);
          $users = $users->orWhere('fp_users.price', 'LIKE', '%' . $_GET['search']['keyword'] . '%');
          $users = $users->orWhere('fp_weapons.weapon_name', 'LIKE', '%' . $_GET['search']['keyword'] . '%');
        } else {          
          $users = $users->where('fp_users.first_name', 'LIKE', '%' . $_GET['search']['keyword'] . '%');
          $users = $users->orWhere('fp_users.last_name', 'LIKE', '%' . $_GET['search']['keyword'] . '%');
          $users = $users->orWhere('fp_category.category_name', 'LIKE', '%' . $_GET['search']['keyword'] . '%');
          $users = $users->orWhere('fp_users.gender', 'LIKE', '%' . $_GET['search']['keyword'] . '%');
          $users = $users->orWhere('fp_users.price', 'LIKE', '%' . $_GET['search']['keyword'] . '%');
          $users = $users->orWhere('fp_weapons.weapon_name', 'LIKE', '%' . $_GET['search']['keyword'] . '%');
        }         
      }      
    }

    

    $users = $users->paginate(10);
    if(isset($_GET['search']) && !empty($_GET['search'])){
      $users->appends(['search' => $_GET['search']]); 
    }

    $userData = [];
    if (!empty ($users)) {
      foreach ($users as $info) {
        $unreadMessage = ChatDetail::where('senderId', '=', $info->id)->where('isRead', '=', 0)->count();
        $totalRatings  = UserReview::where('user_id', '=', $info->id)->where('status', '=', 0)->sum('rating');
        $totalReviews  = UserReview::where('user_id', '=', $info->id)->where('status', '=', 0)->count();
        $totalCustomer = Contract::where('userId', '=', $info->id)->count(DB::raw('DISTINCT clientId'));

        $userLatestChat= Chat::where('userId', '=', $info->id)
                         ->where('clientId', '=', Session::get('roleId'))
                         ->orderBy('updated_at', 'desc')->first();

        $userLastMessage = [];
        $senderData = [];
        if (!empty($userLatestChat)) {
          if(Session::get('role') == 'user'){
             $userLastMessage = ChatDetail::where('chatId', '=', $userLatestChat->chatId)
                          ->select('fp_chat_detail.*','fp_clients.first_name','fp_clients.last_name')
                            ->leftJoin('fp_clients',function ($join) {
                              $join->on('fp_clients.id', '=' , 'fp_chat_detail.senderId') ;
                              $join->where('fp_chat_detail.senderType','=','client');
                          })->orderBy('created_at','DESC')
                          ->first();

          }else{
              $userLastMessage = ChatDetail::where('chatId', '=', $userLatestChat->chatId)
              ->select('fp_chat_detail.*','fp_users.first_name','fp_users.last_name')
              ->leftJoin('fp_users',function ($join) {
                    $join->on('fp_users.id', '=' , 'fp_chat_detail.senderId') ;
                    $join->where('fp_chat_detail.senderType','=','user');
              })->orderBy('created_at','DESC')->first();

          }
         
          
        }

        $info['totalRatings']   = ($totalReviews > 0) ? $totalRatings / $totalReviews : 0;
        $info['totalReviews']   = $totalReviews;
        $info['unreadMessage']  = $unreadMessage;
        $info['totalCustomer']  = $totalCustomer;
        $info['userChatId']= isset($userLatestChat->chatId) ? $userLatestChat->chatId: "";
        $info['userLastMessage']= !empty($userLastMessage) ? $userLastMessage->message: "";
        $info['userLastMessageDate']= !empty($userLastMessage) ? date('d-m-Y h:i:s', strtotime($userLastMessage->created_at)) : "";
        $info['userLastMessageSenderName']= !empty($userLastMessage->first_name) ? $userLastMessage->first_name. ' ' .$userLastMessage->last_name: "Me";
        $userData[] = $info;
      }
    }   
    
      
    // echo "<pre>"; print_r ($userData); die;
    return view("searchresult",compact('users','categoryDeatil'));
  }




  public function questionFilter($catId,$step) {
   // Session::forget('getFilter'.$catId) ;
    $categoryDeatil = Category::where('id', '=', $catId)->first();
    $getQuestion = Session::get('getFilter'.$catId);
    
     $steps = explode("-",$step);
     $number  = $steps[1];
     $oldKey = $number-1;
      if($number>1){
       
        $lastKey  = $getQuestion[$oldKey]['question'];

      }else{
        $lastKey = 0;
      }
     
       
      $question = CategoryQuestion::where('categoryId','=', $catId)
                  ->where('questionId','>',$lastKey)->first();
      //print_R($question);exit;
      $questionQption = QuestionOption::where('questionId','=', $question->questionId)->get();
      $questionCount = CategoryQuestion::where('categoryId','=', $catId)->count();
      $back = "";
      $next = "";
      
      $n = $number+1;
      if($questionCount>$steps[1]){
        $next = "step-".$n;
        
      }
    

   
   
    return view("questionfilter",compact('question','questionQption','next','back','categoryDeatil','questionCount','number'));
  }

  public function setfilter(Request $request) {
    $getQuestion = array();
    $getQuestion = $request->session()->get('getFilter'.$request->catId);
    //print_r($_POST);exit;
    if(!empty($request->questionId)){
      $getQuestion[$request->step]['question'] = $request->questionId;
      $getQuestion[$request->step]['answer'] = '';
      if(isset($request->option) && !empty($request->option)){
          $getQuestion[$request->step]['answer'] = $request->option;
      }
      
    }
    //print_R($getQuestion);exit;
    
   //$request->session()->put('getFilter'.$request->catId, $getQuestion);
    //print_r( $request->session()->get('getFilter'.$request->catId));exit;
    Session::put('getFilter'.$request->catId,$getQuestion);
   // print_r($request->session()->get('getFilter'.$request->catId));exit;


    if(!empty($request->next)){
      return redirect('/question-filter/'.$request->catId.'/'.$request->next);

    }else{
      return redirect('/search-result/'.$request->catId);
    }
    
    
}

  public function freelancerDetail($id= null){
		$user_id = session::get('roleId');
		$common_lib = new Common_helper();
        /* $count_view = DB::table("fp_user_profile_views")
         ->where('created_at', 'like', date('Y-m-d').'%')
         ->where('user_id', '=', $id)
         ->where('client_id', '=', $user_id)
         ->get()->count();
         if($count_view <= 0){
             if($user_id != $id){
                $UserProfileView = new UserProfileView;
                $UserProfileView->client_id = $user_id;
                $UserProfileView->user_id = $id;
                $UserProfileView->save();

             }
         }*/

       $user = User::with(['category'])
       ->select('fp_users.*', 'fp_cities.name as city','fp_countries.name as country',
       'fp_states.name as state','fp_users.id as freelance_id')
       ->leftjoin('fp_countries', 'fp_countries.id', '=', 'fp_users.country')
       ->leftjoin('fp_cities', 'fp_cities.id', '=', 'fp_users.city')
       ->leftjoin('fp_states', 'fp_states.id', '=', 'fp_users.state')
       ->where('fp_users.id', '=', $id)
       ->first();
     //  echo "<pre>"; print_r($user); exit();
      $userskill = UserSkill::with(['skill_data'])
       ->where('user_id', '=', $id)
       ->get();
    $UserExperience = UserExperience::with([])
       ->where('user_id', '=', $id)
       ->where('user_role', '=','user')
       ->get();
    $userportfolio = UserPortfolio::with([])
       ->where('user_id', '=', $id)
       ->where('user_role', '=','user')
       ->get();
    $UserProject = UserProject::with([])
       ->where('user_id', '=', $id)
       ->where('user_role', '=','user')
       ->get();
     $UserEducation = UserEducation::with([])
       ->where('user_id', '=', $id)
       ->where('user_role', '=','user')
       ->get(); 
      $UserInvite = ContractRequest::with([])
       ->where('userId', '=', $id)
       ->where('clientId', '=', session::get('roleId'))
       ->where('status', '=',0)
       ->where('isRequestClose', '=', 0)
       ->orderBy('reqId', 'desc')
       ->first(); 
	
        $reviews = UserReview::with([])
        ->select('fp_user_reviews.*',"fp_clients.*")
        ->leftjoin("fp_clients",'fp_clients.id', '=', 'fp_user_reviews.client_Id')
        ->where('fp_user_reviews.user_id', '=', $id)
        ->paginate(5);
                  
                  
        $totalRating = $common_lib->getTotalRating($id);
        $contract = Contract::where('fp_contract.userId', '=', $id)->where('IsContractComplete','=',1)
        ->count();
        $clients = Contract::where('fp_contract.userId', '=', $id)
        ->count(DB::raw('DISTINCT clientId'));

    return view("user_detail",compact('user','reviews','userskill','UserExperience',
    'userportfolio','UserProject','UserEducation','UserInvite','totalRating','contract','clients'));

  }

   public function about_us(){
    $page_title = "about";
    return view("about_us",compact('page_title'));
  }

  public function blog(){
    $page_title = "blog";
    $queryData  = DB::table("fp_blogs")
               ->where('status', '<>', 2)
               ->paginate(10);
       
   return view("blog",compact('queryData','page_title'));
 }

  public function blog_details($slug){
    $page_title = "blog";
    $blog   = DB::table("fp_blogs")
               ->where('slug', '=', $slug)
               ->first();
   return view("blog-details",compact('blog','page_title'));
 }
   public function why(){
     $page_title = "why";
    return view("why",compact('page_title'));
  }

 public function howitworks(){
  $page_title = "howitworks";
    return view("howitworks",compact('page_title'));
  }
   public function enterprise(){
    $page_title = "enterprise";
    return view("enterprise",compact('page_title'));
  }
  public function contact_us(Request $request){
    $page_title = "contact";
	if($request->isMethod('post')){
		Session::forget('msg');
	    Session::forget('errormsg');
		$validationOn =  array(
				"name"=>$request->name,
                "contactemail"=>$request->contactemail,
                "subject" => $request->subject,
				"message" => $request->message
            );
			$validateRule=  array(
                "name"=>"required",
                'contactemail' => "required|email",
                'subject' => "required",
				'message' => "required"
            );
			 
           $validator = Validator::make($validationOn,$validateRule);
        if ($validator->fails()){
            return redirect("/contact-us")->withErrors($validator)->withInput();
  
        }
		$query = new Query;
		$query->name=$request->name;
        $query->subject=$request->subject;
        $query->email=$request->contactemail;
        $query->message=$request->message;
		 if($query->save()){
       $query->notify(
                    new ContactusRequest($query)
                            );
			 /*Mail::send('email_templates.thank_contact_us', ['url'=>''], function ($m) use ($query) {
                          $m->to($query->email, 'User')->subject('Thank You for contact us');
            });*/
            $request->session()->flash("msg", "We will contact you soon.");
		}else{
			 $request->session()->flash("errormsg", "Something Wrong.");
		}
		
	 }
      return view("contact_us",compact('page_title'));
  }
  public function faq(){
    $page_title = 'faq';
    return view("faq",compact('page_title'));
  } 
  public function security_service(){
    $page_title = 'security';
    return view("security_service_view",compact('page_title'));
  }
  public function logout(Request $request){
    $request->session()->flush();
    Session::forget('job_req_id');
        Session::forget('price');
        Session::forget('user_id');
        Session::forget('client_id');
        Session::forget('job_id');  
    Session::forget(['email','roleId','role','first_name','last_name']); 
    return redirect("/");
  }
  public function dashboard(){
  	return view("dashboard");
  }
  
  public function forgotpassword(Request $request){
   $token = $request->token;
   $count = DB::table("fp_validcoupon")
         ->where('validtoken', '=', $token)
         ->where('status', '=', '0')
        ->first();
    
  
  	/*return $request;
  	exit();*/
  	return view("newpassword",compact('count'));
  }

  public function verifyEmail(Request $request){
    $token = $request->token;
    $email = base64_decode ($token);

    $count = DB::table("fp_auths")
              ->where('email', '=', $email)
              ->where('status', '=', '0')
              ->first();

     if (!empty ($count)) {
      $match = array('email' => $email, 'status' => 0);
      $isInsert = DB ::table('fp_auths')->updateOrInsert($match,['email_verified_at'=>date('Y-m-d H:i:s')]);
     }

     return view("verify_email",compact('count'));
   }
}