<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Skill;
use App\Models\UserReview;
use App\Models\UserSkill;
use App\Models\UserExperience;
use App\Models\UserEducation;
use App\Models\UserProject;
use App\Models\UserPortfolio;
use App\Models\ContractRequest;
use App\Models\Contract;
use App\Models\Setting;
use App\Models\Chat;
use App\Models\Payout;
use App\Models\ChatDetail;
use App\Models\StripeDetail;
use Hash;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Common_helper;
use Validator;
use Stripe;
// use App\Helpers\Common_helper::class;
use Session;
use DB;

class ContractController extends Controller
{
    
     public function __construct()
    {
		parent::__construct();

    }

    public function contract_list()
    {
		$user_id = session::get('roleId');
		
    $pageTitle = "Contract";
   
    $contractData = Contract::where('fp_contract.userId', '=', $user_id)
    ->select("fp_clients.*","fp_contract.*","fp_contract.categoryId",
    "fp_category.category_name",'fp_contract_request.chatId')
   ->leftjoin('fp_clients', 'fp_clients.id', '=', 'fp_contract.clientId')
   ->leftjoin('fp_contract_request', 'fp_contract.contractId', '=', 'fp_contract_request.contractId')
   ->leftjoin('fp_category', 'fp_category.id', '=', 'fp_contract.categoryId')
   ->leftjoin('fp_countries', 'fp_countries.id', '=', 'fp_clients.country')
   ->leftjoin('fp_cities', 'fp_cities.id', '=', 'fp_clients.city')
   ->leftjoin('fp_states', 'fp_states.id', '=', 'fp_clients.state')
   ->where('IsContractComplete','=',0)
   ->where('paymentId','!=',null)
   ->orderBy('fp_contract.updated_at', 'DESC')
   ->paginate(5); 
   
   $pastContract = Contract::where('fp_contract.userId', '=', $user_id)
   ->select("fp_clients.*","fp_contract.*","fp_contract.categoryId",
   "fp_category.category_name",'fp_contract_request.chatId','fp_client_reviews.rating','fp_client_reviews.review')
  ->leftjoin('fp_clients', 'fp_clients.id', '=', 'fp_contract.clientId')
  ->leftjoin('fp_client_reviews','fp_client_reviews.contractId','=','fp_contract.contractId')
  ->leftjoin('fp_contract_request', 'fp_contract.contractId', '=', 'fp_contract_request.contractId')
  ->leftjoin('fp_category', 'fp_category.id', '=', 'fp_contract.categoryId')
  ->leftjoin('fp_countries', 'fp_countries.id', '=', 'fp_clients.country')
  ->leftjoin('fp_cities', 'fp_cities.id', '=', 'fp_clients.city')
  ->leftjoin('fp_states', 'fp_states.id', '=', 'fp_clients.state')
  ->where('IsContractComplete','=',1)
  ->paginate(5);   

   $onGoing = Contract::where('fp_contract.userId', '=', $user_id)->where('IsContractComplete','=',0)
   ->where('paymentId','!=',null)
      ->count();
     $pendingContract = ContractRequest::where('requestStatus', '=', 1)->where('fp_contract_request.userId', '=', $user_id)->where('contractId','=',null)
     ->count();
    $contract = Contract::where('fp_contract.userId', '=', $user_id)->where('IsContractComplete','=',1)->count();
		
		return view("freelancer_contract_list",compact('contractData','pageTitle','pastContract','onGoing','contract','pendingContract'));
        
    }
    public function contract_detail($contractId= null)
    {
       
		$user_id = session::get('roleId');
		$user = User::with([])
			->where('id', '=', $user_id)
			->first();
        $pageTitle = "Contract Detail";
        $contract_detail = Contract::where('fp_contract.userId', '=', $user_id)
          ->select("fp_clients.*","fp_contract.*","fp_contract.categoryId",
          "fp_category.category_name",'fp_contract_request.chatId','fp_release_payment.relId','fp_release_payment.status as payStatus')
          ->leftjoin('fp_clients', 'fp_clients.id', '=', 'fp_contract.clientId')
          ->leftjoin('fp_contract_request', 'fp_contract.contractId', '=', 'fp_contract_request.contractId')
          ->leftjoin('fp_category', 'fp_category.id', '=', 'fp_contract.categoryId')
          ->leftjoin('fp_countries', 'fp_countries.id', '=', 'fp_clients.country')
          ->leftjoin('fp_release_payment', 'fp_release_payment.contractId', '=', 'fp_contract.contractId')
          ->leftjoin('fp_cities', 'fp_cities.id', '=', 'fp_clients.city')
          ->leftjoin('fp_states', 'fp_states.id', '=', 'fp_clients.state')
          ->where('fp_contract.contractId','=',$contractId)
          ->first();
          $detail = StripeDetail::where('userId', '=', $user_id)->first();
          $common_lib = new Common_helper();
          $totalRating = $common_lib->getTotalClientrating($contract_detail->clientId);
		return view("freelancer_contract_detail",compact('contract_detail','pageTitle','user','totalRating','detail'));
        
    }

    public function payment_request(Request $request){
      $setting  = Setting::first();
    $id = $request->input('code');
      $stripe = Stripe\Stripe::setApiKey($setting->secreteKey);
      $user_id = session::get('roleId');
      $user = User::with([])
        ->where('id', '=', $user_id)
        ->first();
        
      $detail = StripeDetail::where('userId', '=', $user_id)->first();
      if(isset($detail->stripeAccId) && !empty($detail->stripeAccId)){
        
          $strpeAcId = $detail->stripeAccId;
      }else{
        try {
            $accData = Stripe\Account::create([
              "country" => "CH",
              "type" => "express",
              "email" => $user->email,
              'requested_capabilities' => [
                'transfers','card_payments',
              ],
            ]);
     
     } catch (Exception $e) {
      $request->session()->flash("errormsg", $e->getMessage());
      }
     
      if (isset($accData->id) && !empty($accData->id)) {
        $updateData['stripeAccId'] = $accData->id;
        $insertStripeData = array();
        $insertStripeData = new StripeDetail;
					$insertStripeData->userId = $user_id;
					$insertStripeData->stripeAccId = $accData->id;
					$insertStripeData->payout_schedule_delay_days = (isset($accData->payout_schedule->delay_days) && !empty($accData->payout_schedule->delay_days))?$accData->payout_schedule->delay_days:'';
					$insertStripeData->payout_schedule_interval= (isset($accData->payout_schedule->interval) && !empty($accData->payout_schedule->interval))?$accData->payout_schedule->interval:'';
					$insertStripeData->tos_acceptance_date = isset($accData->tos_acceptance->date)?$accData->tos_acceptance->date:'';
					$insertStripeData->tos_acceptance_ip = isset($accData->tos_acceptance->ip)?$accData->tos_acceptance->ip:'';
          $insertStripeData->created_at = date('Y-m-d H:i:s');
          $insertStripeData->updated_at = date('Y-m-d H:i:s');
          $insertStripeData->save();
          $strpeAcId = $accData->id;

      }
    }
    try{
      $account = \Stripe\Account::retrieve($strpeAcId);
     }catch(Exception $e) {
      echo  $e->getMessage();
     
    }

   if(!empty($account)){
          $updateData = array(
            "support_phone"								=>$account->support_phone,
            "business_name"								=>$account->business_name,
            "business_url"								=>$account->business_url,
            "email"										=>$account->email,
            "display_name"								=>$account->display_name,
            "legal_entity_address_line1"				=>isset($account->legal_entity->address->line1)?$account->legal_entity->address->line1:'',
            "legal_entity_address_line2"				=>isset($account->legal_entity->address->line2)?$account->legal_entity->address->line2:'',
            "legal_entity_address_postal_code"			=>isset($account->legal_entity->address->postal_code)?$account->legal_entity->address->postal_code:'',
            "legal_entity_address_state"				=>isset($account->legal_entity->address->state)?$account->legal_entity->address->state:'',
            "legal_entity_address_city"					=>isset($account->legal_entity->address->city)?$account->legal_entity->address->city:'',
            "legal_entity_address_country"				=>isset($account->legal_entity->address->country)?$account->legal_entity->address->country:'',
            "legal_entity_business_name"				=>isset($account->legal_entity->business_name)?$account->legal_entity->business_name:'',
            //"legal_entity_business_tax_id"				=>$_POST['legal_entity_business_tax_id'],
            "legal_entity_dob_day"						=>isset($account->legal_entity->dob->day)?$account->legal_entity->dob->day:'',
            "legal_entity_dob_month"					=>isset($account->legal_entity->dob->month)?$account->legal_entity->dob->month:'',
            "legal_entity_dob_year"						=>isset($account->legal_entity->dob->year)?$account->legal_entity->dob->year:'',
            "legal_entity_first_name"					=>isset($account->legal_entity->first_name)?$account->legal_entity->first_name:'',
            "legal_entity_last_name"					=>isset($account->legal_entity->last_name)?$account->legal_entity->last_name:'',
            "legal_entity_personal_address_city"		=>isset($account->legal_entity->personal_address->city)?$account->legal_entity->personal_address->city:'',
            "legal_entity_personal_address_state"		=>isset($account->legal_entity->personal_address->state)?$account->legal_entity->personal_address->state:'',
            "legal_entity_personal_address_country"		=>isset($account->legal_entity->personal_address->country)?$account->legal_entity->personal_address->country:'',
            "legal_entity_personal_address_line1"		=>isset($account->legal_entity->personal_address->line1)?$account->legal_entity->personal_address->line1:'',
            "legal_entity_personal_address_line2"		=>isset($account->legal_entity->personal_address->line2)?$account->legal_entity->personal_address->line2:'',
            "legal_entity_personal_address_postal_code"	=>isset($account->legal_entity->personal_address->postal_code)?$account->legal_entity->personal_address->postal_code:'',
            "legal_entity_personal_id_number_provided"	=>(isset($account->legal_entity->personal_id_number_provided) && $account->legal_entity->personal_id_number_provided)?'1':'0',
            "legal_entity_ssn_last_4_provided"			=>(isset($account->legal_entity->ssn_last_4_provided) && $account->legal_entity->ssn_last_4_provided)?'1':'0',
            "legal_entity_type"							=>isset($account->legal_entity->type)?$account->legal_entity->type:'',
            "legal_entity_verification_document"		=>isset($account->legal_entity->verification->document)?$account->legal_entity->verification->document:'',
            "legal_entity_verification_document2"		=>isset($account->legal_entity->verification->document_back)?$account->legal_entity->verification->document_back:'',
            "legal_entity_verification_status"			=>isset($account->legal_entity->verification->status)?$account->legal_entity->verification->status:'',
            "payouts_enabled"							=>($account->payouts_enabled)?'1':'0',
            "statement_descriptor"						=>$account->statement_descriptor,
            "support_email"								=>$account->support_email,
            "support_phone"								=>$account->support_phone,
            "support_url"								=>$account->support_url,
            "country"									=>$account->country,
            "payout_bank_stripe_id"						=>(isset($account->external_accounts->data[0]['id']) && !empty($account->external_accounts->data[0]['id']) ? $account->external_accounts->data[0]['id']:''),
            "payout_account_holder_name"				=>(isset($account->external_accounts->data[0]['account_holder_name']) && !empty($account->external_accounts->data[0]['account_holder_name'])?$account->external_accounts->data[0]['account_holder_name']:''),
            "payout_account_holder_type"				=>(isset($account->external_accounts->data[0]['account_holder_type']) && !empty($account->external_accounts->data[0]['account_holder_type']) ? $account->external_accounts->data[0]['account_holder_type']:''),
            "payout_bank_name"							=>(isset($account->external_accounts->data[0]['bank_name']) && !empty($account->external_accounts->data[0]['bank_name']) ? $account->external_accounts->data[0]['bank_name']:''),
            "payout_country"							=>(isset($account->external_accounts->data[0]['country']) && !empty($account->external_accounts->data[0]['country']) ? $account->external_accounts->data[0]['country']:''),
            "payout_currency"							=>(isset($account->external_accounts->data[0]['currency']) && !empty($account->external_accounts->data[0]['currency']) ? $account->external_accounts->data[0]['currency']:''),
            "payout_routing_no"							=>(isset($account->external_accounts->data[0]['routing_number']) && !empty($account->external_accounts->data[0]['routing_number']) ? $account->external_accounts->data[0]['routing_number']:''),
            "payout_acc_no"								=>(isset($account->external_accounts->data[0]['last4']) && !empty($account->external_accounts->data[0]['last4']) ? $account->external_accounts->data[0]['last4']:''),
            "tos_acceptance_date" => (isset($account->tos_acceptance->date) && !empty($account->tos_acceptance->date)?$account->tos_acceptance->date:''),
            "tos_acceptance_ip" => (isset($account->tos_acceptance->ip) && !empty($account->tos_acceptance->ip)?$account->tos_acceptance->ip:'')
            //"type"										=>$account->type``
    );

    DB::table('fp_stripe_details')->where('userId', $user_id)->update($updateData);
  }
    try{
      $account_links = \Stripe\AccountLink::create([
        'account' => $strpeAcId,
        'failure_url' => url()->current(),
        'success_url' => url()->current(),
        'type' => 'account_onboarding',
        'collect' => 'currently_due',
      ]);
  }catch(Exception $e) {
     echo  $e->getMessage();
     
  }
      $pastWork = Contract::where('fp_contract.userId', '=', $user_id)
      ->select("fp_clients.*","fp_contract.*","fp_contract.categoryId",
      "fp_category.category_name",'fp_contract_request.chatId',
     "fp_contract_request.userId","fp_release_payment.*")
      ->leftjoin('fp_clients', 'fp_clients.id', '=', 'fp_contract.clientId')
      ->leftjoin('fp_category', 'fp_category.id', '=', 'fp_contract.categoryId')
      ->leftjoin('fp_contract_request', 'fp_contract.contractId', '=', 'fp_contract_request.contractId')
      ->join('fp_release_payment', 'fp_release_payment.contractId', '=', 'fp_contract.contractId')
      ->where('fp_release_payment.status','=',1)
      ->paginate(5);
      
      $escrowAmount = Contract::where('fp_contract.userId', '=', $user_id)
      ->where('IsContractComplete','=',1)
      ->whereNotIn('contractId', DB::table('fp_payout')->where('userId', '=', $user_id)->pluck('contractId'))
      ->sum('freelancerPrice');
     
      $closedContract = Contract::where('fp_contract.userId', '=', $user_id)
                      ->where('IsContractComplete','=',1)
                      ->whereNotIn('contractId', DB::table('fp_release_payment')->where('userId', '=', $user_id)->pluck('contractId'))
                      ->get();

      $year = date('Y');
      $month = date('m');
      $yearlyIncome = Payout::whereYear('created_at', $year)
                      ->where('userId', '=', $user_id)
                      ->sum('amount');
      $totalIncome = Payout::where('userId', '=', $user_id)
                      ->sum('amount');
      $monthlyIncome = Payout::whereMonth('created_at', $month)
                      ->where('userId', '=', $user_id)
                      ->sum('amount');
      $todayIncome = Payout::whereRaw('Date(created_at) = CURDATE()')
                      ->where('userId', '=', $user_id)
                      ->sum('amount');
      
                    
      $Stripedetail = StripeDetail::where('userId', '=', $user_id)->first();
      
      return view("freelancer_payment",compact('closedContract','account_links','escrowAmount','pastWork','totalIncome','Stripedetail','yearlyIncome','monthlyIncome','todayIncome'));

    }
    
     public function delete_portfolio(Request $request){
              if ($request->ajax()) {
            $id= $request->image_id;
              $queryData = UserPortfolio::find($id);

        if (isset($queryData->id)) {
          
            $queryData->delete();

            echo 1;
        } else {
             echo 0;
        }

        die();

        }
      }
      public function delete_experience(Request $request){
              if ($request->ajax()) {
            $id= $request->experience_id;

              $queryData = UserExperience::find($id);

        if (isset($queryData->id)) {
          
            $queryData->delete();

            echo 1;
        } else {
             echo 0;
        }

        die();

        }
      }
      public function delete_project(Request $request){
              if ($request->ajax()) {
            $id= $request->project_id;

              $queryData = UserProject::find($id);

        if (isset($queryData->id)) {
          
            $queryData->delete();

            echo 1;
        } else {
             echo 0;
        }

        die();

        }
      }

      public function delete_education(Request $request){
              if ($request->ajax()) {
            $id= $request->education_id;
              $queryData = UserEducation::find($id);

        if (isset($queryData->id)) {
          
            $queryData->delete();

            echo 1;
        } else {
             echo 0;
        }

        die();

        }
      }
      public function delete_video(Request $request){
              if ($request->ajax()) {
            $id= $request->video_id;
           
              $queryData = User::find($id);

        if (isset($queryData->id)) {
          
             $queryData->video = null;
              $queryData->save();

            echo 1;
        } else {
             echo 0;
        }

        die();

        }
      }
    
}
