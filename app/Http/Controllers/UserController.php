<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Skill;
use App\Models\UserReview;
use App\Models\UserSkill;
use App\Models\UserExperience;
use App\Models\UserEducation;
use App\Models\UserProject;
use App\Models\UserPortfolio;
use App\Models\ContractRequest;
use App\Models\Contract;
use App\Models\Chat;
use App\Models\ChatDetail;
use App\Models\ClientReview;
use Hash;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Common_helper;
use Validator;

// use App\Helpers\Common_helper::class;
use Session;
use DB;

class UserController extends Controller
{
    
     public function __construct()
    {
		parent::__construct();

    }
public function index(){
    $pageTitle = "Dashboard";
    $common_lib = new Common_helper();
    $user_id = session::get('roleId');
    $user = User::with([])
       ->where('id', '=', $user_id)
       ->first();
       $requestData = ContractRequest::where('fp_contract_request.userId', '=', $user_id)
       ->select("fp_clients.*","fp_contract_request.reqId","fp_contract_request.categoryId","fp_contract.paymentId",
       "fp_category.category_name","fp_contract_request.message","fp_contract_request.requestStatus","fp_contract_request.chatId")
      ->leftjoin('fp_clients', 'fp_clients.id', '=', 'fp_contract_request.clientId')
      ->leftjoin('fp_category', 'fp_category.id', '=', 'fp_contract_request.categoryId')
      ->leftjoin('fp_countries', 'fp_countries.id', '=', 'fp_clients.country')
      ->leftjoin('fp_cities', 'fp_cities.id', '=', 'fp_clients.city')
      ->leftjoin('fp_states', 'fp_states.id', '=', 'fp_clients.state')
      ->leftjoin('fp_contract', 'fp_contract.contractId', '=', 'fp_contract_request.contractId')
      ->where('fp_contract_request.contractId','=',null)
      ->orWhere('fp_contract.paymentId','=',null)
      ->where('requestStatus','!=',2)
      ->paginate(2);   

       $contractData = Contract::where('fp_contract.userId', '=', $user_id)
       ->select("fp_clients.*","fp_contract.*","fp_contract.categoryId",
       "fp_category.category_name",'fp_contract_request.chatId')
      ->leftjoin('fp_clients', 'fp_clients.id', '=', 'fp_contract.clientId')
      ->leftjoin('fp_category', 'fp_category.id', '=', 'fp_contract.categoryId')
      ->leftjoin('fp_countries', 'fp_countries.id', '=', 'fp_clients.country')
      ->leftjoin('fp_contract_request', 'fp_contract.contractId', '=', 'fp_contract_request.contractId')
      ->leftjoin('fp_cities', 'fp_cities.id', '=', 'fp_clients.city')
      ->leftjoin('fp_states', 'fp_states.id', '=', 'fp_clients.state')
      ->where('IsContractComplete','=',0)
      ->where('paymentId','!=',null)
      ->orderBy('fp_contract.updated_at', 'DESC')
      ->first();
      

       $pastContract = Contract::where('fp_contract.userId', '=', $user_id)
       ->select("fp_clients.*","fp_contract.*","fp_contract.categoryId",
       "fp_category.category_name",'fp_contract_request.chatId','fp_client_reviews.rating','fp_client_reviews.review')
      ->leftjoin('fp_clients', 'fp_clients.id', '=', 'fp_contract.clientId')
      ->leftjoin('fp_client_reviews','fp_client_reviews.contractId','=','fp_contract.contractId')
      ->leftjoin('fp_contract_request', 'fp_contract.contractId', '=', 'fp_contract_request.contractId')
      ->leftjoin('fp_category', 'fp_category.id', '=', 'fp_contract.categoryId')
      ->leftjoin('fp_countries', 'fp_countries.id', '=', 'fp_clients.country')
      ->leftjoin('fp_cities', 'fp_cities.id', '=', 'fp_clients.city')
      ->leftjoin('fp_states', 'fp_states.id', '=', 'fp_clients.state')
      ->where('IsContractComplete','=',1)
      ->paginate(5);   

      $totalIncome  = 0;
      $onGoing = Contract::where('fp_contract.userId', '=', $user_id)->where('IsContractComplete','=',0)
      ->count();
  //print_r($onGoing); exit();
    return view("dashboard",compact('requestData','pastContract','totalIncome','onGoing','contractData','user'));
  }

  public function chat($chatId)
    {
    $user_id = session::get('roleId');
    $chat = ContractRequest::where('chatId', '=', $chatId)
                  ->select("fp_clients.*","fp_contract_request.reqId","fp_contract_request.categoryId",
                  "fp_contract_request.contractId","fp_contract_request.chatId",
                  "fp_category.category_name",'fp_cities.name as city','fp_countries.name as country','fp_contract_request.clientId',
                  'fp_states.name as state','fp_contract_request.clientId')
                 ->leftjoin('fp_clients', 'fp_clients.id', '=', 'fp_contract_request.clientId')
                 ->leftjoin('fp_category', 'fp_category.id', '=', 'fp_contract_request.categoryId')
                 ->leftjoin('fp_countries', 'fp_countries.id', '=', 'fp_clients.country')
                 ->leftjoin('fp_cities', 'fp_cities.id', '=', 'fp_clients.city')
                 ->leftjoin('fp_states', 'fp_states.id', '=', 'fp_clients.state')
                 ->where('userId', '=', session::get('roleId'))
                 ->orderBy("fp_contract_request.reqId","desc")
                  ->first();
                  $common_lib = new Common_helper();
                  $totalRating = $common_lib->getTotalClientrating($chat->clientId);
                  $reviews = ClientReview::with([])
                  ->select('fp_client_reviews.*',"fp_users.*")
                  ->leftjoin("fp_users",'fp_users.id', '=', 'fp_client_reviews.user_id')
                  ->where('fp_client_reviews.client_id', '=', $chat->clientId)
                  ->paginate(2);
                  $reviewCount = ClientReview::where("client_id",$chat->clientId)->count();
    if(!empty($chat)){
    $pageTitle = "Chat Detail";
    $lastId = DB::table('fp_chat_detail')
                 ->select('fp_chat_detail.*')
                 ->where('fp_chat_detail.chatId', '=', $chatId)
                 ->where('fp_chat_detail.status', '=', 0)
                 ->orderBy('fp_chat_detail.id', 'DESC')
                 ->first();
        $chatDetail = DB::table('fp_chat_detail')
                 ->select('fp_chat_detail.*')
                 ->where('fp_chat_detail.chatId', '=', $chatId)
                 ->where('fp_chat_detail.status', '=', 0)
                 ->orderBy('fp_chat_detail.created_at', 'ASC')
                 ->get();
                 $hired_freelancer  = Contract::where('fp_contract.clientId', '=', $chat->clientId)
                 ->count(DB::raw('DISTINCT userId'));
                 $ongingWork  = Contract::where('fp_contract.clientId', '=', $chat->clientId)->where('IsContractComplete','=',0)
                 ->count();
                 $contract  = Contract::where('fp_contract.clientId', '=', $chat->clientId)
                 ->count();
    return view("user_chat",compact('chatDetail','chat','totalRating','reviews','reviewCount','lastId','contract','ongingWork','hired_freelancer'));
    }else{
      return redirect("/user-dashboard");
    }
  }

  public function profile(){
    $pageTitle = "Profile";
    $user_id = session::get('roleId');
    $common_lib = new Common_helper();
    $user = User::with(['country_data'])
        ->where('id', '=', $user_id)
        ->first();
    $userskill = UserSkill::with(['skill_data'])
       ->where('user_id', '=', $user_id)
       ->get();
    $UserExperience = UserExperience::with([])
       ->where('user_id', '=', $user_id)
       ->where('user_role', '=','user')
       ->get();
    $userportfolio = UserPortfolio::with([])
       ->where('user_id', '=', $user_id)
       ->where('user_role', '=','user')
       ->get();
    $UserProject = UserProject::with([])
       ->where('user_id', '=', $user_id)
       ->where('user_role', '=','user')
       ->get();
     $UserEducation = UserEducation::with([])
       ->where('user_id', '=', $user_id)
       ->where('user_role', '=','user')
       ->get();
      $totalRating = $common_lib->getTotalRating($user_id);
       

        $reviews = UserReview::where('fp_user_reviews.user_id', '=', $user_id)
       ->select("fp_clients.*","fp_user_reviews.*")
      ->leftjoin('fp_clients', 'fp_clients.id', '=', 'fp_user_reviews.client_id')
       ->get();
       $contract = Contract::where('fp_contract.userId', '=', $user_id)->where('IsContractComplete','=',1)
       ->count();
       $clients = Contract::where('fp_contract.userId', '=', $user_id)
       ->count(DB::raw('DISTINCT clientId'));
    return view("user_profile",compact('pageTitle','contract','clients','user','userskill','UserExperience','userportfolio','UserProject','UserEducation','totalRating','reviews'));
  }
public function save_user_data(Request $request){

   //echo "<pre>"; print_r($request->response_time); exit();
        $user_id = session::get('roleId');
		Session::forget('msg');
	    Session::forget('errormsg');
        $validationOn =  array(
                "first_name"=>$request->first_name,
                "last_name"=>$request->last_name,
                "phone"=>$request->phone,
                "email"=>$request->email,
                "about"=>$request->about,
                "address_line_1"=>$request->address_line_1,
                "city"=>$request->city,
                "pincode"=>$request->pincode,
                "state"=>$request->state,
                "country"=>$request->country,
                "hourly_rate"=>$request->hourly_rate
            );
     
               $validateRule=  array(
                "first_name"=>"required",
                "last_name"=>"required",
                "phone"=>"required|numeric|min:8",
               "email"=>"required|email",
                "about"=>"required", 
                "address_line_1"=>"required",
                "city"=>"required",
                "pincode"=>"required|numeric|min:4",
                "state"=>"required",
                "country"=>"required",
               "hourly_rate"=>"required",
               
                
            );
            
            
        $validator = Validator::make($validationOn,$validateRule);
        
        
        if ($validator->fails()){
           
          return redirect("/user-dashboard/edit-profile")->withErrors($validator)->withInput();
            
        }
        $isExist = DB::select("SELECT * FROM fp_users WHERE status != 2 AND id != '".$user_id."' AND email = '".$request->email."' ",[1]);
        /*$isExist1 = DB::select("SELECT * FROM fp_clients WHERE status != 2 AND  email = '".$request->email."' ",[1]);*/
        if($isExist){
            $request->session()->flash("errormsg", "Email already regisered.");
            return redirect("/user-dashboard/edit-profile");
        }
        /*if($isExist1){
            $request->session()->flash("errormsg", "Email already regisered.");
            return redirect("/user-dashboard/edit-profile");
        }*/
        if ($request->hasFile('video')) {
            $fileArray = array('video' => $request->video);
            $validateRule= 'mimes:jpeg,jpg,png,gif|required|max:10000';
           $rules = array(
                   "video"=>"mimes:mp4,mov,ogg,qt | max:20000",
    );
            $validator = Validator::make($fileArray, $rules);
              if ($validator->fails()){
              return redirect("/user-dashboard/edit-profile")->withErrors($validator)->withInput();
    } else
               $video = $request->file('video');
               $new_video  = time().rand().'.'.$video->getClientOriginalExtension();
            $video->move(public_path('uploads/video/user_video'), $new_video);
          }

        $user = User::find($user_id);
        $common_lib = new Common_helper();
        $slug = $common_lib->createSlug($request->first_name,$user_id,'fp_users');
       
        $user->first_name=$request->first_name;
        $user->last_name=$request->last_name;
        $user->phone=$request->phone;
        $user->email=$request->email;
        $user->category_id=$request->category_id;
        $user->response_time=$request->response_time;
        $user->about=$request->about;
        $user->address_line_1=$request->address_line_1;
        $user->city=$request->city;
        $user->state=$request->state;
        $user->country=$request->country;
        $user->pincode=$request->pincode;
        $user->hourly_rate=$request->hourly_rate;
        if (!empty($new_video)) {
         $user->video=$new_video;
        }
       
        $user->slug=$slug;
        $user->save();
        $user->skills()->sync($request->skills);
        if ($request->hasFile('simage_1')) {
           $fileArray = array('simage_1' => $request->simage_1);
            $validateRule= 'mimes:jpeg,jpg,png,gif|required|max:10000';
           $rules = array(
      'simage_1' => 'mimes:jpeg,jpg,png,gif|required|max:10000' // max 10000kb
    );
            $validator = Validator::make($fileArray, $rules);
              if ($validator->fails()){
          
              return redirect("client/edit-profile")->withErrors($validator)->withInput();
    } else

          $countp= $request->count;
       //print_r($countp); exit();
 
   if ($countp > 0 ) {
        foreach($countp as $key => $i) {
          
            $files =   $request->file('simage_'.$i);

        if (!empty($files)) {
                //echo "<pre>";  print_r($image); exit();
          /* $ImageUpload = Image::make($files);
             // for save thumnail image
          $thumbnailPath = 'uploads/user_portfolio/';
          $ImageUpload->resize(150,150);
         $ImageUpload = $ImageUpload->save($thumbnailPath.time().$files->getClientOriginalName());*/
            $new_name  = time().rand().'.'.$files->getClientOriginalExtension();
            $files->move(public_path('uploads/user_portfolio'), $new_name);
            $file = new UserPortfolio;
               /* $file->filename = time().$files->getClientOriginalName();*/
                $file->filename =  $new_name;
                $file->user_id = $user_id;
                $file->user_role = 'user';
                $file->save();
             }
           }   
          }
        }
         if (!empty($request->experience_1)) {
          $expcount= $request->expcount;
       if ($expcount > 0 ) {
        foreach($expcount as $key => $i) {
            $experience =   $request->input('experience_'.$i);
            $experience_year =   $request->input('experience_year_'.$i);
        if (!empty($experience) || !empty($experience_year) ) {
     $data = new UserExperience;
                $data->experience = $experience;
                $data->experience_year = $experience_year;
                $data->user_id = $user_id;
                $data->user_role = 'user';
                $data->save();
              }
            }   
          }
        }
        if (!empty($request->project_title_1)) {
         $procount= $request->procount;
   if ($procount > 0 ) {
        foreach($procount as $key => $i) {
            $project_title =   $request->input('project_title_'.$i);
            $project_dis =   $request->input('project_dis_'.$i);
        if (!empty($project_title) || !empty($project_dis) ) {
                $data = new UserProject;
                $data->project_title = $project_title;
                $data->project_dis = $project_dis;
                $data->user_id = $user_id;
                $data->user_role = 'user';
                $data->save();
              }
            }   
          }
        }
          if (!empty($request->course_1)) {
          $educount= $request->educount;
       if ($educount > 0 ) {
        foreach($educount as $key => $i) {
            $course =   $request->input('course_'.$i);
            $university =   $request->input('university_'.$i);
        if (!empty($course) || !empty($university) ) {
                $data = new UserEducation;
                $data->course = $course;
                $data->university = $university;
                $data->user_id = $user_id;
                $data->user_role = 'user';
                $data->save();
                 }
               }   
             }
           }

		 if($user->save()){
             
        $request->session()->flash("msg", "profile updated successfully.");
         return redirect("user-dashboard/edit-profile");
		}else{
			 $request->session()->flash("errormsg", "profile not updated.");
              return redirect("user-dashboard/edit-profile");
		}
       }
  
  public function edit_profile(){
    $pageTitle = "Edit Profile";
    $user_id = session::get('roleId');
    $user = User::with([])
       ->where('id', '=', $user_id)
       ->first();
       $state_list =  DB::table('fp_states')
       ->where('country_id', '=', $user->country)     
       ->pluck('name', 'id');
       $city_list =  DB::table('fp_cities')
       ->where('state_id', '=', $user->state)     
       ->pluck('name', 'id');
       $userskill = UserSkill::with([])
       ->where('user_id', '=', $user_id)
       ->get();

       $userportfolio = UserPortfolio::with([])
       ->where('user_id', '=', $user_id)
        ->where('user_role', '=','user')
       ->get();
        $UserExperience = UserExperience::with([])
       ->where('user_id', '=', $user_id)
        ->where('user_role', '=','user')
       ->get();
        $UserEducation = UserEducation::with([])
       ->where('user_id', '=', $user_id)
        ->where('user_role', '=','user')
       ->get();
       $UserProject = UserProject::with([])
       ->where('user_id', '=', $user_id)
        ->where('user_role', '=','user')
       ->get();
       $skill_data = DB::table('fp_skills')->get();
       $category_data = DB::table('fp_category')->get();
    return view("edit_profile",compact('user','pageTitle','state_list','city_list','skill_data','userportfolio','UserExperience','UserProject','UserEducation','userskill','category_data'));
  }


  public function change_password(Request $request){
	  
    $pageTitle = "Change Password";
	$user_id = session::get('roleId');
	$user = User::with([])
       ->where('id', '=', $user_id)
       ->first();
	if ($request->isMethod('post')){
        
		Session::forget('msg');
	    Session::forget('errormsg');
        
        $validationOn =  array(
            "old_password"=>$request->old_password,
            "new_password"=>$request->new_password,
            "confirm_password"=>$request->confirm_password,
        );

        $validateRule=  array(
            "old_password"=>"required",
            'new_password' => 'min:6|required_with:confirm_password|same:confirm_password',
            'confirm_password' => 'min:6',
        );
			 
        $validator = Validator::make($validationOn,$validateRule);

        if ($validator->fails()){
            return redirect("/user-dashboard/change-password")->withErrors($validator)->withInput();  
        }
        
        $isExist = DB::select("SELECT * FROM fp_auths WHERE status != 2 AND (role = 'user') AND role_id = '".$user_id."' limit 0, 1");
               
		if (!empty($isExist)) {

			if(Hash::check($request->old_password,$isExist[0]->password)) {
                DB::table('fp_auths') 
                ->where('role_id', $user_id)
                ->limit(1) 
                ->update(['password'=>bcrypt($request->new_password), 'updated_at'=>date('Y-m-d H:i:s')]);

				$request->session()->flash("msg", "Password updated successfully.");
			} else {			
				$request->session()->flash("errormsg", "invalid old password.");
			}
		} else {
			$request->session()->flash("errormsg", "invalid user.");
		}
		
    }		
	return view("change_password",compact('user','pageTitle'));
  }
   public function ajaxImage(Request $request)
    {
        if ($request->isMethod('post')){
           $validationOn =  array(
                "images"=>$request->file
            );
           $validateRule=  array(
                "images"=>"required|image|mimes:jpeg,png,jpg,gif,svg|max:2048"
            );
            $validator = Validator::make($validationOn,$validateRule);
           
            if ($validator->fails())
                return array(
                    'fail' => true,
                    'errors' => $validator->errors()
            );
            $user_id = session::get('roleId');
            $user = User::find($user_id);
            $file = $request->file('file');
            $new_name  = time().rand().'.'.$file->getClientOriginalExtension();
            $file->move(public_path('uploads/user_images'), $new_name);
            $user->image = $new_name;
            $user->save();
            return $new_name;
        }
    }
    public function request_listing()
    {
		$user_id = session::get('roleId');
		$user = User::with(['category_detail'])
       ->where('id', '=', $user_id)
       ->first();
		$pageTitle = "Job Request";
        $jobs = DB::table('fp_jobs')
                 ->select('fp_jobs.*', 'fp_cities.name as city','fp_countries.name as country',
                 'fp_states.name as state','fp_job_requests.*','fp_job_requests.status as job_request_status',
                 'fp_job_requests.id as job_req_id','fp_clients.first_name','fp_clients.last_name')
                 ->join('fp_job_requests', 'fp_job_requests.job_id', '=', 'fp_jobs.id')
                 ->leftjoin('fp_cities', 'fp_cities.id', '=', 'fp_jobs.city_id')
                 ->leftjoin('fp_countries', 'fp_countries.id', '=', 'fp_jobs.country_id')
                 ->leftjoin('fp_clients', 'fp_job_requests.client_id', '=', 'fp_clients.id')
                 ->leftjoin('fp_states', 'fp_states.id', '=', 'fp_jobs.state_id')
                 ->where('fp_job_requests.user_id', '=', $user_id)
                 ->where('fp_job_requests.is_payment', '=', 0)
                 ->where('fp_jobs.status', '!=', 3)
                 ->where('fp_job_requests.status', '=', 0)
                 ->orderBy('fp_job_requests.created_at', 'DESC')
                 ->paginate(5);    
             
		
		return view("freelancer_job_request",compact('jobs','pageTitle','user'));
        
    }
    public function job_detail($id= null)
    {
		$user_id = session::get('roleId');
		$user = User::with([])
			->where('id', '=', $user_id)
			->first();
        $pageTitle = "Job Detail";
        
        $job_detail = DB::table('fp_jobs')
        ->select('fp_jobs.*', 'fp_cities.name as city','fp_countries.name as country',
        'fp_states.name as state','fp_job_requests.*','fp_job_requests.status as job_request_status',
        'fp_job_requests.id as job_req_id','fp_category.category_name','fp_clients.first_name','fp_clients.last_name','fp_payment.token','fp_payment.created_at as payment_date'
        ,'fp_jobs.created_at as job_created_date')
                 ->leftjoin('fp_job_requests', 'fp_job_requests.job_id', '=', 'fp_jobs.id')
                 ->leftjoin('fp_clients', 'fp_job_requests.client_id', '=', 'fp_clients.id')
                 ->leftjoin('fp_payment', 'fp_payment.job_req_id', '=', 'fp_job_requests.id')
                 ->leftjoin('fp_cities', 'fp_cities.id', '=', 'fp_jobs.city_id')
                 ->leftjoin('fp_countries', 'fp_countries.id', '=', 'fp_jobs.country_id')
                 ->leftjoin('fp_category', 'fp_category.id', '=', 'fp_jobs.category_id')
                 ->leftjoin('fp_states', 'fp_states.id', '=', 'fp_jobs.state_id')
                 ->where('fp_job_requests.user_id', '=', $user_id)
                 ->where('fp_jobs.id', '=', $id)
                 ->first();
             
		
		return view("user_job_detail",compact('job_detail','pageTitle','user'));
        
    }

    public function open_job_detail($id= null)
    {
		$user_id = session::get('roleId');
		$user = User::with([])
			->where('id', '=', $user_id)
			->first();
        $pageTitle = "Job Detail";
        
        $job_detail = DB::table('fp_jobs')
        ->select('fp_jobs.*', 'fp_cities.name as city','fp_countries.name as country',
        'fp_states.name as state','fp_job_requests.*','fp_job_requests.status as job_request_status',
        'fp_job_requests.id as job_req_id','fp_category.category_name','fp_clients.first_name','fp_clients.last_name','fp_payment.token','fp_payment.created_at as payment_date'
        ,'fp_jobs.created_at as job_created_date')
                 ->leftjoin('fp_job_requests', 'fp_job_requests.job_id', '=', 'fp_jobs.id')
                 ->leftjoin('fp_clients', 'fp_job_requests.client_id', '=', 'fp_clients.id')
                 ->leftjoin('fp_payment', 'fp_payment.job_req_id', '=', 'fp_job_requests.id')
                 ->leftjoin('fp_cities', 'fp_cities.id', '=', 'fp_jobs.city_id')
                 ->leftjoin('fp_countries', 'fp_countries.id', '=', 'fp_jobs.country_id')
                 ->leftjoin('fp_category', 'fp_category.id', '=', 'fp_jobs.category_id')
                 ->leftjoin('fp_states', 'fp_states.id', '=', 'fp_jobs.state_id')
                 ->where('fp_jobs.id', '=', $id)
                 ->first();
             
		
		return view("open_job_detail",compact('job_detail','pageTitle','user'));
        
    }
    public function user_task()
    {
		$user_id = session::get('roleId');
		
		$user = User::with([])
			->where('id', '=', $user_id)
			->first();
		$pageTitle = "Current Task";
     
        
        $jobs = DB::table('fp_user_hire')
        ->select('fp_jobs.*', 'fp_cities.name as city','fp_countries.name as country',
        'fp_states.name as state','fp_job_requests.*','fp_job_requests.status as job_request_status',
        'fp_job_requests.id as job_req_id','fp_clients.first_name','fp_clients.last_name')
        ->join('fp_job_requests', 'fp_user_hire.job_req_id', '=', 'fp_job_requests.id')
        ->join('fp_jobs', 'fp_job_requests.job_id', '=', 'fp_jobs.id')
        ->leftjoin('fp_cities', 'fp_cities.id', '=', 'fp_jobs.city_id')
        ->leftjoin('fp_countries', 'fp_countries.id', '=', 'fp_jobs.country_id')
        ->leftjoin('fp_states', 'fp_states.id', '=', 'fp_jobs.state_id')
        ->leftjoin('fp_clients', 'fp_job_requests.client_id', '=', 'fp_clients.id')
        ->where('fp_user_hire.user_id', '=', $user_id)
        ->where('fp_jobs.status', '!=', 3)
        ->where('valid_upto', '>=', date('Y-m-d'))
        ->where('fp_user_hire.status', '=', 0)
        ->orderBy('fp_user_hire.created_at', 'DESC');
         $jobs = $jobs->paginate(5);
		 return view("user_current_task",compact('jobs','pageTitle','user'));
        
    }
    public function history()
    {
        
		$user_id = session::get('roleId');
		$user = User::with([])
			->where('id', '=', $user_id)
			->first();
		$pageTitle = "History";
        $jobs = DB::table('fp_user_hire')
                 ->select('fp_jobs.*', 'fp_cities.name as city','fp_countries.name as country',
                 'fp_states.name as state','fp_job_requests.*','fp_job_requests.status as job_request_status',
                 'fp_job_requests.id as job_req_id','fp_clients.first_name','fp_clients.last_name','fp_user_reviews.rating','fp_user_reviews.review')
                 ->join('fp_job_requests', 'fp_user_hire.job_req_id', '=', 'fp_job_requests.id')
                 ->join('fp_jobs', 'fp_job_requests.job_id', '=', 'fp_jobs.id')
                 ->leftjoin('fp_cities', 'fp_cities.id', '=', 'fp_jobs.city_id')
                 ->leftjoin('fp_countries', 'fp_countries.id', '=', 'fp_jobs.country_id')
                 ->leftjoin('fp_states', 'fp_states.id', '=', 'fp_jobs.state_id')
                 ->leftjoin('fp_clients', 'fp_user_hire.client_id', '=', 'fp_clients.id')
                 ->leftjoin('fp_user_reviews', 'fp_job_requests.id', '=', 'fp_user_reviews.job_req_id')
                 ->where('fp_user_hire.user_id', '=', $user_id)
                 ->where('fp_jobs.status', '!=', 3)
                 ->where('valid_upto', '<', date('Y-m-d'))
                 ->where('fp_user_hire.status', '=', 0) 
                 ->orderBy('fp_user_hire.created_at', 'DESC');
                
             
       $jobs = $jobs->paginate(5);
		return view("user_history",compact('jobs','pageTitle','user'));
        
    }


    public function searchJob(){
        $category_list = Category::all()
             ->where('status', '=', '0')   
             ->pluck('category_name', 'id');

        $jobIDs = JobRequest::where('status','!=' ,'3')
             ->where('user_id', session::get('roleId'))
             ->pluck('job_id');
             //print_r($jobIDs);exit;
         
        $jobs = Job::with([])
             ->select('fp_jobs.*', 'fp_cities.name as city','fp_countries.name as country',
             'fp_states.name as state','fp_category.category_name')
            ->leftjoin('fp_countries', 'fp_countries.id', '=', 'fp_jobs.country_id')
            ->leftjoin('fp_cities', 'fp_cities.id', '=', 'fp_jobs.city_id')
            ->leftjoin('fp_states', 'fp_states.id', '=', 'fp_jobs.state_id')
            ->leftjoin('fp_category', 'fp_category.id', '=', 'fp_jobs.category_id')
            ->where('fp_jobs.status', '=', '0')
            ->where('fp_jobs.job_type', '=', '0')
            ->where('fp_jobs.job_start_on', '>=',  date('Y-m-d'));
            if(!empty($jobIDs) && count($jobIDs) > 0){
               
                $jobs->whereNotIn('fp_jobs.id', $jobIDs);  
            }
            $jobs = $jobs->paginate(5);
        return view("searchjob",compact('jobs','category_list'));
    }
    
     public function delete_portfolio(Request $request){
              if ($request->ajax()) {
            $id= $request->image_id;
              $queryData = UserPortfolio::find($id);

        if (isset($queryData->id)) {
          
            $queryData->delete();

            echo 1;
        } else {
             echo 0;
        }

        die();

        }
      }
      public function delete_experience(Request $request){
              if ($request->ajax()) {
            $id= $request->experience_id;

              $queryData = UserExperience::find($id);

        if (isset($queryData->id)) {
          
            $queryData->delete();

            echo 1;
        } else {
             echo 0;
        }

        die();

        }
      }
      public function delete_project(Request $request){
              if ($request->ajax()) {
            $id= $request->project_id;

              $queryData = UserProject::find($id);

        if (isset($queryData->id)) {
          
            $queryData->delete();

            echo 1;
        } else {
             echo 0;
        }

        die();

        }
      }

      public function delete_education(Request $request){
              if ($request->ajax()) {
            $id= $request->education_id;
              $queryData = UserEducation::find($id);

        if (isset($queryData->id)) {
          
            $queryData->delete();

            echo 1;
        } else {
             echo 0;
        }

        die();

        }
      }
      public function delete_video(Request $request){
              if ($request->ajax()) {
            $id= $request->video_id;
           
              $queryData = User::find($id);

        if (isset($queryData->id)) {
          
             $queryData->video = null;
              $queryData->save();

            echo 1;
        } else {
             echo 0;
        }

        die();

        }
      }
    
}
