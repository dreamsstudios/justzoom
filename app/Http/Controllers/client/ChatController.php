<?php

namespace App\Http\Controllers\client;
use App\Models\JobRequest;
use App\Models\Job;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Models\Client;
use App\Models\Contract;
use App\Models\ContractRequest;
use App\Models\UserReview;
use App\Models\Payment;
use App\Models\Chat;
use App\Helpers\Common_helper;
use App\Http\Controllers\Controller;
use Session;
use Validator;
use DB;
use View;
use Stripe;

class ChatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
    parent::__construct();
    $client_id = session::get('roleId');
    
    }
    
	 public function chat($chatId)
    {
    $client_id = session::get('roleId');
    $chat = ContractRequest::where('chatId', '=', $chatId)
                  ->select("fp_users.*","fp_contract_request.reqId","fp_contract_request.categoryId","fp_contract_request.contractId","fp_contract_request.chatId",
                  "fp_category.category_name",'fp_cities.name as city','fp_countries.name as country','fp_contract_request.userId',
                  'fp_states.name as state','fp_contract.paymentId')
                  ->leftjoin('fp_users', 'fp_users.id', '=', 'fp_contract_request.userId')
                  ->leftjoin('fp_category', 'fp_category.id', '=', 'fp_contract_request.categoryId')
                  ->leftjoin('fp_countries', 'fp_countries.id', '=', 'fp_users.country')
                  ->leftjoin('fp_cities', 'fp_cities.id', '=', 'fp_users.city')
                  ->leftjoin('fp_states', 'fp_states.id', '=', 'fp_users.state')
                  ->leftjoin('fp_contract', 'fp_contract.contractId', '=', 'fp_contract_request.contractId')
                  ->where('fp_contract_request.clientId', '=', session::get('roleId'))
                  ->orderBy("fp_contract_request.reqId","desc")
                 ->first();
                   $common_lib = new Common_helper();
                  $totalRating = $common_lib->getTotalRating($chat->userId);
                  $reviews = UserReview::with([])
                  ->select('fp_user_reviews.*',"fp_clients.*")
                  ->leftjoin("fp_clients",'fp_clients.id', '=', 'fp_user_reviews.client_id')
                  ->where('fp_user_reviews.user_id', '=', $chat->userId)
                  ->paginate(2);
                  $reviewCount = UserReview::where("user_id",$chat->userId)->count();
      
                  
    if(!empty($chat)){
    $pageTitle = "Chat Detail";
    $lastId = DB::table('fp_chat_detail')
                 ->select('fp_chat_detail.*')
                 ->where('fp_chat_detail.chatId', '=', $chatId)
                 ->where('fp_chat_detail.status', '=', 0)
                 ->orderBy('fp_chat_detail.id', 'DESC')
                 ->first();
        $chatDetail = DB::table('fp_chat_detail')
                 ->select('fp_chat_detail.*')
                 ->where('fp_chat_detail.chatId', '=', $chatId)
                 ->where('fp_chat_detail.status', '=', 0)
                 ->orderBy('fp_chat_detail.created_at', 'ASC')
                 ->get();
    
    $clients = Contract::where('fp_contract.userId', '=', $chat->userId)
                 ->count(DB::raw('DISTINCT clientId'));
    
    return view("client/client_chat",compact('chatDetail','chat','lastId','totalRating','reviews','reviewCount','clients'));
    }else{
      return redirect("/client");
    }
        
    }

    
    


    
}
