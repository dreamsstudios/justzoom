<?php

namespace App\Http\Controllers\client;
use App\Models\Client;
use App\Models\UserReview;
use App\Models\UserProfileView;
use App\Models\ContractRequest;
use App\Models\Contract;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Helpers\Common_helper;
use Session;
use Validator;
use DB;
use Hash;

class DashboardController extends Controller
{
    
    public function __construct()
    {
        parent::__construct();
        

    }
    public function index(){
    $pageTitle = "Dashboard";
    $common_lib = new Common_helper();
    $user_id = session::get('roleId');
    $user = Client::with([])
      ->where('id', '=', $user_id)
            ->first();
        $request = ContractRequest::select("fp_users.*","fp_contract_request.reqId",
        "fp_contract_request.categoryId","fp_contract_request.chatId","fp_contract_request.userId",
        "fp_category.category_name","fp_cities.name as city","fp_countries.name as country","fp_contract_request.contractId",
        'fp_states.name as state','fp_contract_request.requestStatus','fp_contract_request.chatId',
        'fp_contract_request.message','fp_contract.paymentId')
       ->leftjoin('fp_users', 'fp_users.id', '=', 'fp_contract_request.userId')
       ->leftjoin('fp_category', 'fp_category.id', '=', 'fp_contract_request.categoryId')
       ->leftjoin('fp_countries', 'fp_countries.id', '=', 'fp_users.country')
       ->leftjoin('fp_contract', 'fp_contract.contractId', '=', 'fp_contract_request.contractId')
       ->leftjoin('fp_cities', 'fp_cities.id', '=', 'fp_users.city')
       ->leftjoin('fp_states', 'fp_states.id', '=', 'fp_users.state')
       ->where('fp_contract_request.clientId', '=', session::get('roleId'))
       ->where('fp_contract_request.isRequestClose', '=', 0)
       ->orderBy('fp_contract_request.created_at','DESC')
       ->paginate(2);
      
    
        $pastWork = Contract::where('fp_contract.clientId', '=', $user_id)
        ->select("fp_users.*","fp_contract.*","fp_contract.categoryId",
        "fp_category.category_name",'fp_contract_request.chatId','fp_user_reviews.rating','fp_user_reviews.review','fp_countries.name as country_name','fp_cities.name as city_name','fp_states.name as state_name')
       ->leftjoin('fp_users', 'fp_users.id', '=', 'fp_contract.userId')
       ->leftjoin('fp_user_reviews','fp_user_reviews.contractId','=','fp_contract.contractId')
       ->leftjoin('fp_contract_request', 'fp_contract.contractId', '=', 'fp_contract_request.contractId')
       ->leftjoin('fp_category', 'fp_category.id', '=', 'fp_contract.categoryId')
       ->leftjoin('fp_countries', 'fp_countries.id', '=', 'fp_users.country')
       ->leftjoin('fp_cities', 'fp_cities.id', '=', 'fp_users.city')
       ->leftjoin('fp_states', 'fp_states.id', '=', 'fp_users.state')
       ->where('IsContractComplete','=',1)
       ->paginate(5);
       
        $hired_freelancer  = Contract::where('fp_contract.clientId', '=', $user_id)
                            ->count(DB::raw('DISTINCT userId'));
        $paymentRequest = Contract::where('fp_contract.clientId', '=', $user_id)
                          ->where('fp_release_payment.status','=',0)
                          ->join('fp_release_payment','fp_contract.contractId', '=', 'fp_release_payment.contractId')
                          ->count(DB::raw('DISTINCT relId'));
          

    
                  

$ongingWork  = Contract::where('fp_contract.clientId', '=', $user_id)->where('IsContractComplete','=',0)
                  ->count();
		return view("client/client_dashboard",compact('pageTitle','ongingWork','hired_freelancer','user','request','pastWork','paymentRequest'));
  	}
  	
    public function profile(){
    $pageTitle = 'Client Profile';
    $user_id = session::get('roleId');
    $user = Client::with(['country_data'])
    ->where('id', '=', $user_id)
    ->first();
    $ongingWork  = Contract::where('fp_contract.clientId', '=', $user_id)->where('IsContractComplete','=',0)
                  ->count();
     $completeWork  = Contract::where('fp_contract.clientId', '=', $user_id)->where('IsContractComplete','=',1)
                  ->count();
     $hired_freelancer  = Contract::where('fp_contract.clientId', '=', $user_id)
                            ->count(DB::raw('DISTINCT userId'));
                  //echo $ongingWork; exit();
        return view("client/client_profile",compact('pageTitle','user','ongingWork','hired_freelancer','completeWork'));
    }
  
  public function edit_profile(){
    $pageTitle = "Edit Profile";
    $user_id = session::get('roleId');
    $user = Client::with([])
       ->where('id', '=', $user_id)
       ->first();
    
       
       $state_list =  DB::table('fp_states')
       ->where('country_id', '=', $user->country)     
       ->pluck('name', 'id');
       $city_list =  DB::table('fp_cities')
       ->where('state_id', '=', $user->state)     
       ->pluck('name', 'id');
       //echo "<pre>"; print_r($user); exit();
    return view("client/client_edit_profile",compact('user','pageTitle','state_list','city_list'));
  }
   public function change_password(Request $request){
    $pageTitle = "Change Password";
  $user_id = session::get('roleId');
  $user = Client::with([])
       ->where('id', '=', $user_id)
       ->first();
       
  if ($request->isMethod('post')){
    Session::forget('msg');
    Session::forget('errormsg');
        $validationOn =  array(
        "old_password"=>$request->old_password,
                "new_password"=>$request->new_password,
                "confirm_password"=>$request->confirm_password,
            );
      $validateRule=  array(
                "old_password"=>"required",
                'new_password' => 'min:6|required_with:confirm_password|same:confirm_password',
                'confirm_password' => 'min:6',
            );
       
        $validator = Validator::make($validationOn,$validateRule);
        if ($validator->fails()){
            return redirect("/client/change-password")->withErrors($validator)->withInput();
  
        }
     $isExist = DB::select("SELECT * FROM fp_auths WHERE status != 2 AND (role = 'client') AND role_id = '".$user_id."' limit 0, 1");
    
    if(!empty($isExist)){
      if(Hash::check($request->old_password,$isExist[0]->password)){
        DB::table('fp_auths') ->where('role_id', $user_id) ->limit(1) ->update( ['password'=>bcrypt($request->new_password), 'updated_at'=>date('Y-m-d H:i:s') ]);
        $request->session()->flash("msg", "Password updated successfully.");
      }else{
      
        $request->session()->flash("errormsg", "invalid old password.");
      }
    }else{
       $request->session()->flash("errormsg", "invalid user.");
    }
    
  }   
  return view("client/client_change_password",compact('user','pageTitle'));
  }
  public function save_user_data(Request $request){
       
      Session::forget('msg');
      Session::forget('errormsg');
        $user_id = session::get('roleId');
        $validationOn =  array(
                "first_name"=>$request->first_name,
                "last_name"=>$request->last_name,
                "phone"=>$request->phone,
                "email"=>$request->email,
               "job_title"=>$request->job_title,
                "about"=>$request->about,
                "address_line_1"=>$request->address_line_1,
                "city"=>$request->city,
                "pincode"=>$request->pincode,
                "state"=>$request->state,
                "country"=>$request->country,
                
            );
     
               $validateRule=  array(
                "first_name"=>"required",
                "last_name"=>"required",
                "phone"=>"required|numeric|min:8",
               "email"=>"required|email",
               "job_title"=>"required",
                "about"=>"required", 
                "address_line_1"=>"required",
                "city"=>"required",
        "pincode"=>"required|numeric|min:4",
                "state"=>"required",
                "country"=>"required",
            );
        $validator = Validator::make($validationOn,$validateRule);
        if ($validator->fails()){
       $request->session()->flash("errormsg", "Something wrong.");
          return redirect("/client/edit-profile")->withErrors($validator)->withInput();
        }
        $isExist = DB::select("SELECT * FROM fp_clients WHERE status != 2 AND id != '".$user_id."' AND email = '".$request->email."' ",[1]);
        $isExist1 = DB::select("SELECT * FROM fp_users WHERE status != 2 AND  email = '".$request->email."' ",[1]);
        if($isExist){
            $request->session()->flash("errormsg", "Email already regisered.");
            return redirect("/client/edit-profile");
        }
        if($isExist1){
            $request->session()->flash("errormsg", "Email already regisered.");
            return redirect("/client/edit-profile");
        }
          

        $user = Client::find($user_id);
        $common_lib = new Common_helper();
        $slug = $common_lib->createSlug($request->first_name,$user_id,'fp_clients');
        $user->first_name=$request->first_name;
        $user->last_name=$request->last_name;
        $user->phone=$request->phone;
        $user->email=$request->email;
        $user->job_title=$request->job_title;
        $user->about=$request->about;
        $user->address_line_1=$request->address_line_1;
        $user->city=$request->city;
        $user->state=$request->state;
        $user->country=$request->country;
        $user->pincode=$request->pincode;
        $user->slug=$slug;
        $user->save();
         if($user->save()){
             $request->session()->flash("msg", "profile updated successfully.");
             return redirect("client/edit-profile");
         }else{
              $request->session()->flash("errormsg", "profile not updated");
               return redirect("client/edit-profile");
             }
           }
   
      public function ajaxImage(Request $request)
    {
        if ($request->isMethod('post')){
           $validationOn =  array(
                "images"=>$request->file
            );
           $validateRule=  array(
                "images"=>"required|image|mimes:jpeg,png,jpg,gif,svg|max:2048"
            );
            $validator = Validator::make($validationOn,$validateRule);
           
            if ($validator->fails())
                return array(
                    'fail' => true,
                    'errors' => $validator->errors()
            );
            $user_id = session::get('roleId');
            $user = Client::find($user_id);
            $file = $request->file('file');
            $new_name  = time().rand().'.'.$file->getClientOriginalExtension();
            $file->move(public_path('uploads/user_images'), $new_name);
            $user->image = $new_name;
            $user->save();
            return $new_name;
        }
    }
}
