<?php

namespace App\Http\Controllers\client;
use App\Models\JobRequest;
use App\Models\Job;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Models\Client;
use App\Models\User;
use App\Models\Contract;
use App\Models\ContractRequest;
use App\Models\Payment;
use App\Models\Setting;
use App\Models\UserReview;
use App\Http\Controllers\Controller;
use App\Helpers\Common_helper;
use Session;
use Validator;
use DB;
use View;
use Stripe;
use Mail;
use App\Notifications\CreateContractRequest;

class ContractController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function __construct()
    {
    parent::__construct();
    $client_id = session::get('roleId');
    }
    
	 public function contract_list()
    {
    $client_id = session::get('roleId');
    $user = Client::with([])
    ->where('id', '=', $client_id)
    ->first();
    $contractData = Contract::where('fp_contract.clientId', '=', $client_id)
    ->select("fp_users.*","fp_contract.*","fp_contract.categoryId",
    "fp_category.category_name","uc.category_name as user_category_name",'fp_contract_request.chatId')
   ->leftjoin('fp_users', 'fp_users.id', '=', 'fp_contract.userId')
   ->leftjoin('fp_contract_request', 'fp_contract.contractId', '=', 'fp_contract_request.contractId')
   ->leftjoin('fp_category', 'fp_category.id', '=', 'fp_contract.categoryId')
   ->leftjoin('fp_category as uc', 'uc.id', '=', 'fp_users.category_id')
   ->leftjoin('fp_countries', 'fp_countries.id', '=', 'fp_users.country')
   ->leftjoin('fp_cities', 'fp_cities.id', '=', 'fp_users.city')
   ->leftjoin('fp_states', 'fp_states.id', '=', 'fp_users.state')
   ->where('IsContractComplete','=',0)
   ->orderBy('fp_contract.updated_at', 'DESC')
   ->paginate(5); 
   
   $pastContract = Contract::where('fp_contract.clientId', '=', $client_id)
   ->select("fp_users.*","fp_contract.*","fp_contract.categoryId",
   "fp_category.category_name","uc.category_name as user_category_name",'fp_contract_request.chatId','fp_user_reviews.rating','fp_user_reviews.review')
  ->leftjoin('fp_users', 'fp_users.id', '=', 'fp_contract.userId')
  ->leftjoin('fp_contract_request', 'fp_contract.contractId', '=', 'fp_contract_request.contractId')
  ->leftjoin('fp_category', 'fp_category.id', '=', 'fp_contract.categoryId')
  ->leftjoin('fp_category as uc', 'uc.id', '=', 'fp_users.category_id')
  ->leftjoin('fp_user_reviews','fp_user_reviews.contractId','=','fp_contract.contractId')
  ->leftjoin('fp_countries', 'fp_countries.id', '=', 'fp_users.country')
  ->leftjoin('fp_cities', 'fp_cities.id', '=', 'fp_users.city')
  ->leftjoin('fp_states', 'fp_states.id', '=', 'fp_users.state')
  ->where('IsContractComplete','=',1)
  ->paginate(5);   
  $pendingInvitation  = ContractRequest::where('clientId', '=', $client_id)->where('requestStatus','=',0)
   ->count();
		
		return view("client/contract_list",compact('contractData','pastContract','pendingInvitation'));
		
        
    }
    public function contract_detail($contractId= null,$chatId= null)
    {
    
    $client_id = session::get('roleId');
    $user = Client::with([])
    ->where('id', '=', $client_id)
    ->first();
    $pageTitle = "Contract Detail";
   
    $contract_detail = Contract::where('fp_contract.clientId', '=', $client_id)
    ->select("fp_users.*","fp_contract.*","fp_contract.categoryId",
    "fp_category.category_name",'fp_contract_request.chatId',"uc.category_name as user_category_name","fp_contract_request.userId")
    ->leftjoin('fp_users', 'fp_users.id', '=', 'fp_contract.userId')
   
    ->leftjoin('fp_contract_request', 'fp_contract.contractId', '=', 'fp_contract_request.contractId')
    ->leftjoin('fp_category', 'fp_category.id', '=', 'fp_contract.categoryId')
    ->leftjoin('fp_category as uc', 'uc.id', '=', 'fp_users.category_id')
    ->leftjoin('fp_countries', 'fp_countries.id', '=', 'fp_users.country')
    ->leftjoin('fp_cities', 'fp_cities.id', '=', 'fp_users.city')
    ->leftjoin('fp_states', 'fp_states.id', '=', 'fp_users.state')
    ->where('fp_contract.contractId','=',$contractId)
    ->first();
    $common_lib = new Common_helper();
    $totalRating = $common_lib->getTotalRating($contract_detail->userId);

         
//echo "<pre>"; print_r($reviews); exit();
return view("client/contract_detail",compact('contract_detail','pageTitle','totalRating'));
        
    }
    public function current_task()
    {
      $client_id = session::get('roleId');
      $user = Client::with([])
    ->where('id', '=', $client_id)
    ->first();
		
		$pageTitle = "Current Task";
       
        
        $jobs = DB::table('fp_user_hire')
                 ->select('fp_jobs.*', 'fp_cities.name as city','fp_countries.name as country',
                 'fp_states.name as state','fp_job_requests.*','fp_job_requests.status as job_request_status',
                 'fp_job_requests.id as job_req_id','fp_users.first_name','fp_users.last_name')
                 ->join('fp_job_requests', 'fp_user_hire.job_req_id', '=', 'fp_job_requests.id')
                 ->join('fp_jobs', 'fp_job_requests.job_id', '=', 'fp_jobs.id')
                 ->leftjoin('fp_cities', 'fp_cities.id', '=', 'fp_jobs.city_id')
                 ->leftjoin('fp_countries', 'fp_countries.id', '=', 'fp_jobs.country_id')
                 ->leftjoin('fp_states', 'fp_states.id', '=', 'fp_jobs.state_id')
                 ->leftjoin('fp_users', 'fp_job_requests.user_id', '=', 'fp_users.id')
                 ->where('fp_user_hire.client_id', '=', $client_id)
                 ->where('fp_jobs.status', '!=', 3)
                 ->where('valid_upto', '>=', date('Y-m-d'))
                 ->where('fp_user_hire.status', '=', 0)
                 ->orderBy('fp_user_hire.created_at', 'DESC');
                
             
       $jobs = $jobs->paginate(5);
		return view("client/client_current_task",compact('jobs','pageTitle','user'));
        
    }

    
    public function save_contract(Request $request)
    {
    
      //print_r($Invitation); exit();
      //echo "<pre>"; print_r($ContractRequest->userId); exit();

      $queryData = User::with([])
           ->where('id', '<>', $request->userId)
            ->first();
                
      Session::forget('msg');
	    Session::forget('errormsg');
        $clientId = session::get('roleId');
       
        $validationOn =  array(
                "aboutProject"=>$request->aboutProject,
                "requirement"=>$request->requirement,
                "description"=>$request->description,
                "priceType"=>$request->priceType,
               "duration"=>$request->duration
                
               
            );
     
               $validateRule=  array(
                "aboutProject"=>"required",
                "requirement"=>"required",
                "description"=>"required",
               "priceType"=>"required",
               "duration"=>"required"
              

               
            );
          
        $validator = Validator::make($validationOn,$validateRule);
        if ($validator->fails()){
			   // $request->session()->flash("errormsg", "Something wrong.");
          return redirect("/hire-freelancer/".$request->reqId)->withErrors($validator)->withInput();
        }
        if(!isset($request->nondisclosure)){
          $request->session()->flash("errormsg", "Please check non-disclosure checkbox.");
          return redirect("/hire-freelancer/".$request->reqId);

        }
        if(!isset($request->terms)){
          $request->session()->flash("errormsg", "Please check terms checkbox.");
          return redirect("/hire-freelancer/".$request->reqId);

        }
        if(isset($request->contractId) && !empty($request->contractId)){
           $contract = Contract::find($request->contractId); 
           $msg = "Contract updated successfully.";
        }else{
          $contract = new Contract;
          $contract->created_at = date('Y-m-d H:i:s');
          $msg = "Contract added successfully.";

        }
        
        $contract->aboutProject = $request->aboutProject;
        $contract->requirement=$request->requirement;
        $contract->description=$request->description;
        $contract->priceType=$request->priceType;
        $contract->duration=$request->duration;
        $contract->platformPrice=$request->paltformCharge;
        $contract->freelancerPrice=$request->freelancerAmount;
        $contract->totalPrice=$request->freelancerAmount + $request->paltformCharge;
        $contract->currency='£';
        $contract->userId=$request->userId;
        $contract->clientId=  session::get('roleId');
        $contract->categoryId=  $request->categoryId;
        $contract->save();
          $ContractRequest = ContractRequest::find($request->reqId);
      $user_data = User::with([])
       ->where('id', '=', $ContractRequest->userId)
       ->first();
     $client_data = Client::with([])
       ->where('id', '=', $ContractRequest->clientId)
       ->first();
         $Invitation=[
          'first_name'=>$user_data->first_name,
          'last_name'=>$user_data->last_name,
          'email'=>$user_data->email,
          'client_first_name'=>$client_data->first_name,
          'client_last_name'=>$client_data->last_name,
          'client_email'=>$client_data->email,
          'aboutProject'=>$request->aboutProject,
          'requirement'=>$request->requirement,
          'description'=>$request->description,
          'priceType'=>$request->priceType,
          'duration'=>$request->duration,
          'platformPrice'=>$request->paltformCharge,
          'freelancerPrice'=>$request->freelancerAmount,
          'totalPrice'=>$request->totalPrice,
          'currency'=>'£',
       ];
          //print_r($Invitation); exit();

         if($contract->save()){
            if(empty($request->contractId)){
                          Mail::send('email_templates.create_contract_client', ['invitation'=>$Invitation], function ($m) use ($Invitation) {
                            $m->to($Invitation['client_email'], 'Client')->subject('Contract created Successfully');
              });

            Mail::send('email_templates.create_contract_freelancer', ['invitation'=>$Invitation], function ($m) use ($Invitation) {
                            $m->to($Invitation['email'], 'Freelancer')->subject('Contract created Successfully');
              });
              
            }
           
                
            $req = ContractRequest::find($request->reqId);
            $req->contractId = $contract->contractId;
            $req->save();
			      $request->session()->flash("msg", $msg);
             return redirect("/payment/".$contract->contractId);
		     }else{
			        $request->session()->flash("errormsg", "contract not updated");
               return redirect("/hire-freelancer/".$request->reqId);
             }
        
    
        
    }
    public function payment($contractId= null)
    {
      $setting  = Setting::first();
      $contract = Contract::select("fp_users.*","fp_contract.contractId","fp_contract.categoryId",
      "fp_contract.platformPrice","fp_contract.freelancerPrice","fp_contract.totalPrice","fp_contract.currency",
      "fp_category.category_name",'fp_cities.name as city','fp_countries.name as country',
      'fp_states.name as state','fp_contract.userId','fp_contract_request.reqId')
      ->leftjoin('fp_users', 'fp_users.id', '=', 'fp_contract.userId')
      ->leftjoin('fp_category', 'fp_category.id', '=', 'fp_contract.categoryId')
      ->leftjoin('fp_countries', 'fp_countries.id', '=', 'fp_users.country')
      ->leftjoin('fp_cities', 'fp_cities.id', '=', 'fp_users.city')
      ->leftjoin('fp_states', 'fp_states.id', '=', 'fp_users.state')
      ->leftjoin('fp_contract_request', 'fp_contract_request.contractId', '=', 'fp_contract.contractId')
      ->where('fp_contract.contractId','=',$contractId)
       ->first();
       $common_lib = new Common_helper();
       $totalRating = $common_lib->getTotalRating($contract->userId);
      return view("payment",compact('contract','totalRating','setting'));

    }
public function save_payment(Request $request)
 {
  $setting  = Setting::first();
  $stripe = Stripe\Stripe::setApiKey($setting->secreteKey);
  
    $validationOn =  array(
      "cardNumber"=>$request->cardNumber,
      "cardholder"=>$request->cardholder,
      "cvvcode"=>$request->cvvcode,
      "expiryMonth"=>$request->expiryMonth,
      "expiryYear"=>$request->expiryYear,
      "stripeToken" => $request->stripeToken,
      "totalPrice" => $request->totalPrice,
      "contractId" => $request->contractId
    );

      $validateRule=  array(
        "cardNumber"=>"required",
        "cardholder"=>"required",
        "cvvcode"=>"required",
        "expiryMonth"=>"required",
        "expiryYear"=>"required",
        "stripeToken" => "required",
        "totalPrice" => "required",
        "contractId" => "required"
    );

    $client_id = session::get('roleId');
    $user = Client::with([])
  ->where('id', '=', $client_id)
  ->first();
    
    if(!empty($user->stripeCustomerId)){
      $cc = \Stripe\Customer::createSource(
        $user->stripeCustomerId,
        ['source' => $request->stripeToken]
      );

    }else{
     $customer = \Stripe\Customer::create([
        'email' => $user->email,
        'name' => $user->first_name,
        'address' => [
          'line1' => '510 Townsend St',
          'postal_code' => '98140',
          'city' => 'San Francisco',
          'state' => 'CA',
          'country' => 'US',
        ]
      ]);

      Client::where('id', $client_id)
       ->update([
           'stripeCustomerId' => $customer->id
        ]);
        $cc = \Stripe\Customer::createSource(
          $customer->id,
          ['source' => $request->stripeToken]
        );

    }
   // print_R($cc);exit;
    $source = $cc->id;

    

    $validator = Validator::make($validationOn,$validateRule);
    if ($validator->fails()){
    $request->session()->flash("errormsg", $validator->errors());
    return redirect("/payment/".$request->contractId)->withErrors($validator)->withInput();
    }else{
      
    try {
        $charge = Stripe\Charge::create ([
        'customer' => $cc->customer,
        'source' => $source,
        'currency' =>'GBP',
        'amount' => $request->platformPrice*100,
        'description' => 'Pay to Admin'
        ]);
    
      
 
 } catch (Exception $e) {
  $request->session()->flash("errormsg", $e->getMessage());
  return redirect("/payment/".$request->contractId);
 }
 
 if (isset($charge->status) && $charge->status == 'succeeded'){
   
  $payment = new Payment;
  $payment->created_at = date('Y-m-d H:i:s');
  $payment->contractId = $request->contractId;
  $payment->token=$charge->id;
  $payment->clientId=  session::get('roleId');
  $payment->totalPrice=$request->platformPrice;
  $payment->currency='£';
  $payment->response = $charge;
  $payment->status= $charge->status;
  if($payment->save()){
    $con = Contract::find($request->contractId);
    $con->paymentId = $payment->id;
    $con->cardSource = $source;
    $con->save();
    $request->session()->flash("msg", "Payment Done successfully!");
    return redirect("/client");
  }else{
    $request->session()->flash("errormsg", "Something Wrong!");
    return redirect("/payment/".$request->contractId);
  }
  } 
 }
 }

 public function payment_request()
    {
      $setting  = Setting::first();
      $client_id = session::get('roleId');
      $user = Client::with([])
        ->where('id', '=', $client_id)
        ->first();
      $pageTitle =  'pageTitle';
      $pendingRequest = Contract::where('fp_contract.clientId', '=', $client_id)
      ->select("fp_users.*","fp_contract.*","fp_contract.categoryId",
      "fp_category.category_name",'fp_contract_request.chatId',
      "uc.category_name as user_category_name","fp_contract_request.userId","fp_release_payment.*")
      ->leftjoin('fp_users', 'fp_users.id', '=', 'fp_contract.userId')
      ->leftjoin('fp_contract_request', 'fp_contract.contractId', '=', 'fp_contract_request.contractId')
      ->leftjoin('fp_category', 'fp_category.id', '=', 'fp_contract.categoryId')
      ->leftjoin('fp_category as uc', 'uc.id', '=', 'fp_users.category_id')
      ->join('fp_release_payment', 'fp_release_payment.contractId', '=', 'fp_contract.contractId')
      ->where('fp_release_payment.status','=',0)
      ->paginate(2);

      $countReq = Contract::where('fp_contract.clientId', '=', $client_id)
      ->join('fp_release_payment', 'fp_release_payment.contractId', '=', 'fp_contract.contractId')
      ->where('fp_release_payment.status','=',0)
      ->count();

      $pastWork = Contract::where('fp_contract.clientId', '=', $client_id)
      ->select("fp_users.*","fp_contract.*","fp_contract.categoryId",
      "fp_category.category_name",'fp_contract_request.chatId',
      "uc.category_name as user_category_name","fp_contract_request.userId","fp_release_payment.*")
      ->leftjoin('fp_users', 'fp_users.id', '=', 'fp_contract.userId')
      ->leftjoin('fp_contract_request', 'fp_contract.contractId', '=', 'fp_contract_request.contractId')
      ->leftjoin('fp_category', 'fp_category.id', '=', 'fp_contract.categoryId')
      ->leftjoin('fp_category as uc', 'uc.id', '=', 'fp_users.category_id')
      ->join('fp_release_payment', 'fp_release_payment.contractId', '=', 'fp_contract.contractId')
      ->where('fp_release_payment.status','=',1)
      ->paginate(5);

      return view("client/payment_request",compact('pageTitle','pendingRequest','user','countReq','pastWork','setting'));

    }


    
}
