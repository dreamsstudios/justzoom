<?php
namespace App\Http\Controllers;
session_start();

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Models\Category;
use App\Models\User;
use App\Models\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Common_helper;
use Validator;
use Session;
use DB;
use Mail;
use View;
use Redirect;
use Route;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
	
	public function __construct()
    {


		$routeArray = app('request')->route()->getAction();
        $controllerAction = class_basename($routeArray['controller']);
        list($controller, $action) = explode('@', $controllerAction);


        
        $category_list = Category::all()
			->where('status', '=', '0')   
			->pluck('category_name', 'id');
		 
			$country_list =  DB::table('fp_countries')
			->pluck('name', 'id');
		 
			View::share( 'category_list', $category_list );
			View::share ( 'country_list', $country_list);
       
     
		$user_id = Session::get('roleId');
		$role = Session::get('role');
	
		

		if(!empty($user_id)){
			if($role == 'user'){
				$loginuser = User::with(['category_detail','weapon_detail'])
				->where('id', '=', $user_id)
				->first();
			}else{
				$loginuser = Client::with([])
				->where('id', '=', $user_id)
				->first();
			}
			
			/*if($role == 'user'){
				
				$urlss= explode('@', Route::getCurrentRoute()->getActionName());
				if($user->price<=0){
					if($controller != 'CommonController' && $action != 'edit_profile' && 
					$action != 'save_user_data' &&  $action != 'ajaxImage'){
						Redirect::to("/user-dashboard/edit-profile")->send();
					}
					
				}
			}*/

		}
		
	}
}
