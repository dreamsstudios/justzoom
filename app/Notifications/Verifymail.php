<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;

use Illuminate\Notifications\Notification;

use Illuminate\Contracts\Queue\ShouldQueue;

use Illuminate\Notifications\Messages\MailMessage;

class Verifymail extends Notification implements ShouldQueue

{

    use Queueable;

    protected $token;

    /**

    * Create a new notification instance.

    *

    * @return void

    */

    public function __construct($token)

    {

        $this->token = $token;

    }

    /**

    * Get the notification's delivery channels.

    *

    * @param  mixed  $notifiable

    * @return array

    */

    public function via($notifiable)

    {

        return ['mail'];

    }

     /**

     * Get the mail representation of the notification.

     *

     * @param  mixed  $notifiable

     * @return \Illuminate\Notifications\Messages\MailMessage

     */

     public function toMail($notifiable)

     {

        // $url = url('/api/find/'.$this->token);

        $url = "https://justzoom.devo.website/verify-email/".$this->token;

        return (new MailMessage)

            ->from('info@devo.website', 'Freelancer')

            ->line('Please verify your email address!')

            ->line(' Please click on Verify Email to verify your email on JUSTZOOM.')

            ->action('Verify Email', url($url))

            ->line('JUSTZOOM Team');

           

    }

    /**

    * Get the array representation of the notification.

    *

    * @param  mixed  $notifiable

    * @return array

    */

    public function toArray($notifiable)

    {

        return [

            //

        ];

    }

}