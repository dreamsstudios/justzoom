<?php
use App\Models\UserSkill;
use App\Models\Category;
use App\Models\Weapon;
/**
* change plain number to formatted currency
*
* @param $number
* @param $currency
*/
function formatNumber($number, $currency = 'IDR')
{
   if($currency == 'USD') {
        return number_format($number, 2, '.', ',');
   }
   return number_format($number, 0, '.', '.');

}

function user_skill_data($user_id = null)
    {

    	$categories = App\Models\UserSkill::with(['skill_data'])
    				 ->where('user_id', '=' ,$user_id)
    				 ->get();
    				  ;
        return $categories;

       
    }
    function user_category_data($cat_id = null)
    {

      $categories = Category::where('id', '=' ,$cat_id)
                    ->first();
              ;
        return $categories;

       
    }
    function user_weapon_data($id = null)
    {

      $weapon = Weapon::where('id', '=' ,$id)
                    ->first();
              ;
        return $weapon;
        
       
    }
    function getUserTotalRating($id = 0)
    {
        return DB::table('fp_user_reviews')
                      ->selectRaw('ROUND(AVG(rating)) as average_rating')
                      ->selectRaw('COUNT(review) AS totalreview')
                      ->where('user_id',"=", $id)
                      ->first();
    }
    function getTotalClientrating($id = 0)
    {
        return DB::table('fp_client_reviews')
                      ->selectRaw('ROUND(AVG(rating)) as average_rating')
                      ->selectRaw('COUNT(review) AS totalreview')
                      ->where('client_id',"=", $id)
                      ->first();
    }
    function jobrequestData($id = 0)
    {
        
    }
    function loginuserDetail($id = 0,$role)
    {
        if($role == 'client'){
            return DB::table('fp_clients')
                      ->where('id',"=", $id)
                      ->first();

        }else{
            return DB::table('fp_users')
                      ->where('id',"=", $id)
                      ->first();

        }
    } 
    function countAcceptJob($job_req_id,$job_id)
    {
        return  $countAccetJob =  DB::table('fp_job_requests')
        ->where('job_id','=',$job_id)
        ->where('job_status','=','1')
        ->where('status','=','0')
        ->where('id','!=',$job_req_id)
        ->count();

        
    }

    function lastMessage($chatId)
    {
        if(Session::get('role') == 'user'){
            return  $chatDetail =  DB::table('fp_chat_detail')
            ->select('fp_chat_detail.*','fp_clients.first_name')
            ->where('chatId','=',$chatId)
            ->leftJoin('fp_clients',function ($join) {
                $join->on('fp_clients.id', '=' , 'fp_chat_detail.senderId') ;
                $join->where('fp_chat_detail.senderType','=','client');
            })
            ->orderBy('created_at','DESC')
            ->first();

        }else{
            return  $chatDetail =  DB::table('fp_chat_detail')
        ->select('fp_chat_detail.*','fp_users.first_name')
        ->where('chatId','=',$chatId)
        ->leftJoin('fp_users',function ($join) {
            $join->on('fp_users.id', '=' , 'fp_chat_detail.senderId') ;
            $join->where('fp_chat_detail.senderType','=','user');
        })
        ->orderBy('created_at','DESC')
        ->first();


        }
        
        
    }
    function unreadMsgCount($chatId)
    {
        return  $chatcount =  DB::table('fp_chat_detail')
        ->where('chatId','=',$chatId)
        ->where('senderId','!=',Session::get('roleId'))
        ->where('senderType','!=',Session::get('role'))
        ->count();

        
    }
    
?>