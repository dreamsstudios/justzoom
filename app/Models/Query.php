<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;


class Query extends Model
{
	use Notifiable;
    protected $table = "fp_queries";
}
