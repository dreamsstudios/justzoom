<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserProfileView extends Model
{
    protected $table = "fp_user_profile_views";
}
