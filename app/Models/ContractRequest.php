<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;


class ContractRequest extends Model
{ 
	use Notifiable;
    protected $table = "fp_contract_request";
    protected $primaryKey = 'reqId';
    //
}
