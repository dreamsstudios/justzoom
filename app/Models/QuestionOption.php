<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuestionOption extends Model
{
    protected $table = "fp_question_option";
    protected $primaryKey = "optionId";
    protected $fillable = ['optionText'];
    //
}
