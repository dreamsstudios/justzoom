<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
class Client extends Model
{
    use Notifiable;
    use SoftDeletes;
    protected $table = "fp_clients";
    
    public function auth_detail()
    {
        return $this->hasOne(Auth::class, 'role_id', 'id');
    }
   

    public function skills(){
        return $this->belongsToMany(UserSkill::class,'fp_user_skills','client_id','skill_id');
    }
    

    
     public function skill_detail()
    {
        return $this->hasMany(UserSkill::class, 'user_id', 'id');
    }
     public function country_data()
    {
        return $this->belongsTo(Country::class, 'country');
    }
    public function state_data()
    {
        return $this->belongsTo(State::class, 'state');
    }
    public function city_data()
    {
        return $this->belongsTo(City::class, 'city');
    }
}
