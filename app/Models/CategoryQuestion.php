<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryQuestion extends Model
{
	protected $table = "fp_category_question";
    //
    protected $primaryKey = "questionId";
}
