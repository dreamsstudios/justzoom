<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $table = "fp_blogs";


     public function author()
      {
    return $this->belongsTo(Auth::class, 'user_id');
   }
}
