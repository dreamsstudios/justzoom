<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;



class User extends Model
{
    use Notifiable;
     use SoftDeletes;
    protected $table = "fp_users";

    public function auth_detail()
    {
        return $this->hasOne(Auth::class, 'role_id', 'id');
    }

     public function skill_detail()
    {
        return $this->hasMany(UserSkill::class, 'user_id', 'id');
    }
     public function category(){
        return $this->belongsTo(Category::class,'category_id');
    }
    public function category_detail(){
        return $this->belongsTo(Category::class,'category_id');
    }

    
    
    public function skills(){
        return $this->belongsToMany(UserSkill::class,'fp_user_skills','user_id','skill_id');
    }
    public function country_data()
    {
        return $this->belongsTo(Country::class, 'country');
    }
    public function state_data()
    {
        return $this->belongsTo(State::class, 'state');
    }
    public function city_data()
    {
        return $this->belongsTo(City::class, 'city');
    }
}
