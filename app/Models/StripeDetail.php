<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;


class StripeDetail extends Model
{ 
	use Notifiable;
    protected $table = "fp_stripe_details";
    protected $primaryKey = 'detailsId';
    //
}
