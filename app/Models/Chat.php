<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    protected $table = "fp_chat";
    protected $primaryKey = 'chatId';
    //
}
