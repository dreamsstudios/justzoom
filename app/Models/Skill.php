<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Skill extends Model
{
    protected $table = "fp_skills";
    

    public function userskill(){
        return $this->belongsToMany('App\Model\UserSkill');
    }

    
}
