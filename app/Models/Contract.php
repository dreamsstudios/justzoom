<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
class Contract extends Model
{
	use Notifiable;
    protected $table = "fp_contract";
    protected $primaryKey = 'contractId';
    //
}
