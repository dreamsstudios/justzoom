<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class NewsletterSubscriber extends Model
{
    protected $table = "fp_newsletter_subscribers";
}
